import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './pages/login/login.component';
import {AuthenticationGuardService} from './services/authentication.guard.service';
import {ProfileaccountComponent} from './application/account/profileaccount/profileaccount.component';
import {AccountComponent} from './application/account/account/account.component';
import {EditaccountComponent} from './application/account/editaccount/editaccount.component';
import {LogoutComponent} from './pages/logout/logout.component';
import {DashboardComponent} from './application/dashboard/dashboard.component';
import {RefreshComponent} from './components/refresh/refresh.component';
import {DocumentsComponent} from './application/documents/documents.component';
import {NetworkComponent} from './application/network/network.component';
import {SettingComponent} from './application/account/setting/setting.component';
import {MessagesComponent} from './application/messages/messages.component';
import {PostContentComponent} from './application/post/post-content/post-content.component';
import {ResetPasswordComponent} from './pages/reset-password/reset-password.component';
import {NotificationComponent} from './application/notification/notification.component';
import {EditPostComponent} from './application/post/edit-post/edit-post.component';
import {ViewPostComponent} from './components/posts/view-post/view-post.component';
import {AssistanceComponent} from './application/assistance/assistance.component';
import {SearchResultsComponent} from './application/search-results/search-results.component';
import {CguComponent} from './pages/cgu/cgu.component';
import {EmailConfirmationComponent} from './pages/email-confirmation/email-confirmation.component';
import {AccountProfileViewComponent} from './components/accounts/account-profile-view/account-profile-view.component';
import {ValidateAccountComponent} from './pages/validate-account/validate-account.component';

const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full' },
  {path: 'login', component: LoginComponent},
  {path: 'login/:id', component: LoginComponent},
  {path: 'logout', component: LogoutComponent},
  {path: 'refresh', component: RefreshComponent},
  {path: 'cgu', component: CguComponent},
  {path: 'reset-password', component: ResetPasswordComponent},
  {path: 'email-confirmation', component: EmailConfirmationComponent},
  {path: 'validate-account', component: ValidateAccountComponent},
  {path: 'view/post/:id', component: ViewPostComponent},
  {path: 'view/profile/:id', component: AccountProfileViewComponent},
  {path: 'assistance', component: AssistanceComponent},
  {path: '', component: AccountComponent, canActivate: [AuthenticationGuardService], children: [
      {path: 'dashboard', component: DashboardComponent},
      {path: 'profile/my', component: ProfileaccountComponent},
      {path: 'profile/:id', component: ProfileaccountComponent},
      {path: 'profile/edit/my', component: EditaccountComponent},
      {path: 'profile/view/:id', component: EditaccountComponent},
      {path: 'documents/my', component: DocumentsComponent},
      {path: 'documents/:id', component: DocumentsComponent},
      {path: 'network/my', component: NetworkComponent},
      {path: 'network/:id', component: NetworkComponent},
      {path: 'setting/my', component: SettingComponent},
      {path: 'messages/my', component: MessagesComponent},
      {path: 'posts/:id', component: PostContentComponent},
      {path: 'posts/edit/my', component: EditPostComponent},
      {path: 'posts/edit/my/:id', component: EditPostComponent},
      {path: 'notifications/my', component: NotificationComponent},
      {path: 'search-results', component: SearchResultsComponent}
  ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    onSameUrlNavigation: 'reload',
    scrollPositionRestoration: 'disabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

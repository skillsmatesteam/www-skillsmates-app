import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AccountService } from './services/accounts/account/account.service';
import { AuthenticateService } from './services/accounts/authenticate/authenticate.service';
import { AuthenticationGuardService } from './services/authentication.guard.service';
import { AuthorizationGuardService } from './services/authorization.guard.service';
import { ToastrModule } from 'ngx-toastr';
import { ToastService } from './services/toast/toast.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RootComponent } from './application/root/root.component';
import { ProfileaccountComponent } from './application/account/profileaccount/profileaccount.component';
import { EditaccountComponent } from './application/account/editaccount/editaccount.component';
import { AccountComponent } from './application/account/account/account.component';
import { HeaderComponent } from './components/header/header.component';
import { AccountprofilecardComponent } from './components/accounts/accountprofilecard/accountprofilecard.component';
import { AccountpostComponent } from './components/posts/accountpost/accountpost.component';
import { AccountbiographyComponent } from './components/accounts/accountbiography/accountbiography.component';
import { AccountcurriculumComponent } from './components/accounts/accountcurriculum/accountcurriculum.component';
import { AboutComponent } from './components/about/about.component';
import { AccountdocumentsComponent } from './components/documents/accountdocuments/accountdocuments.component';
import { AccountlinksComponent } from './components/accounts/accountlinks/accountlinks.component';
import { LogoutComponent } from './pages/logout/logout.component';
import { FooterComponent } from './components/footer/footer.component';
import { AccounteditbiographyComponent } from './components/accounts/accounteditbiography/accounteditbiography.component';
import { AccountgeneralinfosComponent } from './components/accounts/accountgeneralinfos/accountgeneralinfos.component';
import { AccounteditskillsComponent } from './components/accounts/accounteditskills/accounteditskills.component';
import { AccounteditacademicsComponent } from './components/accounts/accounteditacademics/accounteditacademics.component';
import { AccounteditprofessionnalComponent } from './components/accounts/accounteditprofessionnal/accounteditprofessionnal.component';
import { DashboardComponent } from './application/dashboard/dashboard.component';
import { CreatepostComponent } from './components/posts/createpost/createpost.component';
import { MediatypeComponent } from './components/mediatype/mediatype.component';
import { MediatypepostComponent } from './components/posts/mediatypepost/mediatypepost.component';
import { InterestingprofilesComponent } from './components/networks/interestingprofiles/interestingprofiles.component';
import { InterestingprofileComponent } from './components/networks/interestingprofile/interestingprofile.component';
import { CookieService } from 'ngx-cookie-service';
import { AccountprofileComponent } from './components/accounts/accountprofile/accountprofile.component';
import { LoadingComponent } from './components/loading/loading.component';
import { AccountonlineComponent } from './components/networks/accountonline/accountonline.component';
import { AccountskillcardComponent } from './components/accounts/accountskillcard/accountskillcard.component';
import { RefreshComponent } from './components/refresh/refresh.component';
import { ProfessionalService } from './services/accounts/account/professional.service';
import { AccountprofessionalcardComponent } from './components/accounts/accountprofessionalcard/accountprofessionalcard.component';
import { AsyncPipe, DatePipe } from '@angular/common';
import { AccountfollowComponent } from './components/networks/accountfollow/accountfollow.component';
import { HttpConfigInterceptor } from './interceptor/httpconfig.interceptor';
import { AroundyouComponent } from './components/aroundyou/aroundyou.component';
import { AcademicService } from './services/accounts/account/academic.service';
import { AccountdegreeobtainedcardComponent } from './components/accounts/accountdegreeobtainedcard/accountdegreeobtainedcard.component';
import { AccountsecondaryeducationComponent } from './components/accounts/accountsecondaryeducation/accountsecondaryeducation.component';
import { AccounthighereducationComponent } from './components/accounts/accounthighereducation/accounthighereducation.component';
import { MultimediafileService } from './services/media/multimedia/multimediafile.service';
import { MultimediaService } from './services/media/multimedia/multimedia.service';
import { AvatarUrlPipe } from './pipes/avatar-url.pipe';
import { ProfessionIconPipe } from './pipes/profession-icon.pipe';
import { PostComponent } from './components/posts/post/post.component';
import { PostfileService } from './services/pages/post/postfile.service';
import { MobileService } from './services/mobile/mobile.service';
import { MediaPostUrlPipe } from './pipes/media-post-url.pipe';
import { PostService } from './services/pages/post/post.service';
import { AccountmediatypeComponent } from './components/accountmediatype/accountmediatype.component';
import { AlertComponent } from './components/alert/alert.component';
import { FilesizePipe } from './pipes/filesize.pipe';
import { MediaUrlPipe } from './pipes/media-url.pipe';
import { DocumentsComponent } from './application/documents/documents.component';
import { DocumentcontentComponent } from './components/documents/documentcontent/documentcontent.component';
import { EditsecondaryeducationComponent } from './components/accounts/editsecondaryeducation/editsecondaryeducation.component';
import { EdithighereducationComponent } from './components/accounts/edithighereducation/edithighereducation.component';
import { EditdegreeobtainedComponent } from './components/accounts/editdegreeobtained/editdegreeobtained.component';
import { ModalAddPostComponent } from './components/posts/modal-add-post/modal-add-post.component';
import { ModalSharePostComponent } from './components/posts/modal-share-post/modal-share-post.component';
import { ModalHeaderPlusComponent } from './components/modal-header-plus/modal-header-plus.component';
import { CommentComponent } from './components/comment/comment.component';
import { AutosizeModule } from '@techiediaries/ngx-autosize';
import { ProgresBarHorizontalComponent } from './components/progres-bar-horizontal/progres-bar-horizontal.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AccountGeneralInfosCardComponent } from './components/accounts/account-general-infos-card/account-general-infos-card.component';
import { NetworkComponent } from './application/network/network.component';
import { AccountNetworkProfileComponent } from './components/networks/account-network-profile/account-network-profile.component';
import { NetworkContentComponent } from './components/networks/network-content/network-content.component';
import { ImagePipe } from './pipes/image.pipe';
import { DocumentpreviewComponent } from './components/documents/documentpreview/documentpreview.component';
import { NgxLinkPreviewModule } from 'ngx-link-preview';
import { SettingComponent } from './application/account/setting/setting.component';
import { SettingContentComponent } from './components/setting-content/setting-content.component';
import { NotificationsComponent } from './components/notification/notifications/notifications.component';
import { YouTubePlayerModule } from '@angular/youtube-player';
import { MultimediaDisplayComponent } from './components/multimedia/multimedia-display/multimedia-display.component';
import { MessagesComponent } from './application/messages/messages.component';
import { MessageContentComponent } from './components/messages/message-content/message-content.component';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AlerteUpdatesComponent } from './components/alerte-updates/alerte-updates.component';
import { UpdateService } from './services/settings/update/update.service';
import { PostContentComponent } from './application/post/post-content/post-content.component';
import { NgxEditorModule } from 'ngx-editor';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
import { NotificationComponent } from './application/notification/notification.component';
import { NotificationContentComponent } from './components/notification/notification-content/notification-content.component';
import { DragDropDirective } from './directives/drag-drop.directive';
import { InfoTextComponent } from './components/info-text/info-text.component';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { SharedPostComponent } from './components/posts/shared-post/shared-post.component';
import { EditPostComponent } from './application/post/edit-post/edit-post.component';
import { PostHeaderComponent } from './components/posts/post-header/post-header.component';
import { PostDescriptionComponent } from './components/posts/post-description/post-description.component';
import { ViewPostComponent } from './components/posts/view-post/view-post.component';
import { NotificationDetailComponent } from './components/notification/notification-detail/notification-detail.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgxSpinnerModule } from 'ngx-spinner';
import { PdfJsViewerModule } from 'ng2-pdfjs-viewer';
import { NgxViewerModule } from 'ngx-viewer';
import { LinkDisplayComponent } from './components/link-display/link-display.component';
import { AccountInterestsComponent } from './components/accounts/account-interests/account-interests.component';
import { AccountInterestComponent } from './components/accounts/account-interest/account-interest.component';
import { SkillsmatesPartnerComponent } from './components/skillsmates-partner/skillsmates-partner.component';
import { SkillschatComponent } from './components/messages/skillschat/skillschat.component';
import { TruncatePipe } from './pipes/truncate.pipe';
import { EditProfileImageComponent } from './components/accounts/edit-profile-image/edit-profile-image.component';
import { ImageCropperModule } from 'ngx-image-cropper';
registerLocaleData(localeFr, 'fr');
import { NgxPaginationModule } from 'ngx-pagination';
import { AssistanceComponent } from './application/assistance/assistance.component';
import { AssistanceDashboardComponent } from './components/assistance/assistance-dashboard/assistance-dashboard.component';
import { AssistanceFaqComponent } from './components/assistance/assistance-faq/assistance-faq.component';
import { AssistanceContactUsComponent } from './components/assistance/assistance-contact-us/assistance-contact-us.component';
import {
  BsDatepickerConfig,
  BsDatepickerModule,
} from 'ngx-bootstrap/datepicker';
import { AlertConfig, AlertModule } from 'ngx-bootstrap/alert';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ReturnButtonComponent } from './components/return-button/return-button.component';
import { ModalSearchComponent } from './components/modal-search/modal-search.component';
import { SearchResultsComponent } from './application/search-results/search-results.component';
import { SearchTypeComponent } from './components/search-type/search-type.component';
import { DocumentSearchResultComponent } from './components/document-search-result/document-search-result.component';
import {CguComponent} from './pages/cgu/cgu.component';
import {SkillsmatesAmbassadorComponent} from './components/skillsmates-ambassador/skillsmates-ambassador.component';
import { SubscriptionButtonComponent } from './components/common/subscription-button/subscription-button.component';
import { HorizontalLineComponent } from './components/common/horizontal-line/horizontal-line.component';
import { MessagePreviewComponent } from './components/messages/message-preview/message-preview.component';
import { NotificationPreviewComponent } from './components/notification-preview/notification-preview.component';
import { MultimediaThumbnailDisplayComponent } from './components/multimedia/multimedia-thumbnail-display/multimedia-thumbnail-display.component';
import { TimeAgoPipe } from './pipes/time-ago.pipe';
import { AccountCertificationComponent } from './components/accounts/account-certification/account-certification.component';
import { EditCertificationComponent } from './components/accounts/edit-certification/edit-certification.component';
import { NotificationSkillschatComponent } from './components/notification/notification-skillschat/notification-skillschat.component';
import { ForumComponent } from './components/forums/forum/forum.component';
import { ForumAccountComponent } from './components/forums/forum-account/forum-account.component';
import { SelectionsComponent } from './components/selection/selections/selections.component';
import { SelectionAccountComponent } from './components/selection/selection-account/selection-account.component';
import { EmailConfirmationComponent } from './pages/email-confirmation/email-confirmation.component';
import { AccountProfileComponent } from './components/accounts/account-profile/account-profile.component';
import { ViewPostAccountComponent } from './components/posts/view-post-account/view-post-account.component';
import { AdBannerComponent } from './components/ad-banner/ad-banner.component';
import {NgxGoogleAnalyticsModule} from 'ngx-google-analytics';
import { JobIconPipe } from './pipes/job-icon.pipe';
import { SelectionTabComponent } from './components/selection-tab/selection-tab.component';
import { DocumentPreviewMobileComponent } from './components/document-preview-mobile/document-preview-mobile.component';
import {IvyCarouselModule} from 'angular-responsive-carousel';
import { StatusPipe } from './pipes/status.pipe';
import { GenderPipe } from './pipes/gender.pipe';
import { SendChatComponent } from './components/send-chat/send-chat.component';
import { HeaderChatComponent } from './components/header-chat/header-chat.component';
import {WebcamModule} from 'ngx-webcam';
import { CountryNamePipe } from './pipes/country-name.pipe';
import { AccountProfileViewComponent } from './components/accounts/account-profile-view/account-profile-view.component';
import { ValidateAccountComponent } from './pages/validate-account/validate-account.component';
import { NgxDocViewerModule } from 'ngx-doc-viewer';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RootComponent,
    ProfileaccountComponent,
    EditaccountComponent,
    AccountComponent,
    HeaderComponent,
    AccountprofilecardComponent,
    AccountpostComponent,
    AccountbiographyComponent,
    AccountcurriculumComponent,
    AboutComponent,
    AccountdocumentsComponent,
    AccountlinksComponent,
    LogoutComponent,
    FooterComponent,
    AccounteditbiographyComponent,
    AccountgeneralinfosComponent,
    AccounteditskillsComponent,
    AccounteditacademicsComponent,
    AccounteditprofessionnalComponent,
    DashboardComponent,
    CreatepostComponent,
    InterestingprofilesComponent,
    InterestingprofileComponent,
    MediatypeComponent,
    MediatypepostComponent,
    InterestingprofileComponent,
    AccountprofileComponent,
    LoadingComponent,
    AccountonlineComponent,
    AccountskillcardComponent,
    RefreshComponent,
    AccountprofessionalcardComponent,
    AccountfollowComponent,
    AroundyouComponent,
    AccountdegreeobtainedcardComponent,
    AccountsecondaryeducationComponent,
    AccounthighereducationComponent,
    AvatarUrlPipe,
    ProfessionIconPipe,
    PostComponent,
    AccountmediatypeComponent,
    AlertComponent,
    FilesizePipe,
    MediaPostUrlPipe,
    MediaUrlPipe,
    DocumentsComponent,
    DocumentcontentComponent,
    EditsecondaryeducationComponent,
    EdithighereducationComponent,
    EditdegreeobtainedComponent,
    ModalAddPostComponent,
    ModalSharePostComponent,
    ModalHeaderPlusComponent,
    EditdegreeobtainedComponent,
    CommentComponent,
    ProgresBarHorizontalComponent,
    AccountGeneralInfosCardComponent,
    NetworkComponent,
    AccountNetworkProfileComponent,
    NetworkContentComponent,
    ImagePipe,
    DocumentpreviewComponent,
    SettingComponent,
    SettingContentComponent,
    NotificationsComponent,
    MultimediaDisplayComponent,
    NotificationsComponent,
    SettingContentComponent,
    MessagesComponent,
    MessageContentComponent,
    SettingContentComponent,
    NotificationsComponent,
    MultimediaDisplayComponent,
    AlerteUpdatesComponent,
    PostContentComponent,
    ResetPasswordComponent,
    NotificationComponent,
    NotificationContentComponent,
    DragDropDirective,
    NotificationContentComponent,
    InfoTextComponent,
    NotificationContentComponent,
    SharedPostComponent,
    EditPostComponent,
    PostHeaderComponent,
    PostDescriptionComponent,
    ViewPostComponent,
    NotificationDetailComponent,
    LinkDisplayComponent,
    AccountInterestsComponent,
    AccountInterestComponent,
    SkillsmatesPartnerComponent,
    SkillschatComponent,
    TruncatePipe,
    EditProfileImageComponent,
    AssistanceComponent,
    AssistanceDashboardComponent,
    AssistanceFaqComponent,
    AssistanceContactUsComponent,
    ReturnButtonComponent,
    EditProfileImageComponent,
    ModalSearchComponent,
    SearchResultsComponent,
    SearchTypeComponent,
    DocumentSearchResultComponent,
    CguComponent,
    SkillsmatesPartnerComponent,
    SkillsmatesAmbassadorComponent,
    SubscriptionButtonComponent,
    HorizontalLineComponent,
    MessagePreviewComponent,
    NotificationPreviewComponent,
    HorizontalLineComponent,
    MultimediaThumbnailDisplayComponent,
    TimeAgoPipe,
    AccountCertificationComponent,
    EditCertificationComponent,
    NotificationSkillschatComponent,
    ForumComponent,
    ForumAccountComponent,
    SelectionsComponent,
    SelectionAccountComponent,
    EmailConfirmationComponent,
    AccountProfileComponent,
    SelectionAccountComponent,
    ViewPostAccountComponent,
    SelectionAccountComponent,
    AdBannerComponent,
    JobIconPipe,
    SelectionTabComponent,
    DocumentPreviewMobileComponent,
    StatusPipe,
    GenderPipe,
    SendChatComponent,
    HeaderChatComponent,
    CountryNamePipe,
    AccountProfileViewComponent,
    ValidateAccountComponent
  ],
  imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot(),
        ReactiveFormsModule,
        AutosizeModule,
        NgbModule,
        NgxLinkPreviewModule,
        YouTubePlayerModule,
        AngularFireDatabaseModule,
        AngularFireAuthModule,
        AngularFireMessagingModule,
        YouTubePlayerModule,
        NgxEditorModule,
        NgxViewerModule,
        PdfJsViewerModule,
        InfiniteScrollModule,
        NgxSpinnerModule,
        NgxDocViewerModule,
        ImageCropperModule,
        NgxSpinnerModule,
        NgxPaginationModule,
        AccordionModule,
        AlertModule,
        ButtonsModule,
        // CarouselModule,
        CollapseModule,
        IvyCarouselModule,
        WebcamModule,
        BsDatepickerModule.forRoot(),
        NgxGoogleAnalyticsModule.forRoot(environment.GA_TRACKING_ID)
    ],
  providers: [
    AccountService,
    AuthenticateService,
    AuthenticationGuardService,
    AuthorizationGuardService,
    ToastService,
    CookieService,
    MobileService,
    DatePipe,
    ProfessionalService,
    AcademicService,
    MultimediafileService,
    MultimediaService,
    AvatarUrlPipe,
    ProfessionIconPipe,
    PostfileService,
    PostService,
    FilesizePipe,
    MediaUrlPipe,
    ImagePipe,
    AsyncPipe,
    TimeAgoPipe,
    UpdateService,
    AlertConfig,
    JobIconPipe,
    StatusPipe,
    GenderPipe,
    BsDatepickerConfig,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpConfigInterceptor,
      multi: true,
    },
    { provide: LOCALE_ID, useValue: 'fr-FR' },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import {
  AfterViewInit,
  Component, HostListener,
  OnChanges, OnDestroy,
  OnInit
} from '@angular/core';
import {variables} from '../../../environments/variables';
import {MediaHelper} from '../../helpers/media-helper';
import {RoutesHelper} from '../../helpers/routes-helper';
import {Account} from '../../models/account/account';
import {InfoMediaType} from '../../models/multimedia/infoMediaType';
import {AccountSocialInteractions} from '../../models/account/accountSocialInteractions';
import {ActivatedRoute} from '@angular/router';
import {environment} from '../../../environments/environment';
import {Alert} from '../../models/alert';
import {ToastService} from '../../services/toast/toast.service';
import {PostSocialInteractionsSearch} from '../../models/post/postSocialInteractionsSearch';
import {AuthenticateService} from '../../services/accounts/authenticate/authenticate.service';
import {PostService} from '../../services/pages/post/post.service';
import {AccountService} from '../../services/accounts/account/account.service';
import {PostSocialInteractions} from '../../models/post/postSocialInteractions';
import {SocialInteractionsHelper} from '../../helpers/social-interactions-helper';
import {AccountSocialInteractionsSearch} from '../../models/account/accountSocialInteractionsSearch';
import {SubscriptionService} from '../../services/accounts/subscription/subscription.service';
import {SearchParam} from '../../models/searchParam';
import {MediaSubtype} from '../../models/multimedia/mediaSubtype';
import {MobileService} from '../../services/mobile/mobile.service';
import {RestCountryService} from '../../services/api/rest-country.service';
import {Status} from '../../enum/status.enum';
import {CountryHelper} from '../../helpers/country-helper';


@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent implements OnInit, OnChanges, OnDestroy {
  routes = RoutesHelper.routes;
  mediaTypes = MediaHelper.mediaTypes;
  mediaTypeProfile = MediaHelper.profil;
  mediaTypeAll = MediaHelper.all;
  allMediaTypes: InfoMediaType[] = [];
  loggedAccount: Account;
  searchContent = '';
  currentMediaType: string;
  selectedSubtypes: MediaSubtype[] = [];
  selectedMediaType: InfoMediaType = {} as InfoMediaType;
  selectedMediaTypes: InfoMediaType[] = [];
  selectedStatuses: any[] = [];
  loading: boolean;
  error: boolean;
  accounts: Account[];
  private positionMediaType: number;
  pages: number[];
  itemsPerPage = 24;
  itemsPerPageDefault: number;
  currentPage: number;
  accountGeneralInfos: AccountSocialInteractions = {} as AccountSocialInteractions;
  accountsPage: Account[];
  selectedCountry = '';
  selectedMediaSubtype: string;
  selectedDate: string;
  selectedPreference: string;
  accountSocialInteractionsSearch: AccountSocialInteractionsSearch = {} as AccountSocialInteractionsSearch;
  postSocialInteractionsSearch: PostSocialInteractionsSearch = {} as PostSocialInteractionsSearch;
  count: number;
  city: string;
  submitted = false;
  alert: Alert = {} as Alert;
  errors;
  postsSocialInteractions: PostSocialInteractions[] = [];
  accountsSocialInteractions: AccountSocialInteractions[] = [];
  postsSocialInteractionsSaved: PostSocialInteractions[];
  accountsSocialInteractionsSaved: AccountSocialInteractions[];
  restCountries: any[] = [];
  currentCountry: any;
  selectedCountries: any[];
  searchParam: SearchParam = {} as SearchParam;
  searchButtonClicked = false;
  isAllProfilesSelected = true;
  public screenWidth: number = window.innerWidth;

  constructor(private authenticateService: AuthenticateService,
              private restCountryService: RestCountryService,
              private activatedRoute: ActivatedRoute,
              private postService: PostService,
              private toastService: ToastService,
              private mobileService: MobileService,
              private subscriptionService: SubscriptionService,
              private accountService: AccountService) {
    this.itemsPerPageDefault = environment.itemsPerPageSearch;
  }

  ngOnInit(): void {
    this.city = '';
    this.currentCountry = '';
    this.postsSocialInteractions = [];
    this.postsSocialInteractionsSaved = [];
    const param = this.getSearchParam();
    localStorage.removeItem(variables.search_param);
    if (param){
      this.searchParam = param;
      this.selectedMediaType = MediaHelper.getInfoMediaTypeByType(this.searchParam.mediaSubtypes[0].mediaType.label);
      // this.onClickMediaTypes(this.selectedMediaType);
      // this.onClickSearch();
    }
    this.mobileService.unsetSkillschatDesign();
    this.allMediaTypes = [this.mediaTypeProfile].concat(this.mediaTypes);
    for (const elt of this.allMediaTypes) {
      elt.found = 0 ;
    }
    this.initErrors();
    this.loggedAccount = this.authenticateService.getCurrentAccount();
    this.findAccountInfos();
    this.itemsPerPage = this.itemsPerPageDefault;
    this.getInfoMediaType();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    this.screenWidth = window.innerWidth;
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }

  ngOnChanges(): void {
  }

  ngOnDestroy(): void {
    // localStorage.removeItem(variables.search_param);
    this.postsSocialInteractions = [];
    this.postsSocialInteractionsSaved = [];
    for (const elt of this.allMediaTypes) {
      elt.found = 0 ;
    }
  }

  findAccountInfos(): void {
    this.loading = true;
    this.accountService.findAccountSocialInteractions(this.loggedAccount.idServer).subscribe((response) => {
      this.accountGeneralInfos = response.resource;
      this.loading = false;
    }, (error) => {
      this.loading = false;
    }, () => {
    });
  }

  getInfoMediaType(): void {
    localStorage.removeItem(variables.current_media_type);
    this.retrieveTextToSearch();
  }

  retrieveTextToSearch(): void {
    const textToSearch = localStorage.getItem(variables.text_to_search);
    if (textToSearch && textToSearch !== 'undefined') {
      this.searchContent = textToSearch;
      this.searchParam.content = textToSearch;
    }
    localStorage.removeItem(variables.text_to_search);
  }

  getSearchParam(): SearchParam {
    const param: any = localStorage.getItem(variables.search_param);
    return param ? JSON.parse(param) : null;
  }

  onClickMediaTypes(m: InfoMediaType): void {
    // for (const elt of this.allMediaTypes) {
    //   elt.found = 0 ;
    // }
    this.resetMediaTypes();
    this.searchButtonClicked = false;
    this.selectedMediaType = m;
    this.currentMediaType = m.type;
    this.count = 0;
    this.postsSocialInteractions = [];
    this.postsSocialInteractionsSaved = [];
    this.updateSelectedMediaTypes(m);
  }

  onChangePreferences(e): void {
    this.selectedPreference = e.target.value;
    this.filterMedias();
  }

  onChangeItemsNumber(e): void {
    this.itemsPerPage = e.target.value;
  }

  onChangeTime(e): void {
    this.selectedDate = e.target.value;
    this.filterMedias();
  }

  onClickSearch(): void {
    this.searchButtonClicked = true;
    for (const elt of this.allMediaTypes) {
      elt.found = 0 ;
    }
    this.postsSocialInteractions = [];
    this.postsSocialInteractionsSaved = [];
    if (this.selectedMediaType.position === 6) {
      this.searchParam.statuses = this.selectedStatuses;
      this.searchAccounts(0);
    } else {
      this.searchMedias(0);
    }
  }

  searchAccounts(page: number): void {
    this.loading = true;
    this.error = false;
    this.searchParam.city = this.city;
    this.searchParam.country = this.currentCountry;
    if (this.validateDataAccount()) {
      if (this.isAllProfilesSelected){
        this.searchParam.mediaSubtypes = [];
      }
      this.accountsSocialInteractions = [];
      this.accountService.searchAccounts(this.searchParam, page, this.itemsPerPage).subscribe((response) => {

        this.accountSocialInteractionsSearch = response.resource;
        this.count = this.accountSocialInteractionsSearch.numberAccounts;
        this.currentPage = this.accountSocialInteractionsSearch.currentPage + 1;
        this.pages = this.accountSocialInteractionsSearch.pageAccounts;

        let retrievedAccounts: AccountSocialInteractions[] = [];
        retrievedAccounts = this.accountSocialInteractionsSearch.accountSocialInteractions;
        if (retrievedAccounts && retrievedAccounts.length > 0){
          retrievedAccounts.forEach(elt => {
            if ((elt.account)){
              this.accountsSocialInteractions.push(elt);
            }
          });
        }
        this.allMediaTypes.forEach((mType: InfoMediaType) => {
          if (mType.type.toUpperCase() === 'DOCUMENT'){
            mType.found = this.accountSocialInteractionsSearch.resultFound.document;
          }
          if (mType.type.toUpperCase() === 'LINK'){
            mType.found = this.accountSocialInteractionsSearch.resultFound.lien;
          }
          if (mType.type.toUpperCase() === 'AUDIO'){
            mType.found = this.accountSocialInteractionsSearch.resultFound.audio;
          }
          if (mType.type.toUpperCase() === 'IMAGE'){
            mType.found = this.accountSocialInteractionsSearch.resultFound.image;
          }
          if (mType.type.toUpperCase() === 'VIDEO'){
            mType.found = this.accountSocialInteractionsSearch.resultFound.video;
          }
          if (mType.type.toUpperCase() === 'PROFILE'){
            mType.found = this.accountSocialInteractionsSearch.resultFound.profile;
          }
        });
        this.accountsSocialInteractionsSaved = this.accountsSocialInteractions;
        this.loading = false;
      }, (error) => {
        this.loading = false;
        // this.toastService.showError('Erreur !', error.message);
        this.error = true;
      }, () => {
      });
    } else {
      this.toastService.showError('Attention !', 'Zone de recherche? Pays? ville?');
      this.loading = false;
    }
  }

  searchMedias(page: number): void {
    this.loading = true;
    this.error = false;
    this.count = 0;
    if (this.validateDataDocument()) {
      this.postsSocialInteractions = [];
      localStorage.setItem(variables.search_param, JSON.stringify(this.searchParam));
      this.postService.searchPosts(this.searchParam, page, this.itemsPerPage).subscribe((response) => {

        this.postSocialInteractionsSearch = response.resource;
        this.currentPage = this.postSocialInteractionsSearch.currentPage + 1;
        this.pages = this.postSocialInteractionsSearch.pagePosts;
        this.count = this.postSocialInteractionsSearch.numberPosts;

        let retrievedPosts: PostSocialInteractions[] = [];

        retrievedPosts = this.postSocialInteractionsSearch.postSocialInteractions;
        if (retrievedPosts && retrievedPosts.length > 0) {
          retrievedPosts.forEach(elt => {
            if ((elt.post.multimedia && elt.post.multimedia.length > 0)) {
              this.postsSocialInteractions.push(elt);
            }
          });
        }

        this.allMediaTypes.forEach((mType: InfoMediaType) => {
          if (mType.type.toUpperCase() === 'DOCUMENT'){
            mType.found = this.postSocialInteractionsSearch.resultFound.document;
          }
          if (mType.type.toUpperCase() === 'LINK'){
            mType.found = this.postSocialInteractionsSearch.resultFound.lien;
          }
          if (mType.type.toUpperCase() === 'AUDIO'){
            mType.found = this.postSocialInteractionsSearch.resultFound.audio;
          }
          if (mType.type.toUpperCase() === 'IMAGE'){
            mType.found = this.postSocialInteractionsSearch.resultFound.image;
          }
          if (mType.type.toUpperCase() === 'VIDEO'){
            mType.found = this.postSocialInteractionsSearch.resultFound.video;
          }
          if (mType.type.toUpperCase() === 'PROFILE'){
            mType.found = this.postSocialInteractionsSearch.resultFound.profile;
          }
        });
        this.postsSocialInteractionsSaved = this.postsSocialInteractions;
        this.loading = false;
      }, (error) => {
        this.loading = false;
        // this.toastService.showError('Erreur !', error.message);
        this.error = true;
      }, () => {
      });
    } else {
      this.toastService.showError('Attention !', 'Zone de recherche?');
      this.loading = false;
    }
  }

  selectAllProfiles(all: boolean): void {
    this.isAllProfilesSelected = all;
  }

  selectMediaSubtype(subtype: string): void {
    if (subtype) {
      this.selectedMediaSubtype = subtype.toLowerCase();
    }
  }

  selectSubType(subtypes: MediaSubtype[]): void {
    if (subtypes) {
      this.selectedSubtypes = subtypes;
      this.searchParam.mediaSubtypes = subtypes;
      this.searchButtonClicked = false;
      if (this.searchParam.content) {
        if (this.searchParam.content.trim().length > 2){
          this.searchButtonClicked = true;
          if (subtypes[0].mediaType.label.toUpperCase() === 'PROFILE'){
            this.searchParam.statuses = this.selectedStatuses;
            this.searchAccounts(0);
          }else {
            this.searchMedias(0);
          }
        }
      }
    }
  }

  getPage(page: number): void {
    if (this.selectedMediaType.position === 6) {
      this.searchAccounts(page - 1);
    } else {
      this.searchMedias(page - 1);
    }
  }

  keyupCity(): void {
    this.errors.city = '';
  }

  keyupContent(): void {
    this.errors.content = '';
  }

  initErrors(): void {
    this.submitted = false;
    this.errors = {valid: true, city: '', searchContent: '', selectedCountry: ''};
    this.alert = {} as Alert;
  }

  validateDataAccount(): boolean {
    this.initErrors();

    this.submitted = true;
    return this.errors.valid;
  }

  validateDataDocument(): boolean {
    this.initErrors();
    this.submitted = true;
    return this.errors.valid;
  }

  onKeydown(event: any): void {
    this.onClickSearch();
  }

  filterMedias(): void {
    this.postsSocialInteractions = this.sortMediasByPreference(this.sortMediasByDate(this.postsSocialInteractionsSaved));
  }

  sortMediasByPreference(postsSocialInteractionsToSort: PostSocialInteractions[]): PostSocialInteractions[] {
    let sortedPosts: PostSocialInteractions[] = [];
    if (this.selectedPreference) {
      if (this.selectedPreference === 'Les plus commentés') { // Plus commentés
        sortedPosts = postsSocialInteractionsToSort.sort((a, b) => {
          return (SocialInteractionsHelper.countComments(b.socialInteractions) - SocialInteractionsHelper.countComments(a.socialInteractions));
        });
      } else if (this.selectedPreference === 'Les plus likés') { // Plus likés
        sortedPosts = postsSocialInteractionsToSort.sort((a, b) => {
          return (SocialInteractionsHelper.countLikes(b.socialInteractions) - SocialInteractionsHelper.countLikes(a.socialInteractions));
        });
      } else if (this.selectedPreference === 'Les plus partagés') { // Plus partagés
        sortedPosts = postsSocialInteractionsToSort.sort((a, b) => {
          return (SocialInteractionsHelper.countShares(b.socialInteractions) - SocialInteractionsHelper.countShares(a.socialInteractions));
        });
      } else {
        sortedPosts = postsSocialInteractionsToSort;
      }
    } else {
      sortedPosts = postsSocialInteractionsToSort;
    }
    return sortedPosts;
  }

  sortMediasByDate(postsSocialInteractionsToSort: PostSocialInteractions[]): PostSocialInteractions[] {
    let sortedPosts: PostSocialInteractions[] = [];
    sortedPosts = postsSocialInteractionsToSort;
    if (this.selectedDate) {
      if (this.selectedDate === 'Plus recents') {
        sortedPosts = postsSocialInteractionsToSort.sort(this.compareDate1);
      }
      if (this.selectedDate === 'Plus anciens') {
        sortedPosts = postsSocialInteractionsToSort.sort(this.compareDate2);
      }
    }
    return sortedPosts;
  }

  // tslint:disable-next-line:typedef
  compareDate1(emp1: PostSocialInteractions, emp2: PostSocialInteractions) {
    const emp1Date = new Date(emp1.post.createdAt).getTime();
    const emp2Date = new Date(emp2.post.createdAt).getTime();
    return emp1Date < emp2Date ? 1 : -1;
  }

  // tslint:disable-next-line:typedef
  compareDate2(emp1: PostSocialInteractions, emp2: PostSocialInteractions) {
    const emp1Date = new Date(emp1.post.createdAt).getTime();
    const emp2Date = new Date(emp2.post.createdAt).getTime();
    return emp1Date > emp2Date ? 1 : -1;
  }

  updateSelectedMediaTypes(mediaType: InfoMediaType): void {
    this.updateListMediaTypes(mediaType);
  }

  updateListMediaTypes(mediaType: InfoMediaType): void {
    this.allMediaTypes.forEach((mType: InfoMediaType) => {
      if (mType.type === mediaType.type) {
        mType.selected = !mType.selected;
      }
    });
  }

  /**
   * remove profil in seleted types
   */
  removeProfileInSelectedMediaTypes(): void {
    let index = this.selectedMediaTypes.indexOf(this.mediaTypeProfile);
    if (index > -1) {
      this.selectedMediaTypes.splice(index, 1);
    }
    index = this.allMediaTypes.indexOf(this.mediaTypeProfile);
    this.allMediaTypes[index].selected = false;
  }

  resetMediaTypes(): void {
    this.selectedMediaTypes = [];
    this.allMediaTypes.forEach((mType: InfoMediaType) => {
      mType.selected = false;
      mType.found = 0;
    });
  }

  onKeyupCountry(): void {
    this.searchCountries();
  }

  searchCountries(): void{
    if (this.currentCountry){
      this.restCountries = CountryHelper.filterCountries(this.currentCountry);
    }
  }

  onSelectCountry(item): void{
    this.selectedCountries = this.restCountries.filter((user) => user.translations.fra.common.toLowerCase().includes(item.toLowerCase()));
    this.searchParam.country = this.selectedCountries[0].cca3;
  }
}

import {ChangeDetectorRef, Component, HostListener, OnInit} from '@angular/core';
import {InfoNetworkType} from '../../models/network/infoNetworkType';
import {NetworkHelper} from '../../helpers/network-helper';
import {variables} from '../../../environments/variables';
import {Account} from '../../models/account/account';
import {AuthenticateService} from '../../services/accounts/authenticate/authenticate.service';
import {AccountService} from '../../services/accounts/account/account.service';
import {SubscriptionService} from '../../services/accounts/subscription/subscription.service';
import {AccountNetwork} from '../../models/network/accountNetwork';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {ToastService} from '../../services/toast/toast.service';
import {RoutesHelper} from '../../helpers/routes-helper';
import {AccountSocialInteractions} from '../../models/account/accountSocialInteractions';
import {MobileService} from '../../services/mobile/mobile.service';
import {StringHelper} from '../../helpers/string-helper';
import {InfoMediaType} from '../../models/multimedia/infoMediaType';
import {MediaHelper} from '../../helpers/media-helper';

@Component({
  selector: 'app-network',
  templateUrl: './network.component.html',
  styleUrls: ['./network.component.css']
})
export class NetworkComponent implements OnInit {

  loading: boolean;
  networkTypes: InfoNetworkType[] = NetworkHelper.networkTypes;
  selectedNetworkType: InfoNetworkType;
  account: Account = {} as Account;
  currentNetworkType: string;
  accounts: Account[] = [];
  accountNetwork: AccountNetwork = {} as AccountNetwork;
  loggedAccount: Account = {} as Account;
  id: string;
  title = 'votre réseau';
  routes = RoutesHelper.routes;
  pages: number[] = [];
  currentPage = 0;
  count: number;
  public screenWidth: number = window.innerWidth;
  messageImageIcon = 'message';
  homeImageIcon = 'home';
  bellImageIcon = 'bell';
  networkImageIcon = 'network';
  selectedImageIcon = 'home';
  infosMediaTypes: InfoMediaType[] = MediaHelper.mediaTypes;

  constructor(private authenticateService: AuthenticateService,
              private accountService: AccountService,
              private activatedRoute: ActivatedRoute,
              private cdRef: ChangeDetectorRef,
              private router: Router,
              private toastService: ToastService,
              private mobileService: MobileService,
              private subscriptionService: SubscriptionService) { }

  ngOnInit(): void {
    this.mobileService.unsetSkillschatDesign();
    this.loading = true;
    this.loggedAccount = this.authenticateService.getCurrentAccount();
    this.id = this.activatedRoute.snapshot.paramMap.get(variables.id);
    if (!this.id){
      this.id = this.loggedAccount.idServer;
    }
    this.activatedRoute.queryParamMap.subscribe((paramMap: ParamMap) => {
      const refresh = paramMap.get('refresh');
      if (refresh) {
        this.loadAccountNetwork();
      }
    });
    this.loadAccountNetwork();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    this.screenWidth = window.innerWidth;
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }

  /**
   * load the network of an account
   */
  loadAccountNetwork(): void{
    if (this.loggedAccount.idServer !== this.id){
      this.findAccount();
    }else {
      this.loadNetwork();
    }
  }

  /**
   * get account
   */
  findAccount(): void{
    this.loading = true;
    this.accountService.findAccountById(this.id).subscribe((response) => {
      this.account = response.resource;
      this.title = 'le réseau de ' + this.account.firstname;
      this.loading = false;
      this.loadNetwork();
    }, (error) => {
      this.toastService.showError('Error', 'Compte non identifié');
      this.loading = false;
    }, () => {
      this.loading = false;
    });
  }

  private loadNetwork(): void{
    this.loading = true;
    const tab = localStorage.getItem(variables.tab);
    this.selectedNetworkType = tab ? NetworkHelper.getNetworkTypeByPosition(tab) : NetworkHelper.networkTypes[0];

    this.currentNetworkType = this.selectedNetworkType.type;
    localStorage.removeItem(variables.tab);
    this.findNetwork();
  }

  onClickTab(networkType: any): void {
    this.selectedNetworkType = networkType;
    this.currentNetworkType = networkType.type;
    this.findNetwork();
    // this.filterNetwork(this.selectedNetworkType);
  }

  findNetwork(): void{
    this.loading = true;
    const page = this.accountNetwork.currentPage ? this.accountNetwork.currentPage : 0;
    this.subscriptionService.findNetworkByPage(this.id, page, this.selectedNetworkType.position).subscribe((response) => {
      this.accountNetwork = response.resource;
      this.loading = false;
    }, (error) => {
      this.loading = false;
    }, () => {});
  }

  countAccountsByNetworkType(networkType: InfoNetworkType): number{
    let nb = 0;
    if (networkType.position === 1) { nb = this.accountNetwork.numberSuggestions; }
    if (networkType.position === 2) { nb = this.accountNetwork.numberFollowers; }
    if (networkType.position === 3) { nb = this.accountNetwork.numberFollowees; }
    if (networkType.position === 4) { nb = this.accountNetwork.numberAroundYou; }
    if (networkType.position === 5) { nb = this.accountNetwork.numberFavorites; }
    if (networkType.position === 6) { nb = this.accountNetwork.numberAccountsOnline; }
    return nb;
    // return NetworkHelper.countAccountsByNetworkType(this.accountNetwork, networkType);
  }

  filterAccountsByNetwork(): AccountSocialInteractions[]{
    // this.findNetwork();
    const accounts: Account[] = [];
    let accountsSocialInteractions: AccountSocialInteractions[] = [];
    this.currentPage = this.accountNetwork.currentPage;

    switch (this.selectedNetworkType.position) {
      case 1:
        if (this.accountNetwork.suggestions){
          accountsSocialInteractions = this.accountNetwork.suggestions;
          this.pages = this.accountNetwork.pageSuggestions;
          this.count = this.accountNetwork.numberSuggestions;
        }
        break;
      case 2:
        if (this.accountNetwork.followers) {
          accountsSocialInteractions = this.accountNetwork.followers;
          this.pages = this.accountNetwork.pageFollowers;
          this.count = this.accountNetwork.numberFollowers;
        }
        break;
      case 3:
        if (this.accountNetwork.followees) {
          accountsSocialInteractions = this.accountNetwork.followees;
          this.pages = this.accountNetwork.pageFollowees;
          this.count = this.accountNetwork.numberFollowees;
        }
        break;
      case 4:
        if (this.accountNetwork.aroundYou) {
          accountsSocialInteractions = this.accountNetwork.aroundYou;
          this.pages = this.accountNetwork.pageAroundYou;
          this.count = this.accountNetwork.numberAroundYou;
        }
        break;
      case 5:
        if (this.accountNetwork.favorites) {
          accountsSocialInteractions = this.accountNetwork.favorites;
          this.pages = this.accountNetwork.pageFavorites;
          this.count = this.accountNetwork.numberFavorites;
        }
        break;
      case 6:
        if (this.accountNetwork.accountsOnline) {
          accountsSocialInteractions = this.accountNetwork.accountsOnline;
          this.pages = this.accountNetwork.pageAccountsOnline;
          this.count = this.accountNetwork.numberAccountsOnline;
        }
        break;
      default:
        break;
    }
    return accountsSocialInteractions;
  }

  getTab(): string {
    return StringHelper.urlToTab(this.router.url);
  }

  // -----------------------------

  onMouseEnter(tab: string): void {
    switch (tab) {
      case 'home':
        this.homeImageIcon = 'home-selected';
        break;
      case 'message':
        this.messageImageIcon = 'message-selected';
        break;
      case 'bell':
        this.bellImageIcon = 'bell-selected';
        break;
      case 'network':
        this.networkImageIcon = 'network-selected';
        break;
    }
  }

  onMouseLeave(tab: string): void {
    switch (tab) {
      case 'home':
        this.homeImageIcon = 'home';
        break;
      case 'message':
        this.messageImageIcon = 'message';
        break;
      case 'bell':
        this.bellImageIcon = 'bell';
        break;
      case 'network':
        this.networkImageIcon = 'network';
        break;
    }
    this.resetImageIcon();
    this.onMouseEnter(this.selectedImageIcon);
  }

  resetImageIcon(): void {
    this.homeImageIcon = 'home';
    this.messageImageIcon = 'message';
    this.bellImageIcon = 'bell';
    this.networkImageIcon = 'network';
  }

  onClick(tab: string): void {
    this.selectedImageIcon = tab;
  }
}

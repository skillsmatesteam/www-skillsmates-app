import {Component, OnChanges, OnDestroy, OnInit} from '@angular/core';
import {Alert} from '../../../models/alert';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {MediaHelper} from '../../../helpers/media-helper';
import {Editor, toHTML, Toolbar} from 'ngx-editor';
import schema from '../../../schema';
import {variables} from '../../../../environments/variables';
import {MediaSubtype} from '../../../models/multimedia/mediaSubtype';
import {Post} from '../../../models/post/post';
import {Multimedia} from '../../../models/multimedia/multimedia';
import {Metadata} from '../../../models/multimedia/metadata';
import {Account} from '../../../models/account/account';
import {PostfileService} from '../../../services/pages/post/postfile.service';
import {PostService} from '../../../services/pages/post/post.service';
import {MultimediaService} from '../../../services/media/multimedia/multimedia.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {ToastService} from '../../../services/toast/toast.service';
import {MetadataService} from '../../../services/media/metadata/metadata.service';
import {InfoMediaType} from '../../../models/multimedia/infoMediaType';
import {infoText} from '../../../../environments/info-text';
import {SocialInteraction} from '../../../models/post/socialInteraction';
import {WebSocketService} from '../../../services/message/web-socket.service';
import {StringHelper} from '../../../helpers/string-helper';
import {MimetypeHelper} from '../../../helpers/mimetype-helper';
import {MobileService} from '../../../services/mobile/mobile.service';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.css']
})
export class EditPostComponent implements OnInit, OnDestroy {

  infosMediaType: InfoMediaType = {} as InfoMediaType;
  infoTextNewPostTitle = infoText.info_text_new_post_title;

  mediaSubtypes: MediaSubtype[];
  nrSelect: string;
  filteredMediaSubtypes: MediaSubtype[] = [];
  closeResult: string;
  post: Post = {} as Post;
  multimedias: Multimedia[] = [];
  multimedia: Multimedia = {} as Multimedia;
  metadata: Metadata = {} as Metadata;
  file: File;
  loading: boolean;
  previewLoading: boolean;
  loggedAccount: Account;
  alert: Alert = {} as Alert;
  submitted = false;
  errors;
  processing = false;
  routes = RoutesHelper.routes;
  mediaTypes = MediaHelper.mediaTypes;
  url: any;
  format: string;
  urlMedia: string;
  isMediaSelected = false;
  isUrlSelected = false;
  description = '';
  maxlength = variables.max_description_size ;
  blockFileDisabled = false;
  blockUrlDisabled = false;
  followers: Account[] = [];
  private positionMediaType: number;
  editor: Editor;
  toolbar: Toolbar = [
    ['bold', 'italic'],
    ['underline', 'strike'],
    ['code', 'blockquote'],
    ['ordered_list', 'bullet_list'],
    [{ heading: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'] }],
    ['link', 'image'],
    ['text_color', 'background_color'],
    ['align_left', 'align_center', 'align_right', 'align_justify']
  ];
  placeholder = variables.placeholder_description_post;
  displayConfig = 'NEW_NONE';
  public screenWidth: number = window.innerWidth;

  constructor(private postfileService: PostfileService,
              private postService: PostService,
              private multimediaService: MultimediaService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private webSocketService: WebSocketService,
              private authenticateService: AuthenticateService,
              private toastService: ToastService,
              private metadataService: MetadataService,
              private mobileService: MobileService)
  {}

  ngOnInit(): void {
    this.mobileService.unsetSkillschatDesign();
    this.editor = new Editor();
    this.initErrors();
    this.loggedAccount = this.authenticateService.getCurrentAccount();
    this.findMediaSubtypes();
    if (history.state.post){
      this.post = history.state.post;
      if (this.post.multimedia && this.post.multimedia[0] && this.post.multimedia.length > 0 && this.post.multimedia[0].url){
        this.displayConfig = 'EDIT_LINK';
        this.urlMedia = this.post.multimedia[0].url ;
        this.metadata = this.post.multimedia[0].metadata;
      }else if (this.post.multimedia && this.post.multimedia[0] && this.post.multimedia.length > 0 && this.post.multimedia[0].checksum){
        this.displayConfig = 'EDIT_FILE';
      }
      this.description = this.post.description;
      this.nrSelect = this.post.mediaSubtype.label;
      if (this.post.multimedia && this.post.multimedia[0] && this.post.multimedia.length > 0){
        this.infosMediaType =
          this.mediaTypes.find(mediaType => mediaType.type.toLowerCase() === this.post.multimedia[0].type.toLowerCase());
      }
      if (!this.infosMediaType){
        this.infosMediaType = MediaHelper.mediaTypes[0];
      }
    }
    this.activatedRoute.params.subscribe(params => {
      this.positionMediaType = params.id;
      this.getInfoMediaType();
    });
  }

  getInfoMediaType(): void{
    this.infosMediaType = JSON.parse(localStorage.getItem(variables.current_media_type));
    if (!this.infosMediaType){
      this.infosMediaType = MediaHelper.mediaTypes[0];
    }
    localStorage.removeItem(variables.current_media_type);
  }

  selectFile(event): void {
    this.displayConfig = 'NEW_FILE';
    this.previewLoading = true;
    this.errors.file = '';
    this.file = event.target.files[0];
    this.resetSelection();
    if (!this.validateFileSize()){
      this.toastService.showError('Erreur', 'La taille maximale est ' + variables.max_file_size + 'MB');
      this.isMediaSelected = false;
    }
    if (!this.validateFileMimetype()){
      this.toastService.showError('Erreur', 'Ce type de document n\'est pas accepté');
      this.isMediaSelected = false;
    }

    if (this.file){
      this.preview();
      this.isMediaSelected = true;
    }
    this.previewLoading = false;
    this.blockUrlDisabled = true;
  }

  getMediaType(type: string): string{
    return MediaHelper.getMediaType(type);
  }

  /**
   * publish post
   */
  onClickPublishMediaPost(): void{
    this.loading = true;
    this.post.type = 'ARTICLE';
    this.post.account = this.loggedAccount;
    this.post.url = this.urlMedia;
    console.log(this.post);
    if (this.validatePost()){
      if (this.post.idServer ){
        this.updateMediaPost();
        // if (this.file != null){
        //   this.updateMediaPost();
        // } else {
        //   this.updatePost();
        // }
      }else {
        this.publishMediaPost();
        // if (this.file != null){
        //   this.publishMediaPost();
        // } else {
        //   this.publishPost();
        // }
      }
    }else {
      this.toastService.showError('Erreur', 'Données invalides');
      this.loading = false;
    }
  }

  /**
   * publish post that contains media file
   */
  publishMediaPost(): void {
    this.loading = true;
    this.post.type = 'ARTICLE';
    if (!this.file){
      const content = '';
      const data = new Blob([content], { type: 'application/txt' });
      const arrayOfBlob = new Array<Blob>();
      arrayOfBlob.push(data);
      this.file = new File(arrayOfBlob, variables.no_filename);
    }
    console.log(this.post);
    console.log(this.file);
    this.postfileService.publishMediaPost(this.file, this.post).subscribe((response) => {
      this.toastService.showSuccess('Sucess', 'Post publié avec succés');
      this.followers = response.resources;
      this.loading = false;
      this.router.navigate([this.routes.get('dashboard')], {
        queryParams: {refresh: new Date().getTime()}
      });
      if (this.followers && this.followers.length > 0){
        this.notifyFollowers();
      }
    }, (error) => {
      this.toastService.showError('Erreur', 'Erreur lors du post');
      this.loading = false;
    }, () => {});
  }

  /**
   * publish post that does not contain media file
   */
  publishPost(): void {
    this.loading = true;
    if (this.isUrlSelected){
      this.multimedia.url = this.urlMedia ;
      this.multimedias.push(this.multimedia);
      this.post.multimedia = this.multimedias;
      this.post.url = this.urlMedia;
    }
    this.postService.publishPost(this.post).subscribe((response) => {
      this.toastService.showSuccess('Success', 'Post publié avec succés');
      this.followers = response.resources;
      this.notifyFollowers();
      this.loading = false;
      this.router.navigate([this.routes.get('dashboard')], {
        queryParams: {refresh: new Date().getTime()}
      });
    }, (error) => {
      this.toastService.showError('Erreur', 'Erreur lors du post du lien');
      this.loading = false;
    }, () => {});
  }

  /**
   * update post that contains media
   */
  updateMediaPost(): void{
    this.loading = true;
    console.log(this.file);
    if (!this.file){
      const content = '';
      const data = new Blob([content], { type: 'application/txt' });
      const arrayOfBlob = new Array<Blob>();
      arrayOfBlob.push(data);
      this.file = new File(arrayOfBlob, variables.no_filename);
    }
    console.log(this.post);
    console.log(this.file);
    this.postfileService.updateMediaPost(this.file, this.post).subscribe((response) => {
      this.toastService.showSuccess('Sucess', 'Publication mis à jour avec succés');
      this.followers = response.resources;
      this.loading = false;
      this.router.navigate([this.routes.get('dashboard')], {
        queryParams: {refresh: new Date().getTime()}
      });
      this.notifyFollowers();
    }, (error) => {
      this.toastService.showError('Erreur', 'Erreur lors de la mise à jour de la publication');
      this.loading = false;
    }, () => {});
  }

  /**
   * update post that does not contains media
   */
  updatePost(): void{
    this.loading = true;
    if (this.isUrlSelected){
      this.multimedia.url = this.urlMedia ;
      this.multimedias.push(this.multimedia);
      this.post.multimedia = this.multimedias;
      this.post.url = this.urlMedia;
    }
    this.postService.updatePost(this.post).subscribe((response) => {
      this.toastService.showSuccess('Success', 'Publication mis à jour avec succés');
      this.followers = response.resources;
      this.notifyFollowers();
      this.loading = false;
      this.router.navigate([this.routes.get('dashboard')], {
        queryParams: {refresh: new Date().getTime()}
      });
    }, (error) => {
      this.toastService.showError('Erreur', 'Erreur lors de la mis à jour de la publication');
      this.loading = false;
    }, () => {});
  }

  /**
   * notify all followers
   */
  notifyFollowers(): void{
    if (this.followers && this.followers.length > 0){
      this.followers.forEach(follower => {
        const socialInteraction: SocialInteraction = {} as SocialInteraction;
        socialInteraction.element = this.post.idServer;
        socialInteraction.emitterAccount = this.loggedAccount;
        socialInteraction.receiverAccount = follower;
        socialInteraction.content = this.post.title;
        socialInteraction.type = variables.post;

        this.webSocketService.sendPushNotification(socialInteraction);
      });
    }
  }

  /**
   * validate a post
   */
  validatePost(): boolean{
    this.initErrors();

    if (this.description) {
      this.post.description = this.post.idServer ? this.description : this.description;
    }

    if (this.post.title === undefined){
      this.errors.valid = false;
      this.errors.title = 'Le titre est requis';
    }

    if (this.post.mediaSubtype === undefined){
      this.errors.valid = false;
      this.errors.mediaSubtype = 'Le type de media est requis';
    }

    if ((this.file === null || this.file === undefined) && this.displayConfig === 'NEW_FILE'){
      this.errors.valid = false;
      this.errors.file = 'Veuillez télécharger un fichier';
    }

    if (!this.urlMedia && this.displayConfig === 'NEW_LINK' ){
      this.errors.valid = false;
      this.errors.url = 'Le lien du média est requis';
    }

    if (!StringHelper.validateUrl(this.urlMedia) && this.displayConfig === 'NEW_LINK'){
      this.errors.valid = false;
      this.errors.url = 'Url invalide';
    }

    if (this.file && !this.validateFileSize() && !this.isUrlSelected){
      this.errors.valid = false;
      this.errors.file = 'La taille maximale est ' + variables.max_file_size + 'MB';
    }

    if (this.file && !this.validateFileMimetype() && !this.isUrlSelected){
      this.errors.valid = false;
      this.errors.file = 'Ce type de document n\'est pas accepté';
    }

    if (this.description && this.post.description && this.post.description.length > variables.max_description_size) {
      this.errors.valid = false;
      this.errors.description = 'Le nombre maximal de caractères est ' + variables.max_description_size;
    }

    if (this.post.keywords && this.post.keywords.length > variables.max_title_size){
      this.errors.valid = false;
      this.errors.keywords = 'Le nombre maximal de caractères est ' + variables.max_title_size;
    }

    if (this.isMediaSelected){
      this.url = '';
    }

    if (this.isUrlSelected){
      this.file = null;
    }

    this.submitted = true;
    return this.errors.valid;
  }

  initErrors(): void{
    this.submitted = false;
    this.errors = {valid: true, url: '', title: '', keywords: '', file: '', description: '', mediaSubtype: '', discipline: ''};
    this.alert = {} as Alert;
  }

  keyupDescription(): void {
    this.errors.description = '';
  }

  keyupKeywords(): void {
    this.errors.keywords = '';
  }

  keyupDiscipline(): void {
    this.errors.discipline = '';
  }

  keyupTitle(): void {
    this.errors.title = '';
  }

  keyupUrlMedia(): void {
    this.displayConfig = 'NEW_LINK';
    this.previewLoading = true;
    this.errors.urlMedia = '';
    this.blockFileDisabled = true;
    const encodedUrl = btoa(this.urlMedia).replace('+', '-').replace('/', '_');
    if (this.validateUrlMedia(this.urlMedia)){
      this.metadataService.getMetadataUrl(encodedUrl).subscribe((response) => {
        this.metadata = response.resource;
        this.post.title = this.metadata.title;
        this.resetSelection();
        this.isUrlSelected = true;
        this.previewLoading = false;
      }, (error) => {
        this.previewLoading = false;
        this.blockFileDisabled = false;
      });
    }else {
      this.previewLoading = false;
      this.blockFileDisabled = false;
    }
  }

  validateUrlMedia(url: string): boolean{
    this.isUrlSelected = StringHelper.validateUrl(url);
    return this.isUrlSelected;
  }

  validateUrl(url: string): boolean{
    return StringHelper.validateUrl(url);
  }

  validateFileSize(): boolean{
    if (this.file && this.file.size > StringHelper.getMaxFileSize()){
      this.errors.valid = false;
      this.errors.file = 'La taille maximale est ' + variables.max_file_size + 'MB';
      return false;
    }
    return true;
  }

  validateFileMimetype(): boolean{
    if (!MimetypeHelper.isMimetypeAllowed(this.file.type)){
      this.errors.valid = false;
      this.errors.file = 'Ce type de document n\'est pas accepté';
      return false;
    }
    return true;
  }

  onChangeMediaSubtype(e): void {
    this.errors.mediaSubtype = '';
    this.mediaSubtypes.forEach(elt => {
      if (elt.idServer === e.target.value.toLowerCase()){
        this.post.mediaSubtype = elt;
      }
    });
  }

  findMediaSubtypes(): void{
    this.loading = true;
    this.multimediaService.findMediaSubtypes().subscribe((response) => {
      this.mediaSubtypes = response.resources;
      this.filterMediaSubtypes(this.getMediaTypeByPosition(this.infosMediaType.position));
      if (history.state.post && history.state.post.multimedia && history.state.post.multimedia[0] && history.state.post.multimedia.length > 0){
        this.filteredMediaSubtypes = [];
        this.filterMediaSubtypes(this.post.multimedia[0].type);
      }
      this.loading = false;
    }, (error) => {
    }, () => {
      this.loading = false;
    });
  }

  filterMediaSubtypes(subtype: string): void{
    if (this.mediaSubtypes){
      this.mediaSubtypes.forEach(elt => {
        if (elt.mediaType.label.toLowerCase() === subtype.toLowerCase()){
          this.filteredMediaSubtypes.push(elt);
        }
      });
    }
  }

  getMediaTypeByPosition(position: number): string{
    let type = this.mediaTypes[0].type;
    this.mediaTypes.forEach(elt => {
      if (elt.position === position) {
        type = elt.type;
      }
    });
    return type;
  }

  /**
   * preview media
   */
  preview(): void {
    if (this.file) {
      const reader = new FileReader();
      reader.readAsDataURL(this.file);
      if (this.file.type.indexOf('image') > -1){
        this.format = 'image';
      } else if (this.file.type.indexOf('video') > -1){
        this.format = 'video';
      }
      reader.onload = (event) => {
        this.url = reader.result;
      };
    }
  }

  resetSelection(): void{
    this.isMediaSelected = false;
    this.isUrlSelected = false;
  }

  testDescriptionSize(): void{
    if (this.description.length > this.maxlength) {
      this.description = this.description.substring(0, this.maxlength);
    }
  }

  computeTitleByMedia(): string{
    let title = 'Présentation';
    switch (this.infosMediaType.position) {
      case 1:
        title = title + ' du document';
        break;
      case 2:
        title = title + ' du lien';
        break;
      case 3:
        title = title + ' de la vidéo';
        break;
      case 4:
        title = title + ' de l\'image';
        break;
      case 5:
        title = title + ' de l\'audio';
        break;
      default:
        break;
    }
    return title;
  }

  onClickChangeMediaType(m: InfoMediaType): void {
    this.filteredMediaSubtypes = [];
    this.filterMediaSubtypes(m.type);
    this.infosMediaType = m;
    if (!this.infosMediaType){
      this.infosMediaType = MediaHelper.mediaTypes[0];
    }
    this.post.mediaSubtype =  this.filteredMediaSubtypes[0]; // default
  }

// make sure to destory the editor
  ngOnDestroy(): void {
    this.editor.destroy();
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }
}

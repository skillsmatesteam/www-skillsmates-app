import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {variables} from '../../../../environments/variables';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {PostService} from '../../../services/pages/post/post.service';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {Account} from '../../../models/account/account';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {PostSocialInteractions} from '../../../models/post/postSocialInteractions';
import {MobileService} from '../../../services/mobile/mobile.service';

@Component({
  selector: 'app-post-content',
  templateUrl: './post-content.component.html',
  styleUrls: ['./post-content.component.css']
})
export class PostContentComponent implements OnInit, OnDestroy {

  account: Account = {} as Account;
  id: string;
  postSocialInteractions: PostSocialInteractions = {} as PostSocialInteractions;
  loading: boolean;
  routes = RoutesHelper.routes;
  public screenWidth: number = window.innerWidth;

  constructor(private postService: PostService,
              private router: Router,
              private mobileService: MobileService,
              private activatedRoute: ActivatedRoute,
              private authenticateService: AuthenticateService) { }

  ngOnInit(): void {
    this.mobileService.unsetNotificationDesign();
    this.loading = true;
    this.account = this.authenticateService.getCurrentAccount();
    this.id = this.activatedRoute.snapshot.paramMap.get(variables.id);
    if (!this.id){
      this.router.navigate([this.routes.get('profile.my')]);
    }
    this.activatedRoute.queryParamMap.subscribe((paramMap: ParamMap) => {
      const refresh = paramMap.get('refresh');
      if (refresh) {
        this.findSinglePost();
      }
    });
    this.findSinglePost();
  }

  ngOnDestroy(): void {
    this.mobileService.unsetNotificationDesign();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    this.screenWidth = window.innerWidth;
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }

  findSinglePost(): void{
    this.postService.findSinglePost(this.id).subscribe((response) => {
      this.postSocialInteractions = response.resource;
      this.loading = false;
    }, error => {
      this.loading = false;
      this.router.navigate([this.routes.get('profile.my')]);
    });
  }

  onRefreshEvent($event: string): void {

  }
}

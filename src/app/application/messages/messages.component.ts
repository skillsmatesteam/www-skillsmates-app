import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {AuthenticateService} from '../../services/accounts/authenticate/authenticate.service';
import {SubscriptionService} from '../../services/accounts/subscription/subscription.service';
import {AccountNetwork} from '../../models/network/accountNetwork';
import {Account} from '../../models/account/account';
import {MessageService} from '../../services/message/message.service';
import {ToastService} from '../../services/toast/toast.service';
import {Message} from '../../models/message/message';
import {AccountMessage} from '../../models/message/accountMessage';
import {StringHelper} from '../../helpers/string-helper';
import {WebSocketService} from '../../services/message/web-socket.service';
import {AccountHelper} from '../../helpers/account-helper';
import {variables} from '../../../environments/variables';
import {SocialInteraction} from '../../models/post/socialInteraction';
import {NotificationHelper} from '../../helpers/notification-helper';
import {NotificationService} from '../../services/notification/notification.service';
import {MobileService} from '../../services/mobile/mobile.service';
import {UtilityService} from "../../services/utility/utility.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit{
  loading: boolean;
  loadingMessage: boolean;
  accountNetwork: AccountNetwork = {} as AccountNetwork;
  accounts: Account[] = [];
  selectedAccount: Account;
  loggedAccount: Account;
  emitterAccount: Account = {} as Account;
  idServerEmitterAccount: string;
  messages: Message[] = [];
  currentMessage: Message;
  account: Account = {} as Account;
  accountsMessages: AccountMessage[] = [];
  allAccountsMessages: AccountMessage[] = [];
  searchContent: string;
  displayMessageContent = false;
  subscription : Subscription;

  public screenWidth: number = window.innerWidth;

  pageYoffset = 0;
  @HostListener('window:scroll', ['$event']) onScroll(event): void {
    this.pageYoffset = window.pageYOffset;
  }

  constructor(private messageService: MessageService,
              private toastService: ToastService,
              private mobileService: MobileService,
              private authenticateService: AuthenticateService,
              private subscriptionService: SubscriptionService,
              private notificationService: NotificationService,
              private webSocketService: WebSocketService,
              private utilityService: UtilityService) { }

  ngOnInit(): void {
    this.loggedAccount = this.authenticateService.getCurrentAccount();
    this.findAccountsMessages();
    this.getEmitterAccount();

    this.webSocketService.pushNotificationWebSocket.subscribe((current) => {
      if (current){
        const socialInteraction: SocialInteraction = JSON.parse(current);
        if (socialInteraction && socialInteraction.type === variables.message){
          this.currentMessage = NotificationHelper.convertSocialInteractionToMessage(socialInteraction);
          this.updateAccountMessage(this.currentMessage);
        }
      }
    });


  }

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    this.screenWidth = window.innerWidth;
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }

  /**
   * update the list of contact on message received
   */
  updateAccountMessage(message: Message): void{
    if (message){
      let accountMessageIndex = -1;
      this.accountsMessages.forEach((elt, index) => {
        if (elt.account.idServer === message.emitterAccount.idServer){
          accountMessageIndex = index;
        }
      });
      if (accountMessageIndex > -1){
        this.accountsMessages[accountMessageIndex].messages.push(message);
      }
    }
    this.sortAccountsMessages();
  }

  /**
   * updates messages of an account
   * @param messages: list des messages
   */
  updateAccountMessages(messages: Message[]): void{
    if (messages && messages.length > 0){
      let accountMessageIndex = -1;
      this.accountsMessages.forEach((elt, index) => {
        if (elt.account.idServer === this.selectedAccount.idServer){
          accountMessageIndex = index;
        }
      });
      if (accountMessageIndex > -1){
        this.accountsMessages[accountMessageIndex].messages = messages;
      }
    }
    this.sortAccountsMessages();
  }

  /**
   * get messages when tab selected
   * @param selectedAccount: account selected
   */
  onClickTab(selectedAccount: Account): void {
    this.selectedAccount = selectedAccount;
    if (this.isMobile()){
      this.utilityService.sendSelectedAccount(selectedAccount);
    }
    this.findMessages(this.selectedAccount.idServer);
  }

  findMessages(id: string): void{
    this.loadingMessage = true;
    this.messageService.findMessagesByAccount(id).subscribe(data => {
      this.loadingMessage = false;
      this.messages = data.resources;
      if (this.isMobile()){
        this.displayMessageContent = true;
      }
      this.notificationService.changeNotificationMessage(NotificationHelper.convertMessagesToNotifications(this.messages));
      this.updateAccountMessages(this.messages);
    }, (error) => {
      this.loadingMessage = false;
    });
  }

  compare(a: AccountMessage, b: AccountMessage): number{
    if (a && b && a.messages && a.messages.length > 0 && b.messages && b.messages.length > 0){
      return a.messages[a.messages.length - 1].createdAt < b.messages[b.messages.length - 1].createdAt ? 1 : -1;
    }
    return 0;
  }

  findAccountsMessages(): void{
    this.loading = true;
    this.messageService.findAccountsMessages().subscribe((response) => {
      this.allAccountsMessages = response.resources;
      this.accountsMessages = this.allAccountsMessages;
      this.sortAccountsMessages();
      this.loading = false;
    }, (error) => {
      this.loading = false;
    }, () => {});
  }

  public name(account: Account): string{
    return StringHelper.truncateName(account, 20);
  }

  /**
   * get the last message received or sent
   * @param accountMessage: account with list of messages
   */
  getLastMessageContent(accountMessage: AccountMessage): string{
    const lastMessage: Message = this.getLastMessage(accountMessage);
    return lastMessage ? StringHelper.truncateText(lastMessage.content, 25) : '';
  }

  getLastMessage(accountMessage: AccountMessage): Message {
    return accountMessage && accountMessage.messages && accountMessage.messages.length > 0 ?
      accountMessage.messages[accountMessage.messages.length - 1] : null;
  }

  /**
   * count unread messages of an account
   * @param accountMessage: account with messages
   */
  countUnreadMessages(accountMessage: AccountMessage): number{
    let nb = 0;
    if (accountMessage && accountMessage.messages && accountMessage.messages.length > 0){
      accountMessage.messages.forEach((message) => {
        nb = !message.active && message.receiverAccount.idServer === this.loggedAccount.idServer ? nb + 1 : nb;
      });
    }
    return nb;
  }

  sortAccountsMessages(): void{
    const accountsWithoutMessage: AccountMessage[] = [];
    const accountsWithMessage: AccountMessage[] = [];

    this.accountsMessages.forEach(elt => {
      if (elt.messages && elt.messages.length > 0){
        elt.unread = this.countUnreadMessages(elt);
        accountsWithMessage.push(elt);
      }else {
        accountsWithoutMessage.push(elt);
      }
    });
    this.accountsMessages = accountsWithMessage.sort(this.compare).concat(accountsWithoutMessage);
  }

  /**
   * refresh list of accountMessage when message is sent
   * @param message: message
   */
  refreshAccountsEvent(message: Message): void{
    const accountMessage: AccountMessage = this.findReceiverMessage(message.receiverAccount);
    accountMessage.messages.push(message);
    this.accountsMessages.unshift(accountMessage);
  }

  /**
   * find the receiver within list of accountMessages
   * @param account: account
   */
  findReceiverMessage( account: Account): AccountMessage{
    let accountMessage: AccountMessage = {} as AccountMessage;
    this.accountsMessages.forEach((elt, index) => {
      if (elt.account.idServer === account.idServer){
        accountMessage = elt;
        this.accountsMessages.splice(index, 1);
      }
    });
    return accountMessage;
  }

  /**
   * filter accounts by searchContent
   */
  onKeyupSearch(): void {
    this.accountsMessages = AccountHelper.filterAccountsMessagesByName(this.allAccountsMessages, this.searchContent);
    this.sortAccountsMessages();
  }

  getEmitterAccount(): void{
    this.emitterAccount = JSON.parse(localStorage.getItem(variables.account_emitter));
    if (!this.emitterAccount){

    } else {
      this.onClickTab(this.emitterAccount);
      localStorage.removeItem(variables.account_emitter);
    }
  }

  refreshDisplayMessageContentEvent(isDisplay: boolean): void{
    this.displayMessageContent = isDisplay;
  }
}

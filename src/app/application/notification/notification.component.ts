import {Component, HostListener, Input, OnInit} from '@angular/core';
import {Notification} from '../../models/notification/notification';
import {NotificationService} from '../../services/notification/notification.service';
import {Notifications} from '../../models/notification/notifications';
import {NotificationHelper} from '../../helpers/notification-helper';
import {MobileService} from '../../services/mobile/mobile.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

  notifications: Notifications = {} as Notifications;
  allNotifications: Notification[] = [];
  public screenWidth: number = window.innerWidth;

  constructor(private notificationService: NotificationService, private mobileService: MobileService) { }

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    this.screenWidth = window.innerWidth;
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }

  ngOnInit(): void {
    this.mobileService.unsetSkillschatDesign();
    this.findNotifications();
  }

  findNotifications(): void{
    this.notificationService.findNotifications().subscribe((response) => {
      this.notifications = response.resource;
      this.allNotifications = NotificationHelper.sortAllNotifications(this.notifications);
    }, (error) => {
    }, () => {
    });
  }
}

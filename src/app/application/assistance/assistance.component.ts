import { Component, HostListener, OnInit } from '@angular/core';
import { Account } from '../../models/account/account';
import { AuthenticateService } from '../../services/accounts/authenticate/authenticate.service';
import { Router } from '@angular/router';
import { RoutesHelper } from '../../helpers/routes-helper';
import { MobileService } from 'src/app/services/mobile/mobile.service';
import {variables} from '../../../environments/variables';

@Component({
  selector: 'app-assistance',
  templateUrl: './assistance.component.html',
  styleUrls: ['./assistance.component.css'],
})
export class AssistanceComponent implements OnInit {
  selectedBlock: string;
  displayAssistance = true;
  loggedAccount: Account = {} as Account;
  buttonReturnTitle = 'Retour à l\'accueil';
  loading = false;
  routes = RoutesHelper.routes;
  public screenWidth: number = window.innerWidth;

  constructor(
    private authenticateService: AuthenticateService,
    private router: Router,
    private mobileService: MobileService
  ) {}

  ngOnInit(): void {
    this.loggedAccount = this.authenticateService.getCurrentAccount();
    const tofaq = localStorage.getItem(variables.faq);
    if (tofaq){
      this.selectedBlock = 'faq';
      this.displayAssistance = false;
      localStorage.removeItem(variables.faq);
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    this.screenWidth = window.innerWidth;
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }

  getSelectedBlock(block: string): void {
    this.selectedBlock = block;
    this.displayAssistance = false;
    this.buttonReturnTitle = 'Retour';
  }

  onClickAssistance(): void {
    this.buttonReturnTitle = 'Retour à l\'accueil';
    this.selectedBlock = '';
    this.displayAssistance = true;
  }

  onclickReturnButton(value: string): void {
    if (this.displayAssistance) {
      this.router.navigate([this.routes.get('dashboard')]);
    } else {
      this.onClickAssistance();
    }
  }
}

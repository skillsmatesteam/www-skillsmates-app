import {Component, HostListener, OnInit} from '@angular/core';
import {PostService} from '../../services/pages/post/post.service';
import {AuthenticateService} from '../../services/accounts/authenticate/authenticate.service';
import {Account} from '../../models/account/account';
import {MultimediaService} from '../../services/media/multimedia/multimedia.service';
import {MediaSubtype} from '../../models/multimedia/mediaSubtype';
import {variables} from '../../../environments/variables';
import {MediaHelper} from '../../helpers/media-helper';
import {InfoMediaType} from '../../models/multimedia/infoMediaType';
import {ActivatedRoute} from '@angular/router';
import {RoutesHelper} from '../../helpers/routes-helper';
import {AccountService} from '../../services/accounts/account/account.service';
import {PostSocialInteractions} from '../../models/post/postSocialInteractions';
import {MobileService} from '../../services/mobile/mobile.service';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.css']
})
export class DocumentsComponent implements OnInit {

  loading: boolean;
  postsSocialInteractions: PostSocialInteractions[] = [];
  postsSocialInteractionsMediaType: PostSocialInteractions[] = [];
  postsSocialInteractionsMediaSubtype: PostSocialInteractions[] = [];
  account: Account = {} as Account;
  mediaSubtypes: MediaSubtype[];
  filteredMediaSubtypes: MediaSubtype[] = [];
  currentMediaType: string;
  currentMediaSubtype: MediaSubtype;
  selectedMediaType: InfoMediaType;
  mediaTypes = MediaHelper.mediaTypes;
  comments: Comment[] = [];
  loggedAccount: Account = {} as Account;
  id: string;
  routes = RoutesHelper.routes;
  dismissPreview = false;
  myPostSocialInteractions: PostSocialInteractions;
  infosMediaTypes: InfoMediaType[] = MediaHelper.mediaTypes;
  documents: string = variables.documents;
  public screenWidth: number = window.innerWidth;

  constructor(private postService: PostService,
              private multimediaService: MultimediaService,
              private activatedRoute: ActivatedRoute,
              private accountService: AccountService,
              private mobileService: MobileService,
              private authenticateService: AuthenticateService) { }

  ngOnInit(): void {
    this.loading = true;
    this.mobileService.unsetSkillschatDesign();
    this.loggedAccount = this.authenticateService.getCurrentAccount();
    this.id = this.activatedRoute.snapshot.paramMap.get(variables.id);
    if (!this.id){
      this.id = this.loggedAccount.idServer;
    }

    const tab = localStorage.getItem(variables.tab);
    if (tab){
      this.selectedMediaType = MediaHelper.getMediaTypeByPosition(tab);
    }else {
      this.selectedMediaType = MediaHelper.mediaTypes[0];
    }

    this.currentMediaType = this.selectedMediaType.type;
    this.currentMediaSubtype = {} as MediaSubtype;
    localStorage.removeItem(variables.tab);
    this.findAccount();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    this.screenWidth = window.innerWidth;
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }

  findAccount(): void{
    this.loading = true;
    this.accountService.findAccountById(this.id).subscribe((response) => {
      this.account = response.resource;
      this.findPosts();
    }, (error) => {
      this.loading = false;
    }, () => {
      this.loading = false;
    });
  }

  findPosts(): void{
    this.postService.findDocumentsByAccount(this.account.idServer, this.postsSocialInteractions.length).subscribe((response) => {
      let retrievedPostsSocialInteractions: PostSocialInteractions[] = [];
      retrievedPostsSocialInteractions = response.resources;
      if (retrievedPostsSocialInteractions && retrievedPostsSocialInteractions.length > 0){
        retrievedPostsSocialInteractions.forEach(elt => {
          if ((elt.post.multimedia && elt.post.multimedia.length > 0)){
            this.postsSocialInteractions.push(elt);
          }
        });
      }
      this.findMediaSubtypes();
    }, (error) => {
      this.loading = false;
    }, () => {});
  }

  /**
   * get all media sub type from server
   */
  findMediaSubtypes(): void{
    this.loading = true;
    this.multimediaService.findMediaSubtypes().subscribe((response) => {
      this.mediaSubtypes = response.resources;
      this.filterMediaSubtypes();
      this.loading = false;
    }, (error) => {
      console.log(error);
    }, () => {
      this.loading = false;
    });
  }

  /**
   * filter correct media sub types
   */
  filterMediaSubtypes(): void{
    this.filteredMediaSubtypes = [];
    if (this.mediaSubtypes){
      this.mediaSubtypes.forEach(elt => {
        if (elt.mediaType.label.toLowerCase() === this.currentMediaType.toLowerCase()){
          this.filteredMediaSubtypes.push(elt);
        }
      });
    }
    if (!this.currentMediaSubtype.id){
      this.currentMediaSubtype = this.filteredMediaSubtypes[0];
    }
    this.filterPostsByMediaType();
  }

  filterPostsByMediaType(): void{
    this.postsSocialInteractionsMediaType = [];
    this.postsSocialInteractions.forEach(elt => {
      if (elt.post.mediaSubtype && elt.post.mediaSubtype.mediaType.label.toLowerCase() === this.currentMediaType.toLowerCase()){
        this.postsSocialInteractionsMediaType.push(elt);
      }
    });
    this.filterPostsByMediaSubtype();
  }

  getNbPostsByMediaType(mediaType: InfoMediaType): number{
    let nb = 0;
    this.postsSocialInteractions.forEach(elt => {
      if (elt.post.mediaSubtype && elt.post.mediaSubtype.mediaType.label.toLowerCase() === mediaType.type.toLowerCase()){
        nb++;
      }
    });
    return nb;
  }

  filterPostsByMediaSubtype(): void{
    this.postsSocialInteractionsMediaSubtype = [];
    this.postsSocialInteractionsMediaType.forEach(elt => {
      if (elt.post.mediaSubtype.id === this.currentMediaSubtype.id ){
        this.postsSocialInteractionsMediaSubtype.push(elt);
      }
    });
  }

  getNbPostsByMediaSubtype(mediaSubtype: MediaSubtype): number{
    let nb = 0;
    this.postsSocialInteractionsMediaType.forEach(elt => {
      if (elt.post.mediaSubtype.id === mediaSubtype.id ){
        nb++;
      }
    });
    return nb;
  }

  getNbPostsByMediaSubtypeString(mediaSubtypeString): number{
    const mediaSubtype = this.getMediaSubtype(this.selectedMediaType, mediaSubtypeString.target.value.toLowerCase());
    let nb = 0;
    if (mediaSubtype){
      this.postsSocialInteractionsMediaType.forEach(elt => {
        if (elt.post.mediaSubtype.id === mediaSubtype.id ){
          nb++;
        }
      });
    }
    return nb;
  }

  onClickTab(mediaType: any): void {
    this.selectedMediaType = mediaType;
    this.currentMediaType = mediaType.type;
    this.currentMediaSubtype = {} as MediaSubtype;
    this.filterMediaSubtypes();
    this.dismissPreview = false;
  }

  onClickSubtype(mediaSubtype: MediaSubtype): void {
    this.currentMediaSubtype = mediaSubtype;
    this.filterPostsByMediaSubtype();
    this.dismissPreview = false;
  }

  selectPostSocialInteractions(postSocialInteractions: PostSocialInteractions): void {
    this.dismissPreview = true;
    this.myPostSocialInteractions = postSocialInteractions;
  }

  onClickReturn(): void {
    this.dismissPreview = false;
  }

  selectMediaType(mediaType: InfoMediaType): void{
    this.onClickTab(mediaType);
  }

  onChangeMediaSubType(e): void {
    const subtype = e.target.value.split(/\((.+)/)[0];
    this.onClickSubtype(this.getMediaSubtype(this.selectedMediaType, subtype.trim()));
  }

  getMediaSubtype(mediaType: InfoMediaType, subtypeString: string): MediaSubtype{
    let mediaSubtype: MediaSubtype = {} as MediaSubtype;
    this.mediaSubtypes.forEach(elt => {
      if (elt.mediaType.label.toLowerCase() === mediaType.type.toLowerCase() && elt.label.toLowerCase() === subtypeString.toLowerCase()){
        mediaSubtype = elt;
      }
    });
    return mediaSubtype;
  }
}

import { Component, HostListener, OnInit } from '@angular/core';
import { AuthenticateService } from '../../../services/accounts/authenticate/authenticate.service';
import { Account } from '../../../models/account/account';
import { AccountService } from '../../../services/accounts/account/account.service';
import { ToastService } from '../../../services/toast/toast.service';
import { ActivatedRoute, Router } from '@angular/router';
import { variables } from '../../../../environments/variables';
import { InfoNetworkType } from '../../../models/network/infoNetworkType';
import { NetworkHelper } from '../../../helpers/network-helper';
import { SettingHelper } from '../../../helpers/setting-helper';
import { InfoSettingType } from '../../../models/account/infoSettingType';
import { MobileService } from 'src/app/services/mobile/mobile.service';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.css'],
})
export class SettingComponent implements OnInit {
  account: Account = {} as Account;
  loggedAccount: Account = {} as Account;
  loading: boolean;
  settingTypes: InfoSettingType[] = SettingHelper.settingTypes;
  selectedSettingType: InfoSettingType;
  currentSettingType: string;
  displaySettingContent = false;
  public screenWidth: number = window.innerWidth;

  constructor(
    /*private accountService: AccountService,
              private toastService: ToastService,
              private router: Router,*/
    private mobileService: MobileService,
    private authenticateService: AuthenticateService /*,
              private activatedRoute: ActivatedRoute*/
  ) {}

  ngOnInit(): void {
    //this.loading = true;
    //this.loggedAccount = this.authenticateService.getCurrentAccount();
    /*this.id = this.activatedRoute.snapshot.paramMap.get(variables.id);
    if (!this.id){
      this.id = this.loggedAccount.idServer;
    }
    this.findAccount();*/
    this.loadSetting();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    this.screenWidth = window.innerWidth;
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }

  private loadSetting(): void {
    // this.loading = true;
    const tab = localStorage.getItem(variables.tab);
    if (tab) {
      this.selectedSettingType = SettingHelper.getSettingTypeByPosition(tab);
    } else {
      this.selectedSettingType = SettingHelper.settingTypes[0];
    }

    this.currentSettingType = this.selectedSettingType.type;
    this.account = this.authenticateService.getCurrentAccount();
    localStorage.removeItem(variables.tab);
    // this.findNetwork();
  }

  onClickTab(settingType: any): void {
    this.selectedSettingType = settingType;
    this.currentSettingType = settingType.type;
    this.displaySettingContent = true;
    // this.filterSetting(this.selectedSettingType);
  }
  /*findAccount(): void{
    this.loading = true;
    this.accountService.findAccountById(this.id).subscribe((response) => {
      this.account = response.resource;
      this.loading = false;
    }, (error) => {
      this.toastService.showError('Error', 'Compte non identifié');
      this.loading = false;
    }, () => {
      this.loading = false;
    });
  }*/
  onClickBack(): void {
    this.displaySettingContent = false;
  }
}

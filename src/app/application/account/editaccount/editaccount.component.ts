import {Component, HostListener, OnInit} from '@angular/core';
import {Account} from '../../../models/account/account';
import {variables} from '../../../../environments/variables';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AccountService} from '../../../services/accounts/account/account.service';
import {ToastService} from '../../../services/toast/toast.service';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {AccountProfileInfos} from '../../../models/account/accountProfileInfos';
import {MobileService} from '../../../services/mobile/mobile.service';

@Component({
  selector: 'app-editaccount',
  templateUrl: './editaccount.component.html',
  styleUrls: ['./editaccount.component.css']
})
export class EditaccountComponent implements OnInit {
  account: Account = {} as Account;
  loading: boolean;
  isInfos: boolean;
  isAcademic: boolean;
  isProfessional: boolean;
  isSkills: boolean;
  isBiography: boolean;
  loggedAccount: Account = {} as Account;
  id: string;
  routes = RoutesHelper.routes;
  accountProfileInfos: AccountProfileInfos = {} as AccountProfileInfos;
  buttonReturnTitle = 'Retour au profil';
  about: string = variables.about;
  public screenWidth: number = window.innerWidth;
  profileTabs: string[] = [];
  selectedTab: string;

  constructor(private accountService: AccountService,
              private toastService: ToastService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private mobileService: MobileService,
              private authenticateService: AuthenticateService) { }

  ngOnInit(): void {
    this.loggedAccount = this.authenticateService.getCurrentAccount();
    this.id = this.activatedRoute.snapshot.paramMap.get(variables.id);
    if (!this.id){
      this.id = this.loggedAccount.idServer;
    }
    this.findAccount();
    const tab = localStorage.getItem(variables.tab);
    if (tab != null){
      this.onClickTab(tab);
    }else {
      this.onClickTab(variables.infos);
    }
    localStorage.removeItem(variables.tab);
    this.countProfileInfos();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    this.screenWidth = window.innerWidth;
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }

  onClickTab(tab: string): void{
    this.selectedTab = tab;
    this.resetTabs();
    localStorage.setItem(variables.tab, tab);
    switch (tab) {
      case variables.infos:
        this.isInfos = true;
        break;
      case variables.academics:
        this.isAcademic = true;
        break;
      case variables.professional:
        this.isProfessional = true;
        break;
      case variables.skills:
        this.isSkills = true;
        break;
      case variables.biography:
        this.isBiography = true;
        break;
      default:
        this.isInfos = true;
        break;
    }
  }

  resetTabs(): void{
    this.isInfos = false;
    this.isAcademic = false;
    this.isProfessional = false;
    this.isSkills = false;
    this.isBiography = false;
  }

  findAccount(): void{
    this.loading = true;
    this.accountService.findAccountById(this.id).subscribe((response) => {
      this.account = response.resource;
      this.loading = false;
    }, (error) => {
      this.loading = false;
    }, () => {
      this.loading = false;
    });
  }

  countProfileInfos(): void{
    this.accountService.countProfileInfos(this.id).subscribe((response) => {
      this.accountProfileInfos = response.resource;
    }, (error) => {});
  }

  clickReturnButton(value: string): void{
    if (this.loggedAccount.id === this.account.id){
      this.router.navigate([this.routes.get('profile.my')]);
    }else {
      this.router.navigate([this.routes.get('profile'), this.account.idServer]);
    }
  }
}

import { Component, HostListener, OnInit } from '@angular/core';
import { MobileService } from 'src/app/services/mobile/mobile.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css'],
})
export class AccountComponent implements OnInit {
  public screenWidth: number = window.innerWidth;

  constructor(private mobileService: MobileService) {}

  ngOnInit(): void {}

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    this.screenWidth = window.innerWidth;
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }
}

import { Component, HostListener, OnInit } from '@angular/core';
import { Account } from '../../../models/account/account';
import { variables } from '../../../../environments/variables';
import { AccountService } from '../../../services/accounts/account/account.service';
import { ToastService } from '../../../services/toast/toast.service';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { AuthenticateService } from '../../../services/accounts/authenticate/authenticate.service';
import { ViewportScroller } from '@angular/common';
import { RoutesHelper } from '../../../helpers/routes-helper';
import { MobileService } from 'src/app/services/mobile/mobile.service';
import {InfoMediaType} from '../../../models/multimedia/infoMediaType';
import {MediaHelper} from '../../../helpers/media-helper';
import {AccountSocialInteractions} from '../../../models/account/accountSocialInteractions';

@Component({
  selector: 'app-profileaccount',
  templateUrl: './profileaccount.component.html',
  styleUrls: ['./profileaccount.component.css'],
})
export class ProfileaccountComponent implements OnInit {
  account: Account = {} as Account;
  loggedAccount: Account = {} as Account;
  loading: boolean;
  id: string;
  pageYoffset = 0;
  infosMediaTypes: InfoMediaType[] = MediaHelper.mediaTypes;
  public screenWidth: number = window.innerWidth;
  routes = RoutesHelper.routes;
  posts: string = variables.posts;
  documents: string = variables.documents;
  about: string = variables.about;
  tab: string = this.posts;
  accountSocialInteractions: AccountSocialInteractions = {} as AccountSocialInteractions;
  @HostListener('window:scroll', ['$event'])
  onScroll(event): void {
    this.pageYoffset = window.pageYOffset;
  }

  constructor(
    private accountService: AccountService,
    private toastService: ToastService,
    private router: Router,
    private scroll: ViewportScroller,
    private authenticateService: AuthenticateService,
    private activatedRoute: ActivatedRoute,
    private mobileService: MobileService
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit(): void {
    this.loading = true;
    this.loggedAccount = this.authenticateService.getCurrentAccount();
    this.id = this.activatedRoute.snapshot.paramMap.get(variables.id);
    if (!this.id && this.loggedAccount) {
      this.id = this.loggedAccount.idServer;
      this.findAccountSocialInteractions();
    }

    if (this.id) {
      this.activatedRoute.queryParamMap.subscribe((paramMap: ParamMap) => {
        const refresh = paramMap.get('refresh');
        if (refresh) {
          this.findAccount();
        }
      });
      this.findAccount();
      this.scrollToTop();
    } else {
      this.router.navigate([this.routes.get('root')]);
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    this.screenWidth = window.innerWidth;
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }

  scrollToTop(): void {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth',
    });
  }

  findAccount(): void {
    this.loading = true;
    this.accountService.findAccountById(this.id).subscribe((response) => {
        this.account = response.resource;
        this.loading = false;
      },
      (error) => {
        this.toastService.showError('Error', 'Compte non identifié');
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }

  onClickTab(tab: string): void {
    this.tab = tab;
  }

  findAccountSocialInteractions(): void{
      this.accountService.findAccountSocialInteractions(this.loggedAccount.idServer).subscribe((response) => {
        this.accountSocialInteractions = response.resource;
        this.findAccount();
        this.loading = false;
      }, (error => {}));
    }
}

import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { AccountService } from '../../services/accounts/account/account.service';
import { ToastService } from '../../services/toast/toast.service';
import { Account } from '../../models/account/account';
import { CookieService } from 'ngx-cookie-service';
import { InfoMediaType } from '../../models/multimedia/infoMediaType';
import { MultimediaService } from '../../services/media/multimedia/multimedia.service';
import { MediaHelper } from '../../helpers/media-helper';
import { AuthenticateService } from '../../services/accounts/authenticate/authenticate.service';
import { InfoNetworkType } from '../../models/network/infoNetworkType';
import { NetworkHelper } from '../../helpers/network-helper';
import { RoutesHelper } from '../../helpers/routes-helper';
import { SubscriptionService } from '../../services/accounts/subscription/subscription.service';
import { AccountNetwork } from '../../models/network/accountNetwork';
import { MediaCount } from '../../models/multimedia/mediaCount';
import { ViewportScroller } from '@angular/common';
import { MobileService } from 'src/app/services/mobile/mobile.service';
import {AccountSocialInteractions} from '../../models/account/accountSocialInteractions';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  infoMediaTypes: InfoMediaType[] = [];
  infoNetworkTypes: InfoNetworkType[] = NetworkHelper.networkTypes;
  mediaCount: MediaCount = {} as MediaCount;
  account: Account = {} as Account;
  loading: boolean;
  routes = RoutesHelper.routes;
  accountNetwork: AccountNetwork = {} as AccountNetwork;
  accountsSuggestions: AccountSocialInteractions[];

  public screenWidth: number = window.innerWidth;

  pageYoffset = 0;
  @HostListener('window:scroll', ['$event']) onScroll(event): void {
    this.pageYoffset = window.pageYOffset;
  }

  constructor(
    private activatedRoute: ActivatedRoute,
    private toastService: ToastService,
    private router: Router,
    private subscriptionService: SubscriptionService,
    private authenticateService: AuthenticateService,
    private cookieService: CookieService,
    private multimediaService: MultimediaService,
    private scroll: ViewportScroller,
    private accountService: AccountService,
    private mobileService: MobileService
  ) {}

  ngOnInit(): void {
    this.mobileService.unsetSkillschatDesign();
    this.activatedRoute.queryParamMap.subscribe((paramMap: ParamMap) => {
      const refresh = paramMap.get('refresh');
      if (refresh) {
        this.loadDashboard();
      }
    });
    this.loadDashboard();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    this.screenWidth = window.innerWidth;
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }

  scrollToTop(): void {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth',
    });
  }

  loadDashboard(): void {
    this.loading = true;
    this.account = this.authenticateService.getCurrentAccount();
    if (this.account) {
      this.findAccount();
    } else {
      this.toastService.showError('Error', 'Compte non identifié');
      this.router.navigate([this.routes.get('root')]);
      this.loading = false;
    }
  }

  findAccount(): void {
    this.loading = true;
    this.accountService.findAccountById(this.account.idServer).subscribe(
      (response) => {
        this.account = response.resource;
        this.loading = false;
        this.findNetwork();
        this.findCountMediaType();
      },
      (error) => {
        this.router.navigate([this.routes.get('root')]);
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }

  findNetwork(): void {
    this.subscriptionService.findNetwork(this.account.idServer).subscribe(
      (response) => {
        this.accountNetwork = response.resource;
        this.sortNetwork();
      },
      (error) => {}
    );
  }

  sortNetwork(): void {
    this.infoNetworkTypes = NetworkHelper.networkTypes;
    this.infoNetworkTypes[1].value = this.accountNetwork.numberFollowers;
    this.infoNetworkTypes[2].value = this.accountNetwork.numberFollowees;
    this.selectAccountsSuggestions();
  }

  sortMultimedias(): void {
    this.infoMediaTypes = [];
    let infoMedia: InfoMediaType = MediaHelper.mediaTypes[0]; // documents
    infoMedia.value = this.mediaCount.documents;
    this.infoMediaTypes.push(infoMedia);

    infoMedia = MediaHelper.mediaTypes[1]; // videos
    infoMedia.value = this.mediaCount.videos;
    this.infoMediaTypes.push(infoMedia);

    infoMedia = MediaHelper.mediaTypes[2]; // links
    infoMedia.value = this.mediaCount.links;
    this.infoMediaTypes.push(infoMedia);

    infoMedia = MediaHelper.mediaTypes[3]; // images
    infoMedia.value = this.mediaCount.images;
    this.infoMediaTypes.push(infoMedia);

    infoMedia = MediaHelper.mediaTypes[4]; // audios
    infoMedia.value = this.mediaCount.audios;
    this.infoMediaTypes.push(infoMedia);
  }

  findCountMediaType(): void {
    this.loading = true;
    this.multimediaService.countMediaType(this.account.idServer).subscribe(
      (response) => {
        this.mediaCount = response.resource;
        this.sortMultimedias();
        this.loading = false;
      },
      (error) => {
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }

  getMaxSize(nb: number, max: number): number {
    return nb < max ? nb : max;
  }

  shuffle(array: AccountSocialInteractions[]): AccountSocialInteractions[] {
    let currentIndex = array.length, temporaryValue, randomIndex;
    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  }

  shuffleArray(array: AccountSocialInteractions[]): AccountSocialInteractions[] {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
  }

  selectAccountsSuggestions(): void{
    this.accountsSuggestions = this.shuffle(this.accountNetwork.suggestions.slice(0, this.getMaxSize(this.accountNetwork.suggestions.length, 10)));
  }
}

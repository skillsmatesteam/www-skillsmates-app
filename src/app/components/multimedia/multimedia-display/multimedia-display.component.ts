import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Multimedia} from '../../../models/multimedia/multimedia';
import {variables} from '../../../../environments/variables';
import {MediaHelper} from '../../../helpers/media-helper';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {Router} from '@angular/router';
import {Metadata} from '../../../models/multimedia/metadata';
import {HttpClient} from '@angular/common/http';
import {MediaUrlPipe} from '../../../pipes/media-url.pipe';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-multimedia-display',
  templateUrl: './multimedia-display.component.html',
  styleUrls: ['./multimedia-display.component.css']
})
export class MultimediaDisplayComponent implements OnInit {

  @Input() multimedia: Multimedia;
  @Input() width: number;
  @Input() height: number;
  @Input() isViewPostOffline: boolean;
  routes = RoutesHelper.routes;
  metadata: Metadata = {} as Metadata;
  url: any;
  viewer = 'google';

  @ViewChild('pdfViewerAutoLoad') pdfViewerAutoLoad;

  public viewerOptions: any = {
    navbar: false,
    toolbar: {
      zoomIn: 4,
      zoomOut: 4,
      oneToOne: 4,
      reset: 4,
      prev: 4,
      play: {
        show: 4,
        size: 'large',
      },
      next: 4,
      rotateLeft: 4,
      rotateRight: 4,
      flipHorizontal: 4,
      flipVertical: 4,
    }
  };

  constructor(private router: Router, private http: HttpClient, private mediaUrlPipe: MediaUrlPipe) {

    if (this.getMultimediaMimetype(this.multimedia).startsWith('application/pdf')){
      this.url =  mediaUrlPipe.transform(this.multimedia);
      this.downloadFile(this.url).subscribe(
        (res) => {
          this.pdfViewerAutoLoad.pdfSrc = res; // pdfSrc can be Blob or Uint8Array
          this.pdfViewerAutoLoad.refresh(); // Ask pdf viewer to load/refresh pdf
        }
      );
    }


  }

  ngOnInit(): void {
  }

  getMultimediaType(multimedia: Multimedia): string{
    return multimedia.type ? multimedia.type.toLowerCase() : variables.document.toLowerCase();
  }

  getMultimediaMimetype(multimedia: Multimedia): string{
    return multimedia && multimedia.mimeType ? multimedia.mimeType : '';
  }

  isYoutubeLink(url): boolean{
    return MediaHelper.isYoutubeLink(url);
  }

  getYoutubeVideoId(url): string{
    return MediaHelper.getYoutubeVideoId(url);
  }

  private downloadFile(url: any): any {
    return this.http.get(url, { responseType: 'blob' })
      .pipe(
        map((result: any) => {
          return result;
        })
      );
  }

  /**
   * if the file is any other file
   * @param multimedia multimedia file
   */
  isAnyOtherFile(multimedia: Multimedia): boolean{
    const mimetype = this.getMultimediaMimetype(multimedia);
    return mimetype && !mimetype.startsWith('application/pdf') && !mimetype.startsWith('video/') &&
      !mimetype.startsWith('image/') && !mimetype.startsWith('audio/');
  }
}

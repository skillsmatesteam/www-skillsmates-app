import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MultimediaThumbnailDisplayComponent } from './multimedia-thumbnail-display.component';

describe('MultimediaThumbnailDisplayComponent', () => {
  let component: MultimediaThumbnailDisplayComponent;
  let fixture: ComponentFixture<MultimediaThumbnailDisplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MultimediaThumbnailDisplayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MultimediaThumbnailDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

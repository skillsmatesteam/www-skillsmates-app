import {Component, Input, OnInit} from '@angular/core';
import {Multimedia} from '../../../models/multimedia/multimedia';
import {MediaHelper} from '../../../helpers/media-helper';

@Component({
  selector: 'app-multimedia-thumbnail-display',
  templateUrl: './multimedia-thumbnail-display.component.html',
  styleUrls: ['./multimedia-thumbnail-display.component.css']
})
export class MultimediaThumbnailDisplayComponent implements OnInit {

  @Input() multimedia: Multimedia;
  viewer = 'google';

  constructor() { }

  ngOnInit(): void {
  }

  getMultimediaMimetype(multimedia: Multimedia): string{
    return multimedia && multimedia.mimeType ? multimedia.mimeType : '';
  }

  isYoutubeLink(url): boolean{
    return MediaHelper.isYoutubeLink(url);
  }

  getYoutubeVideoId(url): string{
    return MediaHelper.getYoutubeVideoId(url);
  }

  /**
   * if the file is any other file
   * @param multimedia multimedia file
   */
  isAnyOtherFile(multimedia: Multimedia): boolean{
    const mimetype = this.getMultimediaMimetype(multimedia);
    return mimetype && !mimetype.startsWith('application/pdf') && !mimetype.startsWith('video/') &&
      !mimetype.startsWith('image/') && !mimetype.startsWith('audio/');
  }
}

import {
  Component, DoCheck,
  EventEmitter,
  Input,
  IterableChanges,
  IterableDiffer, IterableDiffers,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import {InfoMediaType} from '../../models/multimedia/infoMediaType';
import {MediaSubtype} from '../../models/multimedia/mediaSubtype';
import {Account} from '../../models/account/account';
import {AccountSocialInteractions} from '../../models/account/accountSocialInteractions';
import {MediaHelper} from '../../helpers/media-helper';
import {AccountService} from '../../services/accounts/account/account.service';
import {MultimediaService} from '../../services/media/multimedia/multimedia.service';
import {SortedMediaSubtype} from '../../models/multimedia/sortedMediaSubtype';
import {AuthenticationGuardService} from '../../services/authentication.guard.service';
import {AuthenticateService} from '../../services/accounts/authenticate/authenticate.service';

@Component({
  selector: 'app-search-type',
  templateUrl: './search-type.component.html',
  styleUrls: ['./search-type.component.css']
})
export class SearchTypeComponent implements OnInit, OnChanges {
  @Input() mediaType: InfoMediaType;
  @Input() mediaTypes: InfoMediaType[];
  @Input() account: Account = {} as Account;
  @Output() selectSubtypes = new EventEmitter<MediaSubtype[]>();
  @Output() selectMediaSubtype = new EventEmitter<string>();
  @Output() selectAllProfiles = new EventEmitter<boolean>();
  selectedMediaTypes: InfoMediaType[] = [];
  searchType: string[] = [];
  mediaSubtypes: MediaSubtype[];
  currentSearchType: string;
  selectedSubTypes: MediaSubtype[] = [];
  loading: boolean;
  accountGeneralInfos: AccountSocialInteractions = {} as AccountSocialInteractions;
  allMediaSubtypes: MediaSubtype[] = [];
  profileMediaType = MediaHelper.profileMediaType;
  newMediaSubType: MediaSubtype = {} as MediaSubtype;
  image: string;
  label: string;
  sortedDocumentsMediaSubtypes: SortedMediaSubtype[] = [];
  sortedVideosMediaSubtypes: SortedMediaSubtype[] = [];
  sortedLinksMediaSubtypes: SortedMediaSubtype[] = [];
  sortedImagesMediaSubtypes: SortedMediaSubtype[] = [];
  sortedAudiosMediaSubtypes: SortedMediaSubtype[] = [];
  sortedProfilesMediaSubtypes: SortedMediaSubtype[] = [];
  isAllSelected = true;
  isAllProfilesSelected = true;
  // IterableDiffers: utilisé pour détecter les changements de tableau (ajout/suppression) mais pas de modification d'éléménts
  // private _diff: IterableDiffer<InfoMediaType>;

  constructor(private accountService: AccountService,
              private authenticateService: AuthenticateService,
              private multimediaService: MultimediaService) {
  }

  ngOnInit(): void {
    // this._diff = this._iterableDiffers.find(this.mediaTypes).create(); // find : essaie de trouver un différent
    this.account = this.authenticateService.getCurrentAccount();
    this.formatTitle();
    this.findAccountInfos();
  }

  // public ngDoCheck(): void {
  //   const changes: IterableChanges<InfoMediaType> = this._diff.diff(this.mediaTypes);
  //   // const changes: Changes<InfoMediaType> = this._diff.diff(this.mediaType);
  //
  //   if (changes) {
  //     console.log(this.mediaType);
  //     this.isAllSelected = false;
  //     this.selectedMediaTypes = this.mediaTypes;
  //     this.selectedSubTypes = [];
  //     this.formatTitle();
  //     this.findAccountInfos();
  //   }
  // }

  ngOnChanges(changes: SimpleChanges): void {
    this.account = this.authenticateService.getCurrentAccount();
    if (changes.mediaType && changes.mediaType.currentValue) {
      this.isAllSelected = true;
      this.selectedSubTypes = [];
      this.selectedMediaTypes = this.mediaTypes;
      this.formatTitle();
      this.findAccountInfos();
    }
  }

  formatTitle(): void {
    if (this.mediaTypes && this.mediaTypes.length > 0){
      if (this.mediaTypes.includes(MediaHelper.profil)){
        this.label = 'Type de profil';
        this.image = 'user';
      }else {
        this.label = 'Type de contenus';
        this.image = 'document';
      }
    }
  }

  onClickType(mediaSubtype: MediaSubtype): void {
    this.isAllSelected = false;
    this.currentSearchType = mediaSubtype.label;
    this.updateSelectedSubTypes(mediaSubtype);
    this.selectMediaSubtype.emit(this.currentSearchType);
    this.selectSubtypes.emit(this.selectedSubTypes);
    this.selectAllProfiles.emit(this.isAllProfilesSelected);
  }

  selectAllMediaSubtypes(): void{
    this.selectedSubTypes = [];
    if (this.mediaType && this.mediaType.type){
      this.allMediaSubtypes.forEach(subtype => {
        if (subtype.mediaType.label.toLowerCase() === this.mediaType.type.toLowerCase()){
          this.selectedSubTypes.push(subtype);
        }
      });
      // this.isAllSelected = true;
      this.selectSubtypes.emit(this.selectedSubTypes);
      this.selectAllProfiles.emit(this.isAllProfilesSelected);
    }
  }

  findAccountInfos(): void {
    const accountId = this.account.idServer;
    this.accountService.findAccountSocialInteractions(accountId).subscribe((response) => {
      this.accountGeneralInfos = response.resource;
      this.loading = false;
    }, (error) => {
      this.loading = false;
    }, () => {
      this.findAllMediaSubTypes();
    });
  }

  /**
   * get all media sub type from server
   */
  findAllMediaSubTypes(): void {
    this.loading = true;
    this.multimediaService.findMediaSubtypes().subscribe((response) => {
      this.allMediaSubtypes = response.resources;

      const professionalStatuses = ['Etudiant', 'Enseignant', 'Professionnel'];

      /*Ajout du nouvel objet/media 'Personne'*/
      for (const val of professionalStatuses) {
        this.newMediaSubType = new MediaSubtype();
        this.newMediaSubType.label = val;
        this.newMediaSubType.mediaType = this.profileMediaType;
        this.allMediaSubtypes.push(this.newMediaSubType);
      }
      if (this.isAllSelected){
        this.selectAllMediaSubtypes();
        // this.isAllSelected = false;
      }
      this.filterMediaSubTypes();
      this.loading = false;
    }, (error) => {
      console.log(error);
    }, () => {
      this.loading = false;
    });
  }

  filterMediaSubTypes(): void {
    if (this.allMediaSubtypes) {
      this.searchType = [];
      this.mediaSubtypes = [];
      // this.selectedMediaTypes.forEach(mt => {
      //     this.allMediaSubtypes.forEach(elt => {
      //       if ((elt.mediaType.label.toLowerCase() === mt.type.toLowerCase()) && this.mediaSubtypes.indexOf(elt) <= 0) {
      //         this.searchType.push(elt.label);
      //         this.mediaSubtypes.push(elt);
      //       }
      //     });
      // });

      if (this.mediaType && this.mediaType.type){
        this.allMediaSubtypes.forEach(elt => {
          if ((elt.mediaType.label.toLowerCase() === this.mediaType.type.toLowerCase()) && this.mediaSubtypes.indexOf(elt) <= 0) {
            this.searchType.push(elt.label);
            this.mediaSubtypes.push(elt);
          }
        });

        // this.currentSearchType = this.searchType[0];
        this.updateSelectedSubTypes(null);
        this.selectMediaSubtype.emit(this.currentSearchType);
      }
    }
  }

  updateSelectedSubTypes(subtype: MediaSubtype): void{
    if (subtype){
      if (this.selectedSubTypes.includes(subtype)){
        this.selectedSubTypes = this.selectedSubTypes.filter(obj => obj.label !== subtype.label);
      }else {
        this.selectedSubTypes.push(subtype);
      }
    }
    this.sortSelectedMediaSubtypes();
  }

  isSubTypeSelected(subtype: MediaSubtype): boolean {
    return this.selectedSubTypes.filter(obj => obj.label === subtype.label).length > 0;
  }

  sortSelectedMediaSubtypes(): void {
    this.resetSortedMediaSubtypes();
    this.mediaSubtypes.forEach((elt) => {
      const sortedMediaSubtype = {} as SortedMediaSubtype;
      sortedMediaSubtype.mediaType = elt.mediaType;
      sortedMediaSubtype.mediaSubtype = elt;
      switch (elt.mediaType.label.toLowerCase()) {
        case 'document':
          this.sortedDocumentsMediaSubtypes.push(sortedMediaSubtype);
          break;
        case 'video':
          this.sortedVideosMediaSubtypes.push(sortedMediaSubtype);
          break;
        case 'link':
          this.sortedLinksMediaSubtypes.push(sortedMediaSubtype);
          break;
        case 'image':
          this.sortedImagesMediaSubtypes.push(sortedMediaSubtype);
          break;
        case 'audio':
          this.sortedAudiosMediaSubtypes.push(sortedMediaSubtype);
          break;
        case 'profile':
          this.sortedProfilesMediaSubtypes.push(sortedMediaSubtype);
          break;
      }
    });
  }

  onClickAllType(): void {
    if (this.isAllSelected){
      this.isAllSelected = false;
      this.isAllProfilesSelected = false;
      this.selectAllProfiles.emit(false);
      this.selectedSubTypes = [];
    }else{
      this.isAllSelected = true;
      if (this.mediaType.type === 'profile'){
        this.isAllProfilesSelected = true;
      }
      this.selectAllMediaSubtypes();
    }
  }

  resetSortedMediaSubtypes(): void{
    this.sortedDocumentsMediaSubtypes = [];
    this.sortedVideosMediaSubtypes = [];
    this.sortedLinksMediaSubtypes = [];
    this.sortedImagesMediaSubtypes = [];
    this.sortedAudiosMediaSubtypes = [];
    this.sortedProfilesMediaSubtypes = [];
  }
}

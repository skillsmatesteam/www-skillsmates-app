import {Component, Input, OnInit} from '@angular/core';
import {Metadata} from "../../models/multimedia/metadata";
import {MetadataService} from "../../services/media/metadata/metadata.service";

@Component({
  selector: 'app-link-display',
  templateUrl: './link-display.component.html',
  styleUrls: ['./link-display.component.css']
})
export class LinkDisplayComponent implements OnInit {

  @Input() url: string;
  metadata: Metadata = {} as Metadata;
  previewLoading: boolean;

  constructor(private metadataService: MetadataService) { }

  ngOnInit(): void {
    this.previewUrlLink(this.url);
  }

  previewUrlLink(url: string): void {
   this.previewLoading = true;
   const encodedUrl = btoa(url).replace('+', '-').replace('/', '_');
     this.metadataService.getMetadataUrl(encodedUrl).subscribe((response) => {
       this.metadata = response.resource;
       this.previewLoading = false;
     }, (error) => {
       this.previewLoading = false;
     });
   }
}

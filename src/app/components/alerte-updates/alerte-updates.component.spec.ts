import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlerteUpdatesComponent } from './alerte-updates.component';

describe('AlerteUpdatesComponent', () => {
  let component: AlerteUpdatesComponent;
  let fixture: ComponentFixture<AlerteUpdatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlerteUpdatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlerteUpdatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

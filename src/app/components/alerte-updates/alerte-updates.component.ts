import { Component, OnInit } from '@angular/core';
import {UpdateService} from '../../services/settings/update/update.service';
import {Update} from '../../models/update/update';
import {variables} from '../../../environments/variables';

@Component({
  selector: 'app-alerte-updates',
  templateUrl: './alerte-updates.component.html',
  styleUrls: ['./alerte-updates.component.css']
})
export class AlerteUpdatesComponent implements OnInit {

  updates: Update[] = [];
  showAlert = false;
  constructor(private updateService: UpdateService) { }

  ngOnInit(): void {
    this.findUpdates();
  }

  findUpdates(): void{
    this.updateService.findUpdates().subscribe((response) => {
      this.updates = response.resources;
      this.showUpdatesAlert();
    });
  }

  onClickDismiss(): void {
    this.dismissUpdatesAlert();
  }

  dismissUpdatesAlert(): void{
    if (this.updates && this.updates.length > 0){
      localStorage.setItem(variables.updates, this.updates[0].version);
    }
  }

  showUpdatesAlert(): void{
    if (this.updates && this.updates.length > 0){
      const version = localStorage.getItem(variables.updates);
      if (version){
        this.showAlert = version !== this.updates[0].version;
      }else {
        this.showAlert = true;
      }
    }else {
      this.showAlert = false;
    }
  }
}

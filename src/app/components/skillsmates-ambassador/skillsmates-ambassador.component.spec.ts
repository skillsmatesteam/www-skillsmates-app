import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SkillsmatesAmbassadorComponent } from './skillsmates-ambassador.component';

describe('SkillsmatesAmbassadorComponent', () => {
  let component: SkillsmatesAmbassadorComponent;
  let fixture: ComponentFixture<SkillsmatesAmbassadorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SkillsmatesAmbassadorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillsmatesAmbassadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

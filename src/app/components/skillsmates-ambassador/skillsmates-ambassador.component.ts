import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-skillsmates-ambassador',
  templateUrl: './skillsmates-ambassador.component.html',
  styleUrls: ['./skillsmates-ambassador.component.css']
})
export class SkillsmatesAmbassadorComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  onClickOpenAmbassador(url: string): void {
    window.open(url, '_blank');
  }
}

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Account} from '../../models/account/account';
import {AuthenticateService} from '../../services/accounts/authenticate/authenticate.service';
import {RoutesHelper} from '../../helpers/routes-helper';

@Component({
  selector: 'app-return-button',
  templateUrl: './return-button.component.html',
  styleUrls: ['./return-button.component.css']
})
export class ReturnButtonComponent implements OnInit {

  @Input() account: Account = {} as Account;
  @Input() loading: boolean;
  @Input() title: string;
  @Output() clickButton = new EventEmitter<string>();

  loggedAccount: Account = {} as Account;
  routes = RoutesHelper.routes;

  constructor(private authenticateService: AuthenticateService) { }

  ngOnInit(): void {
    this.loggedAccount = this.authenticateService.getCurrentAccount();
  }

  onClickButton(): void {
    this.clickButton.emit('');
  }

}

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Account} from '../../models/account/account';
import {AuthenticateService} from '../../services/accounts/authenticate/authenticate.service';
import {RoutesHelper} from '../../helpers/routes-helper';
import {SocialInteractionService} from '../../services/pages/social-interaction/social-interaction.service';
import {SocialInteraction} from '../../models/post/socialInteraction';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {
  @Input() comment: SocialInteraction;
  @Output() removeCommentEvent = new EventEmitter<SocialInteraction>();
  loggedAccount: Account = {} as Account;
  routes = RoutesHelper.routes;

  constructor(private socialInteractionService: SocialInteractionService, private authenticateService: AuthenticateService) { }

  ngOnInit(): void {
    this.loggedAccount = this.authenticateService.getCurrentAccount();
  }

  onClickDelete(comment: SocialInteraction): void {
    this.socialInteractionService.delete(comment.idServer).subscribe((response) => {
      this.removeCommentEvent.emit(comment);
    }, (error) => {});
  }
}

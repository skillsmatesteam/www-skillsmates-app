import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Post} from '../../../models/post/post';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {variables} from '../../../../environments/variables';
import {Account} from '../../../models/account/account';
import {ActivatedRoute} from '@angular/router';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {PostService} from '../../../services/pages/post/post.service';
import {ToastService} from '../../../services/toast/toast.service';
import {PostUrl} from '../../../models/post/postUrl';
import {StringHelper} from '../../../helpers/string-helper';
import {PostSocialInteractions} from '../../../models/post/postSocialInteractions';
import {RestCountryService} from '../../../services/api/rest-country.service';
import {MobileService} from '../../../services/mobile/mobile.service';

@Component({
  selector: 'app-post-header',
  templateUrl: './post-header.component.html',
  styleUrls: ['./post-header.component.css']
})
export class PostHeaderComponent implements OnInit {

  @Input() postSocialInteractions: PostSocialInteractions = {} as PostSocialInteractions;
  @Output() newPostEvent = new EventEmitter<string>();
  @Input() account: Account = {} as Account;
  @Input() displayMenu: boolean;
  @Input() isSimplePost: boolean;
  loggedAccount: Account = {} as Account;
  loggedAccountId: string;
  postOwnerId: string;
  id: string;
  routes = RoutesHelper.routes;
  postToDelete: Post = {} as Post;
  generatedUrl: string;
  postUrl: PostUrl = {} as PostUrl;
  public screenWidth: number = window.innerWidth;

  constructor(private activatedRoute: ActivatedRoute,
              private toastService: ToastService,
              private authenticateService: AuthenticateService,
              private restCountryService: RestCountryService,
              private postService: PostService,
              private mobileService: MobileService) { }

  ngOnInit(): void {
    this.loggedAccount = this.authenticateService.getCurrentAccount();
    if (this.loggedAccount) {
      this.loggedAccountId = this.loggedAccount.idServer;
    }
    this.postOwnerId = this.activatedRoute.snapshot.paramMap.get(variables.id);
  }

  onClickDelete(post: Post): void {
    this.postToDelete = post;
    localStorage.setItem('postToDelete', post.idServer);
  }

  onClickConfirmDelete(): void {
    this.postService.deletePost(localStorage.getItem('postToDelete')).subscribe((response) => {
      this.toastService.showSuccess('Succès', 'Publication supprimée avec succés');
      this.newPostEvent.emit('refresh');
    }, (error) => {
      this.toastService.showError('Error', 'Publication non supprimée');
    }, () => {
      localStorage.removeItem('postToDelete');
    });
  }



  onClickUpdate(post: Post): void {
    localStorage.setItem('postToUpdate', post.idServer);
  }

  onClickCopy(post: Post): void {
    this.onClickGenerateUrl(post);
  }

  onClickGenerateUrl(post: Post): void {
    this.postService.generateUrlPost(post.idServer).subscribe((response) => {
      this.postUrl = response.resource;
      this.toastService.showSuccess('Succès', 'Lien copié avec succés');

    }, (error) => {
      this.toastService.showError('Error', 'Lien non copié');
    }, () => {
       this.copy(this.postUrl.url);
    });
  }

  copy(url: string): void {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = url;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }

  getEstablishmentIcon(): string{
    return StringHelper.getEstablishmentIcon(this.account);
  }

  public name(account: Account): string {
    return StringHelper.truncateName(account, 25);
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }
}

import {Component, EventEmitter, HostListener, OnDestroy, OnInit, Output} from '@angular/core';
import {variables} from '../../../../environments/variables';
import {ActivatedRoute, Router} from '@angular/router';
import {PostService} from '../../../services/pages/post/post.service';
import {ToastService} from '../../../services/toast/toast.service';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {SocialInteractionService} from '../../../services/pages/social-interaction/social-interaction.service';
import {SocialInteraction} from '../../../models/post/socialInteraction';
import {Account} from '../../../models/account/account';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {SocialInteractionsHelper} from '../../../helpers/social-interactions-helper';
import {ViewPostOffline} from '../../../models/post/viewPostOffline';
import {StringHelper} from '../../../helpers/string-helper';
import {Alert} from '../../../models/alert';
import {WebSocketService} from '../../../services/message/web-socket.service';
import {MobileService} from '../../../services/mobile/mobile.service';
import {SearchParam} from '../../../models/searchParam';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-view-post',
  templateUrl: './view-post.component.html',
  styleUrls: ['./view-post.component.css']
})
export class ViewPostComponent implements OnInit, OnDestroy {
  @Output() newPostEvent = new EventEmitter<string>();
  loading: boolean;
  anotherProfile: boolean;
  postId: string;
  comment: SocialInteraction = {} as SocialInteraction;
  viewPostOffline: ViewPostOffline = {} as ViewPostOffline;
  routes = RoutesHelper.routes;
  shareLoading = false;
  loggedAccount: Account = {} as Account;
  comments: SocialInteraction[] = [];
  toggleDiplayMoreLabel: boolean;
  displayCommentBlock = false;
  errors;
  submitted = false;
  alert: Alert = {} as Alert;
  typedComment: string;
  public screenWidth: number = window.innerWidth;
  closeResult: string;
  isConnectd: boolean;

  constructor(private postService: PostService,
              private toastService: ToastService,
              private socialInteractionService: SocialInteractionService,
              private router: Router,
              private  mobileService: MobileService,
              private webSocketService: WebSocketService,
              private authenticateService: AuthenticateService,
              private activatedRoute: ActivatedRoute,
              private modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.initErrors();
    this.postId = this.activatedRoute.snapshot.paramMap.get(variables.id);
    this.anotherProfile = true;
    if (!this.postId) {
      // this.id = this.loggedAccount.idServer;
    }
    this.findPost(0);
    this.loggedAccount = this.authenticateService.getCurrentAccount();
    this.isConnectd = this.loggedAccount? true : false;

    this.toggleDiplayMoreLabel = true;
  }

  ngOnDestroy(): void {
    // localStorage.removeItem(variables.search_param);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    // this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }

  findPost(page: number): void {
    this.loading = true;
    this.postService.viewSinglePost(this.postId, page).subscribe((response) => {
      this.viewPostOffline = response.resource;
      this.viewPostOffline.postsSocialInteractions = this.viewPostOffline.postsSocialInteractions.filter(obj =>
        obj.post.idServer !== this.viewPostOffline.postSocialInteractions.post.idServer
      );
      this.loading = false;
    }, (error) => {
      console.log(error);
      this.toastService.showError('Error', 'Post not found');
      this.loading = false;
    }, () => {
      this.loading = false;
      this.findComments(this.viewPostOffline.postSocialInteractions.post.idServer);
    });
  }

  computeYoutubeVideoWidth(): number {
    return 565;
  }

  computeYoutubeVideoHeight(): number {
    return 100;
  }

  findComments(postId: string): void {
    this.loading = true;
    this.socialInteractionService.findCommentsByPost(postId).subscribe((response) => {
      this.comments = response.resources;
    }, (error) => {
      this.loading = false;
    }, () => {
      this.loading = false;
    });
  }

  onClickLogin(): void {
    this.router.navigate([this.routes.get('login'), 1]);
  }

  onClickRegister(): void {
    this.router.navigate([this.routes.get('login'), 2]);
  }

  hasLiked(socialInteractions: SocialInteraction[]): boolean {
    return SocialInteractionsHelper.hasLiked(socialInteractions, this.loggedAccount);
  }

  countLikes(socialInteractions: SocialInteraction[]): number {
    return SocialInteractionsHelper.countLikes(socialInteractions);
  }

  hasShared(socialInteractions: SocialInteraction[]): boolean {
    return SocialInteractionsHelper.hasShared(socialInteractions, this.loggedAccount);
  }

  countShares(socialInteractions: SocialInteraction[]): number {
    return SocialInteractionsHelper.countShares(socialInteractions);
  }

  countComments(socialInteractions: SocialInteraction[]): number {
    return SocialInteractionsHelper.countComments(socialInteractions);
  }

  truncateDescription(text: string): string{
    return StringHelper.truncateText(text, 191);
  }

  onClickComment(): void {
    this.displayCommentBlock = !this.displayCommentBlock;
    if (this.displayCommentBlock && this.viewPostOffline.postSocialInteractions && this.viewPostOffline.postSocialInteractions.post){
      this.findComments(this.viewPostOffline.postSocialInteractions.post.idServer);
    }
  }

  removeCommentEvent(comment): void{
    this.comments = this.comments.filter(obj => obj !== comment);
    this.viewPostOffline.postSocialInteractions.socialInteractions = SocialInteractionsHelper.removeSocialInteraction(this.viewPostOffline.postSocialInteractions.socialInteractions, comment);
  }

  initErrors(): void{
    this.submitted = false;
    this.errors = {valid: true, body: ''};
    this.alert = {} as Alert;
  }

  keyupComment(e): void {
    this.errors.title = '';
    if (e.keyCode === 13){
      this.onClickPublishComment();
    }
  }

  /**
   * publish a comment
   */
  onClickPublishComment(): void{
    this.loading = true;
    this.comment.content = this.typedComment.trim();
    if (this.validateComment()){
      this.typedComment = '';
      this.comment.idServer = new Date().getTime().toString(10);
      this.comment.element = this.viewPostOffline.postSocialInteractions.post.idServer;
      this.comment.emitterAccount = this.loggedAccount;
      this.comment.receiverAccount = this.viewPostOffline.account;
      this.comment.type = variables.comment;
      this.comment.createdAt = new Date();
      if (!this.comments){
        this.comments = [];
      }
      this.comments.push(this.comment);
      this.viewPostOffline.postSocialInteractions.socialInteractions.push(this.comment);
      this.socialInteractionService.create(this.comment).subscribe((response) => {
        this.comment = response.resource;
        this.comment.content = this.viewPostOffline.postSocialInteractions.post.title;
        // notify via websocket
        this.webSocketService.sendPushNotification(this.comment);
      }, error => {
      }, () => {
        this.loading = false;
      });
    // }
  }
}
  /**
   * validate comment
   */
  validateComment(): boolean{
    this.initErrors();
    if (this.comment.content === undefined || this.comment.content === ''){
      this.errors.valid = false;
      this.errors.body = 'Le message est requis';
    }

    if (this.comment.content !== undefined && this.comment.content.length > variables.max_title_size){
      this.errors.valid = false;
      this.errors.body = 'Le nombre maximal de caractères est ' + variables.max_title_size;
    }

    this.submitted = true;
    return this.errors.valid;
  }

  onRefreshEvent($event: string): void {

  }

  onClickBack(): void {
    this.router.navigate([this.routes.get('search_results')]);
  }

  getSearchParam(): SearchParam {
    const param: any = localStorage.getItem(variables.search_param);
    return param ? JSON.parse(param) : null;
  }

  onClickLike(): void {
    if (this.loggedAccount){
      let socialInteraction: SocialInteraction = {} as SocialInteraction;
      socialInteraction.element = this.viewPostOffline.postSocialInteractions.post.idServer;
      socialInteraction.emitterAccount = this.loggedAccount;
      socialInteraction.receiverAccount = this.viewPostOffline.postSocialInteractions.post.account;
      socialInteraction.content = this.viewPostOffline.postSocialInteractions.post.title;
      socialInteraction.type = variables.like;
      this.viewPostOffline.postSocialInteractions.socialInteractions.push(socialInteraction);
      this.socialInteractionService.create(socialInteraction).subscribe((response) => {
        socialInteraction = response.resource;
        this.webSocketService.sendPushNotification(socialInteraction);
      }, error => {});
    }
  }

  open_modal_share(content): void {
    if (this.loggedAccount){
      this.shareLoading = true;
      this.modalService.open(content,
        {
          size: 'lg'
        }).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
        this.shareLoading = false;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        this.shareLoading = false;
      });
    }
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  private refresh(): void{
    this.newPostEvent.emit('refresh');
  }
}

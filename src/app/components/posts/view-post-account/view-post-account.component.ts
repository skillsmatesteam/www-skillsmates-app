import {Component, Input, OnInit} from '@angular/core';
import {Account} from '../../../models/account/account';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {StringHelper} from '../../../helpers/string-helper';
import {PostSocialInteractions} from '../../../models/post/postSocialInteractions';
import {SocialInteraction} from '../../../models/post/socialInteraction';
import {SocialInteractionsHelper} from '../../../helpers/social-interactions-helper';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {ViewPostOffline} from '../../../models/post/viewPostOffline';
import {RestCountryService} from '../../../services/api/rest-country.service';

@Component({
  selector: 'app-view-post-account',
  templateUrl: './view-post-account.component.html',
  styleUrls: ['./view-post-account.component.css']
})
export class ViewPostAccountComponent implements OnInit {
  @Input() viewPostOffline: ViewPostOffline;
  routes = RoutesHelper.routes;
  loggedAccount: Account;

  constructor(private authenticateService: AuthenticateService) {
  }

  ngOnInit(): void {
    this.loggedAccount = this.authenticateService.getCurrentAccount();
  }

  getEstablishmentIcon(): string {
    return StringHelper.getEstablishmentIcon(this.viewPostOffline.account);
  }

  hasLiked(socialInteractions: SocialInteraction[]): boolean {
    return SocialInteractionsHelper.hasLiked(socialInteractions, this.loggedAccount);
  }

  countLikes(socialInteractions: SocialInteraction[]): number {
    return SocialInteractionsHelper.countLikes(socialInteractions);
  }

  hasShared(socialInteractions: SocialInteraction[]): boolean {
    return SocialInteractionsHelper.hasShared(socialInteractions, this.loggedAccount);
  }

  countShares(socialInteractions: SocialInteraction[]): number {
    return SocialInteractionsHelper.countShares(socialInteractions);
  }

  countComments(socialInteractions: SocialInteraction[]): number {
    return SocialInteractionsHelper.countComments(socialInteractions);
  }

  public name(account: Account): string {
    return StringHelper.truncateName(account, 21);
  }
}

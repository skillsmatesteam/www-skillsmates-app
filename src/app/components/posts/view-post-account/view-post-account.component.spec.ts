import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPostAccountComponent } from './view-post-account.component';

describe('ViewPostAccountComponent', () => {
  let component: ViewPostAccountComponent;
  let fixture: ComponentFixture<ViewPostAccountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewPostAccountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPostAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

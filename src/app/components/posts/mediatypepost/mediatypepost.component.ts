import {Component, EventEmitter, HostListener, Input, OnInit, Output} from '@angular/core';
import {NgbModal, ModalDismissReasons, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {InfoMediaType} from '../../../models/multimedia/infoMediaType';
import {Router} from '@angular/router';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {variables} from '../../../../environments/variables';
import {PostSocialInteractions} from '../../../models/post/postSocialInteractions';
import {MobileService} from '../../../services/mobile/mobile.service';

@Component({
  selector: 'app-mediatypepost',
  templateUrl: './mediatypepost.component.html',
  styleUrls: ['./mediatypepost.component.css']
})
export class MediatypepostComponent implements OnInit {
  @Input() infosMediaType: InfoMediaType;
  @Output() selectMediaType = new EventEmitter<InfoMediaType>();
  closeResult: string;
  modalOptions: NgbModalOptions;
  routes = RoutesHelper.routes;
  public screenWidth: number = window.innerWidth;

  constructor(private router: Router, private modalService: NgbModal, private mobileService: MobileService) {
     this.modalOptions = {
      backdrop: 'static',
      backdropClass: 'customBackdrop'
    };
  }

  ngOnInit(): void {}

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    this.screenWidth = window.innerWidth;
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }

  open(content): void {
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  onClickEditPost(mediaType: InfoMediaType): void {
    localStorage.setItem(variables.current_media_type, JSON.stringify(this.infosMediaType));
    if (this.isMobile()){
      if (this.router.url.indexOf(this.routes.get('dashboard')) >= 0) {
        this.router.navigate([this.routes.get('post.edit.my')]);
      }else if (this.router.url.indexOf(this.routes.get('documents')) >= 0){
        this.selectMediaType.emit(mediaType);
      }
    }else {
      this.router.navigate([this.routes.get('post.edit.my')]);
    }
  }
}


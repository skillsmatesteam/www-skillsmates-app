import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediatypepostComponent } from './mediatypepost.component';

describe('MediatypepostComponent', () => {
  let component: MediatypepostComponent;
  let fixture: ComponentFixture<MediatypepostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediatypepostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediatypepostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

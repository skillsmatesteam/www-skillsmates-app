import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Post} from '../../../models/post/post';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {Account} from '../../../models/account/account';
import {variables} from '../../../../environments/variables';
import {PostService} from '../../../services/pages/post/post.service';
import {SocialInteractionService} from '../../../services/pages/social-interaction/social-interaction.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {Alert} from '../../../models/alert';
import {SocialInteraction} from '../../../models/post/socialInteraction';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ToastService} from '../../../services/toast/toast.service';
import {PostSocialInteractions} from '../../../models/post/postSocialInteractions';
import {SocialInteractionsHelper} from '../../../helpers/social-interactions-helper';

@Component({
  selector: 'app-shared-post',
  templateUrl: './shared-post.component.html',
  styleUrls: ['./shared-post.component.css']
})
export class SharedPostComponent implements OnInit, OnChanges {

  @Input() postSocialInteractions: PostSocialInteractions;
  @Output() newPostEvent = new EventEmitter<string>();

  post: Post = {} as Post;
  id: string;
  routes = RoutesHelper.routes;
  loggedAccount: Account = {} as Account;
  comments: SocialInteraction[] = [];
  submitted = false;
  errors;
  comment: SocialInteraction = {} as SocialInteraction;
  typedComment: string;
  loading: boolean;
  alert: Alert = {} as Alert;
  closeResult: string;
  showCommentBox = false;

  constructor(private postService: PostService,
              private sharedPostService: SocialInteractionService,
              private activatedRoute: ActivatedRoute,
              private socialInteractionService: SocialInteractionService,
              private router: Router,
              private modalService: NgbModal,
              private toastService: ToastService,
              private authenticateService: AuthenticateService) { }

  ngOnInit(): void {
    this.initErrors();
    this.loggedAccount = this.authenticateService.getCurrentAccount();
    this.id = this.activatedRoute.snapshot.paramMap.get(variables.id);
    if (!this.id){
      this.id = this.loggedAccount.idServer;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.post = changes.postSocialInteractions.currentValue !== undefined ? changes.postSocialInteractions.currentValue.post : {};
    if (this.post && this.post.idServer){
      this.findComments(this.post.idServer);
    }
  }

  findComments(postId: string): void{
    this.loading = true;
    this.socialInteractionService.findCommentsByPost(postId).subscribe((response) => {
      this.comments = response.resources;
    }, (error) => {
      this.loading = false;
    }, () => {
      this.loading = false;
    });
  }

  onClickLike(): void {
    let socialInteraction: SocialInteraction = {} as SocialInteraction;
    socialInteraction.element = this.postSocialInteractions.post.idServer;
    socialInteraction.emitterAccount = this.loggedAccount;
    socialInteraction.receiverAccount = this.post.account;
    socialInteraction.type = variables.like;
    this.postSocialInteractions.socialInteractions.push(socialInteraction);

    this.socialInteractionService.create(socialInteraction).subscribe((response) => {
      socialInteraction = response.resource;
    }, error => {});
  }

  onClickComment(): void {
    this.showCommentBox = !this.showCommentBox;
  }

  removeCommentEvent(comment): void{
    this.comments = this.comments.filter(obj => obj !== comment);
  }

  initErrors(): void{
    this.submitted = false;
    this.errors = {valid: true, body: ''};
    this.alert = {} as Alert;
  }

  keyupComment(e): void {
    this.errors.title = '';
    if (e.keyCode === 13){
      this.onClickPublishComment();
    }
  }

  onClickPublishComment(): void{
    this.loading = true;
    this.comment.content = this.typedComment.trim();
    if (this.validateComment()){
      this.typedComment = '';
      this.comment.element = this.postSocialInteractions.post.idServer;
      this.comment.emitterAccount = this.loggedAccount;
      this.comment.receiverAccount = this.post.account;
      this.comment.type = variables.comment;
      this.comment.createdAt = new Date();
      if (!this.comments){
        this.comments = [];
      }
      this.comments.push(this.comment);

      let newComment: SocialInteraction = {} as SocialInteraction;
      newComment.element = this.post.idServer;
      newComment.emitterAccount = this.loggedAccount;
      newComment.receiverAccount = this.post.account;
      newComment.type = variables.comment;
      newComment.content = this.comment.content;
      this.socialInteractionService.create(newComment).subscribe((response) => {
        newComment = response.resource;
        this.comments.pop();
        this.comments.push(newComment);
      }, error => {
      }, () => {
        this.loading = false;
      });
    }
  }

  validateComment(): boolean{
    this.initErrors();
    if (this.comment.content === undefined || this.comment.content === ''){
      this.errors.valid = false;
      this.errors.body = 'Le message est requis';
    }

    if (this.comment.content !== undefined && this.comment.content.length > 255){
      this.errors.valid = false;
      this.errors.body = 'Le nombre maximal de caractères est 255';
    }

    this.submitted = true;
    return this.errors.valid;
  }

  onClickDelete(post: Post): void {
    // this.postToDelete = post;
    // localStorage.setItem('postToDelete', post.idServer);
  }

  onClickConfirmDelete(): void {
    this.postService.deletePost(localStorage.getItem('postToDelete')).subscribe((response) => {
      this.toastService.showSuccess('Succès', 'Publication supprimée avec succés');
      this.newPostEvent.emit('refresh');
    }, (error) => {
      this.toastService.showError('Error', 'Publication non supprimée');
    }, () => {
      localStorage.removeItem('postToDelete');
    });
  }

  // Ouverture creer une publication
  open_modal_publication(content): void {
    this.modalService.dismissAll();
    this.modalService.open(content, {
        centered: true,
        windowClass: 'publication-modal-2'
      }
    )
      .result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  private refresh(): void{
    this.newPostEvent.emit('refresh');
  }

  hasLiked(socialInteractions: SocialInteraction[]): boolean{
    return SocialInteractionsHelper.hasLiked(socialInteractions, this.loggedAccount);
  }

  countLikes(socialInteractions: SocialInteraction[]): number{
    return SocialInteractionsHelper.countLikes(socialInteractions);
  }

  countComments(socialInteractions: SocialInteraction[]): number{
    return SocialInteractionsHelper.countComments(socialInteractions);
  }

  countShares(socialInteractions: SocialInteraction[]): number{
    return SocialInteractionsHelper.countShares(socialInteractions);
  }
}

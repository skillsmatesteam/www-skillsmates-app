import { Component, OnInit } from '@angular/core';
import {ModalDismissReasons, NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {Post} from '../../../models/post/post';
import {Account} from '../../../models/account/account';
import {Alert} from '../../../models/alert';
import {MediaSubtype} from '../../../models/multimedia/mediaSubtype';
import {variables} from '../../../../environments/variables';
import {ToastService} from '../../../services/toast/toast.service';
import {PostfileService} from '../../../services/pages/post/postfile.service';
import {PostService} from '../../../services/pages/post/post.service';
import {Router} from '@angular/router';
import {MultimediaService} from '../../../services/media/multimedia/multimedia.service';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {MediaHelper} from '../../../helpers/media-helper';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {MimetypeHelper} from '../../../helpers/mimetype-helper';
import {infoText} from "../../../../environments/info-text";

@Component({
  selector: 'app-modal-add-post',
  templateUrl: './modal-add-post.component.html',
  styleUrls: ['./modal-add-post.component.css']
})
export class ModalAddPostComponent implements OnInit {

  infosMediaTypes = MediaHelper.mediaTypes;

  closeResult: string;
  maxTextSize: number;
  infoTextModalAddPost = infoText.info_text_new_modal_post_title;
  modalOptions: NgbModalOptions;
  post: Post = {} as Post;
  file: File;
  loading: boolean;
  account: Account;
  alert: Alert = {} as Alert;
  submitted = false;
  errors;
  mediaSubtypes: MediaSubtype[];
  processing = false;
  routes = RoutesHelper.routes;

  constructor(private modalService: NgbModal,
              private postfileService: PostfileService,
              private postService: PostService,
              private router: Router,
              private multimediaService: MultimediaService,
              private authenticateService: AuthenticateService,
              private toastService: ToastService) { }

  ngOnInit(): void {
    this.loading = true;
    this.account = this.authenticateService.getCurrentAccount();
    this.initErrors();
    this.findMediaSubtypes();
    this.maxTextSize = variables.max_modal_post_size;
  }

  open(content): void {
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    this.initErrors();
    this.file = null;
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  selectFile(event): void {
    this.file = event.target.files[0];
    if (!this.validateFileSize()){
      this.toastService.showError('Erreur', 'La taille maximale est ' + variables.max_file_size + 'MB');
    }
    if (!this.validateFileMimetype()){
      this.toastService.showError('Erreur', 'Ce type de document n\'est pas accepté');
    }
  }

  onClickPublishMediaPost(): void{
    this.loading = true;
    if (this.validatePost()){
      this.post.account = this.account;
      this.post.type = variables.status;
      if (!this.post.title){this.post.title = ''; }
      if (!this.post.keywords){this.post.keywords = ''; }
      if (this.file != null){
        this.publishMediaPost();
      }else {
        this.publishPost();
      }
    }else {
      this.toastService.showError('Erreur', 'Données invalides');
      this.loading = false;
    }
  }

  publishMediaPost(): void {
    this.loading = true;
    this.postfileService.publishMediaPost(this.file, this.post).subscribe((response) => {
      this.toastService.showSuccess('Sucess', 'Post publié avec succés');
      this.loading = false;
      this.modalService.dismissAll();
      // this.router.navigate([this.routes.get('dashboard')]);
      this.router.navigate([this.routes.get('dashboard')], {
        queryParams: {refresh: new Date().getTime()}
      });
    }, (error) => {
      this.toastService.showError('Erreur', 'Erreur lors du post');
      this.loading = false;
      this.modalService.dismissAll();
    }, () => {});
  }

  publishPost(): void {
    this.loading = true;
    this.postService.publishPost(this.post).subscribe((response) => {
      this.toastService.showSuccess('Sucess', 'Post publié avec succés');
      this.loading = false;
      this.modalService.dismissAll();
      // this.router.navigate([this.routes.get('dashboard')]);
      this.router.navigate([this.routes.get('dashboard')], {
        queryParams: {refresh: new Date().getTime()}
      });
    }, (error) => {
      this.toastService.showError('Erreur', 'Erreur lors du post');
      this.loading = false;
      this.modalService.dismissAll();
    }, () => {});
  }

  validatePost(): boolean{
    this.initErrors();
    if (this.post.description === undefined || this.post.description === ''){
      this.errors.valid = false;
      this.errors.description = 'La description est requis';
    }

    if (this.post.description !== undefined && this.post.description.length > variables.max_modal_post_size){
      this.errors.valid = false;
      this.errors.description = 'Le nombre maximale de caractères est ' + variables.max_modal_post_size;
    }

    if (this.file && !this.validateFileSize()){ // max_file_size
      this.errors.valid = false;
      this.errors.file = 'La taille maximale est ' + variables.max_file_size + 'MB';
    }

    if (this.file && !this.validateFileMimetype()){
      this.errors.valid = false;
      this.errors.file = 'Ce type de document n\'est pas accepté';
    }

    this.submitted = true;
    return this.errors.valid;
  }

  initErrors(): void{
    this.submitted = false;
    this.errors = {valid: true, description: '', file: ''};
    this.alert = {} as Alert;
  }

  validateFileSize(): boolean{
    if (!MimetypeHelper.isMimetypeAllowed(this.file.type)){
      this.errors.valid = false;
      this.errors.file = 'La taille maximale est ' + variables.max_file_size + 'MB';
      return false;
    }
    return true;
  }

  validateFileMimetype(): boolean{
    if (!MimetypeHelper.isMimetypeAllowed(this.file.type)){
      this.errors.valid = false;
      this.errors.file = 'Ce type de document n\'est pas accepté';
      return false;
    }
    return true;
  }

  findMediaSubtypes(): void{
    this.loading = true;
    this.multimediaService.findMediaSubtypes().subscribe((response) => {
      this.mediaSubtypes = response.resources;
      this.loading = false;
    }, (error) => {
      console.log(error);
    }, () => {
      this.loading = false;
    });
  }

  filterMediaSubtypes(subtype: string): MediaSubtype[]{
    const filteredMediaSubtypes: MediaSubtype[] = [];
    if (this.mediaSubtypes){
      this.mediaSubtypes.forEach(elt => {
        if (elt.mediaType.label.toLowerCase() === subtype.toLowerCase()){
          filteredMediaSubtypes.push(elt);
        }
      });
    }
    return filteredMediaSubtypes;
  }

  getMediaTypeByPosition(position: number): string{
    switch (position) {
      case 1:
        return 'document';
      case 2:
        return 'link';
      case 3:
        return 'video';
      case 4:
        return 'picture';
      case 5:
        return 'audio';
      default:
        return 'document';
    }
  }

  onClickDismissModal(): void {
    this.modalService.dismissAll();
  }
}

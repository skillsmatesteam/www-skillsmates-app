import { Component, OnInit } from '@angular/core';
import {ModalDismissReasons, NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {InfoMediaType} from '../../../models/multimedia/infoMediaType';
import {MediaHelper} from '../../../helpers/media-helper';
import {Account} from '../../../models/account/account';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {MobileService} from '../../../services/mobile/mobile.service';

@Component({
  selector: 'app-createpost',
  templateUrl: './createpost.component.html',
  styleUrls: ['./createpost.component.css']
})
export class CreatepostComponent implements OnInit {

  infosMediaTypes: InfoMediaType[] = MediaHelper.mediaTypes;
  closeResult: string;
  modalOptions: NgbModalOptions;
  loading: boolean;
  loggedAccount: Account = {} as Account;
  public screenWidth: number = window.innerWidth;

  constructor(private modalService: NgbModal, private authenticateService: AuthenticateService, private mobileService: MobileService) { }

  ngOnInit(): void {
    this.loading = true;
    this.loggedAccount = this.authenticateService.getCurrentAccount();
  }

  open(content): void {
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }
}



import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSharePostComponent } from './modal-share-post.component';

describe('ModalAddMediaPostComponent', () => {
  let component: ModalSharePostComponent;
  let fixture: ComponentFixture<ModalSharePostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSharePostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSharePostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

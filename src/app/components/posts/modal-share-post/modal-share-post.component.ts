import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ModalDismissReasons, NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {Post} from '../../../models/post/post';
import {Account} from '../../../models/account/account';
import {Alert} from '../../../models/alert';
import {PostfileService} from '../../../services/pages/post/postfile.service';
import {PostService} from '../../../services/pages/post/post.service';
import {MultimediaService} from '../../../services/media/multimedia/multimedia.service';
import {Router} from '@angular/router';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {variables} from '../../../../environments/variables';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {SocialInteraction} from '../../../models/post/socialInteraction';
import {SocialInteractionService} from '../../../services/pages/social-interaction/social-interaction.service';
import {WebSocketService} from '../../../services/message/web-socket.service';
import {PostSocialInteractions} from '../../../models/post/postSocialInteractions';

@Component({
  selector: 'app-modal-share-post',
  templateUrl: './modal-share-post.component.html',
  styleUrls: ['./modal-share-post.component.css']
})
export class ModalSharePostComponent implements OnInit {
  @Input() postSocialInteractions: PostSocialInteractions = {} as PostSocialInteractions;
  @Output() newPostEvent = new EventEmitter<string>();
  post: Post = {} as Post;
  closeResult: string;
  modalOptions: NgbModalOptions;

  loading: boolean;
  account: Account;
  alert: Alert = {} as Alert;
  submitted = false;
  errors;
  processing = false;
  routes = RoutesHelper.routes;
  format: string;
  urlMedia: string;
  isMediaSelected = false;
  isUrlSelected = false;
  content: string;

  constructor(private modalService: NgbModal,
              private postfileService: PostfileService,
              private postService: PostService,
              private multimediaService: MultimediaService,
              private socialInteractionService: SocialInteractionService,
              private router: Router,
              private webSocketService: WebSocketService,
              private authenticateService: AuthenticateService) {
    this.modalOptions = {
      backdrop: 'static',
      backdropClass: 'customBackdrop',
      size: 'lg'
    };
  }
  ngOnInit(): void {
    this.initErrors();
    this.account = this.authenticateService.getCurrentAccount();
    this.post = this.postSocialInteractions.post;
  }

  open(content): void {
    this.isMediaSelected = false;
    this.isUrlSelected = false;

    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  onClickShare(): void {
    this.loading = true;
    if (this.validateContent()){
      let socialInteraction: SocialInteraction = {} as SocialInteraction;
      socialInteraction.element = this.post.idServer;
      socialInteraction.emitterAccount = this.account;
      socialInteraction.receiverAccount = this.post.account;
      socialInteraction.type = variables.share;
      socialInteraction.content = this.content;
      this.socialInteractionService.create(socialInteraction).subscribe((response) => {
        socialInteraction = response.resource;
        // notify via websocket
        this.webSocketService.sendPushNotification(socialInteraction);
        this.dismissModalShare();
      }, error => {
        this.dismissModalShare();
      }, () => {
        this.dismissModalShare();
      });
    }
  }

  dismissModalShare(): void{
    this.loading = false;
    this.newPostEvent.emit('refresh');
    this.modalService.dismissAll();
  }

  onClickDismissModal(): void {
    this.modalService.dismissAll();
  }

  computeYoutubeVideoWidth(): number{
    if (this.router.url.startsWith(this.routes.get('profile'))) {
      return 477;
    }else if (this.postSocialInteractions.shared){
      return 525;
    }else {
      return 565;
    }
  }

  initErrors(): void{
    this.submitted = false;
    this.errors = {valid: true, content: ''};
    this.alert = {} as Alert;
  }

  validateContent(): boolean{
    this.initErrors();
    if (this.content !== undefined && this.content.length > 2000){
      this.errors.valid = false;
      this.errors.body = 'Le nombre maximal de caractères est 2000';
    }

    this.submitted = true;
    return this.errors.valid;
  }
}

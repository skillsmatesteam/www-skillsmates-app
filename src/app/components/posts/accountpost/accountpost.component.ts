import {AfterViewInit, Component, ElementRef, HostListener, Input, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {Account} from '../../../models/account/account';
import {PostService} from '../../../services/pages/post/post.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {PostSocialInteractions} from '../../../models/post/postSocialInteractions';
import {SocialInteraction} from '../../../models/post/socialInteraction';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {SocialInteractionsHelper} from '../../../helpers/social-interactions-helper';

@Component({
  selector: 'app-accountpost',
  templateUrl: './accountpost.component.html',
  styleUrls: ['./accountpost.component.css']
})
export class AccountpostComponent implements OnInit, AfterViewInit{
  @Input() account: Account;
  @Input() isOnDashboard: boolean;
  @Input() isOnProfileView: boolean;
  @ViewChild('scrollFrame', {static: false}) scrollFrame: ElementRef;
  @ViewChildren('item') itemElements: QueryList<any>;
  @Input() accountId: string;
  private scrollContainer: any;
  postsSocialInteractions: PostSocialInteractions[] = [];
  loggedAccount: Account = {} as Account;
  loading: boolean;
  notEmptyPost = true;
  notScrolly = true;
  pageNumber = 0;
  node;

  @HostListener('window:scroll', ['$event'])
  onScrollEvent(event): void{
    if (this.notScrolly && this.notEmptyPost) {
      // this.scrollToBottom();
    }
  }

  constructor(private authenticateService: AuthenticateService, private postService: PostService, private spinner: NgxSpinnerService) {}

  ngOnInit(): void {
    this.loggedAccount = this.authenticateService.getCurrentAccount();
  }

  ngAfterViewInit(): void {
      this.findPostsByAccount();
  }

  getDOMElements(): void{
    this.scrollContainer = this.scrollFrame.nativeElement;
    this.itemElements.changes.subscribe(_ => this.onItemElementsChanged());
  }

  private onItemElementsChanged(): void {
    this.scrollToBottom();
  }

  private scrollToBottom(): void {
    this.scrollContainer.scroll({
      top: this.scrollContainer.scrollHeight,
      left: 0,
      behavior: 'smooth'
    });
  }

  findPostsByAccount(): void{
      this.loading = true;
      if (this.isOnDashboard){
        this.postService.findDashboardPostsByAccount(this.account.idServer, this.pageNumber).subscribe((response) => {
          this.pageNumber++;
          // this.processFetchedDashboardPostsSocialInteractions(response.resources);
          this.processFetchedPostsSocialInteractions(response.resources);
        }, (error) => {
          this.loading = false;
        }, () => {});
      }else if (this.isOnProfileView){ // visualisation des posts du profil précédemment copié
        this.loading = true;
        if(this.loggedAccount) { //online
          this.postService.findPostsByAccount(this.accountId, this.postsSocialInteractions.length).subscribe((response) => {
            this.processFetchedPostsSocialInteractions(response.resources);
          }, (error) => {
            this.loading = false;
          }, () => {
          });
        }else{ //offline
          this.postService.findPostsOfflineByAccount(this.accountId, 0).subscribe((response) => {
            this.processFetchedPostsSocialInteractions(response.resources);
          }, (error) => {
            this.loading = false;
          }, () => {
          });
        }

      }
        else {
        this.postService.findPostsByAccount(this.account.idServer, this.postsSocialInteractions.length).subscribe((response) => {
          this.processFetchedPostsSocialInteractions(response.resources);
        }, (error) => {
          this.loading = false;
        }, () => {
        });
      }
    }




  /**
   * process newly fetched posts
   * @param newPostSocialInteractions: fetched posts
   */
  processFetchedDashboardPostsSocialInteractions(newPostSocialInteractions: PostSocialInteractions[]): void{
    this.spinner.hide();
    if (newPostSocialInteractions && newPostSocialInteractions.length > 0){
      const dashboardPostsSocialInteractions: PostSocialInteractions[] = [];
      newPostSocialInteractions.forEach(postSocialInteraction => {
        dashboardPostsSocialInteractions.push(postSocialInteraction);
        const shares: SocialInteraction[] = SocialInteractionsHelper.retrieveShares(postSocialInteraction.socialInteractions);
        if (shares && shares.length > 0){
          shares.forEach(share => {
            const postShare: PostSocialInteractions = {} as PostSocialInteractions;
            postShare.id = postSocialInteraction.post.id;
            postShare.idServer = share.idServer;
            postShare.shared = true;
            postShare.post = postSocialInteraction.post;
            postShare.socialInteractions = postSocialInteraction.socialInteractions;
            postShare.shareSocialInteraction = share;
            dashboardPostsSocialInteractions.push(postShare);
          });
        }else {
          dashboardPostsSocialInteractions.push(postSocialInteraction);
        }
      });
      dashboardPostsSocialInteractions.sort((a, b) => (a.idServer >= b.idServer ? 1 : -1));
      this.postsSocialInteractions = this.postsSocialInteractions.concat(dashboardPostsSocialInteractions);
      this.notEmptyPost =  true;
    }else {
      this.notEmptyPost =  false;
    }
    this.loading = false;
    this.notScrolly = true;
    this.getDOMElements();
  }

  processFetchedPostsSocialInteractions(newPostSocialInteractions: PostSocialInteractions[]): void{
    this.spinner.hide();
    if (newPostSocialInteractions && newPostSocialInteractions.length > 0){
      this.postsSocialInteractions = this.postsSocialInteractions.concat(newPostSocialInteractions);
      this.notEmptyPost =  true;
    }else {
      this.notEmptyPost =  false;
    }
    this.loading = false;
    this.notScrolly = true;
    this.getDOMElements();
  }

  onRefreshEvent(newEvent: string): void {
    this.findPostsByAccount();
  }

  loadPosts(): void {
    this.findPostsByAccount();
  }

  onScroll(): void {
    if (this.notScrolly && this.notEmptyPost && this.loggedAccount) {
      this.spinner.show();
      this.notScrolly = false;
      this.findPostsByAccount();
    }
  }

  hasShared(socialInteractions: SocialInteraction[]): boolean{
    return SocialInteractionsHelper.hasShared(socialInteractions, this.loggedAccount);
  }

  haveBeenShared(socialInteractions: SocialInteraction[]): boolean{
    return SocialInteractionsHelper.countShares(socialInteractions) > 0;
  }
}

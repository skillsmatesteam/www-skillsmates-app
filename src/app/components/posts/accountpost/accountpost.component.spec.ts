import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountpostComponent } from './accountpost.component';

describe('AccountpostComponent', () => {
  let component: AccountpostComponent;
  let fixture: ComponentFixture<AccountpostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountpostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountpostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

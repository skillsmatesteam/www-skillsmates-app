import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {Account} from '../../../models/account/account';
import {Post} from '../../../models/post/post';
import {variables} from '../../../../environments/variables';
import {PostService} from '../../../services/pages/post/post.service';
import {ToastService} from '../../../services/toast/toast.service';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {Alert} from '../../../models/alert';
import {ActivatedRoute, Router} from '@angular/router';
import {RoutesHelper} from '../../../helpers/routes-helper';
import { DomSanitizer} from '@angular/platform-browser';
import {MetadataService} from '../../../services/media/metadata/metadata.service';
import {SocialInteractionService} from '../../../services/pages/social-interaction/social-interaction.service';
import {SocialInteraction} from '../../../models/post/socialInteraction';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {WebSocketService} from '../../../services/message/web-socket.service';
import {PostSocialInteractions} from '../../../models/post/postSocialInteractions';
import {SocialInteractionsHelper} from '../../../helpers/social-interactions-helper';
import {MobileService} from '../../../services/mobile/mobile.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit, OnChanges, OnDestroy {

  @Input() postSocialInteractions: PostSocialInteractions;
  @Input() isSharedPost: boolean;
  @Output() newPostEvent = new EventEmitter<string>();
  postToDelete: Post = {} as Post;
  submitted = false;
  errors;
  comment: SocialInteraction = {} as SocialInteraction;
  typedComment: string;
  maxCommentSize: number;
  loggedAccount: Account = {} as Account;
  alert: Alert = {} as Alert;
  comments: SocialInteraction[] = [];
  loading: boolean;
  post: Post = {} as Post;
  displayMoreLabel: string;
  toggleDiplayMoreLabel: boolean;
  id: string;
  routes = RoutesHelper.routes;
  displayCommentBlock = false;
  closeResult: string;
  shareLoading = false;

  constructor(private postService: PostService,
              private socialInteractionService: SocialInteractionService,
              private toastService: ToastService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private mobileService: MobileService,
              private sanitizer: DomSanitizer,
              private webSocketService: WebSocketService,
              private modalService: NgbModal,
              private metadataService: MetadataService,
              private authenticateService: AuthenticateService) {
  }

  ngOnInit(): void {
    this.initErrors();
    // this.mobileService.setNotificationDesign();
    this.loggedAccount = this.authenticateService.getCurrentAccount();
    this.id = this.activatedRoute.snapshot.paramMap.get(variables.id);
    if (!this.id){
      this.id = this.loggedAccount.idServer;
    }

    this.displayMoreLabel = 'Afficher la suite';
    this.toggleDiplayMoreLabel = true;
    this.maxCommentSize = variables.max_comment_size;
  }

  ngOnDestroy(): void {
    // this.mobileService.unsetNotificationDesign();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.post = changes.postSocialInteractions.currentValue !== undefined ? changes.postSocialInteractions.currentValue.post : {};
    // if (this.post && this.post.idServer){
    //   this.findComments(this.post.idServer);
    // }
  }

  onClickDelete(post: Post): void {
    this.postToDelete = post;
    localStorage.setItem('postToDelete', post.idServer);
  }

  onClickConfirmDelete(): void {
    this.postService.deletePost(localStorage.getItem('postToDelete')).subscribe((response) => {
      this.toastService.showSuccess('Succès', 'Publication supprimée avec succés');
      this.newPostEvent.emit('refresh');
    }, (error) => {
      this.toastService.showError('Error', 'Publication non supprimée');
    }, () => {
      localStorage.removeItem('postToDelete');
    });
  }

  /**
   * triggered when clicked on like
   */
  onClickLike(): void {
    if(this.loggedAccount){
      let socialInteraction: SocialInteraction = {} as SocialInteraction;
      socialInteraction.element = this.post.idServer;
      socialInteraction.emitterAccount = this.loggedAccount;
      socialInteraction.receiverAccount = this.post.account;
      socialInteraction.content = this.post.title;
      socialInteraction.type = variables.like;
      this.postSocialInteractions.socialInteractions.push(socialInteraction);

      this.socialInteractionService.create(socialInteraction).subscribe((response) => {
        socialInteraction = response.resource;
        // notify via websocket
        this.webSocketService.sendPushNotification(socialInteraction);
      }, error => {});
    }
  }

  /**
   * triggered when comment
   * @param e: keycode
   */
  keyupComment(e): void {
    this.errors.title = '';
    if (e.keyCode === 13){
      this.onClickPublishComment();
    }
  }

  /**
   * publish a comment
   */
  onClickPublishComment(): void{
    this.loading = true;
    this.comment.content = this.typedComment.trim();
    if (this.validateComment()){
      this.typedComment = '';
      this.comment.idServer = new Date().getTime().toString(10);
      this.comment.element = this.postSocialInteractions.post.idServer;
      this.comment.emitterAccount = this.loggedAccount;
      this.comment.receiverAccount = this.post.account;
      this.comment.type = variables.comment;
      this.comment.createdAt = new Date();
      if (!this.comments){
        this.comments = [];
      }
      this.comments.push(this.comment);
      this.postSocialInteractions.socialInteractions.push(this.comment);
      this.socialInteractionService.create(this.comment).subscribe((response) => {
        this.comment = response.resource;
        this.comment.content = this.post.title;
        // notify via websocket
        this.webSocketService.sendPushNotification(this.comment);
      }, error => {
      }, () => {
        this.loading = false;
      });
    }
  }

  /**
   * validate comment
   */
  validateComment(): boolean{
    this.initErrors();
    if (this.comment.content === undefined || this.comment.content === ''){
      this.errors.valid = false;
      this.errors.body = 'Le message est requis';
    }

    if (this.comment.content !== undefined && this.comment.content.length > variables.max_comment_size){
      this.errors.valid = false;
      this.errors.body = 'Le nombre maximal de caractères est ' + variables.max_comment_size;
    }

    this.submitted = true;
    return this.errors.valid;
  }

  initErrors(): void{
    this.submitted = false;
    this.errors = {valid: true, body: ''};
    this.alert = {} as Alert;
  }

  /**
   * find all comments for a given post
   * @param postId: post id
   */
  findComments(postId: string): void{
    // this.loading = true;
    // this.socialInteractionService.findCommentsByPost(postId).subscribe((response) => {
    //   this.comments = response.resources;
    //   this.displayCommentBlock = true;
    // }, (error) => {
    //   this.loading = false;
    // }, () => {
    //   this.loading = false;
    // });

    this.comments = SocialInteractionsHelper.retrieveComments(this.postSocialInteractions.socialInteractions);
  }

  removeCommentEvent(comment): void{
    console.log(comment);
    this.comments = this.comments.filter(obj => obj !== comment);
    this.postSocialInteractions.socialInteractions = SocialInteractionsHelper.removeSocialInteraction(this.postSocialInteractions.socialInteractions, comment);
  }

  onClickMore(): void {
    this.toggleDiplayMoreLabel = !this.toggleDiplayMoreLabel;
    this.displayMoreLabel = this.toggleDiplayMoreLabel ? 'Afficher la suite' : 'Afficher moins';
  }

  onClickComment(): void {
      this.displayCommentBlock = !this.displayCommentBlock;
      if (this.displayCommentBlock && this.postSocialInteractions && this.postSocialInteractions.post){
        this.findComments(this.postSocialInteractions.post.idServer);
      }
  }

  computeYoutubeVideoWidth(): number{
    return 565;
  }

  /**
   * Ouverture creer une publication
   */
  open_modal_publication(content): void {
    if(this.loggedAccount){
      this.shareLoading = true;
      this.modalService.open(content,
        {
          size: 'lg'
        }).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
        this.shareLoading = false;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        this.shareLoading = false;
      });
    }
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  private refresh(): void{
    this.newPostEvent.emit('refresh');
  }

  hasLiked(socialInteractions: SocialInteraction[]): boolean {
    if(this.loggedAccount)
      return SocialInteractionsHelper.hasLiked(socialInteractions, this.loggedAccount);
    else return false;
  }

  countLikes(socialInteractions: SocialInteraction[]): number {
    return SocialInteractionsHelper.countLikes(socialInteractions);
  }

  hasShared(socialInteractions: SocialInteraction[]): boolean {
    if(this.loggedAccount)
      return SocialInteractionsHelper.hasShared(socialInteractions, this.loggedAccount);
    else return false;
  }

  countShares(socialInteractions: SocialInteraction[]): number {
    return SocialInteractionsHelper.countShares(socialInteractions);
  }

  countComments(socialInteractions: SocialInteraction[]): number {
    return SocialInteractionsHelper.countComments(socialInteractions);
  }
}

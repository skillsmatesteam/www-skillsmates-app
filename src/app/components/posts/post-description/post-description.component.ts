import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Post} from '../../../models/post/post';
import {PostSocialInteractions} from '../../../models/post/postSocialInteractions';

@Component({
  selector: 'app-post-description',
  templateUrl: './post-description.component.html',
  styleUrls: ['./post-description.component.css']
})
export class PostDescriptionComponent implements OnInit, OnChanges {

  @Input() postSocialInteractions: PostSocialInteractions = {} as PostSocialInteractions;
  post: Post = {} as Post;
  displayMoreLabel: string;
  displayMoreIcon: string;
  toggleDiplayMoreLabel: boolean;

  constructor() { }

  ngOnInit(): void {
    this.displayMoreLabel = 'Afficher la suite';
    this.displayMoreIcon = 'fa fa-sort-desc';
    this.toggleDiplayMoreLabel = true;
    this.post = this.postSocialInteractions.post;
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.post = changes.postSocialInteractions.currentValue !== undefined ? changes.postSocialInteractions.currentValue.post : {};
  }

  onClickMore(): void {
    this.toggleDiplayMoreLabel = !this.toggleDiplayMoreLabel;
    this.displayMoreLabel = this.toggleDiplayMoreLabel ? 'Afficher la suite' : 'Afficher moins';
    this.displayMoreIcon = this.toggleDiplayMoreLabel ? 'fa fa-sort-desc' : 'fa fa-sort-asc';
  }

  explodeKeywords(keywords: string): string[]{
    if (keywords && keywords.length > 0){
      return keywords.trim().replace(/#/g, ',').replace(/;/g, ',').split(',');
    }
    return [];
  }
}

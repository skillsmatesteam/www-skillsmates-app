import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ModalDismissReasons, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {MediaHelper} from "../../helpers/media-helper";
import {InfoMediaType} from "../../models/multimedia/infoMediaType";
import {variables} from "../../../environments/variables";
import {RoutesHelper} from "../../helpers/routes-helper";

@Component({
  selector: 'app-modal-search',
  templateUrl: './modal-search.component.html',
  styleUrls: ['./modal-search.component.css']
})
export class ModalSearchComponent implements OnInit {
  closeResult: string;
  loading: boolean;
  infosMediaTypes = MediaHelper.mediaTypes;
  mediaTypeSelected = this.infosMediaTypes[0];
  routes = RoutesHelper.routes;

  constructor(private router: Router, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.infosMediaTypes = this.infosMediaTypes.concat(MediaHelper.profil).reverse();
  }

  open_modal_search(content): void {
    this.modalService.dismissAll();
    this.modalService.open(content,
      {
        centered: true,
        windowClass: 'publication-modal-2'
      }
    )
      .result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  onClickSearch(infoMediaType: InfoMediaType): void {
    this.modalService.dismissAll();
    localStorage.setItem(variables.current_media_type, JSON.stringify(infoMediaType));
    this.router.navigate([this.routes.get('searchResults')]);
  }
}

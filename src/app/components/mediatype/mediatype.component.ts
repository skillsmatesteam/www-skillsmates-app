import {Component, Input, OnInit} from '@angular/core';
import {InfoMediaType} from '../../models/multimedia/infoMediaType';
import {variables} from '../../../environments/variables';

@Component({
  selector: 'app-mediatype',
  templateUrl: './mediatype.component.html',
  styleUrls: ['./mediatype.component.css']
})
export class MediatypeComponent implements OnInit {
  @Input() infoMediaType;
  constructor() { }

  ngOnInit(): void {
  }

  onClickMore(m: InfoMediaType): void {
    localStorage.setItem(variables.tab, m.position.toString(10));
  }
}

import {Component, Input, OnInit} from '@angular/core';
import {MultimediaService} from '../../services/media/multimedia/multimedia.service';
import {MultimediaInfos} from '../../models/multimedia/multimediaInfos';
import {InfoMediaType} from '../../models/multimedia/infoMediaType';
import {ToastService} from '../../services/toast/toast.service';
import {Account} from '../../models/account/account';
import {MediaHelper} from '../../helpers/media-helper';

@Component({
  selector: 'app-accountmediatype',
  templateUrl: './accountmediatype.component.html',
  styleUrls: ['./accountmediatype.component.css']
})
export class AccountmediatypeComponent implements OnInit {

  @Input() account: Account;
  loading: boolean;
  @Input() isOnProfileView: boolean;
  @Input() accountId: string;
  multimediaInfos: MultimediaInfos = {} as MultimediaInfos;
  documentStyle: InfoMediaType = {} as InfoMediaType;
  videoStyle: InfoMediaType = {} as InfoMediaType;
  audioStyle: InfoMediaType = {} as InfoMediaType;
  linkStyle: InfoMediaType = {} as InfoMediaType;
  imageStyle: InfoMediaType = {} as InfoMediaType;
  infoMediaTypes: InfoMediaType[] = [];

  constructor(private multimediaService: MultimediaService) { }

  ngOnInit(): void {
    this.loading = true;

    this.findMediaType();
  }

  findMediaType(): void{
    this.loading = true;
    if (this.isOnProfileView){
      this.multimediaService.findMediaTypeOffline(this.accountId).subscribe((response) => {
        this.multimediaInfos = response.resource;
        this.generateMediaType();
      }, (error) => {
        this.loading = false;
      }, () => {
        this.loading = false;
      });
    } else {
      this.multimediaService.findMediaType(this.account.idServer).subscribe((response) => {
        this.multimediaInfos = response.resource;
        this.generateMediaType();
      }, (error) => {
        this.loading = false;
      }, () => {
        this.loading = false;
      });
    }

  }

  generateMediaType(): void{
    this.documentStyle = MediaHelper.mediaTypes[0]; // documents;
    this.documentStyle.value = this.multimediaInfos.nbDocuments;
    this.documentStyle.multimedia = this.multimediaInfos.documents;

    this.videoStyle = MediaHelper.mediaTypes[1]; // videos;;
    this.videoStyle.value = this.multimediaInfos.nbVideos;
    this.videoStyle.multimedia = this.multimediaInfos.videos;

    this.linkStyle = MediaHelper.mediaTypes[2];  // links
    this.linkStyle.value = this.multimediaInfos.nbLinks;
    this.linkStyle.multimedia = this.multimediaInfos.links;

    this.imageStyle = MediaHelper.mediaTypes[3]; // images
    this.imageStyle.value = this.multimediaInfos.nbImages;
    this.imageStyle.multimedia = this.multimediaInfos.images;

    this.audioStyle = MediaHelper.mediaTypes[4]; // audios;
    this.audioStyle.value = this.multimediaInfos.nbAudios;
    this.audioStyle.multimedia = this.multimediaInfos.audios;


  }

  sortMultimedias(): void{
    this.infoMediaTypes = [];

    let infoMedia: InfoMediaType = MediaHelper.mediaTypes[0]; // documents
    infoMedia.value = this.multimediaInfos.nbDocuments;
    infoMedia.multimedia = this.multimediaInfos.documents;
    this.infoMediaTypes.push(infoMedia);

    infoMedia = MediaHelper.mediaTypes[1]; // images
    infoMedia.value = this.multimediaInfos.nbImages;
    infoMedia.multimedia = this.multimediaInfos.images;
    this.infoMediaTypes.push(infoMedia);

    infoMedia = MediaHelper.mediaTypes[2]; // videos
    infoMedia.value = this.multimediaInfos.nbVideos;
    infoMedia.multimedia = this.multimediaInfos.videos;
    this.infoMediaTypes.push(infoMedia);

    infoMedia = MediaHelper.mediaTypes[3]; // audios
    infoMedia.value = this.multimediaInfos.nbAudios;
    infoMedia.multimedia = this.multimediaInfos.audios;
    this.infoMediaTypes.push(infoMedia);

    infoMedia = MediaHelper.mediaTypes[4];  // links
    infoMedia.value = this.multimediaInfos.nbLinks;
    infoMedia.multimedia = this.multimediaInfos.links;
    this.infoMediaTypes.push(infoMedia);

    this.loading = false;
  }

}

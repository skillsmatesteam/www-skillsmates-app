import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountmediatypeComponent } from './accountmediatype.component';

describe('AccountmediatypeComponent', () => {
  let component: AccountmediatypeComponent;
  let fixture: ComponentFixture<AccountmediatypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountmediatypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountmediatypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

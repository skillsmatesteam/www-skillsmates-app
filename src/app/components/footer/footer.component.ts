import {Component, HostListener, OnInit} from '@angular/core';
import {RoutesHelper} from '../../helpers/routes-helper';
import {Router} from '@angular/router';
import {MobileService} from '../../services/mobile/mobile.service';
import {variables} from '../../../environments/variables';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  routes = RoutesHelper.routes;
  constructor(private router: Router, private mobileService: MobileService) { }

  public screenWidth: number = window.innerWidth;

  ngOnInit(): void {
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    this.screenWidth = window.innerWidth;
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }

  onClickCGU(): void {
    localStorage.setItem('url', this.router.url);
  }

  onClickFAQ(): void {
    localStorage.setItem(variables.faq, variables.faq);
  }
}

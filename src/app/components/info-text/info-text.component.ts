import {Component, Input, OnInit} from '@angular/core';
import {MobileService} from "../../services/mobile/mobile.service";

@Component({
  selector: 'app-info-text',
  templateUrl: './info-text.component.html',
  styleUrls: ['./info-text.component.css']
})
export class InfoTextComponent implements OnInit {
  public screenWidth: number = window.innerWidth;
  @Input() text: string;
  constructor(private mobileService: MobileService) { }

  ngOnInit(): void {
  }
  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }
}

import { Component, OnInit } from '@angular/core';
import {RoutesHelper} from '../../helpers/routes-helper';
import { version } from '../../../../package.json';
import {Router} from '@angular/router';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  routes = RoutesHelper.routes;
  public version: string = version;
  year: number = new Date().getFullYear();

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  onClickCGU(): void {
    localStorage.setItem('url', this.router.url);
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SkillsmatesPartnerComponent } from './skillsmates-partner.component';

describe('SkillsmatesPartnerComponent', () => {
  let component: SkillsmatesPartnerComponent;
  let fixture: ComponentFixture<SkillsmatesPartnerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SkillsmatesPartnerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillsmatesPartnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

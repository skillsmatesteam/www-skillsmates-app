import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-skillsmates-partner',
  templateUrl: './skillsmates-partner.component.html',
  styleUrls: ['./skillsmates-partner.component.css']
})
export class SkillsmatesPartnerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  onClickOpenPartner(url: string): void {
    window.open(url, '_blank');
  }
}

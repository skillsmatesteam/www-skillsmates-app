import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentPreviewMobileComponent } from './document-preview-mobile.component';

describe('DocumentPreviewMobileComponent', () => {
  let component: DocumentPreviewMobileComponent;
  let fixture: ComponentFixture<DocumentPreviewMobileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DocumentPreviewMobileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentPreviewMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

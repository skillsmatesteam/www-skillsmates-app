import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalHeaderPlusComponent } from './modal-header-plus.component';

describe('ModalHeaderPlusComponent', () => {
  let component: ModalHeaderPlusComponent;
  let fixture: ComponentFixture<ModalHeaderPlusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalHeaderPlusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalHeaderPlusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

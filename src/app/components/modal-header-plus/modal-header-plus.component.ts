import { Component, OnInit } from '@angular/core';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {MediaHelper} from '../../helpers/media-helper';
import {variables} from '../../../environments/variables';
import {RoutesHelper} from '../../helpers/routes-helper';
import {Router} from '@angular/router';
import {InfoMediaType} from '../../models/multimedia/infoMediaType';

@Component({
  selector: 'app-modal-header-plus',
  templateUrl: './modal-header-plus.component.html',
  styleUrls: ['./modal-header-plus.component.css']
})
export class ModalHeaderPlusComponent implements OnInit {

  closeResult: string;
  loading: boolean;
  infosMediaTypes = MediaHelper.mediaTypes;
  mediaTypeSelected = this.infosMediaTypes[0];
  routes = RoutesHelper.routes;

  constructor(private router: Router, private modalService: NgbModal) { }

  ngOnInit(): void {
  }

  // Ouverture creer une publication
  open_modal_publication(content): void {
    this.modalService.dismissAll();
    this.modalService.open(content,
      {
        centered: true,
        windowClass: 'publication-modal-2'
      }
    )
      .result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  // Ouverture du second modal pour charger un media
  open_modal_media(content, infosMediaType ): void {
    this.mediaTypeSelected = infosMediaType; // selection d'un media
    this.modalService.dismissAll(); // fermeture du premier modal
    this.modalService.open(content,
      {
        centered: true,
        windowClass: 'publication-modal-2'
      }
    )
      .result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  onClickEditPost(infoMediaType: InfoMediaType): void {
    this.modalService.dismissAll();
    localStorage.setItem(variables.current_media_type, JSON.stringify(infoMediaType));
    this.router.navigate([this.routes.get('post.edit.my')]);
  }
}

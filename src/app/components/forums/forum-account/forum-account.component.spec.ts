import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForumAccountComponent } from './forum-account.component';

describe('ForumAccountComponent', () => {
  let component: ForumAccountComponent;
  let fixture: ComponentFixture<ForumAccountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ForumAccountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ForumAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

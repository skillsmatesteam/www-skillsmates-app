import {Component, Input, OnInit} from '@angular/core';
import {Account} from '../../../models/account/account';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {StringHelper} from '../../../helpers/string-helper';
import {Forum} from '../../../models/forum';

@Component({
  selector: 'app-forum-account',
  templateUrl: './forum-account.component.html',
  styleUrls: ['./forum-account.component.css']
})
export class ForumAccountComponent implements OnInit {

  @Input() forum: Forum;
  @Input() isLastOne: boolean;
  routes = RoutesHelper.routes;
  loggedAccount: Account;

  constructor(private authenticateService: AuthenticateService) { }

  ngOnInit(): void {
    this.loggedAccount = this.authenticateService.getCurrentAccount();
  }

  public name(account: Account): string{
    return StringHelper.truncateName(account, 18);
  }
}

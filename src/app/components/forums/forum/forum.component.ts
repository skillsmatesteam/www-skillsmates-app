import {Component, Input, OnInit} from '@angular/core';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {Account} from '../../../models/account/account';
import {Forum} from '../../../models/forum';
import {StringHelper} from '../../../helpers/string-helper';
import {AccountSocialInteractions} from '../../../models/account/accountSocialInteractions';

@Component({
  selector: 'app-forum',
  templateUrl: './forum.component.html',
  styleUrls: ['./forum.component.css']
})
export class ForumComponent implements OnInit {

  @Input() accountsSocialInteractions: AccountSocialInteractions[] = [];
  routes = RoutesHelper.routes;
  forums: Forum[] = [];
  constructor() { }

  ngOnInit(): void {
    this.initForum();
  }

  initForum(): void{
    let forum: Forum = {} as Forum;
    forum.account = this.accountsSocialInteractions[StringHelper.generateRandomNumber(0, this.accountsSocialInteractions.length)].account;
    forum.discipline = 'Informatique';
    forum.description = 'How to convert image to URI to Byte[] (Byte array) in React Native?';
    forum.nb = 24;
    this.forums.push(forum);

    forum = {} as Forum;
    forum.account = this.accountsSocialInteractions[StringHelper.generateRandomNumber(0, this.accountsSocialInteractions.length)].account;
    forum.discipline = 'Finance';
    forum.description = 'Comment calculer le délai de recuperation d\'un investissement?';
    forum.nb = 8;
    this.forums.push(forum);

    forum = {} as Forum;
    forum.account = this.accountsSocialInteractions[StringHelper.generateRandomNumber(0, this.accountsSocialInteractions.length)].account;
    forum.discipline = 'Energie';
    forum.description = 'Comment accelerer la bio-decomposition des dechets organiques à partir d\'algues?';
    forum.nb = 18;
    this.forums.push(forum);
  }

}

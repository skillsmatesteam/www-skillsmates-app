import {Component, Input, OnInit} from '@angular/core';
import {Account} from '../../../models/account/account';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {Selection} from '../../../models/selection';
import {StringHelper} from '../../../helpers/string-helper';
import {AccountSocialInteractions} from '../../../models/account/accountSocialInteractions';

@Component({
  selector: 'app-selections',
  templateUrl: './selections.component.html',
  styleUrls: ['./selections.component.css']
})
export class SelectionsComponent implements OnInit {

  @Input() accountsSocialInteractions: AccountSocialInteractions[] = [];
  routes = RoutesHelper.routes;
  selections: Selection[] = [];
  constructor() { }

  ngOnInit(): void {
    this.initSelections();
  }

  initSelections(): void {
    let selection: Selection = {} as Selection;
    selection.account = this.accountsSocialInteractions[StringHelper.generateRandomNumber(0, this.accountsSocialInteractions.length)].account;
    selection.description = 'Optimisation du rangement par la methode des 5S';
    selection.image = 'pdf';
    selection.liked = 12;
    selection.shared = 28;
    selection.comments = 87;
    selection.downloads = 6;
    this.selections.push(selection);

    selection = {} as Selection;
    selection.account = this.accountsSocialInteractions[StringHelper.generateRandomNumber(0, this.accountsSocialInteractions.length)].account;
    selection.description = 'Pourquoi Bill Gates soutient-il la cryptomonnaie XRP Ripple?';
    selection.image = 'video-rouge';
    selection.liked = 6;
    selection.shared = 45;
    selection.comments = 12;
    selection.downloads = 8;
    this.selections.push(selection);

    selection = {} as Selection;
    selection.account = this.accountsSocialInteractions[StringHelper.generateRandomNumber(0, this.accountsSocialInteractions.length)].account;
    selection.description = 'Illustration du theoreme de Pythagore';
    selection.image = 'photo-vert';
    selection.liked = 7;
    selection.shared = 75;
    selection.comments = 63;
    selection.downloads = 9;
    this.selections.push(selection);
  }
}

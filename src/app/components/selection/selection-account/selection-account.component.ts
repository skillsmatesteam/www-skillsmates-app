import {Component, Input, OnInit} from '@angular/core';
import {Account} from '../../../models/account/account';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {StringHelper} from '../../../helpers/string-helper';
import {Selection} from '../../../models/selection';

@Component({
  selector: 'app-selection-account',
  templateUrl: './selection-account.component.html',
  styleUrls: ['./selection-account.component.css']
})
export class SelectionAccountComponent implements OnInit {

  @Input() selection: Selection;
  @Input() isLastOne: boolean;
  routes = RoutesHelper.routes;
  loggedAccount: Account;

  constructor(private authenticateService: AuthenticateService) { }

  ngOnInit(): void {
    this.loggedAccount = this.authenticateService.getCurrentAccount();
  }

  public name(account: Account): string{
    return StringHelper.truncateName(account, 18);
  }

}

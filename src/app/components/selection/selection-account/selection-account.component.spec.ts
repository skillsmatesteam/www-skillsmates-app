import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectionAccountComponent } from './selection-account.component';

describe('SelectionAccountComponent', () => {
  let component: SelectionAccountComponent;
  let fixture: ComponentFixture<SelectionAccountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectionAccountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectionAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

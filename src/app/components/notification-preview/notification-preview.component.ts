import {Component, Input, OnInit} from '@angular/core';
import {Notification} from "../../models/notification/notification";

@Component({
  selector: 'app-notification-preview',
  templateUrl: './notification-preview.component.html',
  styleUrls: ['./notification-preview.component.css']
})
export class NotificationPreviewComponent implements OnInit {

  @Input() notification: Notification ;

  constructor() { }

  ngOnInit(): void {

  }
  generateTitle(notification: Notification): string{
    let title  = '';
    switch (notification.type) {
      case 'LIKE':
        title =  ' a aimé votre publication';
        break;
      case 'COMMENT':
        title = ' a commenté votre publication';
        break;
      case 'FOLLOWER':
        title = ' est votre nouvel abonné';
        break;
      case 'SHARE':
        title =  ' a partagé votre publication';
        break;
      case 'FAVORITE':
        title = ' vous a mis dans ses favoris';
        break;
      case 'MESSAGE':
        title = ' vous a envoyé un message';
        break;
      case 'POST':
        title = ' a publié un nouvel article';
        break;
      default:
        break;
    }
    return title;
  }

}

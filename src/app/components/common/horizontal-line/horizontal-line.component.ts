import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-horizontal-line',
  templateUrl: './horizontal-line.component.html',
  styleUrls: ['./horizontal-line.component.css']
})
export class HorizontalLineComponent implements OnInit {

  @Input() color: string;
  @Input() height: number;
  constructor() {
    if (!this.color){
      this.color = '#FFFFFF';
    }

    if (!this.height || this.height <= 0 ){
      this.height = 1;
    }
  }

  ngOnInit(): void {
  }

}

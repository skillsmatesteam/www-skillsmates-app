import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Account} from '../../../models/account/account';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {SubscriptionService} from '../../../services/accounts/subscription/subscription.service';
import {Router} from '@angular/router';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {Observable} from 'rxjs';
import {SocialInteraction} from '../../../models/post/socialInteraction';
import {SocialInteractionService} from '../../../services/pages/social-interaction/social-interaction.service';
import {SocialInteractionsHelper} from '../../../helpers/social-interactions-helper';
import {variables} from '../../../../environments/variables';
import {ToastService} from '../../../services/toast/toast.service';
import {WebSocketService} from '../../../services/message/web-socket.service';
import {AccountSocialInteractions} from '../../../models/account/accountSocialInteractions';

@Component({
  selector: 'app-subscription-button',
  templateUrl: './subscription-button.component.html',
  styleUrls: ['./subscription-button.component.css']
})
export class SubscriptionButtonComponent implements OnInit {

  @Input() accountSocialInteractions: AccountSocialInteractions;
  @Input() returnUrl: string;
  loggedAccount: Account;
  routes = RoutesHelper.routes;
  loading: boolean;
  display = true;
  constructor(private socialInteractionService: SocialInteractionService,
              private router: Router,
              private toastService: ToastService,
              private webSocketService: WebSocketService,
              private authenticateService: AuthenticateService) { }

  ngOnInit(): void {
    this.loading = false;
    this.loggedAccount = this.authenticateService.getCurrentAccount();
  }

  onClickFollow(account: Account): void {
    this.loading = true;
    this.socialInteractionService.addAccountSocialInteraction(this.loggedAccount, account, variables.follower).subscribe((response) => {
      const socialInteraction = response.resource;
      if (socialInteraction.active) {
        this.toastService.showSuccess('Abonné', 'Vous êtes à présent abonné à ' + account.firstname + ' ' + account.lastname);
      } else {
        this.toastService.showSuccess('Abonné', 'Vous vous êtes désabonné de ' + account.firstname + ' ' + account.lastname);
      }
      this.display = false;
      this.isFollowing(this.accountSocialInteractions.account);
      this.loading = false;
      // send a notification
      this.webSocketService.sendPushNotification(socialInteraction);
    }, error => {
      this.loading = false;
      this.toastService.showSuccess('Abonné', 'Erreur lors du traitement');
    });
  }

  onClickUnfollow(account: Account): void {
    this.loading = true;
    // const socialInteraction = SocialInteractionsHelper.retrieveSocialInteractionsWithCriteria(this.accountSocialInteractions.socialInteractions, account.idServer, this.loggedAccount, account, variables.follower);
    // this.accountSocialInteractions.socialInteractions = SocialInteractionsHelper.removeSocialInteraction(this.accountSocialInteractions.socialInteractions, socialInteraction);
    // this.socialInteractionService.delete(socialInteraction.idServer).subscribe((response) => {
    //   this.toastService.showSuccess('Abonné', 'Vous vous êtes désabonné de ' + account.firstname + ' ' + account.lastname);
    // }, (error) => {
    //   this.toastService.showSuccess('Abonné', 'Erreur lors du traitement');
    // });
    this.loading = false;
  }

  isFollowing(receiver: Account): boolean{
    return SocialInteractionsHelper.isFollowing(this.accountSocialInteractions.socialInteractions, this.loggedAccount, receiver);
  }
}

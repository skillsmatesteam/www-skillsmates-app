import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Account} from '../../models/account/account';
import {MobileService} from '../../services/mobile/mobile.service';
import {StringHelper} from '../../helpers/string-helper';
import {Message} from '../../models/message/message';
import {WebcamImage, WebcamInitError} from 'ngx-webcam';
import {Observable, Subject} from 'rxjs';
import {SocialInteraction} from '../../models/post/socialInteraction';
import {NotificationHelper} from '../../helpers/notification-helper';
import {AuthenticateService} from '../../services/accounts/authenticate/authenticate.service';
import {MessageService} from '../../services/message/message.service';
import {WebSocketService} from '../../services/message/web-socket.service';
import {variables} from '../../../environments/variables';
import {MimetypeHelper} from '../../helpers/mimetype-helper';
import {ToastService} from '../../services/toast/toast.service';
import {RoutesHelper} from '../../helpers/routes-helper';

@Component({
  selector: 'app-header-chat',
  templateUrl: './header-chat.component.html',
  styleUrls: ['./header-chat.component.css']
})
export class HeaderChatComponent implements OnInit {
  @Input() selectedAccount: Account = {} as Account;
  @Output() arrowBackClicked = new EventEmitter<boolean>();

  @ViewChild('fileDropRef', { static: false }) fileDropEl: ElementRef;
  files: any[] = [];
  filePictures: any[] = [];
  @ViewChild('closebutton') closebutton;
  @ViewChild('closebutton_picture') closebuttonPicture;
  // webcam snapshot trigger
  private trigger: Subject<void> = new Subject<void>();
  public showWebcam = false;
  public allowCameraSwitch = true;
  public multipleWebcamsAvailable = false;
  public deviceId: string;
  routes = RoutesHelper.routes;
  public videoOptions: MediaTrackConstraints = {
    // width: {ideal: 1024},
    // height: {ideal: 576}
  };
  public errorsWebcam: WebcamInitError[] = [];

  // latest snapshot
  public webcamImage: WebcamImage = null;
// switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId
  private nextWebcam: Subject<boolean|string> = new Subject<boolean|string>();
  message: Message = {} as Message;
  loggedAccount: Account = {} as Account;
  messages: Message[] = [];
  errors;

  constructor(private mobileService: MobileService,
              private messageService: MessageService,
              private webSocketService: WebSocketService,
              private toastService: ToastService,
              private authenticateService: AuthenticateService) { }

  ngOnInit(): void {
    this.loggedAccount = this.authenticateService.getCurrentAccount();
  }

  onClickArrowLeft(): void {
    // this.mobileService.unsetSkillschatDesign();
    // window.location.reload();
    this.arrowBackClicked.emit(true);
  }

  public name(account: Account): string{
    return StringHelper.truncateName(account, 20);
  }




  onClickCameraUp(): void {
    this.showWebcam = true;
  }

  onClickCameraDown(): void {
    this.showWebcam = false;
  }

  onClickPhone(account: Account): void {
    if (account.phoneNumber) {
      window.location.href = 'tel:' + account.phoneNumber;
    }
    // window.open(window.location.href, '_system'); // TODO : à inclure pour les iPhones
  }

  public triggerSnapshot(): void {
    this.trigger.next();
    if (this.webcamImage) {
      this.filePictures = [];

      const blob = this.dataURItoBlob(this.webcamImage.imageAsDataUrl);
      this.filePictures.push(new File([blob], 'my_picture.jpg'));

      this.uploadFile(this.filePictures);

      this.message = {} as Message;

      if (this.filePictures != null && this.filePictures.length > 0){
        this.message.receiverAccount = this.selectedAccount;
        this.message.emitterAccount =  this.loggedAccount;
        this.message.createdAt = new Date();

        this.sendMediaMessage(this.filePictures, this.message);
        // this.showWebcam = false;
      }
    }
  }

  dataURItoBlob(dataURI): any {
    const byteString = atob(dataURI.split(',')[1]);

    // separate out the mime component
    const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to an ArrayBuffer
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ab], {type: mimeString});
    // return new Blob([ab], {type: mimeString});
  }

  public handleImage(webcamImage: WebcamImage): void {
    console.log('received webcam image', webcamImage);
    this.webcamImage = webcamImage;
  }

  public cameraWasSwitched(deviceId: string): void {
    console.log('active device: ' + deviceId);
    this.deviceId = deviceId;
  }

  public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }

  public get nextWebcamObservable(): Observable<boolean|string> {
    return this.nextWebcam.asObservable();
  }

  public handleInitError(error: WebcamInitError): void {
    this.errorsWebcam.push(error);
  }

  onClickSendMediaMessage(): void {
    this.message = {} as Message;
    if (this.files != null && this.files.length > 0){
      this.message.receiverAccount = this.selectedAccount;
      this.message.emitterAccount =  this.loggedAccount;
      this.message.createdAt = new Date();

      this.sendMediaMessage(this.files, this.message);
    }
  }

  sendMediaMessage(files: Array<any>, message: Message): void {
    // this.loading = true;
    for (const item of files) {
      item.progress = 0;
      item.loading = true;
    }
    this.messageService.sendMediaMessage(files, message).subscribe((response) => {
      this.message = response.resource;
      this.messages.push(this.message);
      // create a social interaction for notification
      const socialInteraction: SocialInteraction = NotificationHelper.convertMessageToSocialInteraction(this.message);
      // via websocket
      this.webSocketService.sendPushNotification(socialInteraction);

      this.files = [];
      for (const item of files) {
        item.progress = 0;
        item.loading = false;
      }
      this.closebutton.nativeElement.click();
      this.closebuttonPicture.nativeElement.click();
    }, (error) => {
      this.toastService.showError('Erreur', 'Erreur lors de l\'envoi du média');
      // this.loading = false;
      for (const item of files) {
        item.progress = 0;
        item.loading = false;
      }
      this.closebutton.nativeElement.click();
      this.closebuttonPicture.nativeElement.click();
    }, () => {});
  }

  // validateUrlLink(url: string): boolean{
  //   this.isValideUrl = StringHelper.validateUrl(url);
  //   return this.isValideUrl;
  // }

  formatBytes(size: any): string {
    return StringHelper.formatBytes(size);
  }

  /*
 * Drag and drop files
 */
  uploadFile(files: Array<any>): void {
    this.prepareFilesList(files);
  }

  deleteAttachment(index: any): void {
    this.files.splice(index, 1);
  }

  prepareFilesList(files: Array<any>): void {
    for (const item of files) {
      if (!this.validateFileSize(item)){
        this.toastService.showError('Erreur', 'La taille maximale est ' + variables.max_file_size + 'MB');
      }else if (!this.validateFileMimetype(item)){
        this.toastService.showError('Erreur', 'Ce type de document n\'est pas accepté');
      } else{
        // item.progress = 0;
        // item.loading = true;
        this.files.push(item);
      }
    }
    this.fileDropEl.nativeElement.value = '';
  }

  validateFileSize(file): boolean{
    if (file && file.size > StringHelper.getMaxFileSize()){
      this.errors.valid = false;
      this.errors.file = 'La taille maximale est ' + variables.max_file_size + 'MB';
      return false;
    }
    return true;
  }

  validateFileMimetype(file): boolean{
    if (!MimetypeHelper.isMimetypeAllowed(file.type)){
      this.errors.valid = false;
      this.errors.file = 'Ce type de document n\'est pas accepté';
      return false;
    }
    return true;
  }

  getEstablishmentIcon(): string{
    return StringHelper.getEstablishmentIcon(this.selectedAccount);
  }

  isSkillschatDesign(): boolean{
    return this.mobileService.isSkillschatDesign();
  }

  isNotificationDesign(): boolean{
    return this.mobileService.isNotificationDesign();
  }
}

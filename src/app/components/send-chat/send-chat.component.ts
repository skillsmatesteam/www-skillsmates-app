import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Alert} from '../../models/alert';
import {Message} from '../../models/message/message';
import {SocialInteraction} from '../../models/post/socialInteraction';
import {NotificationHelper} from '../../helpers/notification-helper';
import {variables} from '../../../environments/variables';
import {Multimedia} from '../../models/multimedia/multimedia';
import {Account} from '../../models/account/account';
import {MessageService} from '../../services/message/message.service';
import {WebSocketService} from '../../services/message/web-socket.service';
import {ToastService} from '../../services/toast/toast.service';
import {MobileService} from '../../services/mobile/mobile.service';
import {AuthenticateService} from '../../services/accounts/authenticate/authenticate.service';
import {UtilityService} from '../../services/utility/utility.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-send-chat',
  templateUrl: './send-chat.component.html',
  styleUrls: ['./send-chat.component.css']
})
export class SendChatComponent implements OnInit, OnDestroy {

  @Input() selectedAccount: Account = {} as Account;
  loggedAccount: Account = {} as Account;
  typedMessage: string;
  alert: Alert = {} as Alert;
  submitted = false;
  errors;
  multimedia: Multimedia = {} as Multimedia;
  loading: boolean;
  content: string;
  message: Message = {} as Message;
  subscription: Subscription;

  constructor(private messageService: MessageService,
              private webSocketService: WebSocketService,
              private toastService: ToastService,
              private mobileService: MobileService,
              private authenticateService: AuthenticateService,
              private utilityService: UtilityService)
  {}

  ngOnInit(): void {
    this.loggedAccount = this.authenticateService.getCurrentAccount();
    this.initErrors();
    this.subscription = this.utilityService.subjectSelectedAccount$.subscribe(
      (account) => {
        if (account){
          this.message = {} as Message;
          this.message.receiverAccount = account;
        }
      }
    );
  }

  keyupMessage(e): void {
    this.errors.title = '';
    if (e.keyCode === 13){
      this.onClickPublishMessage();
    }
  }

  onClickPublishMessage(): void{
    this.loading = true;
    // this.message = {} as Message;
    if (this.typedMessage) {
      this.message.content = this.typedMessage.trim();
    }
    this.typedMessage = '';
    if (this.validateMessage()){
      // this.message.receiverAccount = this.selectedAccount;
      this.message.emitterAccount = this.loggedAccount;
      this.message.createdAt = new Date();
      // this.messages.push(this.message);

      this.messageService.saveMessage(this.message).subscribe((response) => {
        this.message = response.resource;
        // create a social interaction for notification
        const socialInteraction: SocialInteraction = NotificationHelper.convertMessageToSocialInteraction(this.message);
        // via websocket
        this.webSocketService.sendPushNotification(socialInteraction);
        // refresh list of accounts to push this account on top
         // this.refreshAccounts.emit(this.message);
        this.utilityService.sendMessage(this.message);
      }, (error) => {

      });
    }else {
      this.toastService.showError('Echec !', this.errors.content);
      this.loading = false;
    }
  }

  initErrors(): void{
    this.submitted = false;
    this.errors = {valid: true, content: '', title: '', file: ''};
    this.alert = {} as Alert;
  }

  validateMessage(): boolean{
    this.initErrors();
    if (this.message.content === undefined || this.message.content === ''){
      this.errors.valid = false;
      this.errors.content = 'Veuillez entrer un message';
    }

    else if (this.message.content !== undefined && this.message.content.length > variables.max_title_size) {
      this.errors.valid = false;
      this.errors.content  = 'Le nombre maximal de caractères est ' + variables.max_title_size;
    }

    this.submitted = true;
    return this.errors.valid;
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }
}

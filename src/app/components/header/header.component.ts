import {
  Component,
  HostListener,
  OnDestroy,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import { Account } from '../../models/account/account';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticateService } from '../../services/accounts/authenticate/authenticate.service';
import { RoutesHelper } from '../../helpers/routes-helper';
import { NotificationService } from '../../services/notification/notification.service';
import { Notifications } from '../../models/notification/notifications';
import { Notification } from '../../models/notification/notification';
import { Message } from '../../models/message/message';
import { StringHelper } from '../../helpers/string-helper';
import { ActivatedRoute, Router } from '@angular/router';
import { variables } from '../../../environments/variables';
import { WebSocketService } from '../../services/message/web-socket.service';
import { MessageService } from '../../services/message/message.service';
import { SocialInteractionService } from '../../services/pages/social-interaction/social-interaction.service';
import { NotificationHelper } from '../../helpers/notification-helper';
import { SocialInteraction } from '../../models/post/socialInteraction';
import { Subscription } from 'rxjs';
import { MediaHelper } from '../../helpers/media-helper';
import { InfoMediaType } from '../../models/multimedia/infoMediaType';
import { AccountMessage } from '../../models/message/accountMessage';
import { MobileService } from '../../services/mobile/mobile.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  infosMediaTypes = MediaHelper.mediaTypes;
  loggedAccount: Account = {} as Account;
  closeResult: string;
  routes = RoutesHelper.routes;
  notifications: Notifications = {} as Notifications;
  bellNotifications: Notification[] = [];
  account: Account = {} as Account;
  messages: Message[] = [];
  dropdownProfileMenu = false;
  dropdownSearchMenu = false;
  subscription: Subscription;
  mediaTypes = MediaHelper.mediaTypes;
  searchImageIcon = 'search';
  messageImageIcon = 'message';
  plusImageIcon = 'plus-mobile';
  homeImageIcon = 'home';
  bellImageIcon = 'bell';
  networkImageIcon = 'network';
  selectedImageIcon = 'home';
  selectedSkillschatIcon = 'skillschat-inactive';
  allNotifications: Notification[] = [];
  accountsMessages: AccountMessage[] = [];
  loading: boolean;
  allAccountsMessages: AccountMessage[] = [];
  currentMessage: Message;
  textToSearch: string;
  showModalPost = false;
  showModalAccount = false;
  public screenWidth: number = window.innerWidth;
  displayPopupMessage = false;
  displayPopupBell = false;
  displayPopupNetwork = false;

  constructor(
    private messageService: MessageService,
    private modalService: NgbModal,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private chatService: MessageService,
    private socialInteractionService: SocialInteractionService,
    private authenticateService: AuthenticateService,
    private notificationService: NotificationService,
    private webSocketService: WebSocketService,
    private mobileService: MobileService
  ) {}

  ngOnInit(): void {
    this.onMouseLeave(this.selectedImageIcon);
    this.mediaTypes = [MediaHelper.profil].concat(this.mediaTypes);
    this.loggedAccount = this.authenticateService.getCurrentAccount();

    this.findMyMessages();

    if (this.loggedAccount) {
      // open websocket
      this.webSocketService.openWebSocket(this.loggedAccount.idServer);

      // find all notifications
      this.findNotifications();

      this.webSocketService.pushNotificationWebSocket.subscribe((current) => {
        if (current) {
          const socialInteraction: SocialInteraction = JSON.parse(current);
          if (socialInteraction) {
            this.onPushNotificationReceived(socialInteraction);

            if (socialInteraction.type === variables.message) {
              this.currentMessage =
                NotificationHelper.convertSocialInteractionToMessage(
                  socialInteraction
                );
              this.updateAccountMessage(this.currentMessage);
            }
          }
        }
      });

      this.subscription =
        this.notificationService.notificationMessage.subscribe(
          (notifications) => {
            this.removeNotificationsFromNotificationsSet(notifications);
          }
        );
    } else {
      this.router.navigate([this.routes.get('root')]);
    }

    localStorage.setItem(variables.text_to_search, '');

    if (this.router.url.indexOf('messages') >= 0){
      this.selectedSkillschatIcon = 'skillschat-active';
    }else {
      this.selectedSkillschatIcon = 'skillschat-inactive';
    }
  }

  getSkillsChatIcon(): string{
     if (window.location.pathname.indexOf('messages') > 0){
       return 'skillschat-active';
     }else {
       return 'skillschat-inactive';
     }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    this.screenWidth = window.innerWidth;
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }

  isSkillschatDesign(): boolean{
    return this.mobileService.isSkillschatDesign();
  }

  isNotificationDesign(): boolean{
    return this.mobileService.isNotificationDesign();
  }

  // Ouverture du premier modal
  open_modal_principal(content): void {
    this.modalService
      .open(content, {
        centered: true,
        windowClass: 'publication-modal',
      })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  onClickNotification(notification: Notification): void {}
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  public name(): string {
    return StringHelper.truncateName(this.loggedAccount, 18);
  }

  findNotifications(): void {
    this.notificationService.findNotifications().subscribe(
      (response) => {
        this.notifications = response.resource;
        this.bellNotifications = NotificationHelper.sortBellNotifications(
          this.notifications
        );
        this.allNotifications = NotificationHelper.sortAllNotifications(
          this.notifications
        );
      },
      (error) => {},
      () => {}
    );
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  getTab(): string {
    return StringHelper.urlToTab(this.router.url);
  }

  getTabV2(): string {
    return window.location.pathname;
  }

  onClickProfile(): void {
    this.dropdownProfileMenu = true;
  }

  onClickSearch(): void {
    localStorage.setItem(variables.text_to_search, this.textToSearch);
    this.router.navigate([this.routes.get('search_results')]);
  }

  onClickProfileMenu(): void {
    this.showModalAccount = false;
    this.showModalPost = false;
    this.dropdownProfileMenu = false;
    this.resetImageIcon();
  }

  /**
   * put socialInteraction into the correct notifications set
   * @param socialInteraction: received socialInteraction
   */
  onPushNotificationReceived(socialInteraction: SocialInteraction): void {
    const notification: Notification =
      NotificationHelper.convertSocialInteractionToNotification(
        socialInteraction
      );
    switch (socialInteraction.type) {
      case variables.like:
        this.notifications.likes.push(notification);
        break;
      case variables.comment:
        this.notifications.comments.push(notification);
        break;
      case variables.share:
        this.notifications.shares.push(notification);
        break;
      case variables.message:
        this.notifications.messages.push(notification);
        break;
      case variables.favorite:
        this.notifications.favorites.push(notification);
        break;
      case variables.follower:
        this.notifications.followers.push(notification);
        break;
      case variables.post:
        this.notifications.posts.push(notification);
        break;
      default:
        break;
    }
    this.notifications.followers.sort(NotificationHelper.compare);
    this.bellNotifications = NotificationHelper.sortBellNotifications(
      this.notifications
    );
    this.allNotifications = NotificationHelper.sortAllNotifications(
      this.notifications
    );
  }

  removeNotificationsFromNotificationsSet(notifications: Notification[]): void {
    if (notifications && notifications.length > 0) {
      notifications.forEach((notification) => {
        this.removeNotificationFromNotifications(notification);
      });
    }
    this.bellNotifications = NotificationHelper.sortBellNotifications(
      this.notifications
    );
  }

  /**
   * remove one notification from a set of notifications of the same type
   * @param notifications: list of notifications
   * @param notification: notification to remove
   */
  removeNotification(
    notifications: Notification[],
    notification: Notification
  ): Notification[] {
    notifications.forEach((notif, index) => {
      if (notif.element === notification.element) {
        notifications.splice(index, 1);
      }
    });
    return notifications;
  }

  /**
   * remove notification froms array of notifications
   * @param notification to remove
   */
  removeNotificationFromNotifications(notification: Notification): void {
    switch (notification.type) {
      case variables.like:
        this.notifications.likes = this.removeNotification(
          this.notifications.likes,
          notification
        );
        break;
      case variables.comment:
        this.notifications.comments = this.removeNotification(
          this.notifications.comments,
          notification
        );
        break;
      case variables.share:
        this.notifications.shares = this.removeNotification(
          this.notifications.shares,
          notification
        );
        break;
      case variables.message:
        this.notifications.messages = this.removeNotification(
          this.notifications.messages,
          notification
        );
        break;
      case variables.favorite:
        this.notifications.favorites = this.removeNotification(
          this.notifications.favorites,
          notification
        );
        break;
      case variables.follower:
        this.notifications.followers = this.removeNotification(
          this.notifications.followers,
          notification
        );
        break;
      case variables.post:
        this.notifications.posts = this.removeNotification(
          this.notifications.posts,
          notification
        );
        break;
      default:
        break;
    }
  }

  getHomeIcon(): string{
    if (window.location.pathname.indexOf('dashboard') > 0){
      return 'home-selected';
    }else {
      return 'home';
    }
  }

  getMessageIcon(): string{
    if (window.location.pathname.indexOf('message') > 0){
      return 'message-selected';
    }else {
      return 'message';
    }
  }

  getNotificationIcon(): string{
    if (window.location.pathname.indexOf('notification') > 0){
      return 'bell-selected';
    }else {
      return 'bell';
    }
  }

  getNetworkIcon(): string{
    if (window.location.pathname.indexOf('network') > 0){
      return 'network-selected';
    }else {
      return 'network';
    }
  }

  getSearchIcon(): string{
    if (window.location.pathname.indexOf('search') > 0){
      return 'search-selected';
    }else {
      return 'search';
    }
  }

  getPlusIcon(): string{
    if (window.location.pathname.indexOf('profile') > 0 || window.location.pathname.indexOf('setting') > 0 || window.location.pathname.indexOf('assistance') > 0){
      return 'plus-mobile-selected';
    }else {
      return 'plus-mobile';
    }
  }

  onMouseEnter(tab: string): void {
    switch (tab) {
      case 'home':
        this.homeImageIcon = 'home-selected';
        break;
      case 'search':
        this.searchImageIcon = 'search-selected';
        break;
      case 'message':
        this.messageImageIcon = 'message-selected';
        break;
      case 'bell':
        this.bellImageIcon = 'bell-selected';
        break;
      case 'network':
        this.networkImageIcon = 'network-selected';
        break;
      case 'plus-mobile':
        this.plusImageIcon = 'plus-mobile-selected';
        break;
    }
  }

  onMouseLeave(tab: string): void {
    switch (tab) {
      case 'home':
        this.homeImageIcon = 'home';
        break;
      case 'message':
        this.messageImageIcon = 'message';
        break;
      case 'search':
        this.searchImageIcon = 'search';
        break;
      case 'bell':
        this.bellImageIcon = 'bell';
        break;
      case 'network':
        this.networkImageIcon = 'network';
        break;
      case 'plus-mobile':
        this.plusImageIcon = 'plus-mobile';
        break;
    }
    this.resetImageIcon();
    this.onMouseEnter(this.selectedImageIcon);
  }

  onClick(tab: string): void {
    this.selectedImageIcon = tab;
    this.showModalPost = false;
    if (tab === 'plus-mobile'){
      this.showModalAccount = !this.showModalAccount;
    }
    this.displayPopup(tab);
  }

  displayPopup(label: string): void{
    this.displayPopupMessage = false;
    this.displayPopupBell = false;
    this.displayPopupNetwork = false;
    switch (label) {
      case 'message':
        this.displayPopupMessage = true;
        break;
      case 'bell':
        this.displayPopupBell = true;
        break;
      case 'network':
        this.displayPopupNetwork = true;
        break;
    }
  }

  /* function which display a modal when user hit on publish button */
  showModalPublish(): void {
    this.showModalAccount = false;
    this.showModalPost = !this.showModalPost;
  }

  resetImageIcon(): void {
    this.homeImageIcon = 'home';
    this.searchImageIcon = 'search';
    this.bellImageIcon = 'bell';
    this.networkImageIcon = 'network';
  }
  // Ouverture creer une publication
  open_modal_publication(content): void {
    this.showModalAccount = false;
    this.showModalPost = false;
    this.modalService.dismissAll();
    this.modalService
      .open(content, {
        centered: true,
        windowClass: 'publication-modal-2',
      })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  onClickEditPost(infoMediaType: InfoMediaType): void {
    this.showModalPost = false;
    this.showModalAccount = false;
    localStorage.setItem(
      variables.current_media_type,
      JSON.stringify(infoMediaType)
    );
    // this.router.navigate([this.routes.get('post.edit.my')]);
  }

  onClickSearchMsg(): void {}

  findMyMessages(): void {
    this.loading = true;
    this.messageService.findMyMessages().subscribe(
      (response) => {
        this.allAccountsMessages = response.resources;
        this.accountsMessages = this.allAccountsMessages;
        // this.sortAccountsMessages();
        this.loading = false;
      },
      (error) => {
        this.loading = false;
      },
      () => {}
    );
  }

  /**
   * update the list of contact on message received
   */
  updateAccountMessage(message: Message): void {
    if (message) {
      let accountMessageIndex = -1;
      this.accountsMessages.forEach((elt, index) => {
        if (elt.account.idServer === message.emitterAccount.idServer) {
          accountMessageIndex = index;
        }
      });
      if (accountMessageIndex > -1) {
        this.accountsMessages[accountMessageIndex].messages.push(message);
      }
    }
  }
}

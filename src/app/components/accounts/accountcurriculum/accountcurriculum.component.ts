import {Component, Input, OnInit} from '@angular/core';
import {Account} from '../../../models/account/account';
import {variables} from '../../../../environments/variables';
import {ProfessionalService} from '../../../services/accounts/account/professional.service';
import {AccountProfessional} from '../../../models/account/accountProfessional';
import {SkillService} from '../../../services/accounts/account/skill.service';
import {AccountSkills} from '../../../models/account/accountSkills';
import {AcademicService} from '../../../services/accounts/account/academic.service';
import {AccountAcademics} from '../../../models/account/accountAcademics';
import {CursusDisplay} from '../../../models/account/config/cursusDisplay';
import {SkillHelper} from '../../../helpers/skill-helper';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {RoutesHelper} from '../../../helpers/routes-helper';

@Component({
  selector: 'app-accountcurriculum',
  templateUrl: './accountcurriculum.component.html',
  styleUrls: ['./accountcurriculum.component.css']
})
export class AccountcurriculumComponent implements OnInit {

  @Input() content: string;
  @Input() account: Account;
  @Input() accountId: string;
  title: string;
  loading: boolean;
  loggedAccount: Account = {} as Account;
  accountProfessional: AccountProfessional;
  accountSkills: AccountSkills;
  accountAcademics: AccountAcademics;
  cursusDisplays: CursusDisplay[] = [];
  icon: string;
  routes = RoutesHelper.routes;
  nb: number;

  constructor(private professionalService: ProfessionalService,
              private skillService: SkillService,
              private authenticateService: AuthenticateService,
              private academicService: AcademicService) { }

  ngOnInit(): void {
    this.nb = 0;
    this.loading = true;
    this.loggedAccount = this.authenticateService.getCurrentAccount();

    if(!this.loggedAccount)
    {
      this.account =  {} as Account;
      this.account.idServer = this.accountId;
    }

    this.filterForCorrectContent();
  }

  onClickMore(content: string): void {
    localStorage.setItem(variables.tab, content);
  }

  filterForCorrectContent(): void{
    switch (this.content) {
      case variables.professional:
        this.title = 'Parcours pro';
        if (this.loggedAccount) {
          this.findAllProfessionals();
        }
        else {
          this.findAllProfessionalsOfflline();
        }
        break;
      case variables.academics:
        this.title = 'Formations';
        if (this.loggedAccount) {
          this.findAcademics();
        }
        else {
          this.findAcademicsOffline();
        }
        break;
      case variables.skills:
        this.title = 'Compétences';
        if (this.loggedAccount) {
          this.findAllSkills();
        }
        else {
          this.findAllSkillsOffline();
        }
        break;
      default:
        break;
    }
    this.loading = false;
  }

  findAllProfessionals(): void{
    this.loading = true;
    this.professionalService.findProfessionals(this.account.idServer).subscribe((response) => {
      this.accountProfessional = response.resource;
      this.nb = this.accountProfessional.professionals.length;
      this.accountProfessional.professionals.slice(0, 4).forEach(elt => {
        const cursus = {} as CursusDisplay;
        cursus.label = elt.jobTitle;
        cursus.sub = elt.establishmentName;
        cursus.icon = 'professional.svg';
        this.cursusDisplays.push(cursus);
      });
      this.loading = false;
    }, (error) => {
    }, () => {
      this.loading = false;
    });
  }
  findAllProfessionalsOfflline(): void{
    this.loading = true;
    this.professionalService.findProfessionalsOffline(this.account.idServer).subscribe((response) => {
      this.accountProfessional = response.resource;
      this.nb = this.accountProfessional.professionals.length;
      this.accountProfessional.professionals.slice(0, 4).forEach(elt => {
        const cursus = {} as CursusDisplay;
        cursus.label = elt.jobTitle;
        cursus.sub = elt.establishmentName;
        cursus.icon = 'professional.svg';
        this.cursusDisplays.push(cursus);
      });
      this.loading = false;
    }, (error) => {
    }, () => {
      this.loading = false;
    });
  }
  findAllSkills(): void{
    this.loading = true;
    this.skillService.findSkills(this.account.idServer).subscribe((response) => {
      this.accountSkills = response.resource;
      this.nb = this.accountSkills.skills.length;
      const skills = SkillHelper.findFirstSkills(this.accountSkills.skills);
      if (skills && skills.length > 0){
        skills.forEach(elt => {
          const cursus = {} as CursusDisplay;
          cursus.label = elt.label;
          cursus.sub = elt.discipline;
          cursus.icon = elt.level.icon;
          this.cursusDisplays.push(cursus);
        });
      }
    }, (error) => {
    }, () => {
      this.loading = false;
    });
  }

  findAllSkillsOffline(): void{
    this.loading = true;
    this.skillService.findSkillsOffline(this.account.idServer).subscribe((response) => {
      this.accountSkills = response.resource;
      this.nb = this.accountSkills.skills.length;
      const skills = SkillHelper.findFirstSkills(this.accountSkills.skills);
      if (skills && skills.length > 0){
        skills.forEach(elt => {
          const cursus = {} as CursusDisplay;
          cursus.label = elt.label;
          cursus.sub = elt.discipline;
          cursus.icon = elt.level.icon;
          this.cursusDisplays.push(cursus);
        });
      }
    }, (error) => {
    }, () => {
      this.loading = false;
    });
  }

  findAcademics(): void{
      this.loading = true;
      this.academicService.findAccountAcademics(this.account.idServer).subscribe((response) => {
        this.accountAcademics = response.resource;
        this.nb = this.accountAcademics.secondaryEducations.length +
                  this.accountAcademics.higherEducations.length +
                  this.accountAcademics.degreesObtained.length +
                  this.accountAcademics.certificates.length;

        this.accountAcademics.higherEducations.slice(0, 2).forEach(elt => {
          const cursus = {} as CursusDisplay;
          cursus.label = elt.studyLevel.label;
          cursus.icon = 'school.svg';
          cursus.sub = elt.establishmentName;
          this.cursusDisplays.push(cursus);
        });
        this.accountAcademics.secondaryEducations.slice(0, 2).forEach(elt => {
          const cursus = {} as CursusDisplay;
          cursus.label = elt.studyLevel.label;
          cursus.icon = 'school.svg';
          cursus.sub = elt.establishmentName;
          this.cursusDisplays.push(cursus);
        });
        this.accountAcademics.degreesObtained.slice(0, 2).forEach(elt => {
          const cursus = {} as CursusDisplay;
          cursus.label = elt.diploma.label;
          cursus.icon = 'student.svg';
          cursus.sub = elt.establishmentName;
          this.cursusDisplays.push(cursus);
        });
        this.accountAcademics.certificates.slice(0, 2).forEach(elt => {
          const cursus = {} as CursusDisplay;
          cursus.label = elt.entitledCertification;
          cursus.icon = 'certification.svg';
          cursus.sub = elt.issuingInstitutionName;
          this.cursusDisplays.push(cursus);
        });
      }, (error) => {
      }, () => {
        this.loading = false;
      });
  }

  findAcademicsOffline(): void{
    this.loading = true;
    this.academicService.findAccountAcademicsOffline(this.account.idServer).subscribe((response) => {
      this.accountAcademics = response.resource;
      this.nb = this.accountAcademics.secondaryEducations.length +
        this.accountAcademics.higherEducations.length +
        this.accountAcademics.degreesObtained.length;

      this.accountAcademics.higherEducations.slice(0, 2).forEach(elt => {
        const cursus = {} as CursusDisplay;
        cursus.label = elt.studyLevel.label;
        cursus.icon = 'school.svg';
        cursus.sub = elt.establishmentName;
        this.cursusDisplays.push(cursus);
      });
      this.accountAcademics.secondaryEducations.slice(0, 2).forEach(elt => {
        const cursus = {} as CursusDisplay;
        cursus.label = elt.studyLevel.label;
        cursus.icon = 'school.svg';
        cursus.sub = elt.establishmentName;
        this.cursusDisplays.push(cursus);
      });
      this.accountAcademics.degreesObtained.slice(0, 2).forEach(elt => {
        const cursus = {} as CursusDisplay;
        cursus.label = elt.diploma.label;
        cursus.icon = 'student.svg';
        cursus.sub = elt.establishmentName;
        this.cursusDisplays.push(cursus);
      });
      this.accountAcademics.certificates.slice(0, 2).forEach(elt => {
        const cursus = {} as CursusDisplay;
        cursus.label = elt.entitledCertification;
        cursus.icon = 'certification.svg';
        cursus.sub = elt.issuingInstitutionName;
        this.cursusDisplays.push(cursus);
      });
    }, (error) => {
    }, () => {
      this.loading = false;
    });
  }
}

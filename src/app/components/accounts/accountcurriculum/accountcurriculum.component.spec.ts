import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountcurriculumComponent } from './accountcurriculum.component';

describe('AccountcurriculumComponent', () => {
  let component: AccountcurriculumComponent;
  let fixture: ComponentFixture<AccountcurriculumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountcurriculumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountcurriculumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

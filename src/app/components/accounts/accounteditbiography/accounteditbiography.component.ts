import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Account} from '../../../models/account/account';
import {AccountService} from '../../../services/accounts/account/account.service';
import {ToastService} from '../../../services/toast/toast.service';
import {Router} from '@angular/router';
import {Alert} from '../../../models/alert';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {Editor, toHTML, Toolbar} from 'ngx-editor';
import {infoText} from '../../../../environments/info-text';
import schema from '../../../schema';
import {variables} from '../../../../environments/variables';

@Component({
  selector: 'app-accounteditbiography',
  templateUrl: './accounteditbiography.component.html',
  styleUrls: ['./accounteditbiography.component.css']
})
export class AccounteditbiographyComponent implements OnInit, OnDestroy {

  @Input() account: Account;
  alert: Alert = {} as Alert;
  biography = '';
  maxBiographySize: number;
  loading: boolean;
  displayBiography: boolean;
  loggedAccount: Account = {} as Account;

  editor: Editor;
  toolbar: Toolbar = [
    ['bold', 'italic'],
    ['underline', 'strike'],
    ['code', 'blockquote'],
    ['ordered_list', 'bullet_list'],
    [{ heading: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'] }],
    ['text_color', 'background_color'],
    ['align_left', 'align_center', 'align_right', 'align_justify'],
  ];
  infoTextBiography = infoText.info_text_biography;

  constructor(private accountService: AccountService,
              private toastService: ToastService,
              private authenticateService: AuthenticateService,
              private router: Router) { }

  ngOnInit(): void {
    this.loading = true;
    this.displayBiography = true;
    this.loggedAccount = this.authenticateService.getCurrentAccount();
    this.findAccount();
    this.editor = new Editor();
    this.maxBiographySize = variables.max_biography_size;
  }

  findAccount(): void{
    this.accountService.findAccountById(this.account.idServer).subscribe((response) => {
      this.account = response.resource;
      this.readBiography(this.account);
      this.loading = false;
    }, (error) => {
      this.toastService.showError('Error', 'Compte non identifié par le serveur');
      this.router.navigate(['/']);
      this.loading = false;
    }, () => {
      this.loading = false;
    });
  }

  readBiography(account: Account): void {
    if (account.biography == null) {
      this.biography = '';
    }
    else {
      this.biography = account.biography;
    }
  }

  onClickSave(): void {
    // this.account.biography = toHTML(this.biography, schema);
    this.account.biography = this.biography;
    this.loading = true;
    if (!this.account.biography) {this.account.biography = ''; }
    if (this.account.biography.length <= this.maxBiographySize){
      this.accountService.updateAccount(this.account).subscribe((response) => {
        this.account = response.resource;
        this.readBiography(this.account);
        this.loading = false;
        this.displayBiography = true;
      }, (error) => {
        this.alert = {display: true, class: 'danger', title: 'Erreur', message: '  Biographie non sauvegardée'};
        this.loading = false;
      }, () => {
        this.loading = false;
      });
    } else {
      this.alert = {display: true, class: 'danger', title: 'Erreur', message: '  Le nombre maximal de charactères est '
          + this.maxBiographySize};
      this.loading = false;
    }
  }

  onclickCancel(): void {
    this.readBiography(this.account);
    this.displayBiography = true;
  }

  onClickEdit(account: Account): void {
    this.displayBiography = false;
  }

  ngOnDestroy(): void {
    this.editor.destroy();
  }

  testDescriptionSize(): void{
    if (this.biography.length > this.maxBiographySize) {
      this.biography = this.biography.substring(0, this.maxBiographySize);
    }
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccounteditbiographyComponent } from './accounteditbiography.component';

describe('AccountbiographyComponent', () => {
  let component: AccounteditbiographyComponent;
  let fixture: ComponentFixture<AccounteditbiographyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccounteditbiographyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccounteditbiographyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

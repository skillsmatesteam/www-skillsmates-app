import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Professional} from '../../../models/account/professional';
import {ProfessionalService} from '../../../services/accounts/account/professional.service';
import {ToastService} from '../../../services/toast/toast.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DegreeObtained} from '../../../models/account/degreeObtained';
import {AcademicService} from '../../../services/accounts/account/academic.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AuthenticateService} from "../../../services/accounts/authenticate/authenticate.service";
import {Account} from "../../../models/account/account";

@Component({
  selector: 'app-accountdegreeobtainedcard',
  templateUrl: './accountdegreeobtainedcard.component.html',
  styleUrls: ['./accountdegreeobtainedcard.component.css']
})
export class AccountdegreeobtainedcardComponent implements OnInit {

  @Input() degreeObtained: DegreeObtained;
  @Input() account: Account = {} as Account;
  @Output() newDegreeObtainedEvent = new EventEmitter<string>();
  @Output() editDegreeObtainedEvent = new EventEmitter<DegreeObtained>();
  degreeObtainedToDelete: DegreeObtained = {} as DegreeObtained;
  degreeObtainedToEdit: DegreeObtained = {} as DegreeObtained;
  loggedAccount: Account = {} as Account;

  constructor(private academicService: AcademicService,
              private toastService: ToastService,
              private router: Router,
              private authenticateService: AuthenticateService,
              private modalService: NgbModal,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.loggedAccount = this.authenticateService.getCurrentAccount();
  }

  onClickDelete(degreeObtained: DegreeObtained): void {
    this.degreeObtainedToDelete = degreeObtained;
    localStorage.setItem('degreeObtainedToDelete', degreeObtained.idServer);
    this.modalService.dismissAll();
  }

  onClickConfirmDelete(): void {
    this.academicService.deleteDegreeObtained(localStorage.getItem('degreeObtainedToDelete')).subscribe((response) => {
      this.toastService.showSuccess('Succès', 'Diplôme supprimé avec succés');
      this.newDegreeObtainedEvent.emit('refresh');
    }, (error) => {
      this.toastService.showError('Error', 'Diplôme non supprimé');
    }, () => {
      localStorage.removeItem('degreeObtainedToDelete');
    });
  }

  onClickEdit(degreeObtained: DegreeObtained): void {
    this.editDegreeObtainedEvent.emit(degreeObtained);
  }
}

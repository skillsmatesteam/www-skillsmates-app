import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountdegreeobtainedcardComponent } from './accountdegreeobtainedcard.component';

describe('AccountdegreeobtainedcardComponent', () => {
  let component: AccountdegreeobtainedcardComponent;
  let fixture: ComponentFixture<AccountdegreeobtainedcardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountdegreeobtainedcardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountdegreeobtainedcardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

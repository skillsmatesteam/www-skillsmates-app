import {Component, Input, OnInit} from '@angular/core';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {ToastService} from '../../../services/toast/toast.service';
import {ProfessionalService} from '../../../services/accounts/account/professional.service';
import {AccountProfessional} from '../../../models/account/accountProfessional';
import {Professional} from '../../../models/account/professional';
import {Account} from '../../../models/account/account';
import {variables} from '../../../../environments/variables';
import {Alert} from '../../../models/alert';
import {DatePipe} from '@angular/common';
import {ModalDismissReasons, NgbDate, NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {ObjectHelper} from '../../../helpers/object-helper';
import {infoText} from '../../../../environments/info-text';

@Component({
  selector: 'app-accounteditprofessionnal',
  templateUrl: './accounteditprofessionnal.component.html',
  styleUrls: ['./accounteditprofessionnal.component.css']
})
export class AccounteditprofessionnalComponent implements OnInit {
  displayNewProfessionalBlock: boolean;
  displayEditProfessionalBlock: boolean;
  @Input() account: Account;
  professional: Professional = {} as Professional;
  maxDescriptionSize: number;
  accountProfessional: AccountProfessional = {} as AccountProfessional;
  loading: boolean;
  loggedAccount: Account;
  currentPosition: boolean;
  alert: Alert = {} as Alert;
  submitted = false;
  errors;
  closeResult: string;
  professionalStartDate: Date;
  professionalEndDate: Date;
  disabledEndDate = true;
  from: string;
  infoTextPro = infoText.info_text_pro;

  constructor(private professionalService: ProfessionalService,
              private authenticateService: AuthenticateService,
              private datePipe: DatePipe,
              private modalService: NgbModal,
              private toastService: ToastService) { }

  ngOnInit(): void {
    this.initErrors();
    this.currentPosition = false;
    this.displayNewProfessionalBlock = false;
    this.displayEditProfessionalBlock = false;
    this.loggedAccount = this.authenticateService.getCurrentAccount();
    this.findAllProfessionals();
    this.maxDescriptionSize = variables.max_professional_description_size;
  }

  onClickNewProfessionnal(mastered: string): void {
    this.displayNewProfessionalBlock = true;
  }

  onClickEditProfessionnal(mastered: string): void {
    this.displayEditProfessionalBlock = true;
  }

  onChangeActivitySector(value: string): void {
    this.errors.activitySector = '';
    this.accountProfessional.activitySectors.forEach(elt => {
      if (elt.label.toLowerCase() === value.toLowerCase()){
        this.professional.activitySector = elt;
      }
    });
  }

  onChangeActivityArea(value: string): void {
    this.errors.activityArea = '';
    this.accountProfessional.activityAreas.forEach(elt => {
      if (elt.label.toLowerCase() === value.toLowerCase()){
        this.professional.activityArea = elt;
      }
    });
  }

  onclickCancel(): void {
    this.initErrors();
    this.professional = {} as Professional;
    this.displayNewProfessionalBlock = false;
    this.displayEditProfessionalBlock = false;
  }

  onClickSave(): void {
    this.loading = true;
    this.professional.account = this.authenticateService.getCurrentAccount();
    this.professional.currentPosition = this.currentPosition;
    if (this.currentPosition){
      this.professional.endDate = new Date();
    }
    console.log(this.professional);
    if (this.validateProfessional()){
      this.professionalService.createProfessional(this.professional).subscribe((response) => {
        this.professional = {} as Professional;
        if (this.currentPosition){
          const account = this.authenticateService.getCurrentAccount();
          account.currentEstablishmentName = this.professional.establishmentName;
          localStorage.setItem(variables.account_current, JSON.stringify(account));
        }
        this.findAllProfessionals();
        this.displayEditProfessionalBlock = true;
        this.displayNewProfessionalBlock = false;
        this.initErrors();
        this.professionalStartDate = undefined;
        this.professionalEndDate = undefined;
        this.currentPosition = false;
      }, (error) => {
        this.alert = {display: true, class: 'danger', title: 'Erreur', message: '  Parcours professionel non enregistré'};
        this.loading = false;
      }, () => {
        this.loading = false;
      });
    } else {
      this.alert = {display: true, class: 'danger', title: 'Erreur', message: '  Données invalides pour le parcours professionel'};
      this.loading = false;
    }
  }

  findAllProfessionals(): void{
    this.loading = true;
    const accountId = this.account.idServer;
    this.professionalService.findProfessionals(accountId).subscribe((response) => {
      this.accountProfessional = response.resource;
      this.loading = false;
    }, (error) => {
    }, () => {
      this.loading = false;
    });
  }

  onEditProfessionalEvent(professionalToEdit: Professional): void{
    this.loading = true;
    this.disabledEndDate = false;
    this.displayEditProfessionalBlock = true;
    this.professional = professionalToEdit;
    this.currentPosition = this.professional.currentPosition;
    this.professionalStartDate = this.professional.startDate;
    this.professionalEndDate = this.professional.endDate;
    this.onClickNewProfessionnal('');
    this.loading = false;
  }

  validateProfessional(): boolean{
    this.initErrors();
    if (this.professional.establishmentName === undefined || this.professional.establishmentName === ''){
      this.errors.valid = false;
      this.errors.establishmentName = 'Le nom de l\'etablissement est requis';
    }else if (this.professional.establishmentName.length > 255){
      this.errors.valid = false;
      this.errors.establishmentName = 'Le nombre maximal de caractères est 255';
    }

    if (this.professional.activitySector === undefined){
      this.errors.valid = false;
      this.errors.activitySector = 'Selectionner un secteur d\'activité';
    } else{
      if (this.professional.activitySector.specified &&
           (this.professional.anotherActivitySector === undefined || this.professional.anotherActivitySector === '')) {
            this.errors.valid = false;
            this.errors.anotherActivitySector = 'L\'autre secteur d\'activité est requis';
          } else if (this.professional.activitySector.specified &&
           (this.professional.anotherActivitySector !== undefined && this.professional.anotherActivitySector.length > 255)) {
            this.errors.valid = false;
            this.errors.anotherActivitySector = 'Le nombre maximal de caractères est 255';
          }
    }

    if (this.professional.jobTitle === undefined || this.professional.jobTitle === ''){
      this.errors.valid = false;
      this.errors.jobTitle = 'Le nom du poste est requis';
    }

    console.log(this.professional.jobTitle.length);
    if (this.professional.jobTitle !== undefined && this.professional.jobTitle.length > 255){
      this.errors.valid = false;
      this.errors.jobTitle = 'Le nombre maximal de caractères est 255';
    }

    if (this.professional.activityArea === undefined){
      this.errors.valid = false;
      this.errors.activityArea = 'Selectionner un domaine d\'activité';
    } else {
      if (this.professional.activityArea.specified) {
        if (this.professional.anotherActivityArea === undefined || this.professional.anotherActivityArea === '') {
          this.errors.valid = false;
          this.errors.anotherActivityArea = 'L\'autre domaine d\'activité ou discipline est requis';
        } else if (this.professional.anotherActivityArea !== undefined && this.professional.anotherActivityArea.length > 255) {
          this.errors.valid = false;
          this.errors.anotherActivityArea = 'Le nombre maximal de caractères est 255';
        }
      }
    }

    if (this.professional.city === undefined || this.professional.city === ''){
      this.errors.valid = false;
      this.errors.city = 'La ville est requise';
    }

    if (this.professional.city !== undefined && this.professional.city.length > 255){
      this.errors.valid = false;
      this.errors.city = 'Le nombre maximal de caractères est 255';
    }

    if (this.professional.description !== undefined && this.professional.description.length > this.maxDescriptionSize){
      this.errors.valid = false;
      this.errors.description = 'Le nombre maximal de caractères est ' + this.maxDescriptionSize;
    }

    if (this.professional.startDate === undefined){
      this.errors.valid = false;
      this.errors.startDate = 'Selectionner une date de dèbut';
    }

    if (!this.currentPosition && this.professional.endDate === undefined){
      this.errors.valid = false;
      this.errors.endDate = 'Selectionner une date de fin';
    }

    if (!this.areDatesCorrect()){
      this.errors.valid = false;
      this.errors.startDate = 'Date incohérente';
      this.errors.endDate = 'Date incohérente';
      this.professional.startDate = null;
    }
    this.submitted = true;
    console.log(this.errors);
    return this.errors.valid;
  }

  onRefreshEvent(newEvent: string): void {
    this.findAllProfessionals();
  }

  initErrors(): void{
    this.submitted = false;
    this.errors = {valid: true, jobTitle: '', activityArea: '', activitySector: '',
      establishmentName: '', city: '', startDate: '', endDate: '', description: '',
      anotherActivitySector: '', anotherActivityArea: ''};
    this.alert = {} as Alert;
  }

  keyupEstablishmentName(): void {
    this.errors.establishmentName = '';
  }

  keyupJobTitle(): void {
    this.errors.jobTitle = '';
  }

  keyupCity(): void {
    this.errors.city = '';
  }

  keyupAnotherActivitySector(): void {
    this.errors.anotherActivitySector = '';
  }

  keyupAnotherActivityArea(): void {
    this.errors.anotherActivityArea = '';
  }

  dateFormat(date: Date): any{
    this.datePipe.transform(date, 'yyyy-MM-dd');
  }

  open(content): void {
    if (this.accountProfessional.professionals.length > 0){
      this.modalService.open(content,     {
        size: 'lg'
      }).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    }
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  onClickDismissModal(): void {
    this.modalService.dismissAll();
  }

  selectStartDate(): void{
    this.errors.startDate = '';
    // this.professionalStartDate = professionalStartDate;
    if (ObjectHelper.isDateValid(this.professionalStartDate)){
      this.professional.startDate = this.professionalStartDate;
      this.disabledEndDate = false;
      this.errors.valid = true;
      this.submitted = false;
    }else {
      this.disabledEndDate = true;
      this.errors.valid = false;
      this.submitted = true;
      this.professional.startDate = null;
      this.errors.startDate = 'Date invalide';
    }
  }

  selectEndDate(): void{
    this.errors.endDate = '';
    // this.professionalEndDate = professionalEndDate;
    if (ObjectHelper.isDateValid(this.professionalEndDate)){
      this.professional.endDate = this.professionalEndDate;
      if (this.areDatesCorrect()){
        this.errors.valid = true;
        this.submitted = false;
      }else {
        this.errors.valid = false;
        this.submitted = true;
        this.professional.endDate = null;
        this.errors.startDate = 'Date incohérente';
        this.errors.endDate = 'Date incohérente';
      }
    }else {
      this.errors.valid = false;
      this.submitted = true;
      this.professional.endDate = null;
      this.errors.endDate = 'Date invalide';
    }
  }

  areDatesCorrect(): boolean{
    return this.professional.startDate && this.professional.endDate &&
      this.professional.startDate <= this.professional.endDate;
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccounteditprofessionnalComponent } from './accounteditprofessionnal.component';

describe('AccounteditprofessionnalComponent', () => {
  let component: AccounteditprofessionnalComponent;
  let fixture: ComponentFixture<AccounteditprofessionnalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccounteditprofessionnalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccounteditprofessionnalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

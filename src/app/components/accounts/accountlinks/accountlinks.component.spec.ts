import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountlinksComponent } from './accountlinks.component';

describe('AccountlinksComponent', () => {
  let component: AccountlinksComponent;
  let fixture: ComponentFixture<AccountlinksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountlinksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountlinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

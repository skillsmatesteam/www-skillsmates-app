import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-accountlinks',
  templateUrl: './accountlinks.component.html',
  styleUrls: ['./accountlinks.component.css']
})
export class AccountlinksComponent implements OnInit {

  @Input() title;
  image: string;
  label: string;
  constructor() { }

  ngOnInit(): void {
    this.sortByTitle();
  }

  sortByTitle(): void{
    switch (this.title) {
      case 'partner':
        this.label = 'Partenaires';
        this.image = 'partner';
        break;
      case 'ambassador':
        this.label = 'Ambassadeurs';
        this.image = 'ambassador';
        break;
      default:
        this.label = 'Recommendations';
        break;
    }
  }

}

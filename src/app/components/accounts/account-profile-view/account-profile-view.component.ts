import {Component, HostListener, OnInit} from '@angular/core';
import {ToastService} from "../../../services/toast/toast.service";
import {SocialInteractionService} from "../../../services/pages/social-interaction/social-interaction.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MobileService} from "../../../services/mobile/mobile.service";
import {AuthenticateService} from "../../../services/accounts/authenticate/authenticate.service";
import {variables} from "../../../../environments/variables";
import {Alert} from "../../../models/alert";
import {Account} from "../../../models/account/account";
import {RoutesHelper} from "../../../helpers/routes-helper";
import {AccountService} from "../../../services/accounts/account/account.service";
import {AccountSocialInteractions} from "../../../models/account/accountSocialInteractions";
import {InfoMediaType} from "../../../models/multimedia/infoMediaType";
import {MediaHelper} from "../../../helpers/media-helper";

@Component({
  selector: 'app-account-profile-view',
  templateUrl: './account-profile-view.component.html',
  styleUrls: ['./account-profile-view.component.css']
})
export class AccountProfileViewComponent implements OnInit {
  loading: boolean;
  anotherProfile: boolean;
  profileId: string;
  loggedAccount: Account = {} as Account;
  posts: string = variables.posts;
  infosMediaTypes: InfoMediaType[] = MediaHelper.mediaTypes;
  documents: string = variables.documents;
  tab: string = this.posts;
  account: Account = {} as Account;
  public screenWidth: number = window.innerWidth;
  routes = RoutesHelper.routes;
  errors;
  submitted = false;
  alert: Alert = {} as Alert;
  pageYoffset = 0;
  toggleDiplayMoreLabel: boolean;
  accountSocialInteractions: AccountSocialInteractions = {} as AccountSocialInteractions;

  constructor(private toastService: ToastService,
              private socialInteractionService: SocialInteractionService,
              private router: Router,
              private  mobileService: MobileService,
              private accountService: AccountService,
              private authenticateService: AuthenticateService,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.initErrors();
    this.profileId = this.activatedRoute.snapshot.paramMap.get(variables.id);
    this.findAccountOffline(this.profileId);
    this.anotherProfile = true;
    /*if (!this.postId) {
      // this.id = this.loggedAccount.idServer;
    }
    this.findPost(0);*/
    this.loggedAccount = this.authenticateService.getCurrentAccount();

    this.toggleDiplayMoreLabel = true;
  }

  findAccountOffline(accountId: string): void{
    if (accountId){
      this.accountService.findOfflineAccountById(accountId).subscribe((response) => {
        //this.accountSocialInteractions.account = response.resource;
        this.account = response.resource;
        //this.findNetwork();
        this.loading = false;
      }, (error => {}));
    }
  }
  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    // this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
  }
  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }

  onClickLogin(): void {
    this.router.navigate([this.routes.get('login'), 1]);
  }

  onClickRegister(): void {
    this.router.navigate([this.routes.get('login'), 2]);
  }

  initErrors(): void{
    this.submitted = false;
    this.errors = {valid: true, body: ''};
    this.alert = {} as Alert;
  }

  scrollToTop(): void {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth',
    });
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountsecondaryeducationComponent } from './accountsecondaryeducation.component';

describe('AccountsecondaryeducationComponent', () => {
  let component: AccountsecondaryeducationComponent;
  let fixture: ComponentFixture<AccountsecondaryeducationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountsecondaryeducationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountsecondaryeducationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

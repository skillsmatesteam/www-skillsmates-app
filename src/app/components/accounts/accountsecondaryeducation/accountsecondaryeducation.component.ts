import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AcademicService} from '../../../services/accounts/account/academic.service';
import {ToastService} from '../../../services/toast/toast.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SecondaryEducation} from '../../../models/account/secondaryEducation';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Account} from '../../../models/account/account';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';

@Component({
  selector: 'app-accountsecondaryeducation',
  templateUrl: './accountsecondaryeducation.component.html',
  styleUrls: ['./accountsecondaryeducation.component.css']
})
export class AccountsecondaryeducationComponent implements OnInit {

  @Input() secondaryEducation: SecondaryEducation;
  @Input() account: Account = {} as Account;
  @Output() newSecondaryEducationEvent = new EventEmitter<string>();
  @Output() editSecondaryEducationEvent = new EventEmitter<SecondaryEducation>();
  secondaryEducationToDelete: SecondaryEducation = {} as SecondaryEducation;
  secondaryEducationToEdit: SecondaryEducation = {} as SecondaryEducation;
  id: string;
  loggedAccount: Account = {} as Account;

  constructor(private academicService: AcademicService,
              private toastService: ToastService,
              private router: Router,
              private authenticateService: AuthenticateService,
              private modalService: NgbModal,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.loggedAccount = this.authenticateService.getCurrentAccount();
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
  }

  onClickDelete(secondaryEducation: SecondaryEducation): void {
    this.secondaryEducationToDelete = secondaryEducation;
    localStorage.setItem('secondaryEducationToDelete', secondaryEducation.idServer);
    this.modalService.dismissAll();
  }

  onClickConfirmDelete(): void {
    this.academicService.deleteSecondaryEducation(localStorage.getItem('secondaryEducationToDelete')).subscribe((response) => {
      this.toastService.showSuccess('Succès', 'Education secondaire supprimé avec succés');
      this.newSecondaryEducationEvent.emit('refresh');
    }, (error) => {
      this.toastService.showError('Error', 'Education secondaire non supprimé');
    }, () => {
      localStorage.removeItem('secondaryEducationToDelete');
    });
  }

  onClickEdit(secondaryEducation: SecondaryEducation): void {
    this.editSecondaryEducationEvent.emit(secondaryEducation);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountskillcardComponent } from './accountskillcard.component';

describe('AccountskillcardComponent', () => {
  let component: AccountskillcardComponent;
  let fixture: ComponentFixture<AccountskillcardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountskillcardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountskillcardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

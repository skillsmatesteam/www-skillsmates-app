import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import {Skill} from '../../../models/account/skill';
import {SkillService} from '../../../services/accounts/account/skill.service';
import {ToastService} from '../../../services/toast/toast.service';
import {ActivatedRoute, Router} from '@angular/router';
import {variables} from '../../../../environments/variables';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Account} from '../../../models/account/account';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';

@Component({
  selector: 'app-accountskillcard',
  templateUrl: './accountskillcard.component.html',
  styleUrls: ['./accountskillcard.component.css']
})
export class AccountskillcardComponent implements OnInit {

  @Input() skill: Skill;
  @Input() account: Account = {} as Account;
  @Output() newSkillEvent = new EventEmitter<string>();
  @Output() editSkillEvent = new EventEmitter<Skill>();
  skillToDelete: Skill = {} as Skill;
  skillToEdit: Skill = {} as Skill;
  id: string;
  loggedAccount: Account = {} as Account;

  constructor(private skillService: SkillService,
              private toastService: ToastService,
              private router: Router,
              private authenticateService: AuthenticateService,
              private modalService: NgbModal,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.loggedAccount = this.authenticateService.getCurrentAccount();
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
  }

  onClickEdit(skill: Skill): void {
    this.editSkillEvent.emit(skill);
    this.modalService.dismissAll();
  }

  onClickDelete(skill: Skill): void {
    this.skillToDelete = skill;
    localStorage.setItem('skillToDelete', skill.idServer);
    this.modalService.dismissAll();
  }

  onClickConfirmDelete(): void {
    this.skillService.deleteSkill(localStorage.getItem('skillToDelete')).subscribe((response) => {
      this.toastService.showSuccess('Succès', 'Comptence supprimée avec succés');
      this.newSkillEvent.emit(variables.refresh);
    }, (error) => {
      this.toastService.showError('Error', 'Competence non supprimée');
    }, () => {
      localStorage.removeItem('skillToDelete');
    });
  }
}

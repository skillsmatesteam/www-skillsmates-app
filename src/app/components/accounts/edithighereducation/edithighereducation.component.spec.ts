import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdithighereducationComponent } from './edithighereducation.component';

describe('EditshighereducationComponent', () => {
  let component: EdithighereducationComponent;
  let fixture: ComponentFixture<EdithighereducationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdithighereducationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdithighereducationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

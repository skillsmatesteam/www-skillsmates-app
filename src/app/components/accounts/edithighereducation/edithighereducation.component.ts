import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Diploma} from '../../../models/account/config/diploma';
import {Alert} from '../../../models/alert';
import {HigherEducation} from '../../../models/account/higherEducation';
import {AccountAcademics} from '../../../models/account/accountAcademics';
import {StudyLevel} from '../../../models/account/config/studyLevel';
import {TeachingArea} from '../../../models/account/config/teachingArea';
import {EstablishmentType} from '../../../models/account/config/establishmentType';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {AcademicService} from '../../../services/accounts/account/academic.service';
import {TeachingAreaSet} from '../../../models/account/config/teachingAreaSet';
import {ObjectHelper} from '../../../helpers/object-helper';
import {TeachingAreaHelper} from '../../../helpers/teaching-area-helper';
import {TeachingAreaGroup} from '../../../models/account/config/teachingAreaGroup';

@Component({
  selector: 'app-edithighereducation',
  templateUrl: './edithighereducation.component.html',
  styleUrls: ['./edithighereducation.component.css']
})
export class EdithighereducationComponent implements OnInit {
  @Input() accountAcademics: AccountAcademics = {} as AccountAcademics;
  @Input() higherEducation: HigherEducation = {} as HigherEducation;
  @Output() refreshEvent = new EventEmitter<string>();
  diplomasFiltered: Diploma[];
  establishmentTypesFiltered: EstablishmentType[];
  studyLevelsFiltered: StudyLevel[];
  teachingAreaSet: TeachingAreaSet[];
  teachingAreasFiltered: TeachingArea[];
  loading: boolean;
  alert: Alert = {} as Alert;
  submitted = false;
  errors;
  teachingAreaSets: TeachingAreaSet[];
  selectedTeachingAreaGroup: TeachingAreaGroup;

  teachings = [];

  constructor(private authenticateService: AuthenticateService, private academicService: AcademicService) { }

  ngOnInit(): void {
    this.loading = true;
    this.initErrors();
    this.filterEstablishmentTypes();
    this.filterDiplomas();
    this.filterStudyLevels();
    this.teachingAreaSet = TeachingAreaHelper.findTeachingAreaSet(this.accountAcademics.teachingAreas);
    this.filterTeachingAreaGroup();
    if (!ObjectHelper.isEmpty(this.higherEducation)){
      this.selectedTeachingAreaGroup = this.higherEducation.teachingArea.teachingAreaGroup;
      this.teachingAreasFiltered = TeachingAreaHelper.filterTeachingArea(this.accountAcademics.teachingAreas, this.selectedTeachingAreaGroup);
    }
    this.loading = false;
  }

  onClickSaveHigherEducation(): void {
    this.loading = true;
    this.higherEducation.account = this.authenticateService.getCurrentAccount();
    if (this.validateHigherEducation()){
      this.saveHigherEducation();
    } else {
      this.alert = {display: true, class: 'danger', title: 'Erreur', message: '  Certaines valeurs sont invalides'};
      this.loading = false;
    }
  }

  saveHigherEducation(): void{
    this.academicService.createHigherEducation(this.higherEducation).subscribe((response) => {
      this.loading = false;
      this.refreshEvent.emit('higherEducation');
    }, (error) => {
      this.alert = {display: true, class: 'danger', title: 'Erreur', message: '  Erreur lors de l\'enregistrement'};
      this.loading = false;
    }, () => {});
  }

  validateHigherEducation(): boolean{
    this.initErrors();
    if (!this.higherEducation.establishmentName ){
      this.errors.valid = false;
      this.errors.establishmentName = 'Le nom de l\'etablissement est requis';
    }

    if (this.higherEducation.establishmentName !== undefined && this.higherEducation.establishmentName.length > 255){
      this.errors.valid = false;
      this.errors.establishmentName = 'Le nombre maximale de caractères est 255';
    }

    if (!this.higherEducation.city ){
      this.errors.valid = false;
      this.errors.city = 'La ville est requise';
    }

    if (this.higherEducation.city !== undefined && this.higherEducation.city.length > 255){
      this.errors.valid = false;
      this.errors.city = 'Le nombre maximale de caractères est 255';
    }

    if (this.higherEducation.specialty === undefined){
      this.errors.valid = false;
      this.errors.specialty = 'Selectionner une discipline ou spécialité étudiée';
    }

    if (this.higherEducation.establishmentType === undefined){
      this.errors.valid = false;
      this.errors.establishmentType = 'Selectionner un type d\'établissement';
    }else{
      if (this.higherEducation.establishmentType.specified)
      {
        if (this.higherEducation.anotherEstablishmentType === undefined || this.higherEducation.anotherEstablishmentType === '')
        {
          this.errors.valid = false;
          this.errors.anotherEstablishmentType = 'Le type d\'établissement est requis';
        }
        else if (this.higherEducation.anotherEstablishmentType !== undefined && this.higherEducation.anotherEstablishmentType.length > 255)
        {
          this.errors.valid = false;
          this.errors.anotherEstablishmentType = 'Le nombre maximal de caractères est 255';
        }
      }
    }

    if (this.higherEducation.studyLevel === undefined){
      this.errors.valid = false;
      this.errors.studyLevel = 'Selectionner un niveau d\'etudes';
    }else{
      if (this.higherEducation.studyLevel.specified)
      {
        if (this.higherEducation.anotherStudyLevel === undefined || this.higherEducation.anotherStudyLevel === '')
        {
          this.errors.valid = false;
          this.errors.anotherStudyLevel = 'Le niveau d\'études est requis';
        }
        else if (this.higherEducation.anotherStudyLevel !== undefined && this.higherEducation.anotherStudyLevel.length > 255)
        {
          this.errors.valid = false;
          this.errors.anotherStudyLevel = 'Le nombre maximal de caractères est 255';
        }
      }
    }

    if (this.higherEducation.teachingArea === undefined){
      this.errors.valid = false;
      this.errors.teachingArea = 'Selectionner un sous domaine d\'enseignement';
    }

    if (this.higherEducation.targetedDiploma === undefined){
      this.errors.valid = false;
      this.errors.targetedDiploma = 'Selectionner un diplome cible';
    }else{
      if (this.higherEducation.targetedDiploma.specified)
      {
        if (this.higherEducation.anotherTargetedDiploma === undefined || this.higherEducation.anotherTargetedDiploma === '')
        {
          this.errors.valid = false;
          this.errors.anotherTargetedDiploma = 'Le diplome cible est requis';
        }
        else if (this.higherEducation.anotherTargetedDiploma !== undefined && this.higherEducation.anotherTargetedDiploma.length > 255)
        {
          this.errors.valid = false;
          this.errors.anotherTargetedDiploma = 'Le nombre maximal de caractères est 255';
        }
      }
    }

    if (this.selectedTeachingAreaGroup === undefined){
      this.errors.valid = false;
      this.errors.teachingAreaSet = 'Selectionner un domaine d\'enseignment';
    }

    this.submitted = true;
    return this.errors.valid;
  }

  onclickCancel(): void {
    this.higherEducation = {} as HigherEducation;
    this.initErrors();
    this.refreshEvent.emit('higherEducation');
  }

  initErrors(): void{
    this.submitted = false;
    this.errors = {valid: true, frequentedClass: '', studyLevel: '', teachingArea: '',
      establishmentType: '', specialty: '', diploma: '', targetedDiploma: '',
      establishmentName: '', city: '', description: '', education: '', teachingAreaSet: '', anotherTargetedDiploma: '', anotherStudyLevel: '', anotherEstablishmentType: ''};
    this.alert = {} as Alert;
  }

  onChangeTargetedDiploma(event): void {
    this.errors.targetedDiploma = '';
    this.accountAcademics.diplomas.forEach(elt => {
      if (elt.label.toLowerCase() === event.target.value.toLowerCase()){
        this.higherEducation.targetedDiploma = elt;
      }
    });
  }

  onChangeTeachingArea(e): void {
    this.errors.teachingArea = '';
    this.accountAcademics.teachingAreas.forEach(elt => {
      if (elt.label.toLowerCase() === e.target.value.toLowerCase()){
        this.higherEducation.teachingArea = elt;
      }
    });
  }

  onChangeStudyLevel(e): void {
    this.errors.studyLevel = '';
    this.accountAcademics.studyLevels.forEach(elt => {
      if (elt.label.toLowerCase() === e.target.value.toLowerCase()){
        this.higherEducation.studyLevel = elt;
      }
    });
  }

  keyupEstablishmentName(): void {
    this.errors.establishmentName = '';
  }

  keyupCity(): void {
    this.errors.city = '';
  }
  keyupAnotherStudyLevel(): void  {
    this.errors.anotherStudyLevel = '';
  }

  keyupAnotherEstablishmentType(): void {
    this.errors.anotherEstablishmentType = '';
  }

  keyupAnotherTargetedDiploma(): void {
    this.errors.anotherTargetedDiploma = '';
  }
  onChangeEstablishmentType(e): void {
    this.errors.establishmentType = '';
    this.accountAcademics.establishmentTypes.forEach(elt => {
      if (elt.label.toLowerCase() === e.target.value.toLowerCase()){
        this.higherEducation.establishmentType = elt;
      }
    });
  }

  filterEstablishmentTypes(): void{
    this.establishmentTypesFiltered = [];
    this.accountAcademics.establishmentTypes.forEach(elt => {
      if (elt.education.code.toLowerCase() === 'higher'){
        this.establishmentTypesFiltered.push(elt);
      }
    });
  }

  filterDiplomas(): void{
    this.diplomasFiltered = [];
    this.accountAcademics.diplomas.forEach(elt => {
      if (elt.education.code.toLowerCase() === 'higher'){
        this.diplomasFiltered.push(elt);
      }
    });
  }

  filterStudyLevels(): void{
    this.studyLevelsFiltered = [];
    this.accountAcademics.studyLevels.forEach(elt => {
      if (elt.education.code.toLowerCase() === 'higher'){
        this.studyLevelsFiltered.push(elt);
      }
    });
  }

  keyupSpeciality(): void {
    this.errors.specialty = '';
  }

  onChangeTeachingAreaGroup(e): void {
    this.errors.teachingAreaGroup = '';

    this.accountAcademics.teachingAreas.forEach(elt => {
      if (elt.teachingAreaGroup.label.toLowerCase() === e.target.value.toLowerCase()){
        this.selectedTeachingAreaGroup = elt.teachingAreaGroup;
      }
    });

    this.teachingAreasFiltered = TeachingAreaHelper.filterTeachingArea(this.accountAcademics.teachingAreas, this.selectedTeachingAreaGroup);
  }

  filterTeachingAreaGroup(): void{
    this.teachings = TeachingAreaHelper.filterTeachingAreaGroup(this.accountAcademics.teachingAreas);
  }



}

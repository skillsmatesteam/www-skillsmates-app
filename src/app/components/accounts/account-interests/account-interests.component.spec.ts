import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountInterestsComponent } from './account-interests.component';

describe('AccountInterestsComponent', () => {
  let component: AccountInterestsComponent;
  let fixture: ComponentFixture<AccountInterestsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountInterestsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountInterestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

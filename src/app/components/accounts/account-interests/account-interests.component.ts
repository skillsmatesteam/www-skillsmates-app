import {Component, Input, OnInit} from '@angular/core';
import {InfoInterestType} from '../../../models/account/infoInterestType';
import {InterestsHelper} from '../../../helpers/interests-helper';
import {Account} from "../../../models/account/account";
import {AuthenticateService} from "../../../services/accounts/authenticate/authenticate.service";

@Component({
  selector: 'app-account-interests',
  templateUrl: './account-interests.component.html',
  styleUrls: ['./account-interests.component.css']
})
export class AccountInterestsComponent implements OnInit {

  infosInterestTypes: InfoInterestType[] = InterestsHelper.interestsTypes;
  loggedAccount: Account = {} as Account;
  @Input() account: Account;
  constructor(private authenticateService: AuthenticateService) { }

  ngOnInit(): void {
    this.loggedAccount = this.authenticateService.getCurrentAccount();
  }

}

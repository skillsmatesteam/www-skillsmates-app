import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccounteditskillsComponent } from './accounteditskills.component';

describe('AccounteditskillsComponent', () => {
  let component: AccounteditskillsComponent;
  let fixture: ComponentFixture<AccounteditskillsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccounteditskillsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccounteditskillsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

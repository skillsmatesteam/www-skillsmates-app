import {Component, HostListener, Input, OnInit} from '@angular/core';
import {Skill} from '../../../models/account/skill';
import {Account} from '../../../models/account/account';
import {Level} from '../../../models/account/config/level';
import {SkillService} from '../../../services/accounts/account/skill.service';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {ToastService} from '../../../services/toast/toast.service';
import {AccountSkills} from '../../../models/account/accountSkills';
import {Alert} from '../../../models/alert';
import {variables} from '../../../../environments/variables';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SkillHelper} from '../../../helpers/skill-helper';
import {infoText} from '../../../../environments/info-text';
import {MobileService} from '../../../services/mobile/mobile.service';

@Component({
  selector: 'app-accounteditskills',
  templateUrl: './accounteditskills.component.html',
  styleUrls: ['./accounteditskills.component.css']
})
export class AccounteditskillsComponent implements OnInit {

  displayNewSkillBlock: boolean;
  skill: Skill = {} as Skill;
  levels: Level[] = [];
  displayedLevels: Level[] = [];
  skills: Skill[] = [];
  skillsMastered: Skill[] = [];
  skillsToMaster: Skill[] = [];
  skillsToDisplay: Skill[] = [];
  accountSkills: AccountSkills = {} as AccountSkills;
  loading: boolean;
  alert: Alert = {} as Alert;
  submitted = false;
  errors;
  closeResult: string;
  modalTitle: string;
  skillMastered: boolean;
  maxDescriptionSize: number;
  @Input() account: Account = {} as Account;
  loggedAccount: Account = {} as Account;
  infoTextSkills = infoText.info_text_skills;
  skillsMasteredTab: string = variables.skills_mastered;
  skillsToDevelopTab: string = variables.skills_to_develop;
  tab: string = this.skillsMasteredTab;
  public screenWidth: number = window.innerWidth;

  constructor(private skillService: SkillService,
              private authenticateService: AuthenticateService,
              private modalService: NgbModal,
              private mobileService: MobileService,
              private toastService: ToastService) { }

  ngOnInit(): void {
    this.initErrors();
    this.displayNewSkillBlock = false;
    this.loggedAccount = this.authenticateService.getCurrentAccount();
    this.findAllSkills();
    this.maxDescriptionSize = variables.max_professional_description_size;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    this.screenWidth = window.innerWidth;
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }

  onClickNewSkill(skillMastered: boolean): void {
    this.displayNewSkillBlock = true;
    this.skill.skillMastered = skillMastered;
    this.displayedLevels = [];
    this.levels.forEach(elt => {
      if (elt.mastered === skillMastered){
        this.displayedLevels.push(elt);
      }
    });
  }

  onClickLevel(value: Level): void {
    this.errors.level = '';
    this.skill.level = value;
  }

  onclickCancel(): void {
    this.skill = {} as Skill;
    this.initErrors();
    this.displayNewSkillBlock = false;
  }

  onClickSave(): void {
    this.skill.account = this.authenticateService.getCurrentAccount();
    if (this.validateSkill()){
      this.skillService.createSkill(this.skill).subscribe((response) => {
        this.skill = {} as Skill;
        this.findAllSkills();
        this.toastService.showSuccess('Succès', 'Comptence enregistrée');
        this.displayNewSkillBlock = false;
      }, (error) => {
        this.alert = {display: true, class: 'danger', title: 'Erreur', message: '  Competence non enregistrée'};
        this.displayNewSkillBlock = false;
      }, () => {});
    } else {
      this.alert = {display: true, class: 'danger', title: 'Erreur', message: '  Formulaire invalide'};
    }
  }

  findAllSkills(): void{
    this.loading = true;
    const accountId = this.account.idServer;
    this.skillService.findSkills(accountId).subscribe((response) => {
      this.accountSkills = response.resource;
      this.sortSkills();
    }, (error) => {
    }, () => {
      this.loading = false;
    });
  }

  sortSkills(): void {
    this.initSkills();
    this.levels = this.accountSkills.levels;
    this.skills = this.accountSkills.skills;
    this.skillsMastered = this.skillsMastered.concat(SkillHelper.sortSkillsByLevel(this.skills, 2 ));
    this.skillsMastered = this.skillsMastered.concat(SkillHelper.sortSkillsByLevel(this.skills, 1 ));
    this.skillsToMaster = this.skillsToMaster.concat(SkillHelper.sortSkillsByLevel(this.skills, -2 ));
    this.skillsToMaster = this.skillsToMaster.concat(SkillHelper.sortSkillsByLevel(this.skills, -1 ));
  }

  // filterSkills(mastered: boolean): void{
  //   this.skillsToDisplay = [];
  //   const sortedSkills: Skill[] = [];
  //   if (this.skills && this.skills.length > 0){
  //     this.skills.forEach(elt => {
  //       if (elt.skillMastered === mastered){
  //         sortedSkills.push(elt);
  //       }
  //     });
  //   }
  //   this.skillsToDisplay = this.skillsToDisplay.concat(SkillHelper.sortSkillsByLevel(sortedSkills, mastered ? 2 : -2));
  //   this.skillsToDisplay = this.skillsToDisplay.concat(SkillHelper.sortSkillsByLevel(sortedSkills, mastered ? 1 : -1));
  // }

  computeTotalSkills(mastered: boolean): number{
    let nb = 0;
    if (this.skills && this.skills.length > 0){
      this.skills.forEach(elt => {
        if (elt.skillMastered === mastered){
          nb++;
        }
      });
    }
    return nb;
  }

  onSkillEvent(newEvent: string): void {
    if (newEvent === variables.refresh){
      this.skillsMastered = [];
      this.skillsToMaster = [];
      this.findAllSkills();
    } else if (newEvent === variables.edit){
      this.displayNewSkillBlock = true;
    }
  }

  onEditSkillEvent(skillToEdit: Skill): void{
    this.loading = true;
    this.displayNewSkillBlock = true;
    this.skill = skillToEdit;
    this.findAllSkills();
    this.onClickNewSkill(skillToEdit.skillMastered);
  }

  initSkills(): void{
    this.levels = [];
    this.skills = [];
    this.skillsMastered = [];
    this.skillsToMaster = [];
  }

  validateSkill(): boolean{
    this.initErrors();
    if (this.skill.label === undefined || this.skill.label === ''){
      this.errors.valid = false;
      this.errors.label = 'La compétence est requise';
    }

    if (this.skill.label !== undefined && this.skill.label.length > 255){
      this.errors.valid = false;
      this.errors.label = 'Le nombre maximale de caractères est 255';
    }

    if (this.skill.keywords === undefined){
      this.errors.valid = false;
      this.errors.keywords = 'Entrer un type de compétence';
    }

    if (this.skill.keywords !== undefined && this.skill.keywords.length > this.maxDescriptionSize){
      this.errors.valid = false;
      this.errors.description = 'Le nombre maximal de caractères est ' + this.maxDescriptionSize;
    }

    if (this.skill.discipline === undefined){
      this.errors.valid = false;
      this.errors.discipline = 'Entrer une discipline';
    }

    if (this.skill.level === undefined){
      this.errors.valid = false;
      this.errors.level = 'Selectionner un niveau';
    }
    this.submitted = true;
    return this.errors.valid;
  }

  initErrors(): void{
    this.submitted = false;
    this.errors = {valid: true, label: '', keywords: '', discipline: ''};
    this.alert = {} as Alert;
  }

  keyupSkillLabel(): void {
    this.errors.label = '';
  }

  keyupSkillDiscipline(): void {
    this.errors.discipline = '';
  }

  keyupSkillKeywords(): void {
    this.errors.keywords = '';
  }

  // open(content, mastered: boolean): void {
  //   this.skillMastered = mastered;
  //   mastered ? this.modalTitle = 'Compétences maitrisées' : this.modalTitle = 'Compétences à developper';
  //   this.filterSkills(mastered);
  //   if (this.skillsToDisplay.length > 0){
  //     this.modalService.open(content,     {
  //       size: 'lg'
  //     }).result.then((result) => {
  //       this.closeResult = `Closed with: ${result}`;
  //     }, (reason) => {
  //       this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  //     });
  //   }
  // }

  // private getDismissReason(reason: any): string {
  //   if (reason === ModalDismissReasons.ESC) {
  //     return 'by pressing ESC';
  //   } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
  //     return 'by clicking on a backdrop';
  //   } else {
  //     return  `with: ${reason}`;
  //   }
  // }

  onClickTab(tab: string): void {
    this.tab = tab;
  }
}

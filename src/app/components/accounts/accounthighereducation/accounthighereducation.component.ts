import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SecondaryEducation} from '../../../models/account/secondaryEducation';
import {AcademicService} from '../../../services/accounts/account/academic.service';
import {ToastService} from '../../../services/toast/toast.service';
import {ActivatedRoute, Router} from '@angular/router';
import {HigherEducation} from '../../../models/account/higherEducation';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Account} from '../../../models/account/account';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';

@Component({
  selector: 'app-accounthighereducation',
  templateUrl: './accounthighereducation.component.html',
  styleUrls: ['./accounthighereducation.component.css']
})
export class AccounthighereducationComponent implements OnInit {

  @Input() higherEducation: HigherEducation;
  @Input() account: Account = {} as Account;
  @Output() newHigherEducationEvent = new EventEmitter<string>();
  @Output() editHigherEducationEvent = new EventEmitter<HigherEducation>();
  higherEducationToDelete: HigherEducation = {} as HigherEducation;
  higherEducationToEdit: HigherEducation = {} as HigherEducation;
  id: string;
  loggedAccount: Account = {} as Account;

  constructor(private academicService: AcademicService,
              private toastService: ToastService,
              private router: Router,
              private authenticateService: AuthenticateService,
              private modalService: NgbModal,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.loggedAccount = this.authenticateService.getCurrentAccount();
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
  }

  onClickDelete(higherEducation: HigherEducation): void {
    this.higherEducationToDelete = higherEducation;
    localStorage.setItem('higherEducationToDelete', higherEducation.idServer);
    this.modalService.dismissAll();
  }

  onClickConfirmDelete(): void {
    this.academicService.deleteHigherEducation(localStorage.getItem('higherEducationToDelete')).subscribe((response) => {
      this.toastService.showSuccess('Succès', 'Formation superieure supprimé avec succés');
      this.newHigherEducationEvent.emit('refresh');
    }, (error) => {
      this.toastService.showError('Error', 'Formation superieure non supprimé');
    }, () => {
      localStorage.removeItem('higherEducationToDelete');
    });
  }

  onClickEdit(higherEducation: HigherEducation): void {
    this.editHigherEducationEvent.emit(higherEducation);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccounthighereducationComponent } from './accounthighereducation.component';

describe('AccounthighereducationComponent', () => {
  let component: AccounthighereducationComponent;
  let fixture: ComponentFixture<AccounthighereducationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccounthighereducationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccounthighereducationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

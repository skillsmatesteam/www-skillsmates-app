import {Component, Input, OnInit} from '@angular/core';
import {Account} from '../../../models/account/account';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {StringHelper} from '../../../helpers/string-helper';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {AccountSocialInteractions} from '../../../models/account/accountSocialInteractions';
import {RestCountryService} from '../../../services/api/rest-country.service';

@Component({
  selector: 'app-accountprofile',
  templateUrl: './accountprofile.component.html',
  styleUrls: ['./accountprofile.component.css']
})
export class AccountprofileComponent implements OnInit {

  @Input() accountSocialInteractions: AccountSocialInteractions;
  @Input() loading: boolean;
  @Input() showFollowButton: boolean;
  loggedAccount: Account = {} as Account;
  routes = RoutesHelper.routes;
  accountCountryName: string;

  constructor(private authenticationService: AuthenticateService, private restCountryService: RestCountryService) { }

  ngOnInit(): void {
    this.loggedAccount = this.authenticationService.getCurrentAccount();
  }

  getEstablishmentIcon(): string{
    return StringHelper.getEstablishmentIcon(this.accountSocialInteractions.account);
  }

}

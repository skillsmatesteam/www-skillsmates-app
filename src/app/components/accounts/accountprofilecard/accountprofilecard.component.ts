import {Component, HostListener, Input, OnInit} from '@angular/core';
import {Account} from '../../../models/account/account';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {SubscriptionService} from '../../../services/accounts/subscription/subscription.service';
import {AccountService} from '../../../services/accounts/account/account.service';
import {variables} from '../../../../environments/variables';
import {SocialInteractionService} from '../../../services/pages/social-interaction/social-interaction.service';
import {WebSocketService} from '../../../services/message/web-socket.service';
import {Router} from '@angular/router';
import {ToastService} from '../../../services/toast/toast.service';
import {StringHelper} from '../../../helpers/string-helper';
import {AccountSocialInteractions} from '../../../models/account/accountSocialInteractions';
import {SocialInteractionsHelper} from '../../../helpers/social-interactions-helper';
import {MobileService} from '../../../services/mobile/mobile.service';
import {AccountNetwork} from '../../../models/network/accountNetwork';
import {RestCountryService} from '../../../services/api/rest-country.service';
import {AccountProfileUrl} from '../../../models/account/accountProfileUrl';

@Component({
  selector: 'app-accountprofilecard',
  templateUrl: './accountprofilecard.component.html',
  styleUrls: ['./accountprofilecard.component.css']
})
export class AccountprofilecardComponent implements OnInit {
  @Input() accountId: string;
  @Input() isEdit: boolean;
  loading: boolean;
  loggedAccount: Account = {} as Account;
  routes = RoutesHelper.routes;
  accountSocialInteractions: AccountSocialInteractions = {} as AccountSocialInteractions;
  favoriteImage = 'favoris';
  accountNetwork: AccountNetwork = {} as AccountNetwork;
  public screenWidth: number = window.innerWidth;
  accountProfileUrl: AccountProfileUrl = {} as AccountProfileUrl;

  constructor(private authenticateService: AuthenticateService,
              private subscriptionService: SubscriptionService,
              private socialInteractionService: SocialInteractionService,
              private webSocketService: WebSocketService,
              private router: Router,
              private restCountryService: RestCountryService,
              private toastService: ToastService,
              private mobileService: MobileService,
              private accountService: AccountService) { }

  ngOnInit(): void {
    this.loading = true;
    this.loggedAccount = this.authenticateService.getCurrentAccount();
    if (this.loggedAccount) {
      this.findAccountSocialInteractions();
    } else {
      this.findAccountSocialInteractionsOffline();
    }
  }

  public name(account: Account): string {
    return StringHelper.truncateName(account, 30);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    this.screenWidth = window.innerWidth;
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }

  onClickFollow(account: Account): void {
    this.loading = true;
    this.socialInteractionService.addAccountSocialInteraction(this.loggedAccount, account, variables.follower).subscribe((response) => {
      const socialInteraction = response.resource;
      if (socialInteraction.active) {
        this.accountSocialInteractions.socialInteractions.push(socialInteraction);
        this.toastService.showSuccess('Abonné', 'Vous êtes à présent abonné à ' + account.firstname + ' ' + account.lastname);
      } else {
        SocialInteractionsHelper.removeSocialInteraction(this.accountSocialInteractions.socialInteractions, socialInteraction);
        this.toastService.showSuccess('Abonné', 'Vous vous êtes désabonné de ' + account.firstname + ' ' + account.lastname);
      }
      this.loading = false;
      // send a notification
      this.webSocketService.sendPushNotification(socialInteraction);
    }, error => {
      this.loading = false;
      this.toastService.showSuccess('Abonné', 'Erreur lors du traitement');
    });
  }

  onClickUnfollow(account: Account): void {
    this.loading = true;
    // tslint:disable-next-line:max-line-length
    const socialInteraction = SocialInteractionsHelper.retrieveSocialInteractionsWithCriteria(this.accountSocialInteractions.socialInteractions, account.idServer, this.loggedAccount, account, variables.follower);
    // tslint:disable-next-line:max-line-length
    this.accountSocialInteractions.socialInteractions = SocialInteractionsHelper.removeSocialInteraction(this.accountSocialInteractions.socialInteractions, socialInteraction);
    this.socialInteractionService.delete(socialInteraction.idServer).subscribe((response) => {
      this.toastService.showSuccess('Abonné', 'Vous vous êtes désabonné de ' + account.firstname + ' ' + account.lastname);
    }, (error) => {
      this.toastService.showSuccess('Abonné', 'Erreur lors du traitement');
    });
  }

  /**
   *  add an account as favorite
   * @param account: to be our favorite
   */
  onClickFavorite(account: Account): void {
      this.socialInteractionService.addAccountSocialInteraction(this.loggedAccount, account, variables.favorite).subscribe((response) => {
        const socialInteraction = response.resource;
        if (socialInteraction.active) {
          this.favoriteImage = 'favoris-active';
          this.accountSocialInteractions.socialInteractions.push(socialInteraction);
          this.toastService.showSuccess('Favoris', 'Compte mis en favoris');
        } else {
          this.favoriteImage = 'favoris';
          SocialInteractionsHelper.removeSocialInteraction(this.accountSocialInteractions.socialInteractions, socialInteraction);
          this.toastService.showSuccess('Favoris', 'Compte retiré des favoris');
        }
        // send a notification
        this.webSocketService.sendPushNotification(socialInteraction);
      }, error => {
        this.toastService.showSuccess('Favoris', 'Erreur lors du traitement');
      });
  }

  onClickFollowees(): void {
    localStorage.setItem(variables.tab, '3'); // 3 = position for followee
  }

  onClickFollowers(): void {
    localStorage.setItem(variables.tab, '2'); // 2 = position for follower
  }

  getProfessionalIcon(): string{
    return StringHelper.getEstablishmentIcon(this.accountSocialInteractions.account);
  }

  findAccountSocialInteractions(): void{
    if (this.accountId){
      this.accountService.findAccountSocialInteractions(this.accountId).subscribe((response) => {
        this.accountSocialInteractions = response.resource;
        this.findAccount();
        this.loading = false;
      }, (error => {}));
    }
  }

  findAccountSocialInteractionsOffline(): void{
    if (this.accountId){
      this.accountService.findAccountSocialInteractionsOffline(this.accountId).subscribe((response) => {
        this.accountSocialInteractions = response.resource;
        this.findAccountOffline();
        this.loading = false;
      }, (error => {}));
    }
  }

  findAccount(): void{
    if (this.accountId){
      this.accountService.findAccountById(this.accountId).subscribe((response) => {
        this.accountSocialInteractions.account = response.resource;
        this.findNetwork();
        this.loading = false;
      }, (error => {}));
    }
  }
  findAccountOffline(): void{
    if (this.accountId){
      this.accountService.findOfflineAccountById(this.accountId).subscribe((response) => {
        this.accountSocialInteractions.account = response.resource;
        this.findNetworkOffline();
        this.loading = false;
      }, (error => {}));
    }
  }
  countFollowers(): number{
    return this.accountNetwork && this.accountNetwork.numberFollowers ? this.accountNetwork.numberFollowers : 0;
  }

  countFollowees(): number{
    return this.accountNetwork && this.accountNetwork.numberFollowees ? this.accountNetwork.numberFollowees : 0;
  }

  countFavorites(): number{
    return SocialInteractionsHelper.countFavorites(this.accountSocialInteractions.socialInteractions);
  }

  isFollowing(receiver: Account): boolean{
    return SocialInteractionsHelper.isFollowing(this.accountSocialInteractions.socialInteractions, this.loggedAccount, receiver);
  }

  findNetwork(): void{
    this.subscriptionService.findNetworkByPage(this.accountId, 0, 1).subscribe((response) => {
      this.accountNetwork = response.resource;
    }, (error) => {
    }, () => {});
  }

  findNetworkOffline(): void{
    this.subscriptionService.findNetworkOfflineByPage(this.accountId, 0).subscribe((response) => {
      this.accountNetwork = response.resource;
    }, (error) => {
    }, () => {});
  }

  onClickCopy(account: Account): void {
      this.onClickGenerateUrl(account);
  }

  onClickGenerateUrl(account: Account): void {
    this.accountService.generateUrlProfile(account.idServer).subscribe((response) => {
      this.accountProfileUrl = response.resource;
      this.toastService.showSuccess('Succès', 'Lien copié avec succés');

    }, (error) => {
      this.toastService.showError('Error', 'Lien non copié');
    }, () => {
      this.copy(this.accountProfileUrl.url);
    });
  }

  copy(url: string): void {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = url;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }
}

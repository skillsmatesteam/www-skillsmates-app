import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountprofilecardComponent } from './accountprofilecard.component';

describe('AccountprofilecardComponent', () => {
  let component: AccountprofilecardComponent;
  let fixture: ComponentFixture<AccountprofilecardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountprofilecardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountprofilecardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

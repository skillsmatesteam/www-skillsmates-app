import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditdegreeobtainedComponent } from './editdegreeobtained.component';

describe('EditdegreeobtainedComponent', () => {
  let component: EditdegreeobtainedComponent;
  let fixture: ComponentFixture<EditdegreeobtainedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditdegreeobtainedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditdegreeobtainedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Alert} from '../../../models/alert';
import {DegreeObtained} from '../../../models/account/degreeObtained';
import {AccountAcademics} from '../../../models/account/accountAcademics';
import {EstablishmentType} from '../../../models/account/config/establishmentType';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {AcademicService} from '../../../services/accounts/account/academic.service';
import {DatePipe} from '@angular/common';
import {Education} from '../../../models/account/config/education';
import {Diploma} from '../../../models/account/config/diploma';
import {TeachingArea} from '../../../models/account/config/teachingArea';
import {TeachingAreaSet} from '../../../models/account/config/teachingAreaSet';
import {ObjectHelper} from '../../../helpers/object-helper';
import {TeachingAreaGroup} from '../../../models/account/config/teachingAreaGroup';
import {TeachingAreaHelper} from '../../../helpers/teaching-area-helper';

@Component({
  selector: 'app-editdegreeobtained',
  templateUrl: './editdegreeobtained.component.html',
  styleUrls: ['./editdegreeobtained.component.css']
})
export class EditdegreeobtainedComponent implements OnInit {
  @Input() accountAcademics: AccountAcademics = {} as AccountAcademics;
  @Input() degreeObtained: DegreeObtained = {} as DegreeObtained;
  @Output() refreshEvent = new EventEmitter<string>();
  loading: boolean;
  alert: Alert = {} as Alert;
  submitted = false;
  errors;
  establishmentTypesFiltered: EstablishmentType[];
  diplomaFiltered: Diploma[];
  selectedEducation: Education = {} as Education;
  teachingAreas: TeachingArea[];
  teachingAreaSets: TeachingAreaSet[];
  teachingAreasFiltered: TeachingArea[];
  selectedTeachingAreaSet: TeachingAreaSet;
  selectedTeachingAreaGroup: TeachingAreaGroup;
  teachings = [];
  degreeObtainedStartDate: Date;
  degreeObtainedEndDate: Date;
  disabledEndDate = true;

  constructor(private authenticateService: AuthenticateService,
              private academicService: AcademicService,
              private datePipe: DatePipe) { }

  ngOnInit(): void {
    this.loading = true;
    this.initErrors();
    this.filterTeachingAreaGroup();
    if (!ObjectHelper.isEmpty(this.degreeObtained)){
      this.selectedEducation = this.degreeObtained.diploma.education;

      this.degreeObtainedStartDate= this.degreeObtained.startDate;
      this.degreeObtainedEndDate = this.degreeObtained.endDate;

      this.filterDiplomas(this.selectedEducation);
      this.filterEstablishmentTypes(this.selectedEducation);
      if (this.degreeObtained.teachingArea){
        this.selectedTeachingAreaGroup = this.degreeObtained.teachingArea.teachingAreaGroup;
        this.teachingAreasFiltered = TeachingAreaHelper.filterTeachingArea(this.accountAcademics.teachingAreas, this.selectedTeachingAreaGroup);
      }
    }
    this.loading = false;
  }

  onChangeEducation(e): void {
    this.errors.education = '';
    this.accountAcademics.educations.forEach(elt => {
      if (elt.label.toLowerCase() === e.target.value.toLowerCase()){
        this.selectedEducation = elt;
      }
    });
    this.filterDiplomas(this.selectedEducation);
    this.filterEstablishmentTypes(this.selectedEducation);
  }

  filterDiplomas(education: Education): void{
    this.diplomaFiltered = [];
    this.accountAcademics.diplomas.forEach(elt => {
      if (elt.education.id === education.id){
        this.diplomaFiltered.push(elt);
      }
    });
  }

  filterEstablishmentTypes(education: Education): void{
    this.establishmentTypesFiltered = [];
    this.accountAcademics.establishmentTypes.forEach(elt => {
      if (elt.education.id === education.id){
        this.establishmentTypesFiltered.push(elt);
      }
    });
  }

  onChangeDiploma(e): void {
    this.errors.diploma = '';
    this.accountAcademics.diplomas.forEach(elt => {
      if (elt.label.toLowerCase() === e.target.value.toLowerCase()){
        this.degreeObtained.diploma = elt;
      }
    });
  }

  keyupEstablishmentName(): void {
    this.errors.establishmentName = '';
  }

  keyupAnotherEstablishmentType(): void {
    this.errors.anotherEstablishmentType = '';
  }

  keyupCity(): void {
    this.errors.city = '';
  }

  onChangeEstablishmentType(e): void {
    this.errors.establishmentType = '';
    this.accountAcademics.establishmentTypes.forEach(elt => {
      if (elt.label.toLowerCase() === e.target.value.toLowerCase()){
        this.degreeObtained.establishmentType = elt;
      }
    });
  }

  onChangeTeachingArea(e): void {
    this.errors.teachingArea = '';
    this.accountAcademics.teachingAreas.forEach(elt => {
      if (elt.label.toLowerCase() === e.target.value.toLowerCase()){
        this.degreeObtained.teachingArea = elt;
      }
    });
  }

  changeStartDate(): void {
    this.errors.startDate = '';
  }

  changeEndDate(): void {
    this.errors.endDate = '';
  }

  onClickSaveDegreeObtained(): void {
    this.loading = true;
    this.degreeObtained.account = this.authenticateService.getCurrentAccount();
    if (this.validateDegreeObtained()){
      this.saveDegreeObtained();
    } else {
      this.alert = {display: true, class: 'danger', title: 'Erreur', message: '  Certaines valeurs sont invalides'};
      this.loading = false;
    }
  }

  validateDegreeObtained(): boolean{
    this.initErrors();

    if (!this.selectedEducation || !this.selectedEducation.code){
      this.errors.valid = false;
      this.errors.education = 'L\'enseignement est requis';
    }

    if (!this.degreeObtained.establishmentName ){
      this.errors.valid = false;
      this.errors.establishmentName = 'Le nom de l\'etablissement est requis';
    }

    if (this.degreeObtained.establishmentName !== undefined && this.degreeObtained.establishmentName.length > 255){
      this.errors.valid = false;
      this.errors.establishmentName = 'Le nombre maximale de caractères est 255';
    }

    if (!this.degreeObtained.city ){
      this.errors.valid = false;
      this.errors.city = 'La ville est requise';
    }

    if (this.degreeObtained.city !== undefined && this.degreeObtained.city.length > 255){
      this.errors.valid = false;
      this.errors.city = 'Le nombre maximale de caractères est 255';
    }

    if (this.degreeObtained.diploma === undefined){
      this.errors.valid = false;
      this.errors.diploma = 'Sélectionner un niveau d\'étude';
    }else{
      if (this.degreeObtained.diploma.specified)
      {
        if (this.degreeObtained.anotherDiploma === undefined || this.degreeObtained.anotherDiploma === '')
        {
          this.errors.valid = false;
          this.errors.anotherDiploma = 'Le niveau d\'étude du diplome est requis';
        }
        else if (this.degreeObtained.anotherDiploma !== undefined && this.degreeObtained.anotherDiploma.length > 255)
        {
          this.errors.valid = false;
          this.errors.anotherDiploma = 'Le nombre maximal de caractères est 255';
        }
      }
    }

    if (this.degreeObtained.specialty === undefined){
      this.errors.valid = false;
      this.errors.specialty = 'Selectionner une specialité';
    }

    if (this.degreeObtained.establishmentType === undefined){
      this.errors.valid = false;
      this.errors.establishmentType = 'Selectionner un type d\'etablissement';
    } else{
      if (this.degreeObtained.establishmentType.specified)
      {
        if (this.degreeObtained.anotherEstablishmentType === undefined || this.degreeObtained.anotherEstablishmentType === '')
        {
          this.errors.valid = false;
          this.errors.anotherEstablishmentType = 'Le type d\'établissement est requis';
        }
        else if (this.degreeObtained.anotherEstablishmentType !== undefined && this.degreeObtained.anotherEstablishmentType.length > 255)
        {
          this.errors.valid = false;
          this.errors.anotherEstablishmentType = 'Le nombre maximal de caractères est 255';
        }
      }
    }

    if (this.selectedEducation.code.toLowerCase() === 'higher'){
      if (this.selectedTeachingAreaGroup === undefined){
        this.errors.valid = false;
        this.errors.teachingAreaGroup = 'Selectionner un domaine d\'enseignment';
      }

      if (this.degreeObtained.teachingArea === undefined){
        this.errors.valid = false;
        this.errors.teachingArea = 'Selectionner un sous domaine d\'enseignement';
      }
    }else {
      this.degreeObtained.teachingArea = {} as TeachingArea;
    }

    if (!this.degreeObtained.startDate){
      this.errors.valid = false;
      this.errors.startDate = 'Selectionner une date de début';
    }

    if (!this.degreeObtained.endDate){
      this.errors.valid = false;
      this.errors.endDate = 'Selectionner une date de fin';
    }

    if (!this.areDatesCorrect()){
      this.errors.valid = false;
      this.errors.startDate = 'Date incohérente';
      this.errors.endDate = 'Date incohérente';
      this.degreeObtained.startDate = null;
      this.degreeObtained.endDate = null;
    }

    this.submitted = true;
    return this.errors.valid;
  }

  saveDegreeObtained(): void{
    this.academicService.createDegreeObtained(this.degreeObtained).subscribe((response) => {
      this.loading = false;
      this.refreshEvent.emit('degreeObtained');
    }, (error) => {
      this.alert = {display: true, class: 'danger', title: 'Erreur', message: '   Erreur lors de l\'enregistrement du diplome'};
      this.loading = false;
    }, () => {});
  }

  onclickCancel(): void {
    this.initErrors();
    this.degreeObtained = {} as DegreeObtained;
    this.refreshEvent.emit('degreeObtained');
  }

  initErrors(): void{
    this.submitted = false;
    this.errors = {valid: true, studyLevel: '', teachingArea: '', teachingAreaGroup: '',
      establishmentType: '', specialty: '', diploma: '', targetedDiploma: '', preparedDiploma: '',
      establishmentName: '', city: '', description: '', education: '', anotherDiploma: '', anotherEstablishmentType: ''};
    this.alert = {} as Alert;
  }

  keyupSpeciality(): void {
    this.errors.specialty = '';
  }

  keyupAnotherDiploma(): void {
    this.errors.anotherDiploma = '';
  }

  onChangeTeachingAreaGroup(e): void {
    this.errors.teachingAreaGroup = '';

    this.accountAcademics.teachingAreas.forEach(elt => {
      if (elt.teachingAreaGroup.label.toLowerCase() === e.target.value.toLowerCase()){
        this.selectedTeachingAreaGroup = elt.teachingAreaGroup;
      }
    });

    this.teachingAreasFiltered = TeachingAreaHelper.filterTeachingArea(this.accountAcademics.teachingAreas, this.selectedTeachingAreaGroup);
  }

  filterTeachingAreaGroup(): void{
    this.teachings = TeachingAreaHelper.filterTeachingAreaGroup(this.accountAcademics.teachingAreas);
  }

  selectStartDate(): void{
    this.errors.startDate = '';
    // this.degreeObtainedStartDate = degreeObtainedStartDate;
    if (ObjectHelper.isDateValid(this.degreeObtainedStartDate)){
      this.degreeObtained.startDate = this.degreeObtainedStartDate;
      this.disabledEndDate = false;
      this.errors.valid = true;
      this.submitted = false;
    }else {
      this.disabledEndDate = true;
      this.errors.valid = false;
      this.submitted = true;
      this.degreeObtained.startDate = null;
      this.errors.startDate = 'Date invalide';
    }
  }

  selectEndDate(): void{
    this.errors.endDate = '';
    // this.degreeObtainedEndDate = degreeObtainedEndDate;
    if (ObjectHelper.isDateValid(this.degreeObtainedEndDate)){
      this.degreeObtained.endDate = this.degreeObtainedEndDate;
      if (this.areDatesCorrect()){
        this.errors.valid = true;
        this.submitted = false;
      }else {
        this.errors.valid = false;
        this.submitted = true;
        this.degreeObtained.endDate = null;
        this.errors.startDate = 'Date incohérente';
        this.errors.endDate = 'Date incohérente';
      }
    }else {
      this.errors.valid = false;
      this.submitted = true;
      this.degreeObtained.endDate = null;
      this.errors.endDate = 'Date invalide';
    }
  }

  areDatesCorrect(): boolean{
    return this.degreeObtained.startDate && this.degreeObtained.endDate &&
      this.degreeObtained.startDate <= this.degreeObtained.endDate;
  }
}

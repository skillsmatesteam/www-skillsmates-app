import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Alert} from '../../../models/alert';
import {AccountAcademics} from '../../../models/account/accountAcademics';
import {ObjectHelper} from '../../../helpers/object-helper';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {AcademicService} from '../../../services/accounts/account/academic.service';
import {DatePipe} from '@angular/common';
import {Certification} from '../../../models/account/certification';

@Component({
  selector: 'app-edit-certification',
  templateUrl: './edit-certification.component.html',
  styleUrls: ['./edit-certification.component.css']
})
export class EditCertificationComponent implements OnInit {
  @Input() accountAcademics: AccountAcademics = {} as AccountAcademics;
  @Input() certificate: Certification = {} as Certification;
  @Output() refreshEvent = new EventEmitter<string>();
  loading: boolean;
  alert: Alert = {} as Alert;
  submitted = false;
  errors;
  certificateObtainedDate: Date;
  certificateExpirationDate: Date;
  disabledExpirationDate = true;

  constructor(private authenticateService: AuthenticateService,
              private academicService: AcademicService,
              private datePipe: DatePipe) { }

  ngOnInit(): void {
    this.loading = true;
    this.initErrors();
    if (!ObjectHelper.isEmpty(this.certificate)){

      this.certificateObtainedDate = this.certificate.obtainedDate;
      this.certificateExpirationDate = this.certificate.expirationDate;
    }
    this.loading = false;
  }

  keyupIssuingInstitutionName(): void {
    this.errors.issuingInstitutionName = '';
  }
  keyupEntitledCertification(): void {
    this.errors.entitledCertification = '';
  }
  keyupAnotherActivityArea(): void {
    this.errors.anotherActivityArea = '';
  }

  keyupAnotherEstablishmentCertificationType(): void {
    this.errors.anotherEstablishmentCertificationType = '';
  }

  keyupCity(): void {
    this.errors.city = '';
  }

  onChangeEstablishmentCertificationType(e): void {
    this.errors.establishmentCertificationType = '';
    this.accountAcademics.establishmentCertificationTypes.forEach(elt => {
      if (elt.label.toLowerCase() === e.target.value.toLowerCase()){
        this.certificate.establishmentCertificationType = elt;
      }
    });
  }

  onChangeActivityArea(e): void {
    this.errors.activityArea = '';
    this.accountAcademics.activityAreas.forEach(elt => {
      if (elt.label.toLowerCase() === e.target.value.toLowerCase()){
        this.certificate.activityArea = elt;
      }
    });
  }

  changeObtainedDate(): void {
    this.errors.obtainedDate = '';
  }

  changeExpirationDate(): void {
    this.errors.expirationDate = '';
  }

  validateCertificate(): boolean{
    this.initErrors();

    if (!this.certificate.issuingInstitutionName ){
      this.errors.valid = false;
      this.errors.issuingInstitutionName = 'Le nom de l\'etablissement de délivrance est requis';
    }

    if (this.certificate.issuingInstitutionName !== undefined && this.certificate.issuingInstitutionName.length > 255){
      this.errors.valid = false;
      this.errors.issuingInstitutionName = 'Le nombre maximale de caractères est 255';
    }

    if (!this.certificate.entitledCertification ){
      this.errors.valid = false;
      this.errors.entitledCertification = 'L\'intitulé de la certification est requis';
    }

    if (this.certificate.entitledCertification !== undefined && this.certificate.entitledCertification.length > 255){
      this.errors.valid = false;
      this.errors.entitledCertification = 'Le nombre maximale de caractères est 255';
    }


    if (this.certificate.establishmentCertificationType === undefined){
      this.errors.valid = false;
      this.errors.establishmentCertificationType = 'Sélectionner un type d\'établissement de délivrance';
    }else{
      if (this.certificate.establishmentCertificationType.specified) {
        if (this.certificate.anotherEstablishmentCertificationType === undefined || this.certificate.anotherEstablishmentCertificationType === '') {
          this.errors.valid = false;
          this.errors.anotherEstablishmentCertificationType = 'Le type d\'établissement de délivrance est requis';
        } else if (this.certificate.anotherEstablishmentCertificationType !== undefined && this.certificate.anotherEstablishmentCertificationType.length > 255) {
          this.errors.valid = false;
          this.errors.anotherEstablishmentCertificationType = 'Le nombre maximal de caractères est 255';
        }
      }
    }

    if (this.certificate.activityArea === undefined){
      this.errors.valid = false;
      this.errors.activityArea = 'Selectionner un domaine d\'activité';
    } else{
      if (this.certificate.activityArea.specified)
      {
        if (this.certificate.anotherActivityArea === undefined || this.certificate.anotherActivityArea === '')
        {
          this.errors.valid = false;
          this.errors.anotherActivityArea = 'Le domaine d\'activité est requis';
        }
        else if (this.certificate.anotherActivityArea !== undefined && this.certificate.anotherActivityArea.length > 255)
        {
          this.errors.valid = false;
          this.errors.anotherActivityArea = 'Le nombre maximal de caractères est 255';
        }
      }
    }

    if (!this.certificate.temporary && !this.certificate.permanent){
      this.errors.valid = false;
      this.errors.time = 'Veuillez cocher une case';
    }

    if (!this.certificate.obtainedDate){
      this.errors.valid = false;
      this.errors.obtainedDate = 'Selectionner une date d\'obtention';
    }

    if (this.certificate.temporary && !this.certificate.expirationDate){
      this.errors.valid = false;
      this.errors.expirationDate = 'Selectionner une date d\'expiration';
    }

    if (!this.areDatesCorrect()){
      this.errors.valid = false;
      this.errors.obtainedDate = 'Date incohérente';
      this.errors.expirationDate = 'Date incohérente';
      this.certificate.obtainedDate = null;
      this.certificate.expirationDate = null;
    }

    this.submitted = true;
    return this.errors.valid;
  }

  saveCertificate(): void{

    this.academicService.createCertification(this.certificate).subscribe((response) => {
      this.loading = false;
      this.refreshEvent.emit('certificates');
    }, (error) => {
      this.alert = {display: true, class: 'danger', title: 'Erreur', message: '   Erreur lors de l\'enregistrement de la certification'};
      this.loading = false;
    }, () => {});
  }

  initErrors(): void{
    this.submitted = false;
    this.errors = {valid: true, issuingInstitutionName: '', entitledCertification: '', activityArea: '', time: '',
      establishmentCertificationType: '', description: '', anotherEstablishmentCertificationType: '', anotherActivityArea: ''};
    this.alert = {} as Alert;
  }

  keyupSpeciality(): void {
    this.errors.specialty = '';
  }

  keyupAnotherDiploma(): void {
    this.errors.anotherDiploma = '';
  }


  selectObtainedDate(): void{
    this.errors.obtainedDate = '';
    if (ObjectHelper.isDateValid(this.certificateObtainedDate)){
      this.certificate.obtainedDate = this.certificateObtainedDate;
      this.disabledExpirationDate = false;
      this.errors.valid = true;
      this.submitted = false;
    }else {
      this.disabledExpirationDate = true;
      this.errors.valid = false;
      this.submitted = true;
      this.certificate.obtainedDate = null;
      this.errors.obtainedDate = 'Date invalide';
    }
  }

  selectExpirationDate(): void{
    this.errors.expirationDate = '';
    if (ObjectHelper.isDateValid(this.certificateExpirationDate)){
      this.certificate.expirationDate = this.certificateExpirationDate;
      if (this.areDatesCorrect()){
        this.errors.valid = true;
        this.submitted = false;
      }else {
        this.errors.valid = false;
        this.submitted = true;
        this.certificate.expirationDate = null;
        this.errors.obtainedDate = 'Date incohérente';
        this.errors.expirationDate = 'Date incohérente';
      }
    }else {
      this.errors.valid = false;
      this.submitted = true;
      this.certificate.expirationDate = null;
      this.errors.expirationDate = 'Date invalide';
    }
  }

  areDatesCorrect(): boolean{
    if (this.certificate.permanent){
      if (this.certificate.obtainedDate) {
        return true;
      }
      else { return false; }
    }
    else { return this.certificate.obtainedDate && this.certificate.expirationDate &&
        this.certificate.obtainedDate <= this.certificate.expirationDate;
    }
  }

  onClickCancel(): void {
    this.initErrors();
    this.certificate = {} as Certification;
    this.refreshEvent.emit('certificates');
  }

  onClickSaveCertificate(): void {
    this.loading = true;
    this.certificate.account = this.authenticateService.getCurrentAccount();
    if (this.validateCertificate()){
      this.saveCertificate();
    } else {
      this.alert = {display: true, class: 'danger', title: 'Erreur', message: '  Certaines valeurs sont invalides'};
      this.loading = false;
    }
  }

  toggleTemporary($event: Event): void {
    if (this.certificate.temporary) {
      this.certificate.permanent = false;
    }
  }

  togglePermanent($event: Event): void {
    if (this.certificate.permanent) {
      this.certificate.temporary = false;
    }
  }
}

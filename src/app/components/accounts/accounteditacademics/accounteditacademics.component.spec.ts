import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccounteditacademicsComponent } from './accounteditacademics.component';

describe('AccounteditacademicsComponent', () => {
  let component: AccounteditacademicsComponent;
  let fixture: ComponentFixture<AccounteditacademicsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccounteditacademicsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccounteditacademicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

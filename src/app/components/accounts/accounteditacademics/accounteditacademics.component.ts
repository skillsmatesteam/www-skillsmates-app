import {Component, HostListener, Input, OnInit} from '@angular/core';
import {DegreeObtained} from '../../../models/account/degreeObtained';
import {SecondaryEducation} from '../../../models/account/secondaryEducation';
import {HigherEducation} from '../../../models/account/higherEducation';
import {Education} from '../../../models/account/config/education';
import {ToastService} from '../../../services/toast/toast.service';
import {DatePipe} from '@angular/common';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {AccountAcademics} from '../../../models/account/accountAcademics';
import {Account} from '../../../models/account/account';
import {AcademicService} from '../../../services/accounts/account/academic.service';
import {Alert} from '../../../models/alert';
import {ModalDismissReasons, NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {infoText} from '../../../../environments/info-text';
import {variables} from '../../../../environments/variables';
import {MobileService} from '../../../services/mobile/mobile.service';
import {Certification} from '../../../models/account/certification';

@Component({
  selector: 'app-accounteditacademics',
  templateUrl: './accounteditacademics.component.html',
  styleUrls: ['./accounteditacademics.component.css']
})
export class AccounteditacademicsComponent implements OnInit {
  @Input() account: Account = {} as Account;
  degreeObtained: DegreeObtained = {} as DegreeObtained;
  certification: Certification = {} as Certification;
  secondaryEducation: SecondaryEducation = {} as SecondaryEducation;
  higherEducation: HigherEducation = {} as HigherEducation;
  loading: boolean;
  accountAcademics: AccountAcademics = {} as AccountAcademics;
  selectedEducation: Education = {} as Education;
  alert: Alert = {} as Alert;
  submitted = false;
  errors;
  displayAcademics: boolean;
  displayEducation: boolean;
  displayDiplomas: boolean;
  displayCertificates: boolean;
  closeResult: string;
  modalOptions: NgbModalOptions;
  loggedAccount: Account = {} as Account;
  infoTextAcademic = infoText.info_text_academic;
  formation: string = variables.formation;
  diploma: string = variables.diploma;
  certificates: string = variables.certificate;
  certificate: Certification = {} as Certification;
  tab: string = this.formation;
  public screenWidth: number = window.innerWidth;

  constructor(private academicService: AcademicService,
              private toastService: ToastService,
              private datePipe: DatePipe,
              private modalService: NgbModal,
              private mobileService: MobileService,
              private authenticateService: AuthenticateService) { }

  ngOnInit(): void {
    this.loggedAccount = this.authenticateService.getCurrentAccount();
    this.findAccountAcademics();
    this.resetDisplay();
    this.displayAcademics = true;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    this.screenWidth = window.innerWidth;
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }

  onClickNewAcademics(): void {
    this.selectedEducation = {} as Education;
    this.resetDisplay();
    this.displayEducation = true;
  }

  onClickNewDiploma(): void {
    this.resetDisplay();
    this.displayDiplomas = true;
  }

  onClickNewCertificate(): void {
    this.resetDisplay();
    this.displayCertificates = true;
  }

  onChangeEducation(e): void {
    this.accountAcademics.educations.forEach(elt => {
      if (elt.label.toLowerCase() === e.target.value.toLowerCase()){
          this.selectedEducation = elt;
      }
    });
  }

  findAccountAcademics(): void{
    this.loading = true;
    const accountId = this.account.idServer;

    this.academicService.findAccountAcademics(accountId).subscribe((response) => {
      this.accountAcademics = response.resource;
      this.loading = false;
    }, (error) => {
      this.loading = false;
    }, () => {});
  }

  onRefreshEvent(newEvent: string): void {
    this.selectedEducation = {} as Education;
    this.resetDisplay();
    this.findAccountAcademics();
    this.displayAcademics = true;
  }

  onEditSecondaryEducationEvent(secondaryEducationToEdit: SecondaryEducation): void{
    this.modalService.dismissAll();
    this.loading = true;
    this.secondaryEducation = secondaryEducationToEdit;
    this.selectedEducation = this.secondaryEducation.establishmentType.education;
    this.resetDisplay();
    this.displayEducation = true;
    this.loading = false;
  }

  onEditHigherEducationEvent(higherEducationToEdit: HigherEducation): void{
    this.modalService.dismissAll();
    this.loading = true;
    this.higherEducation = higherEducationToEdit;
    this.selectedEducation = this.higherEducation.establishmentType.education;
    this.resetDisplay();
    this.displayEducation = true;
    this.loading = false;
  }

  onEditDegreeObtainedEvent(degreeObtainedToEdit: DegreeObtained): void{
    this.modalService.dismissAll();
    this.degreeObtained = degreeObtainedToEdit;
    this.selectedEducation = this.degreeObtained.diploma.education;
    this.onClickNewDiploma();
  }

  onEditCertificationEvent(certificationToEdit: Certification): void{
    this.modalService.dismissAll();
    this.certification = certificationToEdit;
    this.onClickNewCertificate();
  }

  resetDisplay(): void{
    this.displayAcademics = false;
    this.displayEducation = false;
    this.displayDiplomas = false;
    this.displayCertificates = false;
    this.initErrors();
  }

  initErrors(): void{
    this.submitted = false;
    this.errors = {valid: true, frequentedClass: '', studyLevel: '', teachingArea: '',
      establishmentType: '', specialty: '', diploma: '', targetedDiploma: '', preparedDiploma: '',
      establishmentName: '', city: '', description: '', education: ''};
    this.alert = {} as Alert;
  }

  open(content, type): void {
    let displayModal = false;
    if (type === 'academics'){
      displayModal = this.accountAcademics.secondaryEducations.length > 0 || this.accountAcademics.higherEducations.length > 0;
    }else if (type === 'degrees'){
      displayModal = this.accountAcademics.degreesObtained.length > 0;
    }
    if (displayModal){
      this.modalService.open(content,     {
        size: 'lg'
      }).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    }
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  onClickDismissModal(): void {
    this.modalService.dismissAll();
  }

  onClickTab(tab: string): void {
    this.tab = tab;
  }
}


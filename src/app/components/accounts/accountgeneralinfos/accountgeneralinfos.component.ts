import {Component, Input, OnInit} from '@angular/core';
import {Account} from '../../../models/account/account';
import {AccountService} from '../../../services/accounts/account/account.service';
import {ToastService} from '../../../services/toast/toast.service';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {AccountSocialInteractions} from '../../../models/account/accountSocialInteractions';
import {DatePipe} from '@angular/common';
import {MultimediafileService} from '../../../services/media/multimedia/multimediafile.service';
import {Multimedia} from '../../../models/multimedia/multimedia';
import {variables} from '../../../../environments/variables';
import {Alert} from '../../../models/alert';
import {ObjectHelper} from '../../../helpers/object-helper';
import {ModalDismissReasons, NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {infoText} from '../../../../environments/info-text';
import {SubscriptionService} from '../../../services/accounts/subscription/subscription.service';
import {CommonHelper} from '../../../helpers/common-helper';
import {RestCountryService} from '../../../services/api/rest-country.service';
import {CountryHelper} from '../../../helpers/country-helper';

@Component({
  selector: 'app-accountgeneralinfos',
  templateUrl: './accountgeneralinfos.component.html',
  styleUrls: ['./accountgeneralinfos.component.css']
})
export class AccountgeneralinfosComponent implements OnInit {
  @Input() account: Account = {} as Account;
  multimedia: Multimedia = {} as Multimedia;
  accountSocialInteractions: AccountSocialInteractions = {} as AccountSocialInteractions;
  loading: boolean;
  file: File;
  birthdate: Date;
  alert: Alert = {} as Alert;
  submitted = false;
  errors;
  displayCard: boolean;
  closeResult: string;
  modalOptions: NgbModalOptions;
  loggedAccount: Account = {} as Account;
  infoTextGenerales = infoText.info_text_generales;
  genders: any[] = CommonHelper.genders;
  professionalStatuses: any[] = CommonHelper.statuses;
  restCountries: any[] = [];
  currentCountry: any;
  selectedCountries: any[];

  myDateValue: Date;
  accountCountryName: string;

  constructor(private accountService: AccountService,
              private toastService: ToastService,
              private subscriptionService: SubscriptionService,
              private datePipe: DatePipe,
              private modalService: NgbModal,
              private restCountryService: RestCountryService,
              private multimediafileService: MultimediafileService,
              private authenticateService: AuthenticateService) {
  }

  ngOnInit(): void {
    this.initErrors();
    this.myDateValue = new Date();
    this.loading = true;
    this.displayCard = true;
    this.loggedAccount = this.authenticateService.getCurrentAccount();
    this.findAccountInfos();
  }

  onChangeGender(e): void {
    this.errors.gender = '';
    this.genders.forEach(elt => {
      if (elt.label.toLowerCase() === e.target.value.toLowerCase()) {
        this.account.gender = elt.code;
      }
    });
  }

  onChangeStatus(e): void {
    this.errors.status = '';
    this.professionalStatuses.forEach(elt => {
      if (elt.label.toLowerCase() === e.target.value.toLowerCase()) {
        this.account.status = elt.code;
      }
    });
  }

  onClickSave(): void {
    this.loading = true;
    if (this.validateAccountInfos()) {
      if (this.areDatesCorrect(this.birthdate)) {
        this.account.birthdate = this.birthdate;
      }
      this.accountService.updateAccount(this.account).subscribe(response => {
        this.loading = false;
        this.updateLocalAccount(response.resource);
        this.findAccountInfos();
        this.displayCard = true;
      }, (error) => {
        this.alert = {display: true, class: 'danger', title: 'Erreur', message: '  Données incorrectes'};
        this.loading = false;
      });
    } else {
      this.alert = {display: true, class: 'danger', title: 'Erreur', message: '  Données invalides'};
      this.loading = false;
    }
  }

  findAccountInfos(): void {
    const accountId = this.account.idServer;
    this.accountService.findAccountSocialInteractions(accountId).subscribe((response) => {
      this.accountSocialInteractions = response.resource;
      this.birthdate = this.account.birthdate;
      this.initErrors();
      this.searchCountryByCode();
      this.loading = false;
    }, (error) => {
      this.loading = false;
    }, () => {
    });
  }

  validateAccountInfos(): boolean {
    return true;
  }

  selectFile(event): void {
    this.file = event.target.files[0];
    if (this.file !== null) {
      this.uploadFile();
    } else {
      this.alert = {display: true, class: 'danger', title: 'Erreur', message: '  Erreur lors du téléchargement'};
    }
  }

  uploadFile(): void {
    this.loading = true;
    this.multimediafileService.uploadMultimedia(this.file, this.account).subscribe((response) => {
      this.multimedia = response.resource;
      this.account.profilePicture = this.multimedia.type + '_' + this.multimedia.checksum + '.' + this.multimedia.extension;
      localStorage.setItem(variables.account_current, JSON.stringify(this.account));
      this.loading = false;
    }, (error) => {
      // this.toastService.showError('Error', 'Media uploading avatar');
      this.alert = {display: true, class: 'danger', title: 'Erreur', message: '  Erreur lors du téléchargement'};
      this.loading = false;
    }, () => {
    });
  }

  initErrors(): void {
    this.submitted = false;
    this.errors = {valid: true, firstname: '', lastname: '', birthdate: '', gender: '', status: '', country: '', city: ''};
    this.alert = {} as Alert;
  }

  keyupFirstname(): void {
    this.errors.firstname = '';
  }

  keyupLastname(): void {
    this.errors.lastname = '';
  }

  changeBirthdate(): void {
    this.errors.birthdate = '';
  }

  onclickCancel(): void {
    this.initErrors();
    this.displayCard = true;
  }

  keyupCity(): void {
    this.errors.city = '';
  }

  selectBirthdate(): void {
    this.errors.birthdate = '';
    if (ObjectHelper.isDateValid(this.birthdate)) {
      if (this.areDatesCorrect(this.birthdate)) {
        this.errors.valid = true;
        this.submitted = false;
      } else {
        this.errors.valid = false;
        this.submitted = true;
        this.errors.birthdate = 'Date incohérente';
      }
    } else {
      this.errors.valid = false;
      this.submitted = true;
      this.errors.birthdate = 'Date invalide';
    }
  }

  areDatesCorrect(date: Date): boolean {
    console.log(date);
    return date && date <= (new Date());
  }

  editGeneralInfos(account: Account): void {
    this.displayCard = false;
  }

  onClickDismissModal(): void {
    this.modalService.dismissAll();
  }

  open(content): void {
    this.modalService.open(content, {
      size: 'lg'
    }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  updateLocalAccount(account: Account): void {
    this.loggedAccount = this.authenticateService.getCurrentAccount();
    this.loggedAccount.firstname = account.firstname;
    this.loggedAccount.lastname = account.lastname;
    this.loggedAccount.birthdate = account.birthdate;
    this.loggedAccount.gender = account.gender;
    this.loggedAccount.status = account.status;
    this.loggedAccount.city = account.city;
    this.loggedAccount.country = account.country;
    localStorage.setItem(variables.account_current, JSON.stringify(this.loggedAccount));
  }

  onKeyupCountry(): void {
    this.searchCountries();
  }

  searchCountries(): void{
    this.restCountries = CountryHelper.filterCountries(this.currentCountry);
  }

  onSelectCountry(item): void{
    this.selectedCountries = this.restCountries.filter((user) => user.translations.fra.common.toLowerCase().includes(item.toLowerCase()));
    this.account.country = this.selectedCountries[0].cca3;
  }

  searchCountryByCode(): void{
    if (!this.account.country){
      this.accountCountryName = 'Selectionner un pays';
    }else {
      this.accountCountryName = CountryHelper.findCountryByAlpha3Code(this.account.country);
    }
  }

}

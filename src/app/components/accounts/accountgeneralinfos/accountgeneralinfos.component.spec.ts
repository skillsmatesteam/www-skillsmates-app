import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountgeneralinfosComponent } from './accountgeneralinfos.component';

describe('AccountgeneralinfosComponent', () => {
  let component: AccountgeneralinfosComponent;
  let fixture: ComponentFixture<AccountgeneralinfosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountgeneralinfosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountgeneralinfosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

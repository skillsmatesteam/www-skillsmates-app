import {Component, Input, OnInit} from '@angular/core';
import {InfoInterestType} from '../../../models/account/infoInterestType';

@Component({
  selector: 'app-account-interest',
  templateUrl: './account-interest.component.html',
  styleUrls: ['./account-interest.component.css']
})
export class AccountInterestComponent implements OnInit {

  @Input() infoInterest: InfoInterestType;
  constructor() { }

  ngOnInit(): void {
  }

}

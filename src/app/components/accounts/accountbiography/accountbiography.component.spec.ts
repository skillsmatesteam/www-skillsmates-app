import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountbiographyComponent } from './accountbiography.component';

describe('AccountskillsComponent', () => {
  let component: AccountbiographyComponent;
  let fixture: ComponentFixture<AccountbiographyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountbiographyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountbiographyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

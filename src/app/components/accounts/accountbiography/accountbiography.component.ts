import {Component, Input, OnInit} from '@angular/core';
import {variables} from '../../../../environments/variables';
import {Account} from '../../../models/account/account';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {StringHelper} from '../../../helpers/string-helper';

@Component({
  selector: 'app-accountbiography',
  templateUrl: './accountbiography.component.html',
  styleUrls: ['./accountbiography.component.css']
})
export class AccountbiographyComponent implements OnInit {

  @Input() title: string;
  @Input() content: string;
  @Input() account: Account;
  loading: boolean;
  loggedAccount: Account = {} as Account;
  routes = RoutesHelper.routes;

  constructor(private authenticateService: AuthenticateService) { }

  ngOnInit(): void {
    this.title = 'Biographie';
    this.loggedAccount = this.authenticateService.getCurrentAccount();
  }

  onClickMore(content: string): void {
    localStorage.setItem(variables.tab, content);
  }

  truncateBiography(): string{
    return StringHelper.truncateText(this.account.biography, 255);
  }
}

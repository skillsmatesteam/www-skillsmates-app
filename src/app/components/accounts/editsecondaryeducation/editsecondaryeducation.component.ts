import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SecondaryEducation} from '../../../models/account/secondaryEducation';
import {Alert} from '../../../models/alert';
import {EstablishmentType} from '../../../models/account/config/establishmentType';
import {Diploma} from '../../../models/account/config/diploma';
import {HigherEducation} from '../../../models/account/higherEducation';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {AcademicService} from '../../../services/accounts/account/academic.service';
import {AccountAcademics} from '../../../models/account/accountAcademics';
import {Education} from '../../../models/account/config/education';
import {StudyLevel} from '../../../models/account/config/studyLevel';
import {variables} from '../../../../environments/variables';

@Component({
  selector: 'app-editsecondaryeducation',
  templateUrl: './editsecondaryeducation.component.html',
  styleUrls: ['./editsecondaryeducation.component.css']
})
export class EditsecondaryeducationComponent implements OnInit {
  @Input() accountAcademics: AccountAcademics = {} as AccountAcademics;
  @Input() secondaryEducation: SecondaryEducation = {} as SecondaryEducation;
  @Output() refreshEvent = new EventEmitter<string>();
  loading: boolean;
  maxDescriptionSize: number;
  alert: Alert = {} as Alert;
  submitted = false;
  errors;
  establishmentTypesFiltered: EstablishmentType[];
  studyLevelFiltered: StudyLevel[];
  diplomasFiltered: Diploma[];

  constructor(private authenticateService: AuthenticateService, private academicService: AcademicService) { }

  ngOnInit(): void {
    this.loading = true;
    this.initErrors();
    this.filterEstablishmentTypes();
    this.filterFrequentedClasses();
    this.filterDiplomas();
    this.loading = false;
    this.maxDescriptionSize = variables.max_professional_description_size;
  }

  onChangeEstablishmentType(e): void {
    this.errors.establishmentType = '';
    this.accountAcademics.establishmentTypes.forEach(elt => {
      if (elt.label.toLowerCase() === e.target.value.toLowerCase()){
        this.secondaryEducation.establishmentType = elt;
      }
    });
  }

  keyupEstablishmentName(): void {
    this.errors.establishmentName = '';
  }

  keyupCity(): void {
    this.errors.city = '';
  }

  keyupAnotherPreparedDiploma(): void {
    this.errors.anotherPreparedDiploma = '';
  }

  keyupAnotherStudyLevel(): void  {
    this.errors.anotherStudyLevel = '';
  }

  keyupAnotherEstablishmentType(): void {
    this.errors.anotherEstablishmentType = '';
  }
  onChangeFrequentedClass(event): void {
    this.errors.studyLevel = '';
    this.accountAcademics.studyLevels.forEach(elt => {
      if (elt.label.toLowerCase() === event.target.value.toLowerCase()){
        this.secondaryEducation.studyLevel = elt;
      }
    });
  }

  onChangePreparedDiploma(event): void {
    this.errors.preparedDiploma = '';
    this.accountAcademics.diplomas.forEach(elt => {
      if (elt.label.toLowerCase() === event.target.value.toLowerCase()){
        this.secondaryEducation.preparedDiploma = elt;
      }
    });
  }

  onClickSaveSecondaryEducation(): void {
    this.loading = true;
    this.secondaryEducation.account = this.authenticateService.getCurrentAccount();
    if (this.validateSecondaryEducation()){
      this.saveSecondaryEducation();
    } else {
      this.alert = {display: true, class: 'danger', title: 'Erreur', message: '  Certaines valeurs sont invalides'};
      this.loading = false;
    }
  }

  saveSecondaryEducation(): void{
    this.academicService.createSecondaryEducation(this.secondaryEducation).subscribe((response) => {
      this.loading = false;
      this.refreshEvent.emit('secondaryEducation');
    }, (error) => {
      this.alert = {display: true, class: 'danger', title: 'Erreur', message: '  Erreur lors de l\'enregistrement'};
      this.loading = false;
    }, () => {});
  }

  validateSecondaryEducation(): boolean{
    this.initErrors();
    if (!this.secondaryEducation.establishmentName ){
      this.errors.valid = false;
      this.errors.establishmentName = 'Le nom de l\'etablissement est requis';
    }

    if (this.secondaryEducation.establishmentName !== undefined && this.secondaryEducation.establishmentName.length > 255){
      this.errors.valid = false;
      this.errors.establishmentName = 'Le nombre maximale de caractères est 255';
    }

    if (!this.secondaryEducation.city ){
      this.errors.valid = false;
      this.errors.city = 'La ville est requise';
    }

    if (this.secondaryEducation.city !== undefined && this.secondaryEducation.city.length > 255){
      this.errors.valid = false;
      this.errors.city = 'Le nombre maximale de caractères est 255';
    }

    if (this.secondaryEducation.description !== undefined && this.secondaryEducation.description.length > this.maxDescriptionSize){
      this.errors.valid = false;
      this.errors.description = 'Le nombre maximal de caractères est ' + this.maxDescriptionSize;
    }

    if (this.secondaryEducation.preparedDiploma === undefined){
      this.errors.valid = false;
      this.errors.preparedDiploma = 'Selectionner un diplome à preparer';
    }else{
      if (this.secondaryEducation.preparedDiploma.specified) {
        if (this.secondaryEducation.anotherPreparedDiploma === undefined || this.secondaryEducation.anotherPreparedDiploma === '') {
          this.errors.valid = false;
          this.errors.anotherPreparedDiploma = 'Le diplome préparé est requis';
        } else if (this.secondaryEducation.anotherPreparedDiploma !== undefined && this.secondaryEducation.anotherPreparedDiploma.length > 255) {
          this.errors.valid = false;
          this.errors.anotherPreparedDiploma = 'Le nombre maximal de caractères est 255';
        }
      }
    }

    if (this.secondaryEducation.specialty === undefined){
      this.errors.valid = false;
      this.errors.specialty = 'Selectionner une specialité';
    }

    if (this.secondaryEducation.establishmentType === undefined){
      this.errors.valid = false;
      this.errors.establishmentType = 'Selectionner un type d\'etablissement';
    }else{
      if (this.secondaryEducation.establishmentType.specified) {
        if (this.secondaryEducation.anotherEstablishmentType === undefined || this.secondaryEducation.anotherEstablishmentType === '') {
          this.errors.valid = false;
          this.errors.anotherEstablishmentType = 'Le type d\'établissement est requis';
        } else if (this.secondaryEducation.anotherEstablishmentType !== undefined && this.secondaryEducation.anotherEstablishmentType.length > 255) {
          this.errors.valid = false;
          this.errors.anotherEstablishmentType = 'Le nombre maximal de caractères est 255';
        }
      }
    }

    if (this.secondaryEducation.studyLevel === undefined){
      this.errors.valid = false;
      this.errors.studyLevel = 'Sélectionner une classe fréquentée';
    }else{
      if (this.secondaryEducation.studyLevel.specified) {
        if (this.secondaryEducation.anotherStudyLevel === undefined || this.secondaryEducation.anotherStudyLevel === '') {
          this.errors.valid = false;
          this.errors.anotherStudyLevel = 'La classe fréquentée est requise';
        } else if (this.secondaryEducation.anotherStudyLevel !== undefined && this.secondaryEducation.anotherStudyLevel.length > 255) {
          this.errors.valid = false;
          this.errors.anotherStudyLevel = 'Le nombre maximal de caractères est 255';
        }
      }
    }

    this.submitted = true;
    return this.errors.valid;
  }

  initErrors(): void{
    this.submitted = false;
    this.errors = {valid: true, studyLevel: '', teachingArea: '',
      establishmentType: '', specialty: '', diploma: '', targetedDiploma: '', preparedDiploma: '',
      establishmentName: '', city: '', description: '', education: '', anotherPreparedDiploma: '', anotherEstablishmentType: '', anotherStudyLevel: ''};
    this.alert = {} as Alert;
  }

  onclickCancel(): void {
    this.secondaryEducation = {} as SecondaryEducation;
    this.initErrors();
    this.refreshEvent.emit('secondaryEducation');
  }

  filterEstablishmentTypes(): void{
    this.establishmentTypesFiltered = [];
    this.accountAcademics.establishmentTypes.forEach(elt => {
      if (elt.education.code.toLowerCase() === 'secondary'){
        this.establishmentTypesFiltered.push(elt);
      }
    });
  }

  filterFrequentedClasses(): void{
    this.studyLevelFiltered = [];
    this.accountAcademics.studyLevels.forEach(elt => {
      if (elt.education.code.toLowerCase() === 'secondary'){
        this.studyLevelFiltered.push(elt);
      }
    });
  }

  filterDiplomas(): void{
    this.diplomasFiltered = [];
    this.accountAcademics.diplomas.forEach(elt => {
      if (elt.education.code.toLowerCase() === 'secondary'){
        this.diplomasFiltered.push(elt);
      }
    });
  }

  keyupSpeciality(): void {
    this.errors.specialty = '';
  }

}

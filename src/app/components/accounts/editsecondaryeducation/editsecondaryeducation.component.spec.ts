import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditsecondaryeducationComponent } from './editsecondaryeducation.component';

describe('EditsecondaryeducationComponent', () => {
  let component: EditsecondaryeducationComponent;
  let fixture: ComponentFixture<EditsecondaryeducationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditsecondaryeducationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditsecondaryeducationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

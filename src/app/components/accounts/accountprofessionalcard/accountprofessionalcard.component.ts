import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Skill} from '../../../models/account/skill';
import {SkillService} from '../../../services/accounts/account/skill.service';
import {ToastService} from '../../../services/toast/toast.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ProfessionalService} from '../../../services/accounts/account/professional.service';
import {Professional} from '../../../models/account/professional';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Account} from '../../../models/account/account';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';

@Component({
  selector: 'app-accountprofessionalcard',
  templateUrl: './accountprofessionalcard.component.html',
  styleUrls: ['./accountprofessionalcard.component.css']
})
export class AccountprofessionalcardComponent implements OnInit {
  @Input() professional: Professional;
  @Output() newProfessionalEvent = new EventEmitter<string>();
  @Output() editProfessionalEvent = new EventEmitter<Professional>();
  professionalToDelete: Professional = {} as Professional;
  professionalToEdit: Professional = {} as Professional;
  id: string;
  @Input() account: Account = {} as Account;
  loggedAccount: Account = {} as Account;

  constructor(private professionalService: ProfessionalService,
              private toastService: ToastService,
              private router: Router,
              private authenticateService: AuthenticateService,
              private modalService: NgbModal,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.loggedAccount = this.authenticateService.getCurrentAccount();
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
  }

  onClickDelete(professional: Professional): void {
    this.professionalToDelete = professional;
    localStorage.setItem('professionalToDelete', professional.idServer);
    this.modalService.dismissAll();
  }

  onClickEdit(professional: Professional): void {
    this.editProfessionalEvent.emit(professional);
    this.modalService.dismissAll();
  }

  onClickConfirmDelete(): void {
    this.professionalService.deleteProfessional(localStorage.getItem('professionalToDelete')).subscribe((response) => {
      this.toastService.showSuccess('Succès', 'Parcours professionnel supprimé avec succés');
      this.newProfessionalEvent.emit('refresh');
    }, (error) => {
      this.toastService.showError('Error', 'Parcours professionnel non supprimé');
    }, () => {
      localStorage.removeItem('professionalToDelete');
    });
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountprofessionalcardComponent } from './accountprofessionalcard.component';

describe('AccountprofessionalComponent', () => {
  let component: AccountprofessionalcardComponent;
  let fixture: ComponentFixture<AccountprofessionalcardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountprofessionalcardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountprofessionalcardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

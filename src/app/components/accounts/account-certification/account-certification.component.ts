import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Account} from "../../../models/account/account";
import {AcademicService} from "../../../services/accounts/account/academic.service";
import {ToastService} from "../../../services/toast/toast.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthenticateService} from "../../../services/accounts/authenticate/authenticate.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {Certification} from "../../../models/account/certification";

@Component({
  selector: 'app-account-certification',
  templateUrl: './account-certification.component.html',
  styleUrls: ['./account-certification.component.css']
})
export class AccountCertificationComponent implements OnInit {
  @Input() certificate: Certification;
  @Input() account: Account = {} as Account;
  @Output() newCertificateEvent = new EventEmitter<string>();
  @Output() editCertificateEvent = new EventEmitter<Certification>();
  certificateToDelete: Certification = {} as Certification;
  certificateToEdit: Certification = {} as Certification;
  loggedAccount: Account = {} as Account;

  constructor(private academicService: AcademicService,
              private toastService: ToastService,
              private router: Router,
              private authenticateService: AuthenticateService,
              private modalService: NgbModal,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.loggedAccount = this.authenticateService.getCurrentAccount();
  }

  onClickDelete(certificate: Certification): void {
    this.certificateToDelete = certificate;
    localStorage.setItem('certificateToDelete', certificate.idServer);
    this.modalService.dismissAll();
  }

  onClickConfirmDelete(): void {
    this.academicService.deleteCertification(localStorage.getItem('certificateToDelete')).subscribe((response) => {
      this.toastService.showSuccess('Succès', 'Certification supprimée avec succès');
      this.newCertificateEvent.emit('refresh');
    }, (error) => {
      this.toastService.showError('Error', 'Certification non supprimée');
    }, () => {
      localStorage.removeItem('certificateToDelete');
    });
  }

  onClickEdit(certificate: Certification): void {
    this.editCertificateEvent.emit(certificate);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountGeneralInfosCardComponent } from './account-general-infos-card.component';

describe('AccountGeneralInfosCardComponent', () => {
  let component: AccountGeneralInfosCardComponent;
  let fixture: ComponentFixture<AccountGeneralInfosCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountGeneralInfosCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountGeneralInfosCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Account} from '../../../models/account/account';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {ModalDismissReasons, NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {RestCountryService} from '../../../services/api/rest-country.service';

@Component({
  selector: 'app-account-general-infos-card',
  templateUrl: './account-general-infos-card.component.html',
  styleUrls: ['./account-general-infos-card.component.css']
})
export class AccountGeneralInfosCardComponent implements OnInit {

  @Input() account: Account;
  @Output() editGeneralInfosEvent = new EventEmitter<Account>();
  loggedAccount: Account = {} as Account;
  routes = RoutesHelper.routes;
  closeResult: string;
  modalOptions: NgbModalOptions;

  constructor(private authenticateService: AuthenticateService,
              private restCountryService: RestCountryService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.loggedAccount = this.authenticateService.getCurrentAccount();
  }

  onClickEdit(account: Account): void {
    this.editGeneralInfosEvent.emit(account);
  }

  open(content): void {
    this.modalService.open(content,     {
      size: 'lg'
    }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}

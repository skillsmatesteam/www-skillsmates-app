import {Component, Input, OnInit} from '@angular/core';
import {Dimensions, ImageCroppedEvent, ImageTransform} from 'ngx-image-cropper';
import {variables} from '../../../../environments/variables';
import {Account} from '../../../models/account/account';
import {Multimedia} from '../../../models/multimedia/multimedia';
import {MultimediafileService} from '../../../services/media/multimedia/multimediafile.service';
import {ToastService} from '../../../services/toast/toast.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import {RoutesHelper} from '../../../helpers/routes-helper';

@Component({
  selector: 'app-edit-profile-image',
  templateUrl: './edit-profile-image.component.html',
  styleUrls: ['./edit-profile-image.component.css']
})
export class EditProfileImageComponent implements OnInit {

  @Input() account: Account = {} as Account;
  multimedia: Multimedia = {} as Multimedia;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  croppedFile: File;
  canvasRotation = 0;
  rotation = 0;
  scale = 1;
  zoom: number;
  showCropper = false;
  containWithinAspectRatio = false;
  transform: ImageTransform = {};
  loading: boolean;
  routes = RoutesHelper.routes;
  originalFile: File = null;
  constructor(private multimediafileService: MultimediafileService,
              private router: Router,
              private toastService: ToastService,
              private modalService: NgbModal, ) { }

  ngOnInit(): void {
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
    this.originalFile = this.imageChangedEvent.target.files[0];
  }

  imageCropped(event: ImageCroppedEvent): void {
    this.croppedImage = event.base64;

    this.croppedFile = this.base64ToFile(
      event.base64,
      this.imageChangedEvent.target.files[0].name,
    );
  }

  imageLoaded(): void {
    this.showCropper = true;
  }

  cropperReady(sourceImageDimensions: Dimensions): void {
    // console.log('Cropper ready', sourceImageDimensions);
  }

  loadImageFailed(): void {
    console.log('Load failed');
  }

  rotateLeft(): void {
    this.canvasRotation--;
    this.flipAfterRotate();
  }

  rotateRight(): void {
    this.canvasRotation++;
    this.flipAfterRotate();
  }

  private flipAfterRotate(): void {
    const flippedH = this.transform.flipH;
    const flippedV = this.transform.flipV;
    this.transform = {
      ...this.transform,
      flipH: flippedV,
      flipV: flippedH
    };
  }

  flipHorizontal(): void {
    this.transform = {
      ...this.transform,
      flipH: !this.transform.flipH
    };
  }

  flipVertical(): void {
    this.transform = {
      ...this.transform,
      flipV: !this.transform.flipV
    };
  }

  resetImage(): void {
    this.scale = 1;
    this.rotation = 0;
    this.canvasRotation = 0;
    this.transform = {};
  }

  zoomOut(): void {
    this.scale -= .1;
    this.transform = {
      ...this.transform,
      scale: this.scale
    };
  }

  zoomIn(): void {
    this.scale += .1;
    this.transform = {
      ...this.transform,
      scale: this.scale
    };
  }

  scaleImage(): void {
    this.transform = {
      ...this.transform,
      scale: this.scale
    };
  }

  toggleContainWithinAspectRatio(): void {
    this.containWithinAspectRatio = !this.containWithinAspectRatio;
  }

  updateRotation(): void {
    this.transform = {
      ...this.transform,
      rotate: this.rotation
    };
  }

  base64ToFile(data, filename): File {
    const arr = data.split(',');
    const mime = arr[0].match(/:(.*?);/)[1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);

    while (n--){
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename.split('.').slice(0, -1).join('.') + '.png', { type: mime });
  }

  onClickSaveProfileImage(): void {
    if (this.croppedFile && this.account){
      this.uploadFile();
    }else {
      this.toastService.showError('Error', 'Aucune image selectionnée');
    }
  }

  uploadFile(): void {
    this.loading = true;
    console.log(this.croppedFile);
    this.multimediafileService.uploadAvatarMultimedia(this.croppedFile, this.originalFile, this.account).subscribe((response) => {
      this.multimedia = response.resource;
      this.account.profilePicture = this.multimedia.type + '_' + this.multimedia.checksum + '.' + this.multimedia.extension;
      localStorage.setItem(variables.account_current, JSON.stringify(this.account));
      this.loading = false;
      this.modalService.dismissAll();
      this.router.navigate([this.routes.get('profile.my')], {
        skipLocationChange: true,
        queryParamsHandling: 'merge' // == if you need to keep queryParams
      });
    }, (error) => {
      this.toastService.showError('Error', 'Erreur lors du téléchargement');
      this.loading = false;
      this.modalService.dismissAll();
    }, () => {});
  }

  onClickDismissModal(): void {
    this.modalService.dismissAll();
  }

  onScaleChange(): void {
    this.scaleImage();
  }
}

import {Component, Input, OnInit} from '@angular/core';
import {Account} from '../../../models/account/account';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {StringHelper} from '../../../helpers/string-helper';
import {AccountNetwork} from '../../../models/network/accountNetwork';
import {NetworkHelper} from '../../../helpers/network-helper';
import {SubscriptionService} from '../../../services/accounts/subscription/subscription.service';
import {InfoNetworkType} from '../../../models/network/infoNetworkType';
import {RestCountryService} from '../../../services/api/rest-country.service';
import {AccountSocialInteractions} from '../../../models/account/accountSocialInteractions';
import {AccountService} from '../../../services/accounts/account/account.service';
import {variables} from '../../../../environments/variables';

@Component({
  selector: 'app-account-profile',
  templateUrl: './account-profile.component.html',
  styleUrls: ['./account-profile.component.css']
})
export class AccountProfileComponent implements OnInit {

  @Input() account: Account;
  @Input() loading: boolean;
  @Input() showSubscribeButton: boolean;
  loggedAccount: Account = {} as Account;
  routes = RoutesHelper.routes;
  infoNetworkTypes: InfoNetworkType[] = NetworkHelper.networkTypes;
  accountNetwork: AccountNetwork = {} as AccountNetwork;
  accountSocialInteractions: AccountSocialInteractions = {} as AccountSocialInteractions;

  constructor(private authenticationService: AuthenticateService,
              private restCountryService: RestCountryService,
              private accountService: AccountService,
              private subscriptionService: SubscriptionService) { }

  ngOnInit(): void {
    this.loggedAccount = this.authenticationService.getCurrentAccount();
    this.findNetwork();
  }

  getEstablishmentIcon(): string{
    return StringHelper.getEstablishmentIcon(this.account);
  }

  findNetwork(): void{
    this.subscriptionService.findNetwork(this.account.idServer).subscribe((response) => {
      this.accountNetwork = response.resource;
      this.sortNetwork();
    }, (error) => {

    });
  }

  sortNetwork(): void{
    this.infoNetworkTypes = NetworkHelper.networkTypes;
    this.infoNetworkTypes[1].value = this.accountNetwork.numberFollowers;
    this.infoNetworkTypes[2].value = this.accountNetwork.numberFollowees;
    this.findAccountSocialInteractions();
  }

  findAccountSocialInteractions(): void{
    if (this.account.idServer){
      this.accountService.findAccountSocialInteractions(this.account.idServer).subscribe((response) => {
        this.accountSocialInteractions = response.resource;
        this.loading = false;
      }, (error => {}));
    }
  }

  onClickFollowees(): void {
    localStorage.setItem(variables.tab, '3'); // 3 = position for followee
  }

  onClickFollowers(): void {
    localStorage.setItem(variables.tab, '2'); // 2 = position for follower
  }

  public name(account: Account): string {
    return StringHelper.truncateName(account, 21);
  }
}

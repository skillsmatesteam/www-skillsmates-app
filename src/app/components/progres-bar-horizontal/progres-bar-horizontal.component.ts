import {Component, Input, OnInit, OnChanges, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-progres-bar-horizontal',
  templateUrl: './progres-bar-horizontal.component.html',
  styleUrls: ['./progres-bar-horizontal.component.css']
})
export class ProgresBarHorizontalComponent implements OnInit, OnChanges {

  @Input() loading;
  value: number;
  private timerId: NodeJS.Timeout;
  @Input() progress = 0;

  constructor() { }

  ngOnInit(): void {
    this.value = 0;
  }

  ngOnChanges(changes: SimpleChanges): void {
    changes.loading.currentValue ? this.start() : this.stop();
  }

  start(): void{
    this.timerId = setInterval(() => {
      if (this.value < 100){
        this.value = this.value + 10;
      }
    }, 2000);
  }

  stop(): void{
    this.value = 0;
    clearInterval(this.timerId);
  }

}

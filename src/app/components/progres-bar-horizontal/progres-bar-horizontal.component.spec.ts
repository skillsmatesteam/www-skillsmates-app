import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgresBarHorizontalComponent } from './progres-bar-horizontal.component';

describe('ProgresBarHorizontalComponent', () => {
  let component: ProgresBarHorizontalComponent;
  let fixture: ComponentFixture<ProgresBarHorizontalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgresBarHorizontalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgresBarHorizontalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

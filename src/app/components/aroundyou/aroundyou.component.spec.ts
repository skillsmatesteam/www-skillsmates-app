import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AroundyouComponent } from './aroundyou.component';

describe('AroundyouComponent', () => {
  let component: AroundyouComponent;
  let fixture: ComponentFixture<AroundyouComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AroundyouComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AroundyouComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

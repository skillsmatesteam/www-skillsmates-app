import { Component, OnInit } from '@angular/core';
import {variables} from '../../../environments/variables';
import {RoutesHelper} from '../../helpers/routes-helper';

@Component({
  selector: 'app-aroundyou',
  templateUrl: './aroundyou.component.html',
  styleUrls: ['./aroundyou.component.css']
})
export class AroundyouComponent implements OnInit {

  routes = RoutesHelper.routes;

  constructor() { }

  ngOnInit(): void {
  }

  onClick(tab: string): void {
    localStorage.setItem(variables.tab, tab);
  }
}

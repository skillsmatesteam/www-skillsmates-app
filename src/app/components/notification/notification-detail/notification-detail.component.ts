import {Component, Input, OnInit} from '@angular/core';
import {Notification} from '../../../models/notification/notification';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {NotificationService} from '../../../services/notification/notification.service';
import {ToastService} from '../../../services/toast/toast.service';
import {Router} from '@angular/router';
import {MobileService} from '../../../services/mobile/mobile.service';

@Component({
  selector: 'app-notification-detail',
  templateUrl: './notification-detail.component.html',
  styleUrls: ['./notification-detail.component.css']
})
export class NotificationDetailComponent implements OnInit {

  @Input() notification: Notification = {} as Notification;
  routes = RoutesHelper.routes;

  constructor(private notificationService: NotificationService,
              private toastService: ToastService,
              private mobileService: MobileService,
              private router: Router) { }

  ngOnInit(): void {
  }

  /**
   * click on one notification
   * @param notification clicked
   */
  onClickNotification(notification: Notification): void {
    this.notificationService.deacticvatNotification(notification).subscribe((response) => {
      this.notification = response.resource;
      this.redirectToPost(this.notification);
      const deactivatedNotification: Notification = response.resource;
      const notifications: Notification[] = [];
      notifications.push(deactivatedNotification);
      this.notificationService.changeNotificationMessage(notifications);
    }, (error) => {
      this.toastService.showError('Error', 'Erreur lors de l\'affichage');
    });
  }

  redirectToPost(notification: Notification): void{
    switch (notification.type) {
      case 'LIKE':
      case 'COMMENT':
      case 'SHARE':
        console.log('notification detail');
        // localStorage.setItem('design', 'notification');
        this.mobileService.setNotificationDesign();
        this.router.navigate([this.routes.get('posts'), notification.element], {
          queryParams: {refresh: new Date().getTime()}
        });
        break;
      case 'FOLLOWER':
      case 'FAVORITE':
        this.router.navigate([this.routes.get('profile'), notification.emitterAccount.idServer], {
          queryParams: {refresh: new Date().getTime()}
        });
        break;
      case 'MESSAGE':
        this.router.navigate([this.routes.get('messages.my')], {
          queryParams: {refresh: new Date().getTime()}
        });
        break;
      default:
        break;
    }
  }

  generateTitle(notification: Notification): string{
    let title  = '';
    switch (notification.type) {
      case 'LIKE':
        title =  ' a aimé votre publication';
        break;
      case 'COMMENT':
        title = ' a commenté votre publication';
        break;
      case 'FOLLOWER':
        title = ' est votre nouvel abonné';
        break;
      case 'SHARE':
        title =  ' a partagé votre publication';
        break;
      case 'FAVORITE':
        title = ' vous a mis dans ses favoris';
        break;
      case 'MESSAGE':
        title = ' vous a envoyé un message';
        break;
      case 'POST':
        title = ' a publié un nouvel article';
        break;
      default:
        break;
    }
    return title;
  }
}

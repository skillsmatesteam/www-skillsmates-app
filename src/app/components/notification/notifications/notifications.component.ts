import {Component, Input, OnInit} from '@angular/core';
import {Notification} from '../../../models/notification/notification';
import {NotificationService} from '../../../services/notification/notification.service';
import {Router} from '@angular/router';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {SocialInteractionService} from '../../../services/pages/social-interaction/social-interaction.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {

  @Input() notifications: Notification[];
  @Input() notificationType: string;
  routes = RoutesHelper.routes;

  constructor(private notificationService: NotificationService,
              private socialInteractionService: SocialInteractionService,
              private router: Router) { }

  ngOnInit(): void {
  }

  onClickNotification(notification: Notification): void {
    this.redirectToPost(notification);
    this.notificationService.deacticvatNotification(notification).subscribe((response) => {}, (error) => {});
  }

  redirectToPost(notification: Notification): void{
    switch (notification.type) {
      case 'LIKE':
      case 'COMMENT':
        this.socialInteractionService.findById(notification.element).subscribe((response) => {
          const socialInteraction = response.resource;
          this.router.navigate([this.routes.get('posts'), socialInteraction.element], {
            queryParams: {refresh: new Date().getTime()}
          });
        }, (error) => {});
        break;
      case 'FOLLOWER':
        this.router.navigate([this.routes.get('profile'), notification.receiverAccount.idServer], {
          queryParams: {refresh: new Date().getTime()}
        });
        break;
      default:
        break;
    }
  }

  getNotificationType(notifications: Notification[]): string{
    if (notifications && notifications.length > 0){
      return notifications[0].type === 'FOLLOWER' ? 'FOLLOWERS' : 'BELL';
    }
    return 'BELL';
  }

  generateTitle(notification: Notification): string{
    let title  = '';
    switch (notification.type) {
      case 'LIKE':
        title =  ' a aimé votre publication';
        break;
      case 'COMMENT':
        title = ' a commenté votre publication';
        break;
      case 'FOLLOWER':
        title = ' est votre nouvel abonné';
        break;
      default:
        break;
    }
    return title;
  }
}

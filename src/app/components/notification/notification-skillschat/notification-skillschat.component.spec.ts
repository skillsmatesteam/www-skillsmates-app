import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationSkillschatComponent } from './notification-skillschat.component';

describe('NotificationSkillschatComponent', () => {
  let component: NotificationSkillschatComponent;
  let fixture: ComponentFixture<NotificationSkillschatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotificationSkillschatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationSkillschatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

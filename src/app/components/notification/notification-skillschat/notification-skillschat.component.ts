import {Component, Input, OnInit} from '@angular/core';
import {AccountMessage} from '../../../models/message/accountMessage';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {variables} from '../../../../environments/variables';

@Component({
  selector: 'app-notification-skillschat',
  templateUrl: './notification-skillschat.component.html',
  styleUrls: ['./notification-skillschat.component.css']
})
export class NotificationSkillschatComponent implements OnInit {

  @Input() accountsMessages: AccountMessage[];
  routes = RoutesHelper.routes;
  constructor() { }

  ngOnInit(): void {
  }

  onClickMessage(accountMessage: AccountMessage): void {
    // this.dropdownSearchMenu = false;
    localStorage.setItem(variables.account_emitter, JSON.stringify(accountMessage.account));
  }

}

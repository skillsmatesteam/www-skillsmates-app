import {Component, Input, OnInit} from '@angular/core';
import {MultimediaInfos} from '../../../models/multimedia/multimediaInfos';
import {InfoMediaType} from '../../../models/multimedia/infoMediaType';
import {Multimedia} from '../../../models/multimedia/multimedia';
import {variables} from '../../../../environments/variables';
import {Account} from '../../../models/account/account';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {ActivatedRoute} from '@angular/router';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {MediaHelper} from '../../../helpers/media-helper';

@Component({
  selector: 'app-accountdocuments',
  templateUrl: './accountdocuments.component.html',
  styleUrls: ['./accountdocuments.component.css']
})
export class AccountdocumentsComponent implements OnInit {
  @Input() infoMediaType: InfoMediaType;
  @Input() account: Account;
  loggedAccount: Account = {} as Account;
  id: string;
  routes = RoutesHelper.routes;

  constructor(private authenticateService: AuthenticateService) { }

  ngOnInit(): void {
    this.loggedAccount = this.authenticateService.getCurrentAccount();
  }

  getMultimediaType(multimedia: Multimedia): string{
    return MediaHelper.getMultimediaType(multimedia);
  }

  onClickMore(m: InfoMediaType): void {
    localStorage.setItem(variables.tab, m.position.toString(10));
  }

}

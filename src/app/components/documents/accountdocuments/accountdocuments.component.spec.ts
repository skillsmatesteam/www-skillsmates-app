import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountdocumentsComponent } from './accountdocuments.component';

describe('AccountdocumentsComponent', () => {
  let component: AccountdocumentsComponent;
  let fixture: ComponentFixture<AccountdocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountdocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountdocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

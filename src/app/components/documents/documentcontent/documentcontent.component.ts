import {Component, Input, OnInit, SimpleChanges, OnChanges} from '@angular/core';
import {Multimedia} from '../../../models/multimedia/multimedia';
import {Observable} from 'rxjs';
import {MultimediaService} from '../../../services/media/multimedia/multimedia.service';
import {SocialInteractionService} from '../../../services/pages/social-interaction/social-interaction.service';
import {SocialInteraction} from '../../../models/post/socialInteraction';
import {MediaHelper} from '../../../helpers/media-helper';
import {PostSocialInteractions} from '../../../models/post/postSocialInteractions';

@Component({
  selector: 'app-documentcontent',
  templateUrl: './documentcontent.component.html',
  styleUrls: ['./documentcontent.component.css']
})
export class DocumentcontentComponent implements OnInit, OnChanges {

  @Input() postsSocialInteractions: PostSocialInteractions[] = [];
  @Input() public getApiEndpoint$: (requestUrl: string) => Observable<any>;
  @Input() public apiRoute: string;
  @Input() myPostSocialInteractions;
  @Input() mediaType ;

  comments: SocialInteraction[] = [];
  selectedPostSocialInteractions: PostSocialInteractions = {} as PostSocialInteractions;
  constructor(private multimediaService: MultimediaService, private socialInteractionService: SocialInteractionService) {}

  ngOnInit(): void {
    if (!this.selectedPostSocialInteractions || !this.selectedPostSocialInteractions.post){
      this.selectedPostSocialInteractions = this.postsSocialInteractions[0];
    }
    this.onClickPost(this.myPostSocialInteractions);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.selectedPostSocialInteractions = changes.postsSocialInteractions.currentValue[0];
    if (this.selectedPostSocialInteractions){
      this.findComments(this.selectedPostSocialInteractions.post.idServer);
    }
  }

  onClickPost(postSocialInteractions: PostSocialInteractions): void {
    this.selectedPostSocialInteractions = postSocialInteractions;
    this.findComments(this.selectedPostSocialInteractions.post.idServer);
  }

  getMultimediaType(multimedia: Multimedia): string{
    return MediaHelper.getMultimediaType(multimedia);
  }

  findComments(postId: string): void{
    this.socialInteractionService.findCommentsByPost(postId).subscribe((response) => {
      this.comments = response.resources;
    }, (error) => {
    }, () => {
    });
  }

  onRefreshEvent($event: string): void {

  }
}

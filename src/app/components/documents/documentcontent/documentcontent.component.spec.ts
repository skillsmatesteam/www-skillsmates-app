import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentcontentComponent } from './documentcontent.component';

describe('DocumentcontentComponent', () => {
  let component: DocumentcontentComponent;
  let fixture: ComponentFixture<DocumentcontentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentcontentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentcontentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

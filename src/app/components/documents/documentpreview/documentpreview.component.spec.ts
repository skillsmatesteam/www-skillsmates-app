import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentpreviewComponent } from './documentpreview.component';

describe('DocumentpreviewComponent', () => {
  let component: DocumentpreviewComponent;
  let fixture: ComponentFixture<DocumentpreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentpreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentpreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

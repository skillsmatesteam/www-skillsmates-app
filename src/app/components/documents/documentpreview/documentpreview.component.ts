import {Component, Input, OnInit, EventEmitter, Output, OnChanges} from '@angular/core';
import {Multimedia} from '../../../models/multimedia/multimedia';
import {MediaHelper} from '../../../helpers/media-helper';
import {environment} from '../../../../environments/environment';
import {PostSocialInteractions} from '../../../models/post/postSocialInteractions';


@Component({
  selector: 'app-documentpreview',
  templateUrl: './documentpreview.component.html',
  styleUrls: ['./documentpreview.component.css']
})
export class DocumentpreviewComponent implements OnInit, OnChanges {
  @Input() postsSocialInteractions: PostSocialInteractions[] = [];
  @Input() mediaType;
  @Output() selectPostSocialInteractions = new EventEmitter<PostSocialInteractions>();
  allPostsSocialInteractions: PostSocialInteractions[];
  searchContent: string;
  loading: boolean;
  @Input() currentPage: number;
  itemsPerPage: number;
  pages: number[];
  count: number;

  constructor() {
    this.itemsPerPage = environment.itemsPerPageDocument;
  }

  ngOnInit(): void {
    this.allPostsSocialInteractions = this.postsSocialInteractions;

    // Ajout de la pagination
    this.currentPage = 1;
    this.count = this.postsSocialInteractions.length;
    this.postsSocialInteractions = this.getPageLocal(this.postsSocialInteractions, this.currentPage, this.itemsPerPage);
    this.pages = new Array(this.count);
  }

  onClickDocument(postLikesShares: PostSocialInteractions): void {
    this.selectPostSocialInteractions.emit(postLikesShares);
  }

  ngOnChanges(): void {
    this.allPostsSocialInteractions = this.postsSocialInteractions;

    // Ajout de la pagination
    this.currentPage = 1;
    this.count = this.postsSocialInteractions.length;
    this.postsSocialInteractions = this.getPageLocal(this.postsSocialInteractions, this.currentPage, this.itemsPerPage);
    this.pages = new Array(this.count);

  }

  getMultimediaType(multimedia: Multimedia): string {
    return MediaHelper.getMultimediaType(multimedia);
  }

  onKeyupSearch(): void {
    this.loading = true;
    this.filter();
    this.loading = false;
  }

  filter(): void {
    this.postsSocialInteractions = this.filterPostSocialInteractions(this.allPostsSocialInteractions);

    // Ajout de la pagination
    this.currentPage = 1;
    this.count = this.postsSocialInteractions.length;
    this.postsSocialInteractions = this.getPageLocal(this.postsSocialInteractions, this.currentPage, this.itemsPerPage);
    this.pages = new Array(this.count);
  }

  filterPostSocialInteractions(postSocialInteractionsToFilter: PostSocialInteractions[]): PostSocialInteractions[] {
    let filteredPostSocialInteractions: PostSocialInteractions[];
    if (this.searchContent) {
      filteredPostSocialInteractions = postSocialInteractionsToFilter.filter((postSocialInteractions: PostSocialInteractions) =>
        this.searchContent && postSocialInteractions.post.title &&
        postSocialInteractions.post.title.toLocaleLowerCase().indexOf(this.searchContent.toLowerCase()) >= 0);
    } else {
      filteredPostSocialInteractions = postSocialInteractionsToFilter;
    }
    return filteredPostSocialInteractions;
  }

  getPage(page: number): void {
    this.postsSocialInteractions = this.getPageLocal(this.allPostsSocialInteractions, page, this.itemsPerPage);
  }

  getPageLocal(sourceList: PostSocialInteractions[], page: number, pageSize: number): PostSocialInteractions[] {
    if (pageSize <= 0 || page <= 0) {
      // throw new IllegalArgumentException("invalid page size: " + pageSize);
    }
    const fromIndex = (page - 1) * pageSize;
    if (sourceList == null || sourceList.length <= fromIndex) {
      return [];
    }
      // toIndex exclusive
    let list: PostSocialInteractions[];
    list = sourceList.slice(fromIndex, Math.min(fromIndex + pageSize, sourceList.length));
    return this.randomArrayShuffle(list);
  }

   randomArrayShuffle(array): any {
    let currentIndex = array.length;
    let temporaryValue;
    let randomIndex;
    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  }
}

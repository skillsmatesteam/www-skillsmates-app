import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssistanceFaqComponent } from './assistance-faq.component';

describe('AssistanceFaqComponent', () => {
  let component: AssistanceFaqComponent;
  let fixture: ComponentFixture<AssistanceFaqComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssistanceFaqComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssistanceFaqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {
  Component,
  EventEmitter,
  HostListener,
  OnInit,
  Output,
} from '@angular/core';
import { MobileService } from 'src/app/services/mobile/mobile.service';

@Component({
  selector: 'app-assistance-dashboard',
  templateUrl: './assistance-dashboard.component.html',
  styleUrls: ['./assistance-dashboard.component.css'],
})
export class AssistanceDashboardComponent implements OnInit {
  public screenWidth: number = window.innerWidth;
  @Output() selectedBlock = new EventEmitter<string>();
  constructor(private mobileService: MobileService) {}

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    this.screenWidth = window.innerWidth;
  }

  ngOnInit(): void {}

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }
  onClickFaq(): void {
    this.selectedBlock.emit('faq');
  }

  onClickContactUs(): void {
    this.selectedBlock.emit('contactUs');
  }
}

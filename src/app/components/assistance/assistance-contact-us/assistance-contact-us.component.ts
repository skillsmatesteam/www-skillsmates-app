import { Component, HostListener, OnInit } from '@angular/core';
import { Alert } from '../../../models/alert';
import { Account } from '../../../models/account/account';
import { Assistance } from '../../../models/assistance/assistance';
import { AuthenticateService } from '../../../services/accounts/authenticate/authenticate.service';
import { AssistanceService } from '../../../services/settings/assistance/assistance.service';
import { variables } from '../../../../environments/variables';
import { MobileService } from 'src/app/services/mobile/mobile.service';

@Component({
  selector: 'app-assistance-contact-us',
  templateUrl: './assistance-contact-us.component.html',
  styleUrls: ['./assistance-contact-us.component.css'],
})
export class AssistanceContactUsComponent implements OnInit {
  submitted = false;
  errors;
  alert: Alert = {} as Alert;
  biography;
  loading: boolean;
  loggedAccount: Account = {} as Account;
  assistance: Assistance = {} as Assistance;
  public screenWidth: number = window.innerWidth;

  constructor(
    private authenticateService: AuthenticateService,
    private assistanceService: AssistanceService,
    private mobileService: MobileService
  ) {}

  ngOnInit(): void {
    this.initErrors();
    this.loggedAccount = this.authenticateService.getCurrentAccount();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    this.screenWidth = window.innerWidth;
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }
  sendAssistance(): void {
    this.assistanceService.sendAssistance(this.assistance).subscribe(
      (response) => {
        this.assistance = {} as Assistance;
        this.alert = {
          display: true,
          class: 'success',
          title: 'super!!',
          message: '  Nous avons bien réçu votre requête',
        };
        this.loading = false;
      },
      (error) => {
        this.alert = {
          display: true,
          class: 'danger',
          title: 'Erreur',
          message: '  Erreur de la transmission de votre requête',
        };
        this.loading = false;
      },
      () => {}
    );
  }

  onClickSend(): void {
    this.loading = true;
    this.assistance.account = this.loggedAccount;
    if (this.validateAssistance()) {
      this.sendAssistance();
    } else {
      this.alert = {
        display: true,
        class: 'danger',
        title: 'Erreur',
        message: '  Les valeurs entrées sont invalides',
      };
      this.loading = false;
    }
  }

  onclickCancel(): void {
    this.assistance = {} as Assistance;
    this.alert = { display: false, class: '', title: '', message: '' };
  }

  initErrors(): void {
    this.submitted = false;
    this.errors = { valid: true, topic: '', content: '' };
    this.alert = {} as Alert;
  }

  validateAssistance(): boolean {
    this.initErrors();

    if (this.assistance.topic === undefined) {
      this.errors.valid = false;
      this.errors.topic = "L'objet est requis";
    }

    if (this.assistance.content === undefined) {
      this.errors.valid = false;
      this.errors.content = 'Le message est requis';
    }

    if (
      this.assistance.content !== undefined &&
      this.assistance.content.length > variables.max_description_size
    ) {
      this.errors.valid = false;
      this.errors.description =
        'Le nombre maximal de caractères est ' + variables.max_description_size;
    }

    if (
      this.assistance.topic !== undefined &&
      this.assistance.topic.length > variables.max_title_size
    ) {
      this.errors.valid = false;
      this.errors.keywords =
        'Le nombre maximal de caractères est ' + variables.max_title_size;
    }

    this.submitted = true;
    return this.errors.valid;
  }

  keyupContentAssistance(): void {
    this.errors.content = '';
  }

  keyupObjectAssistance(): void {
    this.errors.topic = '';
  }
}

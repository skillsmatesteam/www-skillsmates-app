import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssistanceContactUsComponent } from './assistance-contact-us.component';

describe('AssistanceContactUsComponent', () => {
  let component: AssistanceContactUsComponent;
  let fixture: ComponentFixture<AssistanceContactUsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssistanceContactUsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssistanceContactUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, HostListener, Input, OnInit} from '@angular/core';
import {Account} from '../../models/account/account';
import {InfoSettingType} from '../../models/account/infoSettingType';
import {AuthenticateService} from '../../services/accounts/authenticate/authenticate.service';
import {Alert} from '../../models/alert';
import {AccountService} from '../../services/accounts/account/account.service';
import {variables} from '../../../environments/variables';
import {ToastService} from '../../services/toast/toast.service';
import {SettingService} from '../../services/settings/setting/setting.service';
import {Router} from '@angular/router';
import {RoutesHelper} from '../../helpers/routes-helper';
import {StringHelper} from '../../helpers/string-helper';
import {MobileService} from '../../services/mobile/mobile.service';

@Component({
  selector: 'app-setting-content',
  templateUrl: './setting-content.component.html',
  styleUrls: ['./setting-content.component.css']
})
export class SettingContentComponent implements OnInit {
  account: Account = {} as Account;
  @Input() selectedSettingType: InfoSettingType;
  loading: boolean;
  errors;
  newPassword: string;
  submitted = false;
  alert: Alert = {} as Alert;
  isOnclickName = false;
  isOnclickEmail = false;
  isOnclickPhone = false;
  isOnclickPassword = false;
  isOnclickDelete = false;
  routes = RoutesHelper.routes;
  flag: string;
  isDeactivate = false;
  isDelete = false;
  map: Map<number, boolean>;
  public screenWidth: number = window.innerWidth;

  constructor(private settingService: SettingService,
              private authenticateService: AuthenticateService,
              private mobileService: MobileService,
              private toastService: ToastService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.initErrors();
    this.loading = false;
    this.isDeactivate = true;
    this.isDelete = false;
    this.account = this.authenticateService.getCurrentAccount();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    this.screenWidth = window.innerWidth;
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }

  initErrors(): void{
    this.submitted = false;
    this.errors = {valid: true, firstname: '', lastname: '', password: '', email: '', phoneNumber: '', newPassword: '', confirmPassword: ''};
    this.alert = {} as Alert;
  }

  keyupFirstname(): void {
    this.errors.firstname = '';
  }

  keyupLastname(): void {
    this.errors.lastname = '';
  }

  keyupPassword(): void {
    this.errors.password = '';
  }

  keyupEmail(): void {
    this.errors.email = '';
  }

  keyupPhone(): void {
    this.errors.phoneNumber = '';
  }

  keyupNewPassword(): void {
    this.errors.newPassword = '';
  }

  keyupConfirmPassword(): void {
    this.errors.confirmPassword = '';
  }

  onClickName(): void {
    this.isOnclickName = true;
    this.isOnclickEmail = false;
    this.isOnclickPhone = false;
  }
  OnClickEmail(): void {
    this.isOnclickName = false;
    this.isOnclickEmail = true;
    this.isOnclickPhone = false;
  }

  OnClickPhone(): void {
    this.isOnclickName = false;
    this.isOnclickEmail = false;
    this.isOnclickPhone = true;
  }
  onClickPassword(): void {
    this.isOnclickPassword = true;
    this.isOnclickDelete = false;
  }
  onClickDelete(): void {
    this.isOnclickPassword = false;
    this.isOnclickDelete = true;
  }
  onClickCancel(): void {
    this.initErrors();
    this.isOnclickName = false;
    this.isOnclickEmail = false;
    this.isOnclickPhone = false;
    this.isOnclickDelete = false;
  }

  onClickSaveName(): void  {
    this.loading = true;
    if (this.validateName()){
      this.settingService.updateAccountBySetting(this.account, 'setting/name').subscribe(data => {
        this.loading = false;
        this.isOnclickName = false;
        this.account = data.resource;
        localStorage.setItem(variables.account_current, JSON.stringify(this.account));
      }, (error) => {
        this.toastService.showError('Echec !', 'Données incorrectes');
        this.loading = false;
      });
    }else {
      this.loading = false;
    }
  }

  onClickSaveEmail(): void {
    this.loading = true;
    if (this.validateEmail()){
      this.settingService.updateAccountBySetting(this.account, 'setting/email').subscribe(data => {
        this.loading = false;
        this.isOnclickEmail = false;
        this.account = data.resource;
        localStorage.setItem(variables.account_current, JSON.stringify(this.account));
      }, (error) => {
        this.toastService.showError('Echec !', 'Données incorrectes');
        this.loading = false;
      });
    }else {
      this.loading = false;
    }
  }

  onClickSavePhone(): void {
    this.loading = true;
    if (this.validatePhone()){
      this.settingService.updateAccountBySetting(this.account, 'setting/phone').subscribe(data => {
        this.loading = false;
        this.isOnclickPhone = false;
        this.account = data.resource;
        localStorage.setItem(variables.account_current, JSON.stringify(this.account));
      }, (error) => {
        this.toastService.showError('Echec !', 'Données incorrectes');
        this.loading = false;
      });
    }else {
      this.loading = false;
    }
  }

  validateName(): boolean{
    this.initErrors();
    if (this.account.firstname === undefined || this.account.firstname === ''){
      this.errors.valid = false;
      this.errors.firstname = 'Prénom requis';
    }

    else if (this.account.firstname !== undefined && this.account.firstname.length < 3){
      this.errors.valid = false;
      this.errors.firstname = 'Le prénom doit être de 3 caractéres minimum';
    }

    else if (this.account.firstname !== undefined && this.account.firstname.length > 50){
      this.errors.valid = false;
      this.errors.firstname  = 'Le nombre maximal de caractères est 50';
    }

    if (this.account.lastname === undefined || this.account.lastname === ''){
      this.errors.valid = false;
      this.errors.lastname = 'Nom requis';
    }

    else if (this.account.lastname !== undefined && this.account.lastname.length < 3){
      this.errors.valid = false;
      this.errors.lastname = 'Le nom doit être de 3 caractéres minimum';
    }

    else if (this.account.lastname !== undefined && this.account.lastname.length > 50){
      this.errors.valid = false;
      this.errors.lastname  = 'Le nombre maximal de caractères est 50';
    }

    if (this.account.password === undefined || this.account.password === ''){
      this.errors.valid = false;
      this.errors.password = 'Mot de passe requis';
    }

    else if (this.account.password !== undefined && this.account.password.length < 5){
      this.errors.valid = false;
      this.errors.password = 'Mot de passe doit être de 5 caractéres minimum';
    }

    else if (this.account.password !== undefined && this.account.password.length > 50){
      this.errors.valid = false;
      this.errors.password  = 'Le nombre maximal de caractères est 50';
    }

    this.submitted = true;
    return this.errors.valid;
  }

  validateAccountCancellation(): boolean{
    this.initErrors();
    if (this.account.password === undefined || this.account.password === ''){
      this.errors.valid = false;
      this.errors.password = 'Mot de passe requis';
    }

    else if (this.account.password !== undefined && this.account.password.length < 5){
      this.errors.valid = false;
      this.errors.password = 'Mot de passe doit être de 5 caractéres minimum';
    }

    else if (this.account.password !== undefined && this.account.password.length > 50){
      this.errors.valid = false;
      this.errors.password  = 'Le nombre maximal de caractères est 50';
    }

    this.submitted = true;
    return this.errors.valid;
  }

  validateEmail(): boolean{
    this.initErrors();
    if (!this.verificateEmail(this.account.email)){
      this.errors.valid = false;
      this.errors.email = 'Email invalide';
    }

    if (this.account.password === undefined || this.account.password === ''){
      this.errors.valid = false;
      this.errors.password = 'Mot de passe requis';
    }

    else if (this.account.password !== undefined && this.account.password.length < 5){
      this.errors.valid = false;
      this.errors.password = 'Mot de passe doit être de 5 caractéres minimum';
    }

    else if (this.account.password !== undefined && this.account.password.length > 50){
      this.errors.valid = false;
      this.errors.password  = 'Le nombre maximal de caractères est 50';
    }

    this.submitted = true;
    return this.errors.valid;
  }

  validatePhone(): boolean{
    this.initErrors();
    if (!this.verificatePhone(this.account.phoneNumber)){
      this.errors.valid = false;
      this.errors.phoneNumber = 'Numéro de téléphone invalide';
    }

    if (this.account.password === undefined || this.account.password === ''){
      this.errors.valid = false;
      this.errors.password = 'Mot de passe requis';
    }

    else if (this.account.password !== undefined && this.account.password.length < 5){
      this.errors.valid = false;
      this.errors.password = 'Mot de passe doit être de 5 caractéres minimum';
    }

    else if (this.account.password !== undefined && this.account.password.length > 50){
      this.errors.valid = false;
      this.errors.password  = 'Le nombre maximal de caractères est 50';
    }

    this.submitted = true;
    return this.errors.valid;
  }

  verificateEmail(email): boolean{
    return StringHelper.validateEmail(email);
  }

  verificatePhone(phoneNumber): boolean{
    const regExp = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/;
    return regExp.test(phoneNumber);
  }


  onClickSavePassword(): void {
    this.loading = true;
    if (this.validateChangePassword()){
      this.settingService.updateAccountBySetting(this.account, 'setting/password').subscribe(data => {
        this.loading = false;
        this.isOnclickPassword = false;
        this.account = data.resource;
        localStorage.setItem(variables.account_current, JSON.stringify(this.account));
      }, (error) => {
        this.toastService.showError('Echec !', 'Données incorrectes');
        this.loading = false;
      });
    }else {
      this.loading = false;
    }
  }

  validateChangePassword(): boolean{
    this.initErrors();

    if (this.account.password === undefined || this.account.password === ''){
      this.errors.valid = false;
      this.errors.password = 'Mot de passe requis';
    }

    else if (this.account.password && this.account.password.length < 5){
      this.errors.valid = false;
      this.errors.password = 'Mot de passe doit être de 5 caractéres minimum';
    }

    else if (this.account.password !== undefined && this.account.password.length > 50){
      this.errors.valid = false;
      this.errors.password  = 'Le nombre maximal de caractères est 50';
    }

    if (this.newPassword === undefined || this.newPassword === ''){
      this.errors.valid = false;
      this.errors.newPassword = 'Nouveau mot de passe requis';
    }

    else{
      this.passwordValidator();
    }

    if (this.account.confirmPassword === undefined || this.account.confirmPassword === ''){
      this.errors.valid = false;
      this.errors.confirmPassword = 'confirmation du mot de passe requis';
    }

    else if (this.newPassword && this.account.confirmPassword && this.newPassword !== this.account.confirmPassword){
      this.errors.valid = false;
      this.errors.confirmPassword = 'Les deux mots de passe sont différents';
    }

    this.submitted = true;
    return this.errors.valid;
  }
  passwordValidator(): void{
    this.map = new Map();

    this.map = this.validatePassword(this.newPassword, this.account)
    if (!this.map.get(1)) {
      this.errors.valid = false;
      this.errors.newPassword = ' Mot de passe doit contenir entre 8 et 16 caractères';

    }
    else if (!this.map.get(2)) {
      this.errors.valid = false;
      this.errors.newPassword = ' Mot de passe doit contenir au moins 1 chiffre ou un caractère spécial : !&%$';

    }
    else if (this.map.get(3)) {
      this.errors.valid = false;
      this.errors.newPassword = ' Mot de passe ne doit pas contenir votre nom ou email';

    }
    else if (!this.map.get(4)) {
      this.errors.valid = false;
      this.errors.newPassword = ' Mot de passe doit contenir au moins une lettre majuscule';

    }
    else if (!this.map.get(5)) {
      this.errors.valid = false;
      this.errors.newPassword = ' Mot de passe doit contenir au moins une lettre miniscule';

    }
  }
  onClickCancelPassword(): void {
    this.initErrors();
    this.isOnclickPassword = false;
    this.newPassword = '';
    this.account.password = '';
    this.account.confirmPassword = '';
  }

  onClickDeleteDeactivate(): void {
    this.loading = true;
    if (this.validateAccountCancellation()){
      if (this.isDelete){
        this.settingService.deleteAccount(this.account).subscribe(data => {
          this.loading = false;
          this.isOnclickDelete = false;
          this.router.navigate([this.routes.get('login')]);
        }, (error) => {
          this.toastService.showError('Echec !', 'Données incorrectes');
          this.loading = false;
        });
      } else if (this.isDeactivate){
        this.settingService.deactivateAccount(this.account).subscribe(data => {
          this.loading = false;
          this.isOnclickDelete = false;
          this.router.navigate([this.routes.get('login')]);
        }, (error) => {
          this.toastService.showError('Echec !', 'Données incorrectes');
          this.loading = false;
        });
      }
    }else{
      this.loading = false;
    }


  }

  onDeactivate($event: Event): void {
    this.isDeactivate = true;
    this.isDelete = false;
  }

  onDelete($event: Event): void{
    this.isDelete = true;
    this.isDeactivate = false;
  }

  validatePassword(password: string, account: Account): Map<number, boolean> {
    return StringHelper.validatePassword(password, account);
  }
}

import {Component, Input, OnInit} from '@angular/core';
import {InfoMediaType} from '../../models/multimedia/infoMediaType';
import {Router} from '@angular/router';
import {RoutesHelper} from '../../helpers/routes-helper';
import {Post} from '../../models/post/post';
import {Account} from '../../models/account/account';
import {AuthenticateService} from '../../services/accounts/authenticate/authenticate.service';
import {StringHelper} from '../../helpers/string-helper';
import {PostSocialInteractions} from '../../models/post/postSocialInteractions';
import {SocialInteraction} from '../../models/post/socialInteraction';
import {SocialInteractionsHelper} from '../../helpers/social-interactions-helper';
import {variables} from '../../../environments/variables';

@Component({
  selector: 'app-document-search-result',
  templateUrl: './document-search-result.component.html',
  styleUrls: ['./document-search-result.component.css']
})
export class DocumentSearchResultComponent implements OnInit {

  @Input() postSocialInteractions: PostSocialInteractions;
  @Input() mediaType: InfoMediaType;
  routes = RoutesHelper.routes;
  post: Post;
  account: Account;
  loggedAccount: Account = {} as Account;

  constructor(private authenticateService: AuthenticateService, private router: Router) { }

  ngOnInit(): void {
    this.loggedAccount = this.authenticateService.getCurrentAccount();
    this.post = this.postSocialInteractions && this.postSocialInteractions.post ? this.postSocialInteractions.post : null;
    this.account = this.post && this.post.account ? this.post.account : null;
  }

  onClickPost(post: Post): void {
    this.router.navigate([this.routes.get('post.view'), post.idServer]);
  }

  public name(account: Account): string{
    return StringHelper.truncateName(account, 25);
  }

  onClickAccount(account: Account): void {
    this.router.navigate([this.routes.get('profile'), account.idServer]);
  }

  hasLiked(socialInteractions: SocialInteraction[]): boolean {
    return SocialInteractionsHelper.hasLiked(socialInteractions, this.loggedAccount);
  }

  countLikes(socialInteractions: SocialInteraction[]): number {
    return SocialInteractionsHelper.countLikes(socialInteractions);
  }

  hasShared(socialInteractions: SocialInteraction[]): boolean {
    return SocialInteractionsHelper.hasShared(socialInteractions, this.loggedAccount);
  }

  countShares(socialInteractions: SocialInteraction[]): number {
    return SocialInteractionsHelper.countShares(socialInteractions);
  }

  countComments(socialInteractions: SocialInteraction[]): number {
    return SocialInteractionsHelper.countComments(socialInteractions);
  }
}

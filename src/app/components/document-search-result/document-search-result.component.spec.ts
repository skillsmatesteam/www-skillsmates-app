import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentSearchResultComponent } from './document-search-result.component';

describe('DocumentSearchResultComponent', () => {
  let component: DocumentSearchResultComponent;
  let fixture: ComponentFixture<DocumentSearchResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DocumentSearchResultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentSearchResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

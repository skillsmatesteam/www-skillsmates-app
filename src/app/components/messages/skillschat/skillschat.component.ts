import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-skillschat',
  templateUrl: './skillschat.component.html',
  styleUrls: ['./skillschat.component.css']
})
export class SkillschatComponent implements OnInit {

  media: string[] = [];
  constructor() { }

  ngOnInit(): void {
    // modification des images
    this.media[0] = './assets/images/document-bleu.svg';
    this.media[1] = './assets/images/photo-vert.svg';
    this.media[2] = './assets/images/video-rouge.svg';
    this.media[3] = './assets/images/audio-rose.svg';
    this.media[4] = './assets/images/lien-jaune.svg';
  }

}

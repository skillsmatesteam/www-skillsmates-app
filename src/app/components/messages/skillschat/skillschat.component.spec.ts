import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SkillschatComponent } from './skillschat.component';

describe('SkillschatComponent', () => {
  let component: SkillschatComponent;
  let fixture: ComponentFixture<SkillschatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SkillschatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillschatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

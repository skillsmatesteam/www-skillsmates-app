import {Component, Input, OnInit} from '@angular/core';
import {AccountMessage} from '../../../models/message/accountMessage';
import {Account} from '../../../models/account/account';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {StringHelper} from '../../../helpers/string-helper';
import {Message} from '../../../models/message/message';
import {Router} from '@angular/router';
import {RoutesHelper} from '../../../helpers/routes-helper';

@Component({
  selector: 'app-message-preview',
  templateUrl: './message-preview.component.html',
  styleUrls: ['./message-preview.component.css']
})
export class MessagePreviewComponent implements OnInit {

  loggedAccount: Account = {} as Account;
  routes = RoutesHelper.routes;
  @Input() accountMessage: AccountMessage;

  constructor(private authenticateService: AuthenticateService, private router: Router) { }

  ngOnInit(): void {
    this.loggedAccount = this.authenticateService.getCurrentAccount();
  }

  public name(account: Account): string{
    return StringHelper.truncateName(account, 20);
  }

  /**
   * get the last message received or sent
   * @param accountMessage: account with list of messages
   */
  getLastMessageContent(accountMessage: AccountMessage): string{
    const lastMessage: Message = this.getLastMessage(accountMessage);
    return lastMessage ? StringHelper.truncateText(lastMessage.content, 25) : '';
  }

  getLastMessage(accountMessage: AccountMessage): Message {
    return accountMessage && accountMessage.messages && accountMessage.messages.length > 0 ?
      accountMessage.messages[accountMessage.messages.length - 1] : null;
  }

  onClickAccountMessage(accountMessage: AccountMessage): void {
    this.router.navigate([this.routes.get('message.my')]);
  }
}

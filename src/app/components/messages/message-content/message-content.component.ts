import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {Account} from '../../../models/account/account';
import {Message} from '../../../models/message/message';
import {Alert} from '../../../models/alert';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {MessageService} from '../../../services/message/message.service';
import {ToastService} from '../../../services/toast/toast.service';
import {variables} from '../../../../environments/variables';
import {Multimedia} from '../../../models/multimedia/multimedia';
import {WebSocketService} from '../../../services/message/web-socket.service';
import {SocialInteraction} from '../../../models/post/socialInteraction';
import {NotificationHelper} from '../../../helpers/notification-helper';
import {StringHelper} from '../../../helpers/string-helper';
import {MimetypeHelper} from '../../../helpers/mimetype-helper';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {MobileService} from '../../../services/mobile/mobile.service';
import {Observable, Subject, Subscription} from 'rxjs';
import {WebcamImage, WebcamInitError} from 'ngx-webcam';
import {UtilityService} from '../../../services/utility/utility.service';

@Component({
  selector: 'app-message-content',
  templateUrl: './message-content.component.html',
  styleUrls: ['./message-content.component.css']
})
export class MessageContentComponent implements OnInit, OnDestroy{
  @Input() selectedAccount: Account = {} as Account;
  @Input() selectedMessages: Message[];
  @Output() refreshAccounts = new EventEmitter<Message>();
  @Output() displayMessageContent = new EventEmitter<boolean>();
  routes = RoutesHelper.routes;
  account: Account = {} as Account;
  multimedia: Multimedia = {} as Multimedia;
  loading: boolean;
  content: string;
  submitted = false;
  errors;
  message: Message = {} as Message;
  messages: Message[] = [];
  typedMessage: string;
  alert: Alert = {} as Alert;
  loggedAccount: Account = {} as Account;
  loadingMessage: boolean;
  @ViewChild('fileDropRef', { static: false }) fileDropEl: ElementRef;
  files: any[] = [];
  filePictures: any[] = [];
  @ViewChild('closebutton') closebutton;
  @ViewChild('closebutton_picture') closebutton_picture;

  isValideUrl: boolean;
  public screenWidth: number = window.innerWidth;

  // webcam snapshot trigger
  private trigger: Subject<void> = new Subject<void>();
  public showWebcam = false;
  public allowCameraSwitch = true;
  public multipleWebcamsAvailable = false;
  public deviceId: string;
  public videoOptions: MediaTrackConstraints = {
    // width: {ideal: 1024},
    // height: {ideal: 576}
  };
  public errorsWebcam: WebcamInitError[] = [];

  // latest snapshot
  public webcamImage: WebcamImage = null;
// switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId
  private nextWebcam: Subject<boolean|string> = new Subject<boolean|string>();
  subscription: Subscription;

  constructor(private messageService: MessageService,
              private webSocketService: WebSocketService,
              private toastService: ToastService,
              private mobileService: MobileService,
              private utilityService: UtilityService,
              private authenticateService: AuthenticateService)
  {}

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    this.screenWidth = window.innerWidth;
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }

  @HostListener('window:focus', ['$event'])
  tabActivation(event): void{
    if (this.selectedAccount){
      this.findMessages(this.selectedAccount);
    }
  }

  @HostListener('window:blur', ['$event'])
  tabDeactivation(event): void {

  }

  ngOnInit(): void {
    this.mobileService.setSkillschatDesign();
    this.initErrors();
    this.loggedAccount = this.authenticateService.getCurrentAccount();
    this.findMessages(this.selectedAccount);
    this.webSocketService.pushNotificationWebSocket.subscribe((current) => {
      if (current){
        const socialInteraction: SocialInteraction = JSON.parse(current);
        if (socialInteraction && socialInteraction.type === variables.message){
         this.messages.push(NotificationHelper.convertSocialInteractionToMessage(socialInteraction));
        }
      }
    });

    this.subscription = this.utilityService.subjectMessage$.subscribe(
      (msg) => {
        if (msg) {
          this.refreshAccounts.emit(msg);
        }
      }
    );

    this.messages = this.selectedMessages;


  }

  ngOnDestroy(): void {
    this.mobileService.unsetSkillschatDesign();
    this.subscription.unsubscribe();
  }

  keyupMessage(e): void {
    this.errors.title = '';
    if (e.keyCode === 13){
      this.onClickPublishMessage();
    }
  }

  onClickPublishMessage(): void{
    this.loading = true;
    this.message = {} as Message;
    if (this.typedMessage) {
      this.message.content = this.typedMessage.trim();
    }
    this.typedMessage = '';
    if (this.validateMessage()){
      this.message.receiverAccount = this.selectedAccount;
      this.message.emitterAccount = this.loggedAccount;
      this.message.createdAt = new Date();
      this.messages.push(this.message);

      this.messageService.saveMessage(this.message).subscribe((response) => {
        this.message = response.resource;
        // create a social interaction for notification
        const socialInteraction: SocialInteraction = NotificationHelper.convertMessageToSocialInteraction(this.message);
        // via websocket
        this.webSocketService.sendPushNotification(socialInteraction);
        // refresh list of accounts to push this account on top
        this.refreshAccounts.emit(this.message);
      }, (error) => {

      });
    }else {
      this.toastService.showError('Echec !', this.errors.content);
      this.loading = false;
    }
  }

  initErrors(): void{
    this.submitted = false;
    this.errors = {valid: true, content: '', title: '', file: ''};
    this.alert = {} as Alert;
  }

  validateMessage(): boolean{
    this.initErrors();
    if (this.message.content === undefined || this.message.content === ''){
      this.errors.valid = false;
      this.errors.content = 'Veuillez entrer un message';
    }

    else if (this.message.content !== undefined && this.message.content.length > variables.max_title_size) {
      this.errors.valid = false;
      this.errors.content  = 'Le nombre maximal de caractères est ' + variables.max_title_size;
    }

    this.submitted = true;
    return this.errors.valid;
  }

  findMessages(account: Account): void{
    if (account && account.idServer){
      this.messageService.findMessagesByAccount(account.idServer).subscribe(data => {
        this.messages = data.resources;
      }, (error) => {
        this.toastService.showError('Echec !', 'Erreur pendant la récupération des anciens messages : ' + error.toString());
        this.loadingMessage = false;
      });
    }
  }

  /*
   * Drag and drop files
   */
  uploadFile(files: Array<any>): void {
      this.prepareFilesList(files);
  }

  deleteAttachment(index: any): void {
    this.files.splice(index, 1);
  }

  public name(account: Account): string{
    return StringHelper.truncateName(account, 20);
  }

  prepareFilesList(files: Array<any>): void {
    for (const item of files) {
      if (!this.validateFileSize(item)){
        this.toastService.showError('Erreur', 'La taille maximale est ' + variables.max_file_size + 'MB');
      }else if (!this.validateFileMimetype(item)){
        this.toastService.showError('Erreur', 'Ce type de document n\'est pas accepté');
      } else{
        // item.progress = 0;
        // item.loading = true;
        this.files.push(item);
      }
    }
    this.fileDropEl.nativeElement.value = '';
  }

  validateFileSize(file): boolean{
    if (file && file.size > StringHelper.getMaxFileSize()){
      this.errors.valid = false;
      this.errors.file = 'La taille maximale est ' + variables.max_file_size + 'MB';
      return false;
    }
    return true;
  }

  validateFileMimetype(file): boolean{
    if (!MimetypeHelper.isMimetypeAllowed(file.type)){
      this.errors.valid = false;
      this.errors.file = 'Ce type de document n\'est pas accepté';
      return false;
    }
    return true;
  }

  onClickLeft(): void {
    // this.router.navigate([this.routes.get('messages.my')]);
    this.displayMessageContent.emit(false);
  }

  onClickCameraUp(): void {
    this.showWebcam = true;
  }

  onClickCameraDown(): void {
    this.showWebcam = false;
  }



  onClickPhone(account: Account): void {
    if (account.phoneNumber) {
      window.location.href = 'tel:' + account.phoneNumber;
    }
    // window.open(window.location.href, '_system'); // TODO : à inclure pour les iPhones
  }

  public triggerSnapshot(): void {
    this.trigger.next();
    if (this.webcamImage) {
      this.filePictures = [];

      const blob = this.dataURItoBlob(this.webcamImage.imageAsDataUrl);
      this.filePictures.push(new File([blob], 'my_picture.jpg'));

      this.uploadFile(this.filePictures);

      this.message = {} as Message;

      if (this.filePictures != null && this.filePictures.length > 0){
        this.message.receiverAccount = this.selectedAccount;
        this.message.emitterAccount =  this.loggedAccount;
        this.message.createdAt = new Date();

        this.sendMediaMessage(this.filePictures, this.message);
        // this.showWebcam = false;
    }
  }
  }

   dataURItoBlob(dataURI): any {
    const byteString = atob(dataURI.split(',')[1]);

    // separate out the mime component
    const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to an ArrayBuffer
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ab], {type: mimeString});
     // return new Blob([ab], {type: mimeString});
  }

  public handleImage(webcamImage: WebcamImage): void {
    console.log('received webcam image', webcamImage);
    this.webcamImage = webcamImage;
  }

  public cameraWasSwitched(deviceId: string): void {
    console.log('active device: ' + deviceId);
    this.deviceId = deviceId;
  }

  public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }

  public get nextWebcamObservable(): Observable<boolean|string> {
    return this.nextWebcam.asObservable();
  }

  public handleInitError(error: WebcamInitError): void {
    this.errorsWebcam.push(error);
  }

  onClickSendMediaMessage(): void {
    this.message = {} as Message;
    if (this.files != null && this.files.length > 0){
      this.message.receiverAccount = this.selectedAccount;
      this.message.emitterAccount =  this.loggedAccount;
      this.message.createdAt = new Date();

      this.sendMediaMessage(this.files, this.message);
    }
  }

  sendMediaMessage(files: Array<any>, message: Message): void {
    // this.loading = true;
    for (const item of files) {
        item.progress = 0;
        item.loading = true;
    }
    this.messageService.sendMediaMessage(files, message).subscribe((response) => {
      this.message = response.resource;
      this.messages.push(this.message);
      // create a social interaction for notification
      const socialInteraction: SocialInteraction = NotificationHelper.convertMessageToSocialInteraction(this.message);
      // via websocket
      this.webSocketService.sendPushNotification(socialInteraction);

      this.files = [];
      for (const item of files) {
        item.progress = 0;
        item.loading = false;
      }
      this.closebutton.nativeElement.click();
      this.closebutton_picture.nativeElement.click();
    }, (error) => {
      this.toastService.showError('Erreur', 'Erreur lors de l\'envoi du média');
      // this.loading = false;
      for (const item of files) {
        item.progress = 0;
        item.loading = false;
      }
      this.closebutton.nativeElement.click();
      this.closebutton_picture.nativeElement.click();
    }, () => {});
  }

  validateUrlLink(url: string): boolean{
    this.isValideUrl = StringHelper.validateUrl(url);
    return this.isValideUrl;
  }

  formatBytes(size: any): string {
    return StringHelper.formatBytes(size);
  }

  isSkillschatDesign(): boolean{
    return this.mobileService.isSkillschatDesign();
  }

  arrowBackClicked(isDisplay: boolean): void{
    this.displayMessageContent.emit(false);
  }

  getEstablishmentIcon(): string{
    return StringHelper.getEstablishmentIcon(this.account);
  }
}

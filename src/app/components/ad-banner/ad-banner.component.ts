import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ad-banner',
  templateUrl: './ad-banner.component.html',
  styleUrls: ['./ad-banner.component.css']
})
export class AdBannerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  public openNewTab(): void {
    window.open('https://www.ac-deco.com/', '_blank');
  }
}

import {Component, Input, OnInit} from '@angular/core';
import {RoutesHelper} from '../../helpers/routes-helper';
import {variables} from '../../../environments/variables';
import {AuthenticateService} from '../../services/accounts/authenticate/authenticate.service';
import {ActivatedRoute} from '@angular/router';
import {Account} from '../../models/account/account';

@Component({
  selector: 'app-selection-tab',
  templateUrl: './selection-tab.component.html',
  styleUrls: ['./selection-tab.component.css']
})
export class SelectionTabComponent implements OnInit {

  @Input() tab: string;
  @Input() account: Account;
  @Input() isOnProfileView: boolean;
  routes = RoutesHelper.routes;
  posts: string = variables.posts;
  documents: string = variables.documents;
  about: string = variables.about;
  loggedAccount: Account = {} as Account;
  id: string;

  constructor(private authenticateService: AuthenticateService) { }

  ngOnInit(): void {
    if (!this.tab){
      this.tab = this.posts;
    }

    this.loggedAccount = this.authenticateService.getCurrentAccount();
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterestingprofilesComponent } from './interestingprofiles.component';

describe('InterestingprofilesComponent', () => {
  let component: InterestingprofilesComponent;
  let fixture: ComponentFixture<InterestingprofilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterestingprofilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterestingprofilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

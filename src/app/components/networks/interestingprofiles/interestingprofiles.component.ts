import {Component, Input, OnInit} from '@angular/core';
import {Account} from '../../../models/account/account';
import {variables} from '../../../../environments/variables';
import {routes} from '../../../../environments/routes';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {AccountSocialInteractions} from '../../../models/account/accountSocialInteractions';

@Component({
  selector: 'app-interestingprofiles',
  templateUrl: './interestingprofiles.component.html',
  styleUrls: ['./interestingprofiles.component.css']
})
export class InterestingprofilesComponent implements OnInit {

  @Input() accountsSocialInteractions: AccountSocialInteractions[];
  routes = RoutesHelper.routes;
  constructor() { }

  ngOnInit(): void {
  }

  onClick(position: number): void {
    localStorage.setItem(variables.tab, position.toString(10));
  }
}

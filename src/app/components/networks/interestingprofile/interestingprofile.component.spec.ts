import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterestingprofileComponent } from './interestingprofile.component';

describe('InterestingprofileComponent', () => {
  let component: InterestingprofileComponent;
  let fixture: ComponentFixture<InterestingprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterestingprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterestingprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, Input, OnInit} from '@angular/core';
import {Account} from '../../../models/account/account';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {Router} from '@angular/router';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {WebSocketService} from '../../../services/message/web-socket.service';
import {variables} from '../../../../environments/variables';
import {SocialInteractionService} from '../../../services/pages/social-interaction/social-interaction.service';
import {ToastService} from '../../../services/toast/toast.service';

@Component({
  selector: 'app-interestingprofile',
  templateUrl: './interestingprofile.component.html',
  styleUrls: ['./interestingprofile.component.css']
})
export class InterestingprofileComponent implements OnInit {

  @Input() account: Account;
  routes = RoutesHelper.routes;
  loading: boolean;
  loggedAccount: Account;

  constructor(private socialInteractionService: SocialInteractionService,
              private router: Router,
              private toastService: ToastService,
              private webSocketService: WebSocketService,
              private authenticateService: AuthenticateService) { }

  ngOnInit(): void {
    this.loggedAccount = this.authenticateService.getCurrentAccount();
  }

  /**
   * subscribe to an account ans notify him
   * @param account: followee
   */
  onClickFollow(account: Account): void {
    this.loading = true;
    this.socialInteractionService.addAccountSocialInteraction(this.loggedAccount, account, variables.follower).subscribe((response) => {
      const socialInteraction = response.resource;
      if (socialInteraction.active) {
        this.toastService.showSuccess('Abonné', 'Vous êtes à présent abonné à ' + account.firstname + ' ' + account.lastname);
      } else {
        this.toastService.showSuccess('Abonné', 'Vous vous êtes désabonné de ' + account.firstname + ' ' + account.lastname);
      }
      this.loading = false;
      // send a notification
      this.webSocketService.sendPushNotification(socialInteraction);
      this.router.navigate([this.routes.get('dashboard')], {
        queryParams: {refresh: new Date().getTime()}
      });
    }, error => {
      this.loading = false;
      this.toastService.showSuccess('Abonné', 'Erreur lors du traitement');
    });
  }
}

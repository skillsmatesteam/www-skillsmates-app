import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountNetworkProfileComponent } from './account-network-profile.component';

describe('AccountNetworkProfileComponent', () => {
  let component: AccountNetworkProfileComponent;
  let fixture: ComponentFixture<AccountNetworkProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountNetworkProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountNetworkProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

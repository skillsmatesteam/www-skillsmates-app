import {Component, Input, OnInit} from '@angular/core';
import {Account} from '../../../models/account/account';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {SubscriptionService} from '../../../services/accounts/subscription/subscription.service';
import {Router} from '@angular/router';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {StringHelper} from '../../../helpers/string-helper';

@Component({
  selector: 'app-account-network-profile',
  templateUrl: './account-network-profile.component.html',
  styleUrls: ['./account-network-profile.component.css']
})
export class AccountNetworkProfileComponent implements OnInit {

  @Input() account: Account;
  loggedAccount: Account;
  routes = RoutesHelper.routes;
  loading: boolean;

  constructor(private subscriptionService: SubscriptionService,
              private router: Router,
              private authenticateService: AuthenticateService) { }

  ngOnInit(): void {
    this.loading = false;
    this.loggedAccount = this.authenticateService.getCurrentAccount();
  }

  getProfessionalIcon(): string{
    return StringHelper.getEstablishmentIcon(this.account);
  }
}

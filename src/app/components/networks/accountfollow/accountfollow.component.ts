import {Component, Input, OnInit} from '@angular/core';
import {Account} from '../../../models/account/account';
import {InfoNetworkType} from '../../../models/network/infoNetworkType';
import {variables} from '../../../../environments/variables';
import {routes} from '../../../../environments/routes';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {AccountSocialInteractions} from '../../../models/account/accountSocialInteractions';

@Component({
  selector: 'app-accountfollow',
  templateUrl: './accountfollow.component.html',
  styleUrls: ['./accountfollow.component.css']
})
export class AccountfollowComponent implements OnInit {

  @Input() accountsSocialInteractions: AccountSocialInteractions[];
  @Input() infoNetworkType: InfoNetworkType;
  loading: boolean;
  routes = RoutesHelper.routes;
  constructor() { }

  ngOnInit(): void {
  }

  onClick(position: number): void {
    localStorage.setItem(variables.tab, position.toString(10));
  }
}

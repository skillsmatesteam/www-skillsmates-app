import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountfollowComponent } from './accountfollow.component';

describe('AccountfollowComponent', () => {
  let component: AccountfollowComponent;
  let fixture: ComponentFixture<AccountfollowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountfollowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountfollowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

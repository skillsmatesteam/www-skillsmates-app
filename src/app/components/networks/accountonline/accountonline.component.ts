import {Component, Input, OnInit} from '@angular/core';
import {RoutesHelper} from '../../../helpers/routes-helper';
import {InfoNetworkType} from '../../../models/network/infoNetworkType';
import {variables} from '../../../../environments/variables';
import {AccountSocialInteractions} from '../../../models/account/accountSocialInteractions';

@Component({
  selector: 'app-accountonline',
  templateUrl: './accountonline.component.html',
  styleUrls: ['./accountonline.component.css']
})
export class AccountonlineComponent implements OnInit {
  @Input() accountsSocialInteractions: AccountSocialInteractions[] ;
  @Input() infoNetworkType: InfoNetworkType;
  @Input() nbOnlineAccounts: number;
  routes = RoutesHelper.routes;

  constructor() { }

  ngOnInit(): void {
  }

  onClick(position: number): void {
    localStorage.setItem(variables.tab, position.toString(10));
  }

  onClickOnline(): void {
    console.log('online');
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountonlineComponent } from './accountonline.component';

describe('AccountonlineComponent', () => {
  let component: AccountonlineComponent;
  let fixture: ComponentFixture<AccountonlineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountonlineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountonlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, HostListener, Input, OnChanges, OnInit} from '@angular/core';
import {Account} from '../../../models/account/account';
import {AccountService} from '../../../services/accounts/account/account.service';
import {ToastService} from '../../../services/toast/toast.service';
import {DatePipe} from '@angular/common';
import {MultimediafileService} from '../../../services/media/multimedia/multimediafile.service';
import {AuthenticateService} from '../../../services/accounts/authenticate/authenticate.service';
import {AccountSocialInteractions} from '../../../models/account/accountSocialInteractions';
import {ProfessionalService} from '../../../services/accounts/account/professional.service';
import {AccountHelper} from '../../../helpers/account-helper';
import {SubscriptionService} from '../../../services/accounts/subscription/subscription.service';
import {AccountNetwork} from '../../../models/network/accountNetwork';
import {variables} from '../../../../environments/variables';
import {ActivatedRoute} from '@angular/router';
import {InfoNetworkType} from '../../../models/network/infoNetworkType';
import {environment} from '../../../../environments/environment';
import {CommonHelper} from '../../../helpers/common-helper';
import {MobileService} from '../../../services/mobile/mobile.service';
import {RestCountryService} from '../../../services/api/rest-country.service';
import {CountryHelper} from '../../../helpers/country-helper';
import {SearchNetworkParam} from '../../../models/searchNetworkParam';
import {Status} from '../../../enum/status.enum';

@Component({
  selector: 'app-network-content',
  templateUrl: './network-content.component.html',
  styleUrls: ['./network-content.component.css']
})
export class NetworkContentComponent implements OnInit, OnChanges {

  @Input() accountsSocialInteractions: AccountSocialInteractions[];
  @Input() pages: number[];
  @Input() count: number;
  itemsPerPage: number;
  @Input() currentPage: number;
  @Input() selectedNetworkType: InfoNetworkType;

  loggedAccount: Account = {} as Account;
  allAccountsSocialInteractions: AccountSocialInteractions[];
  accountSocialInteractions: AccountSocialInteractions = {} as AccountSocialInteractions;
  loading: boolean;
  selectedCountry: string;
  selectedStatus: string;
  id: string;
  searchContent: string;
  accountNetwork: AccountNetwork = {} as AccountNetwork;
  professionalStatuses: any[] = CommonHelper.statuses;
  restCountries: any[] = [];
  currentCountry: any;
  selectedCountries: any[];
  searchNetworkParam: SearchNetworkParam = {} as SearchNetworkParam;
  public screenWidth: number = window.innerWidth;

  constructor(private accountService: AccountService,
              private professionalService: ProfessionalService,
              private toastService: ToastService,
              private datePipe: DatePipe,
              private activatedRoute: ActivatedRoute,
              private mobileService: MobileService,
              private restCountryService: RestCountryService,
              private multimediafileService: MultimediafileService,
              private authenticateService: AuthenticateService,
              private subscriptionService: SubscriptionService) {
    this.itemsPerPage = environment.itemsPerPage;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    this.screenWidth = window.innerWidth;
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }

  ngOnInit(): void {
    this.loading = false;
    this.loggedAccount = this.authenticateService.getCurrentAccount();
    this.id = this.activatedRoute.snapshot.paramMap.get(variables.id);
    if (!this.id) {
      this.id = this.loggedAccount.idServer;
    }
    this.findAccountInfos();
    this.allAccountsSocialInteractions = this.accountsSocialInteractions;
  }

  ngOnChanges(): void {
    this.allAccountsSocialInteractions = this.accountsSocialInteractions;
    this.currentPage = 1;
  }

  onChangeStatus(e): void {
    this.loading = true;
    this.selectedStatus = e.target.value.toLowerCase();
    this.filter();
    this.loading = false;
  }

  findAccountInfos(): void {
    this.loading = true;
    this.accountService.findAccountSocialInteractions(this.id).subscribe((response) => {
      this.accountSocialInteractions = response.resource;
      this.loading = false;
    }, (error) => {
      this.loading = false;
    }, () => {
    });
  }

  onClickSearch(): void {
    // this.findNetworkByPage('0');
    // this.findAccountsNetwork();
    this.onKeyupSearch();
  }

  onKeyupSearch(): void {
    this.loading = true;
    this.filter();
    this.loading = false;
  }

  filter(): void {
    this.accountsSocialInteractions = AccountHelper.filterAccountsByName(
      this.filterAccountsByCountry(this.filterAccountsByStatus(this.allAccountsSocialInteractions)),
      this.searchContent);
  }

  filterAccountsByStatus(accountsToFilter: AccountSocialInteractions[]): AccountSocialInteractions[] {
    let filteredAccounts: AccountSocialInteractions[];
    if (this.selectedStatus && this.selectedStatus !== 'tous') {
      this.professionalStatuses.forEach(elt => {
        if (elt.label.toLowerCase() === this.selectedStatus) {
          filteredAccounts = accountsToFilter.filter((accountSI: AccountSocialInteractions) =>
            accountSI.account.status && accountSI.account.status === elt.code);
        }
      });
    } else {
      filteredAccounts = accountsToFilter;
    }
    return filteredAccounts;
  }

  filterAccountsByCountry(accountsToFilter: AccountSocialInteractions[]): AccountSocialInteractions[] {
    let filteredAccounts: AccountSocialInteractions[];
    if (this.selectedCountry && this.selectedCountry !== 'tous') {
      this.restCountries.forEach(elt => {
        if (elt.alpha3Code === this.selectedCountry) {
          filteredAccounts = accountsToFilter.filter((accountSI: AccountSocialInteractions) =>
            accountSI.account.country && accountSI.account.country === this.selectedCountry);
        }
      });
    } else {
      filteredAccounts = accountsToFilter;
    }
    return filteredAccounts;
  }

  getPage(page: number): void {
    this.findNetworkByPage('' + (page - 1));
  }

  findNetworkByPage(page: string): void {
    this.loading = true;
    this.subscriptionService.findNetworkByPageAndType(this.id, page, '' + this.selectedNetworkType.position).subscribe((response) => {
      this.accountNetwork = response.resource;
      this.accountsSocialInteractions = this.filterAccountsByNetwork();
      this.loading = false;
    }, (error) => {
      this.loading = false;
    }, () => {
    });
  }

  findAccountsNetwork(): void {
    this.loading = true;
    const status: Status = CommonHelper.getStatusByLabel(this.selectedStatus);
    this.searchNetworkParam.type = this.selectedNetworkType.position;
    this.searchNetworkParam.content = this.searchContent;
    this.searchNetworkParam.status = status ? status : '';
    this.searchNetworkParam.country = this.currentCountry;
    console.log(this.searchNetworkParam);
    this.subscriptionService.searchAccountsNetwork(this.id, this.searchNetworkParam).subscribe((response) => {
      this.accountNetwork = response.resource;
      this.accountsSocialInteractions = this.filterAccountsByNetwork();
      this.loading = false;
    }, (error) => {
      this.loading = false;
    }, () => {
    });
  }

  filterAccountsByNetwork(): AccountSocialInteractions[] {
    let accountsSocialInteractions: AccountSocialInteractions[] = [];
    this.currentPage = this.accountNetwork.currentPage + 1;
    switch (this.selectedNetworkType.position) {
      case 1:
        if (this.accountNetwork.suggestions) {
          accountsSocialInteractions = this.accountNetwork.suggestions;
          // this.pages = [];
        }
        break;
      case 2:
        if (this.accountNetwork.followers) {
          accountsSocialInteractions = this.accountNetwork.followers;
          // this.pages = this.accountNetwork.pageFollowers;
        }
        break;
      case 3:
        if (this.accountNetwork.followees) {
          accountsSocialInteractions = this.accountNetwork.followees;
          // this.pages = this.accountNetwork.pageFollowees;
        }
        break;
      case 4:
        if (this.accountNetwork.aroundYou) {
          accountsSocialInteractions = this.accountNetwork.aroundYou;
        }
        break;
      case 5:
        if (this.accountNetwork.favorites) {
          accountsSocialInteractions = this.accountNetwork.favorites;
          // this.pages = [];
        }
        break;
      case 6:
        if (this.accountNetwork.accountsOnline) {
          accountsSocialInteractions = this.accountNetwork.accountsOnline;
        }
        break;
      default:
        break;
    }
    return accountsSocialInteractions;
  }

  onKeyupCountry(): void {
    this.searchCountries();
  }

  searchCountries(): void{
    if (this.currentCountry){
      this.restCountries = CountryHelper.filterCountries(this.currentCountry);
    }
  }

  onSelectCountry(item): void{
    this.loading = true;
    this.selectedCountries = this.restCountries.filter((user) => user.translations.fra.common.toLowerCase().includes(item.toLowerCase()));
    if (this.selectedCountries && this.selectedCountries.length > 0){
      this.selectedCountry = this.selectedCountries[0].cca3;
    }
    this.filter();
    this.loading = false;
  }
}

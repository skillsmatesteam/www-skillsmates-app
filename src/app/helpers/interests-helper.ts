import {InfoInterestType} from '../models/account/infoInterestType';

export class InterestsHelper {

  static interestsTypes: InfoInterestType[] = [
    {id: 0, idServer: '', image: 'restaurant', position : 1, title : 'Bar-Restaurant'},
    {id: 0, idServer: '', image: 'cinema', position : 2, title : 'Cinéma'},
    {id: 0, idServer: '', image: 'location', position : 3, title : 'Lieu'},
    {id: 0, idServer: '', image: 'sport', position : 4, title : 'Sport'},
    {id: 0, idServer: '', image: 'shopping', position : 5, title : 'Shopping'},
    {id: 0, idServer: '', image: 'training', position : 6, title : 'Lecture'}
  ];

  static getInterestTypeByPosition(position: string): any{
    let selected;
    InterestsHelper.interestsTypes.forEach(elt => {
      if (elt.position.toString(10) === position){
        selected = elt;
      }
    });
    return selected ? selected : InterestsHelper.interestsTypes[0];
  }
}

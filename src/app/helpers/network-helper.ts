import {InfoNetworkType} from '../models/network/infoNetworkType';
import {AccountNetwork} from '../models/network/accountNetwork';

export class NetworkHelper {

  static networkTypes: InfoNetworkType[] = [
    {id: 0, idServer: '', type: 'suggestions', color: '#16152D', picture: './assets/images/follow-black.svg', position : 1, title : 'Suggestions', value: 0, description : 'En ligne'},
    {id: 0, idServer: '', type: 'followers', color: '#16152D', picture: './assets/images/followers-black.svg', position : 2, title : 'Abonnés', value: 0, description : 'Ils vous suivent'},
    {id: 0, idServer: '', type: 'followees', color: '#16152D', picture: './assets/images/following-black.svg', position : 3, title : 'Abonnements', value: 0, description : 'Vous les suivez'},
    {id: 0, idServer: '', type: 'aroundyou', color: '#16152D', picture: './assets/images/around-me.svg', position : 4, title : 'Autour de vous', value: 0, description : 'Autour de vous'},
    {id: 0, idServer: '', type: 'favorites', color: '#16152D', picture: './assets/images/favoris.svg', position : 5, title : 'Favoris', value: 0, description : 'Favoris'},
    {id: 0, idServer: '', type: 'online', color: '#16152D', picture: './assets/images/online.svg', position : 6, title : 'En ligne', value: 0, description : 'En ligne'}
  ];

  static getNetworkTypeByPosition(position: string): any{
    let selectedNetworkType;
    NetworkHelper.networkTypes.forEach(elt => {
      if (elt.position.toString(10) === position){
        selectedNetworkType = elt;
      }
    });
    return selectedNetworkType ? selectedNetworkType : NetworkHelper.networkTypes[0];
  }

  static countAccountsByNetworkType(accountNetwork: AccountNetwork, networkType: InfoNetworkType): number{
    let nb = 0;
    switch (networkType.position) {
      case 1:
        if (accountNetwork.suggestions){
          nb = accountNetwork.numberSuggestions;
        }
        break;
      case 2:
        if (accountNetwork.followers) {
          nb = accountNetwork.numberFollowers;
        }
        break;
      case 3:
        if (accountNetwork.followees) {
          nb = accountNetwork.numberFollowees;
        }
        break;
      case 4:
        if (accountNetwork.aroundYou) {
          nb = accountNetwork.numberAroundYou;
        }
        break;
      case 5:
        if (accountNetwork.favorites) {
          nb = accountNetwork.numberFavorites;
        }
        break;
      case 6:
        if (accountNetwork.accountsOnline) {
          nb = accountNetwork.numberAccountsOnline;
        }
        break;
      default:
        break;
    }
    return nb;
  }
}

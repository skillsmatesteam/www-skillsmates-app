import {Mimetype} from '../models/multimedia/mimetype';

export class MimetypeHelper {

  static mimetypes: Mimetype[] = [
    {id: 0, idServer: '', extension: 'audio', type : 'audio/'},
    {id: 0, idServer: '', extension: 'image', type : 'image/'},
    {id: 0, idServer: '', extension: 'video', type : 'video/'},
    {id: 0, idServer: '', extension: 'pdf', type : 'application/pdf'},
    {id: 0, idServer: '', extension: 'txt', type : 'text/'},
    {id: 0, idServer: '', extension: 'doc', type : 'application/msword'},
    {id: 0, idServer: '', extension: 'xls', type : 'application/vnd.ms-excel'},
    {id: 0, idServer: '', extension: 'ppt', type : 'application/vnd.ms-powerpoint'},
    {id: 0, idServer: '', extension: 'mdb', type : 'application/vnd.ms-access'},
    {id: 0, idServer: '', extension: 'docx', type : 'application/vnd.openxmlformats-officedocument'},
  ];

  static isMimetypeAllowed(mimetype: string): boolean{
    let allowed = false;
    MimetypeHelper.mimetypes.forEach(elt => {
      if (mimetype.startsWith(elt.type)){
        allowed = true;
      }
    });
    return allowed;
  }

  static isAudioFile(mimetype: string): boolean{
    return mimetype && (mimetype.startsWith('audio/'));
  }

  static isImageFile(mimetype: string): boolean{
    return mimetype && (mimetype.startsWith('image/'));
  }

  static isVideoFile(mimetype: string): boolean{
    return mimetype && (mimetype.startsWith('video/'));
  }

  static isWordFile(mimetype: string): boolean{
    return mimetype && (mimetype.startsWith('application/msword') || mimetype.startsWith('application/vnd.openxmlformats-officedocument.wordprocessingml'));
  }

  static isPowerPointFile(mimetype: string): boolean{
    return mimetype && (mimetype.startsWith('application/vnd.ms-powerpoint') || mimetype.startsWith('application/vnd.openxmlformats-officedocument.presentationml'));
  }

  static isExcelFile(mimetype: string): boolean{
    return mimetype && (mimetype.startsWith('application/vnd.ms-excel') || mimetype.startsWith('application/vnd.openxmlformats-officedocument.spreadsheetml'));
  }

  static isAccessFile(mimetype: string): boolean{
    return mimetype && (mimetype.startsWith('application/vnd.ms-access'));
  }

  static isPdfFile(mimetype: string): boolean{
    return mimetype && (mimetype.startsWith('application/pdf'));
  }
}

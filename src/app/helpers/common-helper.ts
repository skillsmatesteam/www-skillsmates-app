import {Gender} from '../enum/gender.enum';
import {Status} from '../enum/status.enum';

export class CommonHelper {

  static statuses: any[] = [
    {code: Status[Status.STUDENT], label : 'Etudiant'},
    {code: Status[Status.TEACHER], label : 'Enseignant'},
    {code: Status[Status.PROFESSIONAL], label : 'Professionnel'}
  ];

  static genders: any[] = [
    {code: Gender[Gender.MALE], label : 'Homme'},
    {code: Gender[Gender.FEMALE], label : 'Femme'}
  ];

  static getStatusByLabel(label: string): Status {
    let status: Status = null;
    if (label){
      CommonHelper.statuses.forEach(value => {
        if (value.label.toLowerCase() === label.toLowerCase()) {
          status = value.code;
        }
      });
    }
    return status;
  }
}

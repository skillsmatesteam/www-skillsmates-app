import {Skill} from '../models/account/skill';

export class SkillHelper {

  static grades = [2, 1, -2, -1];

  public static findFirstSkillByGrade(skills: Skill[], grade: number): Skill{
    let skill: Skill;
    if (skills && skills.length > 0){
      skills.forEach(elt => {
        if (!skill && elt.level.grade === grade){
          skill = elt;
        }
      });
    }
    return skill;
  }

  public static sortSkillsByLevel(skills: Skill[], grade: number): Skill[]{
    const skillsSorted: Skill[] = [];
    let skill: Skill = this.findFirstSkillByGrade(skills, grade);
    while (skill) {
      skillsSorted.push(skill);
      skills = skills.filter(elt => elt !== skill);
      skill = this.findFirstSkillByGrade(skills, grade);
    }
    return skillsSorted;
  }

  public static findFirstSkills(skills: Skill[]): Skill[]{
    const skillsFiltered: Skill[] = [];
    for (const grade of this.grades){
      const skill = this.findFirstSkillByGrade(skills, grade);
      if (skill) { skillsFiltered.push(skill); }
    }
    return skillsFiltered;
  }
}

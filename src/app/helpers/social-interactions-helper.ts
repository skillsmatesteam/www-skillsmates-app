import {SocialInteraction} from '../models/post/socialInteraction';
import {variables} from '../../environments/variables';
import {Account} from '../models/account/account';

export class SocialInteractionsHelper {

  public static countSocialInteractionsByType(socialInteractions: SocialInteraction[], type: string): number{
    let nb = 0;
    if (socialInteractions && socialInteractions.length > 0) {
      socialInteractions.forEach(interaction => {
        if (interaction.type === type) {
          nb++;
        }
      });
    }
    return nb;
  }

  public static countLikes(socialInteractions: SocialInteraction[]): number{
    return this.countSocialInteractionsByType(socialInteractions, variables.like);
  }

  public static countShares(socialInteractions: SocialInteraction[]): number{
    return this.countSocialInteractionsByType(socialInteractions, variables.share);
  }

  public static countComments(socialInteractions: SocialInteraction[]): number{
    return this.countSocialInteractionsByType(socialInteractions, variables.comment);
  }

  public static countFavorites(socialInteractions: SocialInteraction[]): number{
    return this.countSocialInteractionsByType(socialInteractions, variables.favorite);
  }

  public static hasInteracted(socialInteractions: SocialInteraction[], account: Account, type: string): boolean{
    let hasInteracted = false;
    if (socialInteractions && socialInteractions.length > 0){
      socialInteractions.forEach(interaction => {
        if (interaction.type === type && interaction.emitterAccount.idServer === account.idServer){
          hasInteracted = true;
        }
      });
    }
    return hasInteracted;
  }

  public static hasLiked(socialInteractions: SocialInteraction[], account: Account): boolean{
    return this.hasInteracted(socialInteractions, account, variables.like);
  }

  public static hasShared(socialInteractions: SocialInteraction[], account: Account): boolean{
    return this.hasInteracted(socialInteractions, account, variables.share);
  }

  public static isFollowing(socialInteractions: SocialInteraction[], emitter: Account, receiver: Account): boolean{
    let followed = false;
    if (socialInteractions && socialInteractions.length > 0){
      socialInteractions.forEach(interaction => {
        if (interaction.type === variables.follower &&
          interaction.emitterAccount.idServer === emitter.idServer &&
          interaction.receiverAccount.idServer === receiver.idServer){
          followed = true;
        }
      });
    }
    return followed;
  }

  public static retrieveSocialInteractionsWithCriteria(socialInteractions: SocialInteraction[], element: string, emitterAccount: Account, receiverAccount: Account, type: string): SocialInteraction{
    let socialInteractionsRetrieved: SocialInteraction = {} as SocialInteraction;
    if (socialInteractions && socialInteractions.length > 0) {
      socialInteractions.forEach(interaction => {
        if (interaction.type === type &&
          interaction.emitterAccount.idServer === emitterAccount.idServer &&
          interaction.receiverAccount.idServer === receiverAccount.idServer && interaction.element === element) {
          socialInteractionsRetrieved = interaction;
        }
      });
    }
    return socialInteractionsRetrieved;
  }

  public static retrieveSocialInteractionsByType(socialInteractions: SocialInteraction[], type: string): SocialInteraction[]{
    const socialInteractionsRetrieved: SocialInteraction[] = [];
    if (socialInteractions && socialInteractions.length > 0) {
      socialInteractions.forEach(interaction => {
        if (interaction.type === type) {
          socialInteractionsRetrieved.push(interaction);
        }
      });
    }
    return socialInteractionsRetrieved;
  }

  public static retrieveLikes(socialInteractions: SocialInteraction[]): SocialInteraction[]{
    return this.retrieveSocialInteractionsByType(socialInteractions, variables.like);
  }

  public static retrieveShares(socialInteractions: SocialInteraction[]): SocialInteraction[]{
    return this.retrieveSocialInteractionsByType(socialInteractions, variables.share);
  }

  public static retrieveComments(socialInteractions: SocialInteraction[]): SocialInteraction[]{
    return this.retrieveSocialInteractionsByType(socialInteractions, variables.comment);
  }

  public static retrieveFavorites(socialInteractions: SocialInteraction[]): SocialInteraction[]{
    return this.retrieveSocialInteractionsByType(socialInteractions, variables.favorite);
  }

  public static retrieveFollowers(socialInteractions: SocialInteraction[], account: Account): SocialInteraction[]{
    const followersInteractions: SocialInteraction[] = [];
    const interactions = this.retrieveSocialInteractionsByType(socialInteractions, variables.follower);
    if (interactions && interactions.length > 0){
      interactions.forEach( interaction => {
        if (interaction.receiverAccount.idServer === account.idServer){
          followersInteractions.push(interaction);
        }
      });
    }
    return followersInteractions;
  }

  public static retrieveFollowees(socialInteractions: SocialInteraction[], account: Account): SocialInteraction[]{
    const followeesInteractions: SocialInteraction[] = [];
    const interactions = this.retrieveSocialInteractionsByType(socialInteractions, variables.follower);
    if (interactions && interactions.length > 0){
      interactions.forEach( interaction => {
        if (interaction.emitterAccount.idServer === account.idServer){
          followeesInteractions.push(interaction);
        }
      });
    }
    return followeesInteractions;
  }

  public static removeSocialInteraction(socialInteractions: SocialInteraction[], socialInteraction: SocialInteraction): SocialInteraction[]{
    const socialInteractionsRemoved: SocialInteraction[] = [];
    if (socialInteractions && socialInteractions.length > 0) {
      socialInteractions.forEach((interaction, index) => {
        if (interaction.type === socialInteraction.type && interaction.idServer === socialInteraction.idServer) {
          socialInteractionsRemoved.splice(index, 1);
        }
      });
    }
    return socialInteractionsRemoved;
  }
}

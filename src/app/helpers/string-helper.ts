import {Account} from '../models/account/account';
import {RoutesHelper} from './routes-helper';
import {routes} from '../../environments/routes';
import {variables} from '../../environments/variables';
import { Status } from '../enum/status.enum';

export class StringHelper {
  routes = RoutesHelper.routes;

  static truncateName(account: Account, nb: number): string{
    let name = ' ' + account.firstname + ' ' + account.lastname;
    if (name.length > nb){
      name = name.slice(0, nb - 1) + '...';
    }
    return name;
  }

  static truncateString(value: string, nb: number): string{
    if (value.length > nb){
      value = value.slice(0, nb - 1) + '...';
    }
    return value;
  }

  static toNumber(str: string): number {
    return +str;
  }

  static getMaxFileSize(): number{
    return variables.max_file_size * 1048576;
  }

  static formatBytes(bytes, decimals = 2): string {
    if (bytes === 0) {
      return '0 Bytes';
    }
    const k = 1024;
    const dm = decimals <= 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  static truncateText(text: string, nb: number): string{
    if (text && text.length > nb){
      text = text.slice(0, nb - 1) + '...';
    }
    return text;
  }

  static validateEmail(email: string): boolean{
    const regExp = /^[a-z][a-zA-Z0-9_.]*(\.[a-zA-Z][a-zA-Z0-9_.]*)?@[a-z][a-zA-Z-0-9]*\.[a-z]+(\.[a-z]+)?$/;
    return regExp.test(email);
  }

  static validatePassword(password: string, account: Account): Map<number, boolean>{
    const map = new Map();
    map.set(1, false);
    map.set(2, false);
    map.set(3, false);
    map.set(4, false);
    map.set(5, false);
    // const regExp = /^(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
    const regExpCa = /^(?=^.{8,16})/; // 8 - 16 caracteres
    const regExpLCh = /^((?=.*\d)|(?=.*[!&%$]))/; // Chiffre ou caractere special
    const regExpLm = /^(?=.*[a-z])/; // Lettre mininuscule
    const regExpLM = /^(?=.*[A-Z])/; // Lettre majuscule

    if (regExpCa.test(password)){
      map.set(1, true);
    }
    if (regExpLCh.test(password)){
      map.set(2, true);
    }
    if (password.toLowerCase().includes(account.lastname.toLowerCase()) || password.toLowerCase().includes(account.firstname.toLowerCase()) || password.toLowerCase().includes(account.email.toLowerCase())){
      map.set(3, true);
    }
    if(regExpLM.test(password)){
      map.set(4, true);
    }
    if(regExpLm.test(password)){
      map.set(5, true);
    }
    return map;
  }

  static validateUrl(url): boolean{
    const regExp = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
    return regExp.test(url);
  }

  static urlToTab(url: string): string{
    let tab = routes.dashboard;
    if (url.indexOf(routes.dashboard) >= 0){
      tab = routes.dashboard;
    }

    if (url.indexOf(routes.my_messages) >= 0){
      tab = routes.my_messages;
    }

    if (url.indexOf(routes.my_setting) >= 0){
      tab = routes.profile;
    }

    if (url.indexOf(routes.my_notifications) >= 0){
      tab = routes.my_notifications;
    }

    if (url.indexOf(routes.my_network) >= 0){
      tab = routes.my_network;
    }

    if (url.indexOf(routes.profile) >= 0){
      tab = routes.profile;
    }

    if (url.indexOf(routes.assistance) >= 0){
      tab = routes.profile;
    }

    if (url.indexOf(routes.posts) >= 0){
      tab = routes.posts;
    }
    return tab;
  }

  static getEstablishmentIcon(account: Account): string{
    if (account && account.status){
      if (account.status === Status[Status.PROFESSIONAL]){
        return 'company';
      }

      if (account.status === Status[Status.TEACHER]){
        return 'school';
      }

      if (account.status === Status[Status.STUDENT]){
        return 'school';
      }
    }
    return 'company';
  }

  static generateRandomNumber(min: number, max: number): number{
    return Math.floor(Math.random() * max) + min;
  }
}

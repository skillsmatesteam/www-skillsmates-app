import {variables} from '../../environments/variables';

export class CountryHelper {

  /**
   * filter countries by label in french
   * @param label to filter
   */
  static filterCountries(label: string): any[] {
    const filteredCountries: any[] = [];
    const countries: any[] = JSON.parse(localStorage.getItem(variables.countries));
    countries.forEach(country => {
      if (country.translations.fra.common.toLowerCase().startsWith(label)){
        filteredCountries.push(country);
      }
    });
    return filteredCountries;
  }

  static findCountryByAlpha3Code( code: string): any{
    const countries: any[] = JSON.parse(localStorage.getItem(variables.countries));
    let alphaCodeCountry: any = {};
    if (countries && countries.length > 0){
      countries.forEach(country => {
        if (country.cca3.toLowerCase() === code.toLowerCase()){
          alphaCodeCountry = country;
        }
      });
    }
    return alphaCodeCountry;
  }

}

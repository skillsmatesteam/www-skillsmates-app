import {InfoSettingType} from '../models/account/infoSettingType';

export class SettingHelper {

  static settingTypes: InfoSettingType[] = [
    {id: 0, idServer: '', type: 'general', color: '#DADADA', picture: './assets/images/user.svg', position : 1, title : 'Compte', value: 0, description : 'Général'},
    {id: 0, idServer: '', type: 'security_connections', color: '#DADADA', picture: './assets/images/security.svg', position : 2, title : 'Sécurité & Connexions', value: 0, description : 'Sécurité & Connexions'},
    {id: 0, idServer: '', type: 'confidentiality', color: '#DADADA', picture: './assets/images/confidentiality.svg', position : 3, title : 'Confidentialité', value: 0, description : 'Confidentialité'},
    {id: 0, idServer: '', type: 'location', color: '#DADADA', picture: './assets/images/location.svg', position : 4, title : 'Localisation', value: 0, description : 'Localisation'}
  ];

  static getSettingTypeByPosition(position: string): any{
    let selectedSettingType;
    SettingHelper.settingTypes.forEach(elt => {
      if (elt.position.toString(10) === position){
        selectedSettingType = elt;
      }
    });
    return selectedSettingType ? selectedSettingType : SettingHelper.settingTypes[0];
  }
}

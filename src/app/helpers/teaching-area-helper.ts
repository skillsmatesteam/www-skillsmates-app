import {TeachingAreaGroup} from '../models/account/config/teachingAreaGroup';
import {TeachingAreaSet} from '../models/account/config/teachingAreaSet';
import {TeachingArea} from '../models/account/config/teachingArea';

export class TeachingAreaHelper {
  public static filterTeachingArea(teachingAreas: TeachingArea[], teachingAreaGroup: TeachingAreaGroup): TeachingArea[]{
    const teachingAreasFiltered = [];
    teachingAreas.forEach(elt => {
      if (elt.teachingAreaGroup.id === teachingAreaGroup.id){
        teachingAreasFiltered.push(elt);
      }
    });
    return teachingAreasFiltered;
  }

  public static findTeachingAreaSet(teachingAreas: TeachingArea[]): TeachingAreaSet[]{
    const teachingAreaSetAll: TeachingAreaSet[] = [];
    teachingAreas.forEach(elt => {
      teachingAreaSetAll.push(elt.teachingAreaGroup.teachingAreaSet);
    });

    const teachingAreaSets = Array.from(new Set(teachingAreaSetAll.map(a => a.id)))
      .map(id => {
        return teachingAreaSetAll.find(a => a.id === id);
      });

    return teachingAreaSets;
  }

  public static findTeachingAreaGroups(teachingAreas: TeachingArea[], teachingAreaSet: TeachingAreaSet): any[]{
    const teachingGroups = [];
    const teachings = [];
    let teaching = {disabled: true, id: 0, label: ''};
    teaching.disabled = true;
    teaching.label = teachingAreaSet.label;
    teachings.push(teaching);
    teachingAreas.forEach(elt => {
      if (elt.teachingAreaGroup.teachingAreaSet.id === teachingAreaSet.id){
        teaching = {disabled: false, id: 0, label: ''};
        teaching.disabled = false;
        teaching.id = elt.teachingAreaGroup.id;
        teaching.label = elt.teachingAreaGroup.label;
        teachingGroups.push(teaching);
      }
    });

    const teachingGroupsFiltered = Array.from(new Set(teachingGroups.map(a => a.id)))
      .map(id => {
        return teachingGroups.find(a => a.id === id);
      });

    teachingGroupsFiltered.forEach(elt => teachings.push(elt));
    return teachings;
  }

  public static filterTeachingAreaGroup(teachingAreas: TeachingArea[]): any[]{
    const teachingAreaSets = this.findTeachingAreaSet(teachingAreas);
    let teachings = [];
    teachingAreaSets.forEach(elt => {
      teachings = teachings.concat(this.findTeachingAreaGroups(teachingAreas, elt));
    });
    return teachings;
  }
}

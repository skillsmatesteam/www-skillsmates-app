import {Notifications} from '../models/notification/notifications';
import {Notification} from '../models/notification/notification';
import {SocialInteraction} from '../models/post/socialInteraction';
import {Message} from '../models/message/message';
import {variables} from '../../environments/variables';
import {AccountMessage} from '../models/message/accountMessage';

export class NotificationHelper {

  /**
   * compare notifications
   * @param a: first notification
   * @param b: second notification
   */
  static compare(a: Notification, b: Notification): number{
    if (a && b){
      return a.createdAt < b.createdAt ? 1 : -1;
    }
    return 0;
  }

  /**
   * filter notifications to be displayed on the bell icon
   * @param notifications: notifications to sort
   */
  static sortBellNotifications(notifications: Notifications): Notification[]{
    const bellNotifications: Notification[] = [];
    if (notifications){
      if (notifications.likes && notifications.likes.length > 0){
        notifications.likes.forEach(elt => bellNotifications.push(elt));
      }

      if (notifications.comments && notifications.comments.length > 0){
        notifications.comments.forEach(elt => bellNotifications.push(elt));
      }

      if (notifications.shares && notifications.shares.length > 0){
        notifications.shares.forEach(elt => bellNotifications.push(elt));
      }

      if (notifications.favorites && notifications.favorites.length > 0){
        notifications.favorites.forEach(elt => bellNotifications.push(elt));
      }

      if (notifications.posts && notifications.posts.length > 0){
        notifications.posts.forEach(elt => bellNotifications.push(elt));
      }
    }
    return bellNotifications.sort(this.compare);
  }

  /**
   * withdraw all notifications
   * @param notifications: set of all notifications
   */
  static sortAllNotifications(notifications: Notifications): Notification[]{
    const allNotifications = this.sortBellNotifications(notifications);
    if (notifications){
      if (notifications.followers.length > 0){
        notifications.followers.forEach(elt => allNotifications.push(elt));
      }

      if (notifications.messages.length > 0){
        notifications.messages.forEach(elt => allNotifications.push(elt));
      }
    }
    return allNotifications.sort(this.compare);
  }

  /**
   * convert social interaction to message
   * @param socialInteraction: the social interaction
   */
  static convertSocialInteractionToMessage(socialInteraction: SocialInteraction): Message{
    const message = {} as Message;
    message.id = socialInteraction.id;
    message.idServer = socialInteraction.idServer;
    message.active = socialInteraction.active;
    message.deleted = socialInteraction.deleted;
    message.revision = socialInteraction.revision;
    message.createdAt = socialInteraction.createdAt;
    message.modifiedAt = socialInteraction.modifiedAt;
    message.emitterAccount = socialInteraction.emitterAccount;
    message.receiverAccount = socialInteraction.receiverAccount;
    message.content = socialInteraction.content;
    message.idServer = socialInteraction.element;
    message.multimedia = socialInteraction.multimedia && socialInteraction.multimedia.length > 0 ? socialInteraction.multimedia : [];
    return message;
  }

  /**
   * convert message into socialInteraction
   * @param message to be converted
   */
  static convertMessageToSocialInteraction(message: Message): SocialInteraction{
    const socialInteraction: SocialInteraction = {} as SocialInteraction;
    socialInteraction.id = message.id;
    socialInteraction.idServer = message.idServer;
    socialInteraction.active = message.active;
    socialInteraction.deleted = message.deleted;
    socialInteraction.createdAt = message.createdAt;
    socialInteraction.modifiedAt = message.modifiedAt;
    socialInteraction.revision = message.revision;
    socialInteraction.emitterAccount = message.emitterAccount;
    socialInteraction.receiverAccount = message.receiverAccount;
    socialInteraction.content = message.content;
    socialInteraction.element = message.idServer;
    socialInteraction.type = variables.message;
    socialInteraction.multimedia = message.multimedia && message.multimedia.length > 0 ? message.multimedia : [];
    return socialInteraction;
  }

  /**
   * convert message into notification
   * @param message to be converted
   */
  static convertMessageToNotification(message: Message): Notification{
    const notification: Notification = {} as Notification;
    notification.id = message.id;
    notification.idServer = message.idServer;
    notification.active = message.active;
    notification.deleted = message.deleted;
    notification.createdAt = message.createdAt;
    notification.modifiedAt = message.modifiedAt;
    notification.revision = message.revision;
    notification.emitterAccount = message.emitterAccount;
    notification.receiverAccount = message.receiverAccount;
    notification.title = message.content;
    notification.element = message.idServer;
    notification.type = variables.message;
    return notification;
  }

  /**
   * convert list of messages into notifications
   * @param messages list of messages to be converted
   */
  static convertMessagesToNotifications(messages: Message[]): Notification[]{
    const notifications: Notification[] = [];
    if (messages && messages.length > 0){
      messages.forEach(message => {notifications.push(NotificationHelper.convertMessageToNotification(message)); });
    }
    return notifications;
  }

  /**
   * convert a social interaction into notification
   * @param socialInteraction to be converted
   */
  static convertSocialInteractionToNotification(socialInteraction: SocialInteraction): Notification {
    const notification: Notification = {} as Notification;
    notification.id = socialInteraction.id;
    notification.idServer = socialInteraction.idServer;
    notification.active = socialInteraction.active;
    notification.deleted = socialInteraction.deleted;
    notification.createdAt = socialInteraction.createdAt;
    notification.modifiedAt = socialInteraction.modifiedAt;
    notification.revision = socialInteraction.revision;
    notification.emitterAccount = socialInteraction.emitterAccount;
    notification.receiverAccount = socialInteraction.receiverAccount;
    notification.title = socialInteraction.content;
    notification.element = socialInteraction.idServer;
    notification.type = socialInteraction.type;
    return notification;
  }
}

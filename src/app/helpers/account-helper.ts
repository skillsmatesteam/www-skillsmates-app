import {AccountMessage} from '../models/message/accountMessage';
import {AccountSocialInteractions} from '../models/account/accountSocialInteractions';

export class AccountHelper {

  /**
   * filter accounts by name (firstname, lastname) that contain searchContent
   * @param accountsToFilter: Accounts to filter
   * @param searchContent: the filter string
   */
  static filterAccountsByName(accountsToFilter: AccountSocialInteractions[], searchContent): AccountSocialInteractions[]{
    let filteredAccounts: AccountSocialInteractions[];
    if (searchContent){
      filteredAccounts = accountsToFilter.filter( (accountSI: AccountSocialInteractions) =>
        searchContent && accountSI.account.firstname && accountSI.account.lastname &&
        (accountSI.account.lastname.toLocaleLowerCase() + ' ' + accountSI.account.firstname.toLocaleLowerCase()).indexOf(searchContent.toLowerCase()) >= 0);
    }else {
      filteredAccounts = accountsToFilter;
    }
    return filteredAccounts;
  }

  /**
   * filter accounts by searchContent
   * @param accountsMessagesToFilter: accounts to filter
   * @param searchContent: filter string
   */
  static filterAccountsMessagesByName(accountsMessagesToFilter: AccountMessage[], searchContent): AccountMessage[]{
    let filteredAccounts: AccountMessage[];
    if (searchContent){
      filteredAccounts = accountsMessagesToFilter.filter( (accountMessage: AccountMessage) =>
        searchContent && accountMessage.account.firstname && accountMessage.account.lastname &&
        (accountMessage.account.lastname.toLocaleLowerCase() + ' ' + accountMessage.account.firstname.toLocaleLowerCase()).indexOf(searchContent.toLowerCase()) >= 0);
    }else {
      filteredAccounts = accountsMessagesToFilter;
    }
    return filteredAccounts;
  }

}

import {routes} from '../../environments/routes';

export class RoutesHelper {
  static routes: Map<string, string> = new Map<string, string>([
    ['root', routes.root],
    ['login', routes.login],
    ['logout', routes.logout],
    ['refresh', routes.refresh],
    ['post.view', routes.view_post],
    ['cgu', routes.cgu],
    ['dashboard', routes.dashboard],
    ['assistance', routes.assistance],
    ['profile', routes.profile],
    ['profile.my', routes.my_profile],
    ['profile.edit.my', routes.edit_my_profile],
    ['profile.view', routes.view_profile],
    ['documents', routes.documents],
    ['documents.my', routes.my_documents],
    ['network.my', routes.my_network],
    ['network', routes.network],
    ['setting.my', routes.my_setting],
    ['messages.my', routes.my_messages],
    // ['messages.my', routes.skillschat],
    ['posts', routes.posts],
    ['post.edit.my', routes.edit_my_post],
    ['reset_password', routes.reset_password],
    ['notifications.my', routes.my_notifications],
    ['search_results', routes.search_results],
    ['url_view_profile', routes.url_view_profile],
    ['validate.account', routes.validate_account]
  ]);
}

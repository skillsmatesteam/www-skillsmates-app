import {InfoMediaType} from '../models/multimedia/infoMediaType';
import {Multimedia} from '../models/multimedia/multimedia';
import {variables} from '../../environments/variables';
import {MimetypeHelper} from './mimetype-helper';
import {MediaType} from '../models/multimedia/mediaType';

export class MediaHelper {

  static mediaTypes: InfoMediaType[] = [
    {id: 0, selected: false, idServer: 'application/pdf', type: 'document', accept: '', color: '#476db4', picture: './assets/images/document.svg', position : 1, title : 'Documents', value: 0, description : 'Partagez des cours, exercices, mémoires, astuces, etc.', multimedia: [], subtitle: 'Documents partagés', found: 0},
    {id: 0, selected: false, idServer: '', type: 'video', accept: 'video/*', color: '#e14033', picture: './assets/images/photogram.svg', position : 3, title : 'Vidéos', value: 0, description : 'Partagez des tutoriels, des conférences ou des actualités.', multimedia: [], subtitle: 'Vidéos partagées', found: 0},
    {id: 0, selected: false, idServer: '', type: 'link', accept: '', color: '#f2c600', picture: './assets/images/lien.svg', position : 2, title : 'Liens', value: 0, description : 'Partagez un article, une revue ou un site web.', multimedia: [], subtitle: 'Liens partagés', found: 0},
    {id: 0, selected: false, idServer: '', type: 'image', accept: 'image/*', color: '#20b197', picture: './assets/images/phot.svg', position : 4, title : 'Images', value: 0, description : 'Partagez des scans ou vos photos personnelles.', multimedia: [], subtitle: 'Images partagées', found: 0},
    {id: 0, selected: false, idServer: '', type: 'audio', accept: 'audio/*', color: '#dc5f9f', picture: './assets/images/audio.svg', position : 5, title : 'Audios', value: 0, description : 'Partagez des conférences ou des actualités.', multimedia: [], subtitle: 'Audios partagés', found: 0}
  ];

  static profil: InfoMediaType = {id: 0, selected: false, idServer: '', type: 'profile', accept: '', color: '#16162D', picture: './assets/images/user.svg', position : 6, title : 'Profils', value: 0, description : '', multimedia: [], subtitle: '', found: 0};
  static all: InfoMediaType = {id: 0, selected: false, idServer: '', type: 'all', accept: '', color: '#16162D', picture: './assets/images/select-all.svg', position : 7, title : 'Tous', value: 0, description : '', multimedia: [], subtitle: '', found: 0};

  static profileMediaType: MediaType = {id: 0, idServer: '', label: 'PROFILE'};

  static getInfoMediaTypeByType(type: string): any{
    let selectedMediaType;
    MediaHelper.mediaTypes.forEach(elt => {
      if (elt.type.toLowerCase() === type.toLowerCase()){
        selectedMediaType = elt;
      }
    });
    return selectedMediaType ? selectedMediaType : null;
  }

  static getMediaTypeByPosition(position: string): any{
    let selectedMediaType;
    MediaHelper.mediaTypes.forEach(elt => {
      if (elt.position.toString(10) === position){
        selectedMediaType = elt;
      }
    });
    return selectedMediaType ? selectedMediaType : MediaHelper.mediaTypes[0];
  }

  static getMultimediaType(multimedia: Multimedia): string{
    return multimedia.mimeType ? this.getMediaType(multimedia.mimeType.toLowerCase()) : variables.link;
  }

  static getMediaType(type: string): string{
    if (MimetypeHelper.isImageFile(type)){
      return 'image';
    }else if (MimetypeHelper.isPdfFile(type)){
      return 'pdf';
    }else if (MimetypeHelper.isAudioFile(type)){
      return 'audio';
    }else if (MimetypeHelper.isVideoFile(type)){
      return 'video';
    }else if (MimetypeHelper.isWordFile(type)){
      return 'word';
    }else if (MimetypeHelper.isExcelFile(type)){
      return 'excel';
    }else if (MimetypeHelper.isPowerPointFile(type)){
      return 'powerpoint';
    }else if (MimetypeHelper.isAccessFile(type)){
      return 'access';
    }else {
      return variables.document;
    }
  }

  static getYouTubeID(url): string{
    let ID = '';
    url = url.replace(/(>|<)/gi, '').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
    if (url[2] !== undefined) {
      ID = url[2].split(/[^0-9a-z_\-]/i);
      ID = ID[0];
    }
    else {
      ID = url;
    }
    return ID;
  }

  static getYoutubeVideoId(url: string): string {
    const p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
    const matches = url.match(p);
    if (matches){
      return matches[1];
    }
    return null;
  }

  static isYoutubeLink(url: string): boolean{
    return !!MediaHelper.getYoutubeVideoId(url);
  }
}

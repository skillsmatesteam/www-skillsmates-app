import { Injectable } from '@angular/core';
// import { ErrorDialogService } from '../error-dialog/errordialog.service';
import {
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import {variables} from '../../environments/variables';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
    constructor() { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
          let url = null;
          url = request.url;

        // if (request.url.indexOf('skillsmatesresources') < 0){
          const id: string = localStorage.getItem(variables.header_id);
          if (id) {
            request = request.clone({ headers: request.headers.set(variables.header_id, id) });
          }

          const token: string = localStorage.getItem(variables.header_token);
          if (token) {
            request = request.clone({ headers: request.headers.set(variables.header_token, token) });
          }

          if (!request.headers.has('Content-Type')) {
            request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
          }

          if (request.headers.get('Content-Type') === 'multipart/form-data'){
            request = request.clone({ headers: request.headers.delete('Content-Type') });
          }

          request = request.clone({ headers: request.headers.set('Accept', 'application/json') });

          return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                  if (event.body.headers) { // TODO : à revoir
                    const idResponse = event.body.headers[variables.header_id];
                    if (idResponse && !url.includes('/skillsmates-pages/posts/view/')) {
                      localStorage.setItem(variables.header_id, idResponse[0]);
                    }

                    const tokenResponse = event.body.headers[variables.header_token];
                    if (tokenResponse) {
                      localStorage.setItem(variables.header_token, tokenResponse[0]);
                    }
                    // this.errorDialogService.openDialog(event);
                  }
                }
                return event;
            }),
            catchError((error: HttpErrorResponse) => {
                let data = {};
                data = {
                    reason: error && error.error && error.error.reason ? error.error.reason : '',
                    status: error.status
                };
                // this.errorDialogService.openDialog(data);
                return throwError(error);
            }));
    }
}

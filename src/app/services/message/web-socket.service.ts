import { Injectable } from '@angular/core';
import {Message} from '../../models/message/message';
import {environment} from '../../../environments/environment';
import {BehaviorSubject} from 'rxjs';

import * as SockJS from 'sockjs-client';
import {Stomp} from '@stomp/stompjs';
import {SocialInteraction} from '../../models/post/socialInteraction';

@Injectable({
  providedIn: 'root'
})

export class WebSocketService {
  msWebsocketHost = environment.ms_messages_host + '/websocket';
  topic = '/user';
  idServer: string;
  websocket: WebSocket;
  messages: Message[] = [];
  pushNotificationWebSocket = new BehaviorSubject(null);
  stompClient: any;

  constructor(){
  }

  openWebSocket(idServer: string): void {

    const ws = new SockJS(this.msWebsocketHost);
    const _this = this;
    _this.stompClient = Stomp.over(ws);
    _this.topic = '/user/' + idServer + '/queue/chat';
    this.idServer = idServer;
    _this.stompClient.connect({}, function(frame) {
      _this.stompClient.subscribe(_this.topic, function(event) {
        _this.onPushNotificationReceived(event);
      });
       // this.stompClient.reconnect_delay = 2000;
    }, this.errorCallBack);
  }

  closeWebsocket(): void{
    // this.websocket.close();
    if (this.stompClient !== null) {
      this.stompClient.disconnect();
    }
    console.log('Disconnected');
  }

  // on error, schedule a reconnection attempt
  errorCallBack(error): void {
    console.log('errorCallBack -> ' + error);
    setTimeout(() => {
      this.openWebSocket(this.idServer);
    }, 5000);
  }

  sendPushNotification(socialInteraction: SocialInteraction): void {
    this.stompClient.send('/app/user', {}, JSON.stringify(socialInteraction));
  }

  onPushNotificationReceived(event): void {
    this.pushNotificationWebSocket.next(JSON.parse(JSON.stringify(event.body)));
  }
}

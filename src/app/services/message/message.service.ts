import { Injectable } from '@angular/core';
import {variables} from '../../../environments/variables';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Message} from '../../models/message/message';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  url = '/skillsmates-messages/messages';
  HEADER_ID = variables.header_id;
  HEADER_TOKEN = variables.header_token;
  headers = new HttpHeaders();
  httpHeaders = this.headers.set('Content-Type', 'application/json; charset=utf-8');
  httpOptions;

  constructor(private httpClient: HttpClient) {
    this.httpOptions = { headers: this.httpHeaders };
  }

  /**
   * create a message
   */
  saveMessage(message: Message): Observable<any>{
    return this.httpClient.post<any>(environment.ms_messages_host + this.url, message);
  }

  /**
   * find all messages by account
   */
  findMessagesByAccount(id: string): Observable<any>{
    return this.httpClient.get<any>(environment.ms_messages_host + this.url + '/' + id, this.httpOptions);
  }

  findAccountsMessages(): Observable<any>{
    return this.httpClient.get<any>(environment.ms_messages_host + this.url, this.httpOptions);
  }

  /**
   * find my messages
   */
  findMyMessages(): Observable<any>{
    return this.httpClient.get<any>(environment.ms_messages_host + this.url + '/my', this.httpOptions);
  }

  sendMediaMessage(files: File[], message: Message): Observable<any>{
    this.httpHeaders = this.headers.set('Content-Type', 'multipart/form-data');
    this.httpOptions = { headers: this.httpHeaders };
    const data: FormData = new FormData();
    data.append('file', files[0]);
    data.append('receiver', message.receiverAccount.idServer);
    return this.httpClient.post<any>(environment.ms_messages_host + this.url + 'file', data, this.httpOptions);
  }
}

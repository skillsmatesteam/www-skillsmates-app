import { Injectable } from '@angular/core';
import {variables} from '../../../../environments/variables';
import {Account} from '../../../models/account/account';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UpdateService {

  url = '/skillsmates-settings/updates';
  HEADER_ID = variables.header_id;
  HEADER_TOKEN = variables.header_token;
  loggedAccount: Account = {} as Account;
  httpHeaders = new HttpHeaders();
  httpOptions;

  constructor(private httpClient: HttpClient) {
    this.httpOptions = { headers: this.httpHeaders };
  }

  findUpdates(): Observable<any>{
    return this.httpClient.get<any>(environment.ms_settings_host + this.url, this.httpOptions);
  }

  dismissUpdates(): Observable<any>{
    return this.httpClient.put<any>(environment.ms_settings_host + this.url, this.httpOptions);
  }
}

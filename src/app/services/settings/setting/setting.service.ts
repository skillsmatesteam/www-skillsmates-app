import { Injectable } from '@angular/core';
import {variables} from "../../../../environments/variables";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Account} from "../../../models/account/account";
import {Observable} from "rxjs";
import {environment} from "../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class SettingService {
  url = '/skillsmates-settings/settings';
  HEADER_ID = variables.header_id;
  HEADER_TOKEN = variables.header_token;
  HEADER_ACCOUNT_ID = 'X-Ms-Account-Id';
  loggedAccount: Account = {} as Account;
  headers = new HttpHeaders();
  httpHeaders = this.headers.set('Content-Type', 'application/json; charset=utf-8');
  httpOptions;

  constructor(private httpClient: HttpClient) {
    this.httpOptions = { headers: this.httpHeaders };
  }

  /**
   * update account
   */
  updateAccountBySetting(account: Account, param: string): Observable<any>{
    return this.httpClient.put(environment.ms_settings_host + this.url+ '/' + param, account, this.httpOptions);
  }

  initResetPassword(email: string): Observable<any>{
    return this.httpClient.get(environment.ms_settings_host + this.url+ '/resetpassword/'+email, this.httpOptions);
  }

  /**
   * deactivate account
   */
  deactivateAccount(account: Account): Observable<any>{
    return this.httpClient.put(environment.ms_settings_host + this.url+ '/deactivate' , account, this.httpOptions);
  }

  /**
   * delete account
   */
  deleteAccount(account: Account): Observable<any>{
    return this.httpClient.put(environment.ms_settings_host + this.url+ '/delete' , account, this.httpOptions);
  }

}

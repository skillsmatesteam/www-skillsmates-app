import { Injectable } from '@angular/core';
import {variables} from '../../../../environments/variables';
import {Account} from '../../../models/account/account';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Post} from '../../../models/post/post';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {Assistance} from '../../../models/assistance/assistance';

@Injectable({
  providedIn: 'root'
})
export class AssistanceService {
  url = '/skillsmates-settings/assistance';
  HEADER_ID = variables.header_id;
  HEADER_TOKEN = variables.header_token;
  loggedAccount: Account = {} as Account;
  httpHeaders = new HttpHeaders();
  params: HttpParams = new HttpParams();
  httpOptions;

  constructor(private httpClient: HttpClient) {
    this.httpOptions = { headers: this.httpHeaders };
  }

  sendAssistance(assistance: Assistance): Observable<any>{
    return this.httpClient.post<any>(environment.ms_settings_host + this.url, assistance, this.httpOptions);
  }
}

import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {variables} from '../../../../environments/variables';

@Injectable({
  providedIn: 'root'
})
export class MetadataService {

  url = '/skillsmates-media/metadata';
  HEADER_ID = variables.header_id;
  HEADER_TOKEN = variables.header_token;
  loggedAccount: Account = {} as Account;
  httpHeaders = new HttpHeaders();
  httpOptions;

  constructor(private httpClient: HttpClient) {
    this.httpOptions = { headers: this.httpHeaders };
  }

  getMetadataUrl(url: string): Observable<any> {
    return this.httpClient.get<any>(environment.ms_media_host + this.url + '/preview/' + url, this.httpOptions);
  }
}

import { TestBed } from '@angular/core/testing';

import { MultimediafileService } from './multimediafile.service';

describe('DocumentfileService', () => {
  let service: MultimediafileService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MultimediafileService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {AuthenticateService} from '../../accounts/authenticate/authenticate.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {variables} from '../../../../environments/variables';
import {Account} from '../../../models/account/account';
import {Multimedia} from '../../../models/multimedia/multimedia';

@Injectable({
  providedIn: 'root'
})
export class MultimediafileService {

  url = '/skillsmates-media/multimediafiles';
  HEADER_ID = variables.header_id;
  HEADER_TOKEN = variables.header_token;
  HEADER_ACCOUNT_ID = 'X-Ms-Account-Id';
  loggedAccount: Account = {} as Account;
  headers = new HttpHeaders();
  httpHeaders = this.headers.set('Content-Type', 'multipart/form-data');
  httpOptions;

  constructor(private httpClient: HttpClient) {
    this.httpOptions = { headers: this.httpHeaders };
  }

  uploadMultimedia(file: File, account: Account): Observable<any>{
    this.httpHeaders = this.httpHeaders.set(this.HEADER_ACCOUNT_ID, account.idServer);
    const httpOptions = { headers: this.httpHeaders };
    const data: FormData = new FormData();
    data.append('file', file);
    data.append('X-Ms-Type-Multimedia', 'avatar');
    data.append('X-Ms-Multimedia-Id', '');
    return this.httpClient.post<any>(environment.ms_media_host + this.url, data, httpOptions);
  }

  uploadAvatarMultimedia(avatarFile: File, originalFile: File, account: Account): Observable<any>{
    this.httpHeaders = this.httpHeaders.set(this.HEADER_ACCOUNT_ID, account.idServer);
    const httpOptions = { headers: this.httpHeaders };
    const data: FormData = new FormData();
    data.append('avatar', avatarFile);
    data.append('image', originalFile);
    return this.httpClient.post<any>(environment.ms_media_host + this.url + '/avatar', data, httpOptions);
  }
}

import { Injectable } from '@angular/core';
import {variables} from '../../../../environments/variables';
import {Account} from '../../../models/account/account';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MultimediaService {
  url = '/skillsmates-media/multimedia';
  HEADER_ID = variables.header_id;
  HEADER_TOKEN = variables.header_token;
  HEADER_ACCOUNT_ID = 'X-Ms-Account-Id';
  loggedAccount: Account = {} as Account;
  httpHeaders = new HttpHeaders();
  httpOptions;

  constructor(private httpClient: HttpClient) {
    this.httpOptions = { headers: this.httpHeaders };
  }

  findMultimedias(accountId: string): Observable<any> {
    this.httpHeaders = this.httpHeaders.set(this.HEADER_ACCOUNT_ID, accountId);
    const httpOptions = { headers: this.httpHeaders };
    return this.httpClient.get<any>(environment.ms_media_host + this.url, httpOptions);
  }

  findMediaType(accountId: string): Observable<any> {
    const httpOptions = { headers: this.httpHeaders };
    return this.httpClient.get<any>(environment.ms_media_host + this.url + '/mediatype/' + accountId, httpOptions);
  }

  findMediaTypeOffline(accountId: string): Observable<any> {
    const httpOptions = { headers: this.httpHeaders };
    return this.httpClient.get<any>(environment.ms_media_host + this.url + '/mediatype/offline/' + accountId, httpOptions);
  }

  countMediaType(accountId: string): Observable<any> {
    const httpOptions = { headers: this.httpHeaders };
    return this.httpClient.get<any>(environment.ms_media_host + this.url + '/countmedia/' + accountId, httpOptions);
  }

  findMediaSubtypes(): Observable<any> {
    return this.httpClient.get<any>(environment.ms_media_host + this.url + '/mediasubtype', this.httpOptions);
  }

  getMetadataUrl(url: string): Observable<any> {
    return this.httpClient.get<any>(environment.ms_media_host + this.url + '/' + url, this.httpOptions);
  }
}

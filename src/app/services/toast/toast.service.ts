import { Injectable } from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {CookieService} from 'ngx-cookie-service';
import {variables} from '../../../environments/variables';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  successSrc = './assets/audio/success.mp3';
  errorSrc = './assets/audio/error.mp3';
  constructor(private toastr: ToastrService, private cookieService: CookieService) {}

  showSuccess(title?: string, message?: string): void {
    this.toastr.success(message, title);
  }

  saveToken(response: any): void{
    if (response && response.headers){
      const id = response.headers[variables.header_id][0];
      if (id !== undefined){
        localStorage.setItem(variables.header_id, id);
        this.cookieService.set(variables.account_current, id, 1, '/', window.location.hostname, true, 'Strict');
      }
      const token = response.headers[variables.header_token][0];
      if (token !== undefined){
        localStorage.setItem(variables.header_token, token);
        this.cookieService.set(variables.header_token, token, 1, '/', window.location.hostname, true, 'Strict');
      }
    }
  }

  showError(title: string, message: string): void {
    this.toastr.error(message, title);
  }

  showGeneralError(): void {
    this.toastr.error('Une erreur est survenue', 'Erreur');
  }

  showWarning(title: string, message: string): void {
    this.toastr.warning(message, title);
  }

  showInfo(title: string, message: string): void {
    this.toastr.info(message, title);
  }
}

import { HostListener, Injectable } from '@angular/core';
import { ScreenHelper } from 'src/app/helpers/screen-helper';

@Injectable({
  providedIn: 'root',
})
export class MobileService {
  public screenWidth: number = window.innerWidth;

  constructor() {}

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    this.screenWidth = window.innerWidth;
  }

  isMobile(screenWidth): boolean {
    return ScreenHelper.isMobile(screenWidth);
  }

  setSkillschatDesign(): void{
    if (this.isMobile(this.screenWidth)){
      localStorage.setItem('design', 'skillschat');
    }
  }

  unsetSkillschatDesign(): void{
    localStorage.removeItem('design');
  }

  isSkillschatDesign(): boolean{
    return this.isMobile(this.screenWidth) && localStorage.getItem('design') && localStorage.getItem('design') === 'skillschat';
  }

  setNotificationDesign(): void{
    if (this.isMobile(this.screenWidth)){
      localStorage.setItem('design', 'notification');
    }
  }

  unsetNotificationDesign(): void{
    localStorage.removeItem('design');
  }

  isNotificationDesign(): boolean{
    return this.isMobile(this.screenWidth) && localStorage.getItem('design') && localStorage.getItem('design') === 'notification';
  }
}

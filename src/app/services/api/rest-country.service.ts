import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RestCountryService {
  constructor(private httpClient: HttpClient) { }

  // searchCountries(label: string): Observable<any> {
  //   return this.httpClient.get<any>(environment.countries_name_api + label);
  // }
  //
  // searchCountriesByCode(code: string): Observable<any> {
  //   return this.httpClient.get<any>(environment.countries_code_api + code);
  // }

  searchAllCountries(): Observable<any> {
    return this.httpClient.get<any>(environment.countries_api);
  }
}

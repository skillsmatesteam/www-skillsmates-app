import { TestBed } from '@angular/core/testing';

import { RestCountryService } from './rest-country.service';

describe('RestCountryService', () => {
  let service: RestCountryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RestCountryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

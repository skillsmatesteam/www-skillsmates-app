import { Injectable } from '@angular/core';
import {Message} from "../../models/message/message";
import {BehaviorSubject, Observable, Subject} from "rxjs";
import {Account} from "../../models/account/account";


@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  private _subjectMessage : BehaviorSubject<Message> = new BehaviorSubject(null);
  private _subjectSelectedAccount :  BehaviorSubject<Account> = new BehaviorSubject(null);
  public subjectMessage$: Observable<Message> = this._subjectMessage.asObservable();
  public subjectSelectedAccount$: Observable<Account> = this._subjectSelectedAccount.asObservable();

  constructor() { }


  sendMessage(message: Message){
    this._subjectMessage.next(message);
  }
  sendSelectedAccount(account: Account){
    this._subjectSelectedAccount.next(account);
  }

  /*receiveMessage(){
    return this.subjectMessage;
  }
  receiveSelectedAccount(){
    return this.subjectSelectedAccount;
  }*/
  clearMessage(){
    //this.message = {} as Message;
  }
}

import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Router} from '@angular/router';
import {AuthenticateService} from './accounts/authenticate/authenticate.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuardService {
  constructor(public authenticationService: AuthenticateService, public router: Router) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    if (this.authenticationService.isAuthenticated() ) {
      return true;
    }
    this.router.navigate(['/']);
    return false;
  }
}

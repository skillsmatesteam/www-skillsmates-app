import { Injectable } from '@angular/core';
import {variables} from '../../../environments/variables';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Notification} from '../../models/notification/notification';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  url = '/skillsmates-notifications/notifications';
  HEADER_ID = variables.header_id;
  HEADER_TOKEN = variables.header_token;
  httpHeaders = new HttpHeaders();
  httpOptions;

  private notificationSource = new BehaviorSubject(new Array<Notification>());
  notificationMessage = this.notificationSource.asObservable();

  constructor(private httpClient: HttpClient) {
    this.httpOptions = { headers: this.httpHeaders };
  }

  changeNotificationMessage(notifications: Notification[]): void {
    this.notificationSource.next(notifications);
  }

  findNotifications(): Observable<any>{
    return this.httpClient.get<any>(environment.ms_notifications_host + this.url, this.httpOptions);
  }

  deacticvatNotification(notification: Notification): Observable<any>{
    return this.httpClient.put<any>(environment.ms_notifications_host + this.url, notification, this.httpOptions);
  }
}

import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Account} from '../../../models/account/account';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {variables} from '../../../../environments/variables';
import {CookieService} from 'ngx-cookie-service';
import {ActivatedRoute} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticateService {

  url = '/skillsmates-accounts/authenticate';
  HEADER_EMAIL = 'X-Ms-Email';
  HEADER_PASSWORD = 'X-Ms-Password';

  headers = new HttpHeaders();
  httpHeaders = this.headers.append('Content-Type', 'application/json; charset=utf-8');

  constructor(private httpClient: HttpClient, private cookieService: CookieService, private activatedRoute: ActivatedRoute) { }

  authenticate(email: string, password: string): Observable<any>{
    const account: Account = new Account();
    this.httpHeaders = this.httpHeaders.set(this.HEADER_EMAIL, email.toString());
    this.httpHeaders = this.httpHeaders.set(this.HEADER_PASSWORD, password.toString());
    const httpOptions = { headers: this.httpHeaders };

    return this.httpClient.post<any>(environment.ms_accounts_host + this.url, account, httpOptions);
  }

  /**
   * check if the account is authenticated
   */
  isAuthenticated(): boolean{
    return true;
  }

  /**
   * check if the account is authorized
   */
  isAuthorized(): boolean{
    const param: any = localStorage.getItem(variables.account_current);
    const account: Account = JSON.parse(param);
    if ( this.isAuthenticated() ){
        return true; // TODO: to be completed
    }
    return false;
  }

  /**
   * get current logged account
   */
  getCurrentAccount(): Account{
    const param: any = localStorage.getItem(variables.account_current);
    return JSON.parse(param);
  }

  getCurrentAccountId(): string{
    return localStorage.getItem(variables.header_id);
  }

  logout(): void{
    this.clearLocalMemory();
  }

  clearLocalMemory(): void{
    localStorage.removeItem(variables.header_id);
    localStorage.removeItem(variables.header_token);
    localStorage.removeItem(variables.account_current);
  }

  verificateEmail(email: string): Observable<any>{
    const account: Account = new Account();
    this.httpHeaders = this.httpHeaders.set(this.HEADER_EMAIL, email.toString());
    const httpOptions = { headers: this.httpHeaders };
    return this.httpClient.post<any>(environment.ms_accounts_host + this.url + '/email', account, httpOptions);
  }
}

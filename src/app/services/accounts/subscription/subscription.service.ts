import { Injectable } from '@angular/core';
import {variables} from '../../../../environments/variables';
import {Account} from '../../../models/account/account';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {SearchParam} from '../../../models/searchParam';
import {SearchNetworkParam} from '../../../models/searchNetworkParam';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionService {
  url = '/skillsmates-accounts/subscriptions';
  HEADER_ID = variables.header_id;
  HEADER_TOKEN = variables.header_token;
  loggedAccount: Account = {} as Account;
  httpHeaders = new HttpHeaders();
  httpOptions;

  constructor(private httpClient: HttpClient) {
    this.httpOptions = { headers: this.httpHeaders };
  }

  findNetwork(accountId: string): Observable<any>{
    return this.httpClient.get<any>(environment.ms_accounts_host + this.url + '/network/' + accountId, this.httpOptions);
  }

  // findNetworkByPage(accountId: string, page: number): Observable<any>{
  //   const httpParams = new HttpParams().set('page', page.toString(10)).set('limit', environment.itemsPerPage.toString(10));
  //   this.httpOptions = { headers: this.httpHeaders , params: httpParams};
  //   return this.httpClient.get<any>(environment.ms_accounts_host + this.url + '/network/' + accountId, this.httpOptions);
  // }

  findNetworkByPage(accountId: string, page: number, networkType: number): Observable<any>{
    const httpParams = new HttpParams().set('page', page.toString(10)).set('limit', environment.itemsPerPage.toString(10)).set('networkType', '' + networkType);
    this.httpOptions = { headers: this.httpHeaders , params: httpParams};
    return this.httpClient.get<any>(environment.ms_accounts_host + this.url + '/networkByType/' + accountId, this.httpOptions);
  }

  findNetworkOfflineByPage(accountId: string, page: number): Observable<any>{
    const httpParams = new HttpParams().set('page', page.toString(10)).set('limit', environment.itemsPerPage.toString(10));
    this.httpOptions = { headers: this.httpHeaders , params: httpParams};
    return this.httpClient.get<any>(environment.ms_accounts_host + this.url + '/network/offline/' + accountId, this.httpOptions);
  }

  findNetworkByPageAndType(accountId: string, page: string, networkType: string): Observable<any>{
    let httpParams = new HttpParams();
    httpParams = httpParams.append('page', page).set('limit', environment.itemsPerPage.toString(10));
    httpParams = httpParams.append('networkType', networkType);
    this.httpOptions = { headers: this.httpHeaders , params: httpParams};
    return this.httpClient.get<any>(environment.ms_accounts_host + this.url + '/networkByType/' + accountId, this.httpOptions);
  }

  searchAccountsNetwork(accountId: string, searchNetworkParam: SearchNetworkParam): Observable<any>{
    return this.httpClient.post<any>(environment.ms_accounts_host + this.url + '/networkByType/' + accountId, searchNetworkParam, this.httpOptions);
  }
}

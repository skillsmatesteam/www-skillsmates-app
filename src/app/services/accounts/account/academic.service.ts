import { Injectable } from '@angular/core';
import {variables} from '../../../../environments/variables';
import {Account} from '../../../models/account/account';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';
import {AuthenticateService} from '../authenticate/authenticate.service';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {Professional} from '../../../models/account/professional';
import {DegreeObtained} from '../../../models/account/degreeObtained';
import {SecondaryEducation} from '../../../models/account/secondaryEducation';
import {HigherEducation} from '../../../models/account/higherEducation';
import {Certification} from "../../../models/account/certification";

@Injectable({
  providedIn: 'root'
})
export class AcademicService {

  url = '/skillsmates-accounts/accounts/academics';
  HEADER_ID = variables.header_id;
  HEADER_TOKEN = variables.header_token;
  HEADER_DEGREE_OBTAINED_ID = 'X-Ms-Degree-Obtained-Id';
  HEADER_SECONDARY_EDUCATION_ID = 'X-Ms-Secondary-Education-Id';
  HEADER_HIGHER_EDUCATION_ID = 'X-Ms-Higher-Education-Id';
  HEADER_CERTIFICATION_ID = 'X-Ms-Certification-Id';
  loggedAccount: Account = {} as Account;
  headers = new HttpHeaders();
  httpHeaders = this.headers.set('Content-Type', 'application/json; charset=utf-8');
  httpOptions;

  constructor(private httpClient: HttpClient) {
    this.httpOptions = { headers: this.httpHeaders };
  }

  /**
   * find account for academics infos
   */
  findAccountAcademics(id: string): Observable<any>{
    return this.httpClient.get<any>(environment.ms_accounts_host + this.url + '/' + id, this.httpOptions);
  }
  findAccountAcademicsOffline(id: string): Observable<any>{
    return this.httpClient.get<any>(environment.ms_accounts_host + this.url + '/offline/' + id, this.httpOptions);
  }
  createDegreeObtained(degreeObtained: DegreeObtained): Observable<any>{
    return this.httpClient.post<any>(environment.ms_accounts_host + this.url + '/degrees', degreeObtained, this.httpOptions);
  }

  createCertification(certification: Certification): Observable<any>{
    return this.httpClient.post<any>(environment.ms_accounts_host + this.url + '/certificates', certification, this.httpOptions);
  }

  deleteDegreeObtained(id: string): Observable<any>{
    this.httpHeaders = this.httpHeaders.set(this.HEADER_DEGREE_OBTAINED_ID, id);
    const httpOptions = { headers: this.httpHeaders };
    return this.httpClient.delete(environment.ms_accounts_host + this.url + '/degrees', httpOptions);
  }

  deleteCertification(id: string): Observable<any>{
    this.httpHeaders = this.httpHeaders.set(this.HEADER_CERTIFICATION_ID, id);
    const httpOptions = { headers: this.httpHeaders };
    return this.httpClient.delete(environment.ms_accounts_host + this.url + '/certificates', httpOptions);
  }

  createSecondaryEducation(secondaryEducation: SecondaryEducation): Observable<any>{
    return this.httpClient.post<any>(environment.ms_accounts_host + this.url + '/secondary', secondaryEducation, this.httpOptions);
  }

  deleteSecondaryEducation(id: string): Observable<any>{
    this.httpHeaders = this.httpHeaders.set(this.HEADER_SECONDARY_EDUCATION_ID, id);
    const httpOptions = { headers: this.httpHeaders };
    return this.httpClient.delete(environment.ms_accounts_host + this.url + '/secondary', httpOptions);
  }

  createHigherEducation(higherEducation: HigherEducation): Observable<any>{
    return this.httpClient.post<any>(environment.ms_accounts_host + this.url + '/higher', higherEducation, this.httpOptions);
  }

  deleteHigherEducation(id: string): Observable<any>{
    this.httpHeaders = this.httpHeaders.set(this.HEADER_HIGHER_EDUCATION_ID, id);
    const httpOptions = { headers: this.httpHeaders };
    return this.httpClient.delete(environment.ms_accounts_host + this.url + '/higher', httpOptions);
  }
}

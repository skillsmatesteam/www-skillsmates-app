import { Injectable } from '@angular/core';
import {variables} from '../../../../environments/variables';
import {Account} from '../../../models/account/account';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';
import {AuthenticateService} from '../authenticate/authenticate.service';
import {Skill} from '../../../models/account/skill';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {Professional} from '../../../models/account/professional';
import {AccountFilter} from "../../../models/account/accountFilter";

@Injectable({
  providedIn: 'root'
})
export class ProfessionalService {
  url = '/skillsmates-accounts/accounts/professional';
  HEADER_ID = variables.header_id;
  HEADER_TOKEN = variables.header_token;
  HEADER_ACCOUNT_ID = 'X-Ms-Account-Id';
  HEADER_PROFESSIONAL_ID = 'X-Ms-Professional-Id';
  loggedAccount: Account = {} as Account;
  headers = new HttpHeaders();
  httpHeaders = this.headers.set('Content-Type', 'application/json; charset=utf-8');
  httpOptions;
  constructor(private httpClient: HttpClient,
              private cookieService: CookieService,
              private authenticateService: AuthenticateService) {
    this.httpOptions = { headers: this.httpHeaders };
  }

  createProfessional(professional: Professional): Observable<any>{
    return this.httpClient.post<any>(environment.ms_accounts_host + this.url, professional, this.httpOptions);
  }

  deleteProfessional(id: string): Observable<any>{
    this.httpHeaders = this.httpHeaders.set(this.HEADER_PROFESSIONAL_ID, id);
    const httpOptions = { headers: this.httpHeaders };
    return this.httpClient.delete(environment.ms_accounts_host + this.url, httpOptions);
  }

  findProfessionals(id: string): Observable<any>{
    return this.httpClient.get<any>(environment.ms_accounts_host + this.url + '/' + id, this.httpOptions);
  }

  findProfessionalsOffline(id: string): Observable<any>{
    return this.httpClient.get<any>(environment.ms_accounts_host + this.url + '/offline/' + id, this.httpOptions);
  }

  filterProfessionals(accountFilter: AccountFilter): Observable<any>{
    return this.httpClient.post<any>(environment.ms_accounts_host + this.url + '/filter',accountFilter, this.httpOptions);
  }
}

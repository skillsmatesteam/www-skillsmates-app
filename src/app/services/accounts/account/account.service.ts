import { Injectable } from '@angular/core';
import {variables} from '../../../../environments/variables';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Account} from '../../../models/account/account';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {Favorite} from '../../../models/account/favorite';
import {SearchParam} from '../../../models/searchParam';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  url = '/skillsmates-accounts/accounts';
  HEADER_ID = variables.header_id;
  HEADER_TOKEN = variables.header_token;
  HEADER_ACCOUNT_ID = 'X-Ms-Account-Id';
  loggedAccount: Account = {} as Account;
  headers = new HttpHeaders();
  httpHeaders = this.headers.set('Content-Type', 'application/json; charset=utf-8');
  httpOptions;

  constructor(private httpClient: HttpClient) {
    this.httpOptions = { headers: this.httpHeaders };
  }

  /**
   * find all accounts
   */
  findAllAccounts(): Observable<any> {
    return this.httpClient.get<any>(environment.ms_accounts_host + this.url, this.httpOptions);
  }

  /**
   * create an account
   */
  createAccount(account: Account): Observable<any>{
    return this.httpClient.post<any>(environment.ms_accounts_host + this.url, account);
  }

  /**
   * find account by idServer
   */
  findAccountById(id: string): Observable<any>{
    return this.httpClient.get<any>(environment.ms_accounts_host + this.url + '/' + id, this.httpOptions);
  }

  /**
   * find account by idServer
   */
  findOfflineAccountById(id: string): Observable<any>{
    return this.httpClient.get<any>(environment.ms_accounts_host + this.url + '/offline/' + id, this.httpOptions);
  }

  /**
   * find account for general infos
   */
  findAccountSocialInteractions(id: string): Observable<any>{
    return this.httpClient.get<any>(environment.ms_accounts_host + this.url + '/infos/' + id, this.httpOptions);
  }

  findAccountSocialInteractionsOffline(id: string): Observable<any>{
    return this.httpClient.get<any>(environment.ms_accounts_host + this.url + '/infos/offline/' + id, this.httpOptions);
  }

  /**
   * find account by idServer
   */
  findAccountByIdWithToken(id: string, token: string): Observable<any>{
    return this.httpClient.get<any>(environment.ms_accounts_host + this.url + '/' + id, this.httpOptions);
  }

  /**
   * update account
   */
  updateAccount(account: Account): Observable<any>{
    return this.httpClient.put(environment.ms_accounts_host + this.url, account, this.httpOptions);
  }

  /**
   * update account
   */
  updateAccountBySetting(account: Account, param: string): Observable<any>{
    return this.httpClient.put(environment.ms_accounts_host + this.url + '/' + param, account, this.httpOptions);
  }



  /**
   * update account
   */
  updateAccountByBlock(account: Account, block: string): Observable<any>{
    return this.httpClient.put(environment.ms_accounts_host + this.url + '/' + block, account, this.httpOptions);
  }

  /**
   * delete account
   */
  deleteAccount(id: string): Observable<any>{
    this.httpHeaders = this.httpHeaders.set(this.HEADER_ACCOUNT_ID, id);
    const httpOptions = { headers: this.httpHeaders };
    return this.httpClient.delete(environment.ms_accounts_host + this.url, httpOptions);
  }

  logoutAccount(): Observable<any>{
    return this.httpClient.get(environment.ms_accounts_host + this.url + '/logout', this.httpOptions);
  }

  countProfileInfos(accountId: string): Observable<any>{
    return this.httpClient.get(environment.ms_accounts_host + this.url + '/profile/' + accountId, this.httpOptions);
  }

  saveAccountFavorite(favorite: Favorite): Observable<any>{
    return this.httpClient.post<any>(environment.ms_accounts_host + this.url + '/favorite', favorite, this.httpOptions);
  }

  /**
   * search accounts
   */
  searchAccounts(searchParam: SearchParam, page: number, itemsPerPage: number): Observable<any>{
    const httpParams = new HttpParams().set('page', page.toString(10))
      .set('limit', itemsPerPage.toString(10));
    this.httpOptions = { headers: this.httpHeaders , params: httpParams};
    return this.httpClient.post<any>(environment.ms_accounts_host + this.url + '/searchPerson', searchParam, this.httpOptions);
  }

  /**
   * generate profile url
   */
  generateUrlProfile(id: string): Observable<any>{
    return this.httpClient.get<any>(environment.ms_accounts_host + this.url + '/generateUrl/' + id, this.httpOptions);
  }
}

import { Injectable } from '@angular/core';
import {variables} from '../../../../environments/variables';
import {Account} from '../../../models/account/account';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthenticateService} from '../authenticate/authenticate.service';
import {Skill} from '../../../models/account/skill';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {CookieService} from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class SkillService {
  url = '/skillsmates-accounts/accounts/skills';
  HEADER_ID = variables.header_id;
  HEADER_TOKEN = variables.header_token;
  HEADER_ACCOUNT_ID = 'X-Ms-Account-Id';
  HEADER_SKILL_ID = 'X-Ms-Skill-Id';
  loggedAccount: Account = {} as Account;
  headers = new HttpHeaders();
  httpHeaders = this.headers.set('Content-Type', 'application/json; charset=utf-8');
  httpOptions;

  constructor(private httpClient: HttpClient,
              private cookieService: CookieService,
              private authenticateService: AuthenticateService) {
    this.httpOptions = { headers: this.httpHeaders };
  }

  createSkill(skill: Skill): Observable<any>{
    return this.httpClient.post<any>(environment.ms_accounts_host + this.url, skill, this.httpOptions);
  }

  updateSkill(skill: Skill): Observable<any>{
    return this.httpClient.put<any>(environment.ms_accounts_host + this.url, skill, this.httpOptions);
  }

  deleteSkill(id: string): Observable<any>{
    this.httpHeaders = this.httpHeaders.set(this.HEADER_SKILL_ID, id);
    const httpOptions = { headers: this.httpHeaders };
    return this.httpClient.delete(environment.ms_accounts_host + this.url, httpOptions);
  }

  findSkills(id: string): Observable<any>{
    return this.httpClient.get<any>(environment.ms_accounts_host + this.url + '/' + id, this.httpOptions);
  }

  findSkillsOffline(id: string): Observable<any>{
    return this.httpClient.get<any>(environment.ms_accounts_host + this.url + '/offline/' + id, this.httpOptions);
  }
}

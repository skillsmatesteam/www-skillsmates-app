import { Injectable } from '@angular/core';
import {Account} from '../../../models/account/account';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {SocialInteraction} from '../../../models/post/socialInteraction';

@Injectable({
  providedIn: 'root'
})
export class SocialInteractionService {
  url = '/skillsmates-pages/social-interaction';
  loggedAccount: Account = {} as Account;
  httpHeaders = new HttpHeaders();
  httpOptions;

  constructor(private httpClient: HttpClient) {
    this.httpOptions = { headers: this.httpHeaders };
  }

  findById(id: string): Observable<any>{
    return this.httpClient.get<any>(environment.ms_pages_host + this.url + '/' + id, this.httpOptions);
  }

  create(socialInteraction: SocialInteraction): Observable<any>{
    return this.httpClient.post<any>(environment.ms_pages_host + this.url, socialInteraction, this.httpOptions);
  }

  findCommentsByPost(postId: string): Observable<any>{
    return this.httpClient.get<any>(environment.ms_pages_host + this.url + '/comments/' + postId, this.httpOptions);
  }

  delete(socialInteractionId: string): Observable<any>{
    return this.httpClient.delete<any>(environment.ms_pages_host + this.url + '/' + socialInteractionId, this.httpOptions);
  }

  addAccountSocialInteraction(emitterAccount: Account, receiverAccount: Account, type: string): Observable<any> {
    const socialInteraction: SocialInteraction = {} as SocialInteraction;
    socialInteraction.element = receiverAccount.idServer;
    socialInteraction.emitterAccount = emitterAccount;
    socialInteraction.receiverAccount = receiverAccount;
    socialInteraction.type = type;
    socialInteraction.content = emitterAccount.firstname + ' ' + emitterAccount.lastname;
    return this.create(socialInteraction);
  }
}

import { TestBed } from '@angular/core/testing';

import { SocialInteractionService } from './social-interaction.service';

describe('SharedPostService', () => {
  let service: SocialInteractionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SocialInteractionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

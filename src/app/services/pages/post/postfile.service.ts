import { Injectable } from '@angular/core';
import {variables} from '../../../../environments/variables';
import {Account} from '../../../models/account/account';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {Post} from '../../../models/post/post';

@Injectable({
  providedIn: 'root'
})
export class PostfileService {
  url = '/skillsmates-pages/postsfile';
  HEADER_ID = variables.header_id;
  HEADER_TOKEN = variables.header_token;
  HEADER_ACCOUNT_ID = 'X-Ms-Account-Id';
  loggedAccount: Account = {} as Account;
  headers = new HttpHeaders();
  httpHeaders = this.headers.set('Content-Type', 'multipart/form-data');
  httpOptions;

  constructor(private httpClient: HttpClient) {
    this.httpOptions = { headers: this.httpHeaders };
  }

  publishMediaPost(file: File, post: Post): Observable<any>{
    const data: FormData = this.appendData(file, post);
    return this.httpClient.post<any>(environment.ms_pages_host + this.url + '/new', data, this.httpOptions);
  }

  updateMediaPost(file: File, post: Post): Observable<any>{
    const data: FormData = this.appendData(file, post);
    data.append('postId', post.idServer ? post.idServer : '');
    return this.httpClient.post<any>(environment.ms_pages_host + this.url + '/new/' + post.idServer, data, this.httpOptions);
  }

  appendData(file: File, post: Post): FormData{
    const data: FormData = new FormData();
    data.append('file', file);
    // data.append('post', JSON.stringify(post));


    const postBlob = new Blob([JSON.stringify(post)], {type: 'application/json'});
    data.append('post', postBlob);


    // data.append('mediasubtype', post.mediaSubtype ? post.mediaSubtype.idServer : '');
    // data.append('title', post.title);
    // data.append('keywords', post.keywords ? post.keywords : '');
    // data.append('discipline', post.discipline ? post.discipline : '');
    // data.append('description', post.description ? post.description : '');
    // data.append('type', post.type ? post.type : '');
    return data;
  }
}

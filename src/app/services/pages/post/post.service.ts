import { Injectable } from '@angular/core';
import {variables} from '../../../../environments/variables';
import {Account} from '../../../models/account/account';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Post} from '../../../models/post/post';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {SearchParam} from '../../../models/searchParam';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  url = '/skillsmates-pages/posts';
  HEADER_ID = variables.header_id;
  HEADER_TOKEN = variables.header_token;
  HEADER_ACCOUNT_ID = 'X-Ms-Account-Id';
  HEADER_POST_ID = 'X-Ms-Post-Id';
  loggedAccount: Account = {} as Account;
  httpHeaders = new HttpHeaders();
  params: HttpParams = new HttpParams();
  httpOptions;

  constructor(private httpClient: HttpClient) {
    this.httpOptions = { headers: this.httpHeaders };
  }

  publishPost(post: Post): Observable<any>{
    return this.httpClient.post<any>(environment.ms_pages_host + this.url, post, this.httpOptions);
  }

  updatePost(post: Post): Observable<any>{
    return this.httpClient.put<any>(environment.ms_pages_host + this.url, post, this.httpOptions);
  }

  /**
   * find all posts by account
   */
  findPostsByAccount(id: string, page: number): Observable<any>{
    this.params = this.params.set('page', page.toString(10)).set('limit', '1');
    this.httpOptions = { params: this.params };
    return this.httpClient.get<any>(environment.ms_pages_host + this.url + '/' + id, this.httpOptions);
  }

  findPostsOfflineByAccount(id: string, page: number): Observable<any>{
    this.params = this.params.set('page', page.toString(10)).set('limit', '4');
    this.httpOptions = { params: this.params };
    return this.httpClient.get<any>(environment.ms_pages_host + this.url + '/offline/' + id, this.httpOptions);
  }

  /**
   * find all documents by account
   */
  findDocumentsByAccount(id: string, page: number): Observable<any>{
    this.params = this.params.set('page', page.toString(10)).set('limit', '500');
    this.httpOptions = { params: this.params };
    return this.httpClient.get<any>(environment.ms_pages_host + this.url + '/' + id, this.httpOptions);
  }

  /**
   * find all posts by account
   */
  findDashboardPostsByAccount(id: string, page: number): Observable<any>{
    this.params = this.params.set('page', page.toString(10)).set('limit', '1');
    this.httpOptions = { params: this.params };
    return this.httpClient.get<any>(environment.ms_pages_host + this.url + '/dashboard/' + id, this.httpOptions);
  }

  /**
   * search Documents
   */
  searchPosts(searchParam: SearchParam, page: number, itemsPerPage: number): Observable<any>{
    const httpParams = new HttpParams().set('page', page.toString(10))
      .set('limit', itemsPerPage.toString(10));
    this.httpOptions = { headers: this.httpHeaders , params: httpParams};
    return this.httpClient.post<any>(environment.ms_pages_host + this.url + '/searchPosts', searchParam, this.httpOptions);
  }

  findSinglePost(id: string): Observable<any>{
    return this.httpClient.get<any>(environment.ms_pages_host + this.url + '/single/' + id, this.httpOptions);
  }

  deletePost(id: string): Observable<any>{
    this.httpHeaders = this.httpHeaders.set(this.HEADER_POST_ID, id);
    const httpOptions = { headers: this.httpHeaders };
    return this.httpClient.delete(environment.ms_pages_host + this.url, httpOptions);
  }

  generateUrlPost(id: string): Observable<any>{
    return this.httpClient.get<any>(environment.ms_pages_host + this.url + '/generateUrl/' + id, this.httpOptions);
  }

  viewSinglePost(id: string, page: number): Observable<any>{
    this.params = this.params.set('page', page.toString(10)).set('limit', '4');
    this.httpOptions = { params: this.params };
    return this.httpClient.get<any>(environment.ms_pages_host + this.url + '/view/' + id, this.httpOptions);
  }
}

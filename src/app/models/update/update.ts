import {BaseActiveModel} from '../baseActiveModel';

export class Update extends BaseActiveModel{
    constructor(){
        super();
    }
  description: string;
  version: string;
}

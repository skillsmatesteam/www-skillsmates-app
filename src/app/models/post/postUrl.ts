import {BaseActiveModel} from '../baseActiveModel';

export class PostUrl extends BaseActiveModel{
    constructor(){
        super();
    }
  url: string;
}

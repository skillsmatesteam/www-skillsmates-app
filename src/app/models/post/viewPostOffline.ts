import {PostSocialInteractions} from './postSocialInteractions';
import {BaseModel} from '../baseModel';
import {Account} from '../account/account';

export class ViewPostOffline extends BaseModel{
    constructor(){
        super();
    }
  account: Account;
  posts: number;
  followers: number;
  followees: number;
  postSocialInteractions: PostSocialInteractions;
  postsSocialInteractions: PostSocialInteractions[];
}

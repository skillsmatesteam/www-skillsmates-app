import {BaseActiveModel} from '../baseActiveModel';
import {Multimedia} from '../multimedia/multimedia';
import {Account} from '../account/account';
import {MediaSubtype} from '../multimedia/mediaSubtype';
import {Post} from './post';

export class SharedPost extends BaseActiveModel{
    constructor(){
        super();
    }
  description: string;
  account: Account;
  post: Post;
}

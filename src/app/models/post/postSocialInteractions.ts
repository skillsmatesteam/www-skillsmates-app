import {Post} from './post';
import {SocialInteraction} from './socialInteraction';
import {BaseModel} from '../baseModel';

export class PostSocialInteractions extends BaseModel{
    constructor(){
        super();
    }
    post: Post;
    socialInteractions: SocialInteraction[] = [];
    shared: boolean;
    shareSocialInteraction: SocialInteraction;
}

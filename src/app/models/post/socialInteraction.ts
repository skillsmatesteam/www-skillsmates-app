import {BaseActiveModel} from '../baseActiveModel';
import {Account} from '../account/account';
import {Multimedia} from '../multimedia/multimedia';

export class SocialInteraction extends BaseActiveModel{
    constructor(){
        super();
    }
  content: string;
  emitterAccount: Account;
  receiverAccount: Account;
  element: string;
  type: string;
  multimedia: Multimedia[];
}

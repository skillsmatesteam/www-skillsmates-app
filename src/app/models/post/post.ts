import {BaseActiveModel} from '../baseActiveModel';
import {Multimedia} from '../multimedia/multimedia';
import {Account} from '../account/account';
import {MediaSubtype} from '../multimedia/mediaSubtype';

export class Post extends BaseActiveModel{
    constructor(){
        super();
    }
  title: string;
  keywords: string;
  description: string;
  discipline: string;
  account: Account;
  multimedia: Multimedia[];
  mediaSubtype: MediaSubtype;
  type: string;
  likes: number;
  shares: number;
  url: string;
}

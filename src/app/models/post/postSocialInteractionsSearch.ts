import {BaseModel} from '../baseModel';
import {PostSocialInteractions} from './postSocialInteractions';
import {ResultFound} from './ResultFound';

export class PostSocialInteractionsSearch extends BaseModel{
    constructor(){
        super();
    }

  postSocialInteractions: PostSocialInteractions[] = [];
  numberPosts: number;
  pagePosts: number[] = [];
  currentPage: number;
  resultFound: ResultFound;

}

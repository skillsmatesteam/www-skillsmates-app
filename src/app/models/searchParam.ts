import {BaseModel} from './baseModel';
import {MediaSubtype} from './multimedia/mediaSubtype';

export class SearchParam extends BaseModel{
    constructor(){
        super();
    }
  content: string;
  city: string;
  country: string;
  mediaSubtypes: MediaSubtype[];
  statuses: any[];
}

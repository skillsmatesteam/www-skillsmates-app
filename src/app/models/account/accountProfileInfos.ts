import {BaseModel} from '../baseModel';

export class AccountProfileInfos extends BaseModel{
  constructor(){
    super();
  }

  academics: number;
  professionals: number;
  skills: number;
  posts: number;
  followers: number;
  followees: number;
  favorites: number;
}

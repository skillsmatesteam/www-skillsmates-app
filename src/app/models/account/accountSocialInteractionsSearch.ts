import {BaseModel} from '../baseModel';
import {AccountSocialInteractions} from './accountSocialInteractions';
import {ResultFound} from '../post/ResultFound';

export class AccountSocialInteractionsSearch extends BaseModel{
    constructor(){
        super();
    }

  accountSocialInteractions: AccountSocialInteractions[] = [];
  numberAccounts: number;
  pageAccounts: number[] = [];
  currentPage: number;
  resultFound: ResultFound;
}

import {ActivityArea} from "./config/activityArea";
import {EstablishmentCertificationType} from "./config/establishmentCertificationType";
import {BaseActiveModel} from "../baseActiveModel";
import {Account} from "./account";

export class Certification extends BaseActiveModel{
  constructor(){
    super();
  }

  establishmentCertificationType: EstablishmentCertificationType;
  issuingInstitutionName: string;
  entitledCertification: string;
  activityArea: ActivityArea;
  permanent: boolean;
  temporary: boolean;
  obtainedDate: Date ;
  expirationDate: Date ;
  anotherActivityArea: string;
  anotherEstablishmentCertificationType: string;
  description: string;
  private String ;
  account: Account;
}

import {BaseActiveModel} from '../baseActiveModel';
import {Account} from './account';

export class Cursus extends BaseActiveModel{
  constructor(){
    super();
  }

  establishmentName: string;
  city: string;
  description: string;
  account: Account;
  specialty: string ;
  currentPosition: boolean;
}

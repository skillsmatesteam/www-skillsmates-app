import {Account} from './account';
import {BaseModel} from '../baseModel';

export class Favorite extends BaseModel{
  constructor(){
    super();
  }

  account: Account;
  favorite: Account;
}

import {Level} from './config/level';
import {BaseModel} from '../baseModel';
import {Skill} from './skill';

export class AccountSkills extends BaseModel{
  constructor(){
    super();
  }

  skills: Skill[];
  levels: Level[];
}

import {EstablishmentType} from './config/establishmentType';
import {BaseActiveModel} from '../baseActiveModel';
import {ActivityArea} from './config/activityArea';

export class Certificate extends BaseActiveModel{
  constructor(){
    super();
  }

  establishmentType: EstablishmentType;
  establishmentName: string;
  title: string;
  teachingArea: ActivityArea;
  startDate: Date ;
  endDate: Date ;
  description: string;
}

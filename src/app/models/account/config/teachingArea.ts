import {BaseModel} from '../../baseModel';
import {TeachingAreaGroup} from './teachingAreaGroup';

export class TeachingArea extends BaseModel{
  constructor(){
    super();
  }

  label: string;
  description: string;
  teachingAreaGroup: TeachingAreaGroup;
}

import {BaseModel} from '../../baseModel';

export class Level extends BaseModel{
  constructor(){
    super();
  }

  label: string;
  grade: number;
  icon: string;
  mastered: boolean;
  description: string;
}

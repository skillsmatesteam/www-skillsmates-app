import {BaseModel} from '../../baseModel';

export class ActivityArea extends BaseModel{
  constructor(){
    super();
  }

  label: string;
  description: string;
  specified: boolean;
}

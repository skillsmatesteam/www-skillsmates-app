import {BaseModel} from '../../baseModel';
import {TeachingAreaSet} from './teachingAreaSet';

export class TeachingAreaGroup extends BaseModel{
  constructor(){
    super();
  }

  label: string;
  description: string;
  teachingAreaSet: TeachingAreaSet;
}

import {BaseModel} from '../../baseModel';

export class EstablishmentCertificationType extends BaseModel{
  constructor(){
    super();
  }

  label: string;
  description: string;
  specified: boolean;
}

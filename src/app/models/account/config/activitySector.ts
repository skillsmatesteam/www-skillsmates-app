import {BaseModel} from '../../baseModel';

export class ActivitySector extends BaseModel{
  constructor(){
    super();
  }

  label: string;
  description: string;
  specified: boolean;
}

import {BaseModel} from '../../baseModel';
import {Education} from './education';

export class TeachingAreaSet extends BaseModel{
  constructor(){
    super();
  }

  label: string;
  description: string;
  education: Education;
}

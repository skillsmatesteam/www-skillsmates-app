import {BaseModel} from '../../baseModel';

export class Education extends BaseModel{
  constructor(){
    super();
  }

  code: string;
  label: string;
  description: string;
}

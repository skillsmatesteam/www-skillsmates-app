import {BaseModel} from '../../baseModel';
import {Education} from './education';

export class EstablishmentType extends BaseModel{
  constructor(){
    super();
  }

  label: string;
  description: string;
  education: Education;
  specified: boolean;
}

import {BaseModel} from '../../baseModel';

export class Discipline extends BaseModel{
  constructor(){
    super();
  }

  label: string;
  description: string;
}

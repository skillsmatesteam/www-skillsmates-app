import {SchoolPath} from './schoolPath';
import {Diploma} from './config/diploma';

export class SecondaryEducation extends SchoolPath{
  constructor(){
    super();
  }
  preparedDiploma: Diploma ;
  anotherPreparedDiploma: string;
}

import {ActivitySector} from './config/activitySector';
import {ActivityArea} from './config/activityArea';
import {Cursus} from './cursus';

export class Professional extends Cursus{
  constructor(){
    super();
  }

  jobTitle: string;

  activitySector: ActivitySector ;

  activityArea: ActivityArea ;

  startDate: Date ;

  endDate: Date ;

  anotherActivitySector: string;

  anotherActivityArea: string;
}

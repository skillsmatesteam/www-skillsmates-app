import {Account} from './account';
import {BaseModel} from '../baseModel';
import {SocialInteraction} from '../post/socialInteraction';

export class AccountSocialInteractions extends BaseModel{
  constructor(){
    super();
  }

  account: Account;
  socialInteractions: SocialInteraction[];
  numberPosts: number;
}

import {BaseActiveModel} from '../baseActiveModel';

export class Account extends BaseActiveModel{
    constructor(){
        super();
    }
    firstname: string;
    lastname: string;
    email: string;
    address: string;
    phoneNumber: string;
    birthdate: Date;
    password: string;
    confirmPassword: string;
    description: string;
    gender: string;
    biography: string;
    status: string;
    country: string;
    city: string;
    currentEstablishmentName: string;
    currentJobTitle: string;
    profilePicture: string;
    connected: boolean;
}

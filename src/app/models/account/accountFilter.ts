import {Account} from './account';
import {BaseModel} from '../baseModel';

export class AccountFilter extends BaseModel{
  constructor(){
    super();
  }

  accountsToFilter: Account[];
  searchContent: string;
}

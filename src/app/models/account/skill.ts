import {BaseActiveModel} from '../baseActiveModel';
import {Discipline} from './config/discipline';
import {Level} from './config/level';
import {Account} from './account';

export class Skill extends BaseActiveModel{
  constructor(){
    super();
  }

  label: string;
  skillMastered: boolean;
  discipline: string;
  keywords: string;
  level: Level;
  account: Account;
}

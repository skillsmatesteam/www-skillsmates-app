import {Cursus} from './cursus';
import {EstablishmentType} from './config/establishmentType';
import {StudyLevel} from './config/studyLevel';

export class SchoolPath extends Cursus{
  constructor(){
    super();
  }
  establishmentType: EstablishmentType;
  studyLevel: StudyLevel ;
  anotherStudyLevel: string;
  anotherEstablishmentType: string;
}

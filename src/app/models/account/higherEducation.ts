import {StudyLevel} from './config/studyLevel';
import {TeachingArea} from './config/teachingArea';
import {SchoolPath} from './schoolPath';
import {Diploma} from './config/diploma';

export class HigherEducation extends SchoolPath{
  constructor(){
    super();
  }

  teachingArea: TeachingArea ;
  targetedDiploma: Diploma ;
  anotherTargetedDiploma: string;
}

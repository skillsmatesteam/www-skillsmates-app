import {BaseModel} from '../baseModel';

export class InfoSettingType extends BaseModel{
    constructor(){
        super();
    }
  color: string;
  picture: string;
  title: string;
  value: number;
  position: number;
  description: string;
  type: string;
}

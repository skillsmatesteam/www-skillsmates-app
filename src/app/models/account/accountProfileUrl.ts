import {BaseModel} from "../baseModel";


export class AccountProfileUrl extends BaseModel{
    constructor(){
        super();
    }
  url: string;
}

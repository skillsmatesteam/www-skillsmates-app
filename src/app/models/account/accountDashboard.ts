import {Account} from './account';
import {BaseModel} from '../baseModel';
import {Post} from '../post/post';

export class AccountDashboard extends BaseModel{
  constructor(){
    super();
  }
  account: Account;
  accountsOnline: Account[];
  accountsFollowed: Account[];
  accountsFollowing: Account[];
  accountsInteresting: Account[];
  posts: Post[];
  shortcuts: string[];
  recommendations: string[];
  documents: number;
  links: number;
  videos: number;
  audios: number;
  photos: number;
}

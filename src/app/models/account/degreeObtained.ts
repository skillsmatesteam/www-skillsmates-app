import {Cursus} from './cursus';
import {TeachingArea} from './config/teachingArea';
import {EstablishmentType} from './config/establishmentType';
import {Diploma} from './config/diploma';

export class DegreeObtained extends Cursus{
  constructor(){
    super();
  }

  diploma: Diploma;
  teachingArea: TeachingArea ;
  establishmentType: EstablishmentType;
  startDate: Date ;
  endDate: Date ;
  anotherDiploma: string;
  anotherEstablishmentType: string;
  certificate: boolean;
}

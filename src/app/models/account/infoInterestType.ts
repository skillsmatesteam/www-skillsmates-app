import {BaseModel} from '../baseModel';

export class InfoInterestType extends BaseModel{
    constructor(){
        super();
    }
  image: string;
  title: string;
  position: number;
}

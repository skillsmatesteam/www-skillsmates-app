import {Account} from './account';
import {BaseModel} from '../baseModel';
import {DegreeObtained} from './degreeObtained';
import {SecondaryEducation} from './secondaryEducation';
import {StudyLevel} from './config/studyLevel';
import {HigherEducation} from './higherEducation';
import {TeachingArea} from './config/teachingArea';
import {Education} from './config/education';
import {Diploma} from './config/diploma';
import {EstablishmentType} from './config/establishmentType';
import {Certification} from "./certification";
import {EstablishmentCertificationType} from "./config/establishmentCertificationType";
import {ActivityArea} from "./config/activityArea";

export class AccountAcademics extends BaseModel{
  constructor(){
    super();
  }

  account: Account;
  degreesObtained: DegreeObtained[];
  secondaryEducations: SecondaryEducation[];
  higherEducations: HigherEducation[];
  certificates: Certification[];
  studyLevels: StudyLevel[];
  teachingAreas: TeachingArea[];
  educations: Education[];
  diplomas: Diploma[];
  activityAreas: ActivityArea[];
  establishmentTypes: EstablishmentType[];
  establishmentCertificationTypes: EstablishmentCertificationType[];
}

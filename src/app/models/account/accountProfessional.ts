import {BaseModel} from '../baseModel';
import {Professional} from './professional';
import {ActivityArea} from './config/activityArea';
import {ActivitySector} from './config/activitySector';

export class AccountProfessional extends BaseModel{
  constructor(){
    super();
  }

  professionals: Professional[];

  activityAreas: ActivityArea[];

  activitySectors: ActivitySector[];
}

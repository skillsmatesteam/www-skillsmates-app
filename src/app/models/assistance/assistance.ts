import {BaseActiveModel} from '../baseActiveModel';
import {Account} from '../account/account';

export class Assistance extends BaseActiveModel{
    constructor(){
        super();
    }
  topic: string;
  content: string;
  account: Account = {} as Account;
}

import {BaseActiveModel} from '../baseActiveModel';
import {Account} from '../account/account';

export class Faq extends BaseActiveModel{
    constructor(){
        super();
    }
  heading: string;
  collapse: string;
  question: string;
  answer: string;
  accordionClass: string;
  showBody: boolean;
}

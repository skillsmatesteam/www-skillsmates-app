import {BaseModel} from '../baseModel';
import {Multimedia} from './multimedia';

export class InfoMediaType extends BaseModel{
    constructor(){
        super();
    }
  color: string;
  picture: string;
  title: string;
  value: number;
  position: number;
  description: string;
  type: string;
  accept: string;
  multimedia: Multimedia[];
  subtitle: string;
  selected: boolean;
  found: number;
}

import {BaseModel} from '../baseModel';
import {Multimedia} from './multimedia';

export class MultimediaInfos extends BaseModel{
    constructor(){
        super();
    }
  documents: Multimedia[];
  nbDocuments: number;
  videos: Multimedia[];
  nbVideos: number;
  audios: Multimedia[];
  nbAudios: number;
  images: Multimedia[];
  nbImages: number;
  links: Multimedia[];
  nbLinks: number;
}

import {BaseModel} from '../baseModel';
import {Multimedia} from './multimedia';
import {MediaType} from './mediaType';
import {MediaSubtype} from './mediaSubtype';

export class SortedMediaSubtype extends BaseModel{
    constructor(){
        super();
    }
  mediaType: MediaType;
  mediaSubtype: MediaSubtype;
}

import {BaseModel} from '../baseModel';

export class MediaCount extends BaseModel{
    constructor(){
        super();
    }
    documents: number;
    videos: number;
    audios: number;
    images: number;
    links: number;
}

import {BaseModel} from '../baseModel';

export class Metadata extends BaseModel{
    constructor(){
        super();
    }
   title: string;
   description: string;
   image: string;
   canonical: string;
   url: string;
   author: string;
   availability: string;
   keywords: string;
   audio: string;
   video: string;
}

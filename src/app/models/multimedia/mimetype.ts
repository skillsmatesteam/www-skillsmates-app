import {BaseModel} from '../baseModel';

export class Mimetype extends BaseModel{
    constructor(){
        super();
    }
  extension: string;
  type: string;
}

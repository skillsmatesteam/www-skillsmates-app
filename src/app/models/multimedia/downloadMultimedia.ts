import {BaseModel} from '../baseModel';
import {Multimedia} from './multimedia';

export class DownloadMultimedia extends BaseModel{
    constructor(){
        super();
    }
  multimedia: Multimedia;
  content: any;
}

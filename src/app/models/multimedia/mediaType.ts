import {BaseModel} from '../baseModel';

export class MediaType extends BaseModel{
    constructor(){
        super();
    }
  label: string;
}

import {BaseActiveModel} from '../baseActiveModel';
import {Metadata} from './metadata';

export class Multimedia extends BaseActiveModel{
    constructor(){
        super();
    }
  name: string;
  extension: string;
  mimeType: string;
  checksum: boolean;
  directory: string;
  type: string;
  account: Account;
  url: string;
  metadata: Metadata;
}

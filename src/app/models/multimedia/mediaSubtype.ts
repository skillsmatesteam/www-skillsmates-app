import {BaseModel} from '../baseModel';
import {MediaType} from './mediaType';

export class MediaSubtype extends BaseModel{
    constructor(){
        super();
    }
    mediaType: MediaType;
    label: string;
}

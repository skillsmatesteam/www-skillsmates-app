import {BaseModel} from '../baseModel';
import {Account} from '../account/account';
import {AccountSocialInteractions} from '../account/accountSocialInteractions';

export class AccountNetwork extends BaseModel{
    constructor(){
        super();
    }

  followers: AccountSocialInteractions[] = [];
  followees: AccountSocialInteractions[] = [];
  suggestions: AccountSocialInteractions[] = [];
  aroundYou: AccountSocialInteractions[] = [];
  accountsOnline: AccountSocialInteractions[] = [];
  favorites: AccountSocialInteractions[] = [];
  numberFollowers: number;
  numberFollowees: number;
  numberSuggestions: number;
  numberAroundYou: number;
  numberFavorites: number;
  numberAccountsOnline: number;
  pageFollowers: number[] = [];
  pageFollowees: number[] = [];
  pageSuggestions: number[] = [];
  pageAroundYou: number[] = [];
  pageAccountsOnline: number[] = [];
  pageFavorites: number[] = [];
  currentPage: number;
}

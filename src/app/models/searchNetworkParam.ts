import {BaseModel} from './baseModel';

export class SearchNetworkParam extends BaseModel{
    constructor(){
        super();
    }
  content: string;
  type: number;
  country: string;
  status: any;
}

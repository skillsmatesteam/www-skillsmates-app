import {BaseActiveModel} from '../baseActiveModel';
import {Account} from '../account/account';
import {Multimedia} from '../multimedia/multimedia';

export class Message extends BaseActiveModel{
    constructor(){
        super();
    }
  content: string;
  emitterAccount: Account = {} as Account;
  multimedia: Multimedia[];
  receiverAccount: Account = {} as Account;
}

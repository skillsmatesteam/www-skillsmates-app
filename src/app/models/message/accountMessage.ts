import {Account} from '../account/account';
import {BaseModel} from '../baseModel';
import {Message} from './message';

export class AccountMessage extends BaseModel{
    constructor(){
        super();
    }
  account: Account = {} as Account;
  unread: number;
  messages: Message[] = [];
}

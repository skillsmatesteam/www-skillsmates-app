import {BaseActiveModel} from './baseActiveModel';
import {Account} from './account/account';

export class Selection extends BaseActiveModel{
    constructor(){
        super();
    }
  account: Account;
  image: string;
  description: string;
  liked: number;
  shared: number;
  comments: number;
  downloads: number;
}

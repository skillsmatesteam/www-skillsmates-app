import {BaseActiveModel} from './baseActiveModel';
import {Account} from './account/account';

export class Forum extends BaseActiveModel{
    constructor(){
        super();
    }
  account: Account;
  discipline: string;
  description: string;
  nb: number;
}

import {BaseModel} from '../baseModel';
import {Notification} from './notification';

export class Notifications extends BaseModel{
    constructor(){
        super();
    }

  likes: Notification[] = [];
  comments: Notification[] = [];
  followers: Notification[] = [];
  shares: Notification[] = [];
  favorites: Notification[] = [];
  messages: Notification[] = [];
  posts: Notification[] = [];
}

import {BaseActiveModel} from '../baseActiveModel';
import {Account} from '../account/account';

export class Notification extends BaseActiveModel{
    constructor(){
        super();
    }

    title: string;
    type: string;
    emitterAccount: Account;
    element: string;
    receiverAccount: Account;
}

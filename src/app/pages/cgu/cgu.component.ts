import {Component, Input, OnInit} from '@angular/core';
import {RoutesHelper} from '../../helpers/routes-helper';
import {Router} from '@angular/router';

@Component({
  selector: 'app-cgu',
  templateUrl: './cgu.component.html',
  styleUrls: ['./cgu.component.css']
})
export class CguComponent implements OnInit {

  displayCheckCGU: boolean;
  routes = RoutesHelper.routes;
  checked = false;
  url: string;

  constructor(private router: Router) {
    // document.body.style.background = 'rgb(218,218,218)';
  }

  ngOnInit(): void {
    this.url = localStorage.getItem('url');
    console.log(this.url);
    localStorage.removeItem('url');
    const displayCheck = localStorage.getItem('displayCheckCGU');
    console.log(displayCheck);
    if (displayCheck){
      this.displayCheckCGU = true;
    }
    localStorage.removeItem('displayCheckCGU');
  }

  onClickCGU(): void {
    localStorage.setItem('checked', 'true');
  }

  onClickReturn(): void {
    if (this.url){
      this.router.navigate([this.url]);
    }else {
      this.router.navigate([this.routes.get('dashboard')]);
    }
  }
}

import { Component, OnInit } from '@angular/core';
import {RoutesHelper} from '../../helpers/routes-helper';
import {Router} from '@angular/router';
import {MobileService} from '../../services/mobile/mobile.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-validate-account',
  templateUrl: './validate-account.component.html',
  styleUrls: ['./validate-account.component.css']
})
export class ValidateAccountComponent implements OnInit {
  routes = RoutesHelper.routes;
  BASE_URL: string;
  FACEBOOK_LINK: string;
  INSTAGRAM_LINK: string;
  TWITTER_LINK: string;
  SNAPCHAT_LINK: string;
  TIKTOK_LINK: string;
  public screenWidth: number = window.innerWidth;

  constructor(private router: Router,
              private mobileService: MobileService) { }

  ngOnInit(): void {
    this.BASE_URL = environment.ms_resources_baseurl;
    this.FACEBOOK_LINK = environment.ms_resources_facebook;
    this.INSTAGRAM_LINK = environment.ms_resources_instagram;
    this.TWITTER_LINK = environment.ms_resources_twitter;
    this.SNAPCHAT_LINK = environment.ms_resources_snapchat;
    this.TIKTOK_LINK = environment.ms_resources_tiktok;
  }
  onClickCGU(): void {
    localStorage.setItem('url', this.router.url);
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }

}

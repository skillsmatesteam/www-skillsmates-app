import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {ToastService} from '../../services/toast/toast.service';
import {AuthenticateService} from '../../services/accounts/authenticate/authenticate.service';
import {AccountService} from '../../services/accounts/account/account.service';
import {WebSocketService} from '../../services/message/web-socket.service';
import {RoutesHelper} from '../../helpers/routes-helper';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  loading: boolean;
  routes = RoutesHelper.routes;

  constructor(private authenticateService: AuthenticateService,
              private accountService: AccountService,
              private toastService: ToastService,
              private webSocketService: WebSocketService,
              private router: Router) { }

  ngOnInit(): void {
    this.loading = true;
    this.webSocketService.closeWebsocket();
    this.accountService.logoutAccount().subscribe((response) => {
      this.logout();
    }, (error) => {
      this.logout();
    });
  }

  logout(): void{
    // this.authenticateService.logout();
    this.toastService.showSuccess('Déconnexion', 'Deconnexion avec succces');
    this.loading = false;
    this.router.navigate([this.routes.get('login')]);
  }
}

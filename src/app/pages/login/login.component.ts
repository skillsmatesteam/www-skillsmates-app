import {Component, ElementRef, HostListener, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { Account } from '../../models/account/account';
import { AccountService } from '../../services/accounts/account/account.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastService } from '../../services/toast/toast.service';
import { AuthenticateService } from '../../services/accounts/authenticate/authenticate.service';
import { variables } from '../../../environments/variables';
import { Alert } from '../../models/alert';
import { RoutesHelper } from '../../helpers/routes-helper';
import { StringHelper } from '../../helpers/string-helper';
import { MobileService } from 'src/app/services/mobile/mobile.service';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {RestCountryService} from '../../services/api/rest-country.service';
import {CommonHelper} from '../../helpers/common-helper';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  @ViewChild('modalMessage', { static: false })
  modalMessage: ElementRef;

  account: Account = {} as Account;
  email: string;
  password: string;
  loading: boolean;
  checked: boolean;
  submitted = false;
  alert: Alert = {} as Alert;
  errors;
  routes = RoutesHelper.routes;
  id: string;
  isLoginBox = true;
  map: Map<number, boolean>;
  msgErrors: string[];
  msgError: string ;
  public screenWidth: number = window.innerWidth;
  closeModal: string;
  selected = false;
  countries: any[];
  professionalStatuses: any[] = CommonHelper.statuses;

  constructor(
    private accountService: AccountService,
    private authenticateService: AuthenticateService,
    private toastService: ToastService,
    private router: Router,
    private modalService: NgbModal,
    private activatedRoute: ActivatedRoute,
    private restCountryService: RestCountryService,
    private mobileService: MobileService
  ) {}

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    this.screenWidth = window.innerWidth;
  }

  scrollToTop(): void {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth',
    });
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }

  ngOnInit(): void {
    this.searchCountries();
    // this.openModalMessage();
    this.mobileService.unsetSkillschatDesign();
    this.scrollToTop();
    this.initErrors();
    this.authenticateService.logout();
    this.checked = false;
    this.loading = false;

    this.id = this.activatedRoute.snapshot.paramMap.get(variables.id);
    if (this.id) {
      this.isLoginBox = this.id !== '2';
    }

    // to verify is accept CGU box has been checked
    const isCGUChecked = localStorage.getItem('checked');
    if (isCGUChecked && isCGUChecked === 'true') {
      this.checked = true;
      this.isLoginBox = false;
      localStorage.removeItem('checked');
    }

    if (this.isMobile()){
      if (!localStorage.getItem('reload')) {
        localStorage.setItem('reload', 'no reload');
        window.location.reload();
      } else {
        localStorage.removeItem('reload');
      }
    }

    localStorage.removeItem('design');
  }

  onClickRegister(): void {
    this.loading = true;
    if (this.validateAccountForRegistration()) {
      if (this.account.id) {
        this.accountService.updateAccount(this.account).subscribe(
          (response) => {
            this.router.navigate([this.routes.get('root')]);
            this.loading = false;
          },
          (error) => {
            this.toastService.showError(
              'Erreur',
              'Compte ne peut pas être mis à jour'
            );
            this.loading = false;
          },
          () => {}
        );
      } else {
        this.accountService.createAccount(this.account).subscribe(
          (response) => {
            console.log('account created');
            this.router.navigate([this.routes.get('validate.account')]);
          },
          (error) => {
            this.alert = {
              display: true,
              class: 'danger',
              title: 'Erreur',
              message: '  ' + error.error.message,
            };
            this.loading = false;
          },
          () => {
            this.loading = false;
          }
        );
      }
    } else {
      this.msgError = '';
      if (this.msgErrors) {
        this.msgErrors.forEach(msg => this.msgError = this.msgError + msg + '\n' );
      }

      this.alert = {display: true, class: 'danger', title: 'Erreur', message: '  Les valeurs entrées sont invalides' + this.msgError};

      this.loading = false;
    }
  }

  onClickLogin(): void {
    this.loading = true;
    if (this.validateAccountForLogin()) {
      this.authenticateService
        .authenticate(this.email, this.password)
        .subscribe(
          (response) => {
            this.account = response.resource;
            localStorage.setItem(
              variables.account_current,
              JSON.stringify(this.account)
            );
            document.body.style.background = 'rgb(249,250,251)';
            this.router.navigate([this.routes.get('dashboard')]);
          },
          (error) => {
            this.alert = {
              display: true,
              class: 'danger',
              title: 'Erreur',
              message: '  ' + error.error.message,
            };
            this.loading = false;
          },
          () => {
            this.loading = false;
          }
        );
    } else {
      this.alert = {
        display: true,
        class: 'danger',
        title: 'Erreur',
        message: '  Les valeurs entrées sont invalides',
      };
      this.loading = false;
    }
  }

  validateAccountForRegistration(): boolean {
    this.initErrors();
    if (this.account.firstname === undefined || this.account.firstname === '') {
      this.errors.valid = false;
      this.errors.firstname = 'Prénom requis';
    }

    if (this.account.lastname === undefined || this.account.lastname === '') {
      this.errors.valid = false;
      this.errors.lastname = 'Nom requis';
    }

    if (!this.validateEmail(this.account.email)) {
      this.errors.valid = false;
      this.errors.email = 'Email invalide';
    }

    if (this.account.password === undefined || this.account.password === '') {
      this.errors.valid = false;
      this.errors.password = 'Mot de passe requis';
    } else {
      this.passwordValidator();
    }

    if (
      this.account.confirmPassword === undefined ||
      this.account.confirmPassword === ''
    ) {
      this.errors.valid = false;
      this.errors.confirmPassword = 'confirmation du mot de passe requis';
    }

    if (this.account.password && this.account.confirmPassword && this.account.password !== this.account.confirmPassword) {
      this.errors.valid = false;
      this.errors.password = 'Les deux mots de passe sont différents';
    }

    if (this.errors.valid && !this.checked) {
      this.errors.valid = false;
      this.errors.cgu = 'Valider les conditions d\'utilisation';
    }

    this.submitted = true;
    return this.errors.valid;
  }

  passwordValidator(): void{
    this.map = new Map();
    this.msgErrors = [];
    this.map = this.validatePassword(this.account.password, this.account);
    if (!this.map.get(1)) {
      this.errors.valid = false;
      this.errors.password = ' - Mot de passe doit contenir entre 8 et 16 caractères';
      this.msgErrors.push(this.errors.password);

    }
    if (!this.map.get(2)) {
      this.errors.valid = false;
      this.errors.password = ' - Mot de passe doit contenir au moins 1 chiffre ou un caractère spécial : !&%$';
      this.msgErrors.push(this.errors.password);
    }
    if (this.map.get(3)) {
      this.errors.valid = false;
      this.errors.password = ' - Mot de passe ne doit pas contenir votre nom ou email';
      this.msgErrors.push(this.errors.password);
    }
    if (!this.map.get(4)) {
      this.errors.valid = false;
      this.errors.password = ' - Mot de passe doit contenir au moins une lettre majuscule';
      this.msgErrors.push(this.errors.password);
    }
    if (!this.map.get(5)) {
      this.errors.valid = false;
      this.errors.password = ' - Mot de passe doit contenir au moins une lettre miniscule';
      this.msgErrors.push(this.errors.password);
    }
    if (this.account.password && this.account.confirmPassword && this.account.password !== this.account.confirmPassword){
      this.errors.valid = false;
      this.errors.password = ' - Les deux mots de passe sont différents';
      this.msgErrors.push(this.errors.password);
    }
  }

  validateAccountForLogin(): boolean {
    this.initErrors();
    if (!this.validateEmail(this.email)) {
      this.errors.valid = false;
      this.errors.email = 'Email invalide';
    }

    if (this.password === undefined || this.password === '') {
      this.errors.valid = false;
      this.errors.password = 'Mot de passe requis';
    }

    if (this.password && this.password.length < 5) {
      this.errors.valid = false;
      this.errors.password = 'Mot de passe doit être de 5 caractéres minimum';
    }
    this.submitted = true;
    return this.errors.valid;
  }

  validateEmail(email): boolean {
    return StringHelper.validateEmail(email);
  }

  validatePassword(password: string, account: Account): Map<number, boolean> {
    return StringHelper.validatePassword(password, account);
  }

  initErrors(): void {
    this.alert.display = false;
    this.submitted = false;
    this.errors = {
      valid: true,
      firstname: '',
      lastname: '',
      email: '',
      password: '',
      confirmPassword: '',
      cgu: '',
    };
  }

  keyupEmail(): void {
    this.errors.email = '';
  }

  keyupPassword(): void {
    this.errors.password = '';
  }

  keyupFirstname(): void {
    this.errors.firstname = '';
  }

  keyupLastname(): void {
    this.errors.lastname = '';
  }

  keyupConfirmPassword(): void {
    this.errors.confirmPassword = '';
  }

  onclickTab(): void {
    this.initErrors();
  }

  onClickChangeBox(): void {
    this.isLoginBox = !this.isLoginBox;
    this.initErrors();
  }

  onClickCGU(): void {
    localStorage.setItem('url', 'login');
    localStorage.setItem('displayCheckCGU', 'true');
  }

  triggerModal(content): void {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((res) => {
      this.closeModal = `Closed with: ${res}`;
    }, (res) => {
      this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  private searchCountries(): void {
    this.restCountryService.searchAllCountries().subscribe((response) => {
      localStorage.setItem(variables.countries, JSON.stringify(response));
    });
  }

  onChangeStatus(e): void {
    this.errors.status = '';
    this.professionalStatuses.forEach(elt => {
      if (elt.label.toLowerCase() === e.target.value.toLowerCase()) {
        this.account.status = elt.code;
      }
    });
  }

  onClickFAQ(): void {
    localStorage.setItem(variables.faq, variables.faq);
  }
}

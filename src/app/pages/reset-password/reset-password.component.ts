import {Component, HostListener, OnInit} from '@angular/core';
import {Alert} from '../../models/alert';
import {Router} from '@angular/router';
import {RoutesHelper} from '../../helpers/routes-helper';
import {SettingService} from '../../services/settings/setting/setting.service';
import {AuthenticateService} from '../../services/accounts/authenticate/authenticate.service';
import {StringHelper} from '../../helpers/string-helper';
import {MobileService} from '../../services/mobile/mobile.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  alert: Alert = {} as Alert;
  errors;
  email: string;
  loading: boolean;
  submitted = false;
  routes = RoutesHelper.routes;
  public screenWidth: number = window.innerWidth;

  constructor(private settingService: SettingService,
              private mobileService: MobileService,
              private authenticateService: AuthenticateService,
              private router: Router) {
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    // this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
  }

  isMobile(): boolean {
    return this.mobileService.isMobile(this.screenWidth);
  }

  ngOnInit(): void {
    this.initErrors();
    this.loading = false;
  }

  initErrors(): void{
    this.submitted = false;
    this.errors = {valid: true, password: ''};
  }

  keyupEmail(): void {
    this.errors.email = '';
  }

  validateEmail(email): boolean{
    return StringHelper.validateEmail(email);
  }

  onClickReset(): void {
    this.loading = true;
    if (this.validateDataForReset()){
      this.authenticateService.verificateEmail(this.email).subscribe((response) => {
        this.router.navigate([this.routes.get('login')]);
      }, (error) => {
        this.alert = {display: true, class: 'danger', title: 'Erreur', message: ' Compte ne peut pas être identifié'};
        this.loading = false;
      }, () => {
        this.loading = false;
      });
    }else {
      this.alert = {display: true, class: 'danger', title: 'Erreur', message: ' L\'adresse e-mail est invalide'};
      this.loading = false;
    }
  }

  validateDataForReset(): boolean{
    this.initErrors();
    if (!this.validateEmail(this.email)){
      this.errors.valid = false;
      this.errors.email = 'Email invalide';
    }
    this.submitted = true;
    return this.errors.valid;
  }

  onClickCGU(): void {
    localStorage.setItem('url', this.router.url);
  }
}

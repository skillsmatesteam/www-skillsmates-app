import { Pipe, PipeTransform } from '@angular/core';
import {Account} from '../models/account/account';
import {environment} from '../../environments/environment';

@Pipe({
  name: 'avatarUrl'
})
export class AvatarUrlPipe implements PipeTransform {
  transform(account: Account, ...args: unknown[]): unknown {
    if (account && account.profilePicture){
      return environment.aws_s3_multimedia_bucket + account.idServer + '/' + account.profilePicture.toLowerCase();
    } else {
      return './assets/images/default-user.svg';
    }
  }
}

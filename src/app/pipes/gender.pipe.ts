import { Pipe, PipeTransform } from '@angular/core';
import {Gender} from '../enum/gender.enum';
import {Account} from '../models/account/account';

@Pipe({
  name: 'gender'
})
export class GenderPipe implements PipeTransform {

  transform(account: Account, ...args: unknown[]): unknown {
    if (account && account.gender){
      if (account.gender === Gender[Gender.MALE]){
        return 'Homme';
      }else if (account.gender === Gender[Gender.FEMALE]){
        return 'Femme';
      }else {
        return '';
      }
    }
  }

}

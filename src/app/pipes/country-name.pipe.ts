import { Pipe, PipeTransform } from '@angular/core';
import {CountryHelper} from '../helpers/country-helper';

@Pipe({
  name: 'countryName'
})
export class CountryNamePipe implements PipeTransform {
  transform(value: string, ...args: unknown[]): unknown {
    const country: any = CountryHelper.findCountryByAlpha3Code(value);
    return country && country.translation ? country.translations.fra.common : value;
  }
}

import { Pipe, PipeTransform } from '@angular/core';
import {environment} from '../../environments/environment';
import {Multimedia} from '../models/multimedia/multimedia';

@Pipe({
  name: 'mediaUrl'
})
export class MediaUrlPipe implements PipeTransform {

  transform(multimedia: Multimedia, ...args: unknown[]): unknown {
    if (multimedia){
      return environment.aws_s3_multimedia_bucket +
        multimedia.directory + '/' +
        multimedia.type.toLowerCase() + '_' + multimedia.checksum + '.' + multimedia.extension;
    } else {
      return '';
    }
  }

}

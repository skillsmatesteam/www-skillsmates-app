import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncate'
})
export class TruncatePipe implements PipeTransform {

  transform(value: string, ...args: number[]): unknown {
    let nb = 20;
    if (args && args.length > 0){
      nb = args[0];
    }
    if (value && value.length > nb){
      value = value.slice(0, nb - 1) + '...';
    }
    return value;
  }
}

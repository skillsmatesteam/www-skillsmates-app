import { Pipe, PipeTransform } from '@angular/core';
import {environment} from '../../environments/environment';
import {Post} from '../models/post/post';

@Pipe({
  name: 'mediaPostUrl'
})
export class MediaPostUrlPipe implements PipeTransform {

  transform(post: Post, ...args: unknown[]): unknown {
    if (post && post.multimedia && post.multimedia.length > 0){
      const media = post.multimedia[0];
      return environment.aws_s3_multimedia_bucket +
        post.account.idServer + '/' +
        media.type.toLowerCase() + '_' + media.checksum + '.' + media.extension;
    } else {
      return '';
    }
  }

}

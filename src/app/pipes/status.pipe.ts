import { Pipe, PipeTransform } from '@angular/core';
import {Account} from '../models/account/account';
import {Status} from '../enum/status.enum';

@Pipe({
  name: 'status'
})
export class StatusPipe implements PipeTransform {

  transform(account: Account, ...args: unknown[]): unknown {
    if (account && account.status){
      if (account.status === Status[Status.TEACHER]){
        return 'Enseignant';
      }else if (account.status === Status[Status.STUDENT]){
        return 'Etudiant';
      }else if (account.status === Status[Status.PROFESSIONAL]){
        return 'Professionnel';
      }else {
        return '';
      }
    }
  }

}

import { Pipe, PipeTransform } from '@angular/core';
import {Account} from '../models/account/account';
import {variables} from '../../environments/variables';
import { Status } from '../enum/status.enum';
@Pipe({
  name: 'professionIcon'
})
export class ProfessionIconPipe implements PipeTransform {

  transform(account: Account, ...args: string[]): unknown {
    if (account && account.status){
      if (args && args[0]){
        if (account.status === Status[Status.TEACHER]){
          return 'fa fa-graduation-cap';
        }else if (account.status === Status[Status.STUDENT]){
          return 'fa fa-graduation-cap';
        }else {
          return 'fa fa-graduation-cap';
        }
      }else {
        if (account.status === Status[Status.TEACHER]){
          return './assets/images/teacher.svg';
        }else if (account.status === Status[Status.STUDENT]){
          return './assets/images/student.svg';
        }else {
          return './assets/images/professional.svg';
        }
      }
    }
  }

}

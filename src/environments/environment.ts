// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   // ms_accounts_host: 'http://localhost:8030',
   // ms_media_host: 'http://localhost:8031',
   // ms_pages_host: 'http://localhost:8032',
   // ms_notifications_host: 'http://localhost:8033',
   // ms_messages_host: 'http://localhost:8034',
   // ms_settings_host: 'http://localhost:8035',
   // ms_statistics_host: 'http://localhost:8036',
   // ms_groups_host: 'http://localhost:8037',
   // aws_s3_multimedia_bucket: 'https://skillsmatesmedia.s3.us-east-2.amazonaws.com/dev.multimedia/',

  ms_resources_baseurl: 'https://skillsmatesresources.s3.us-east-2.amazonaws.com',
  ms_resources_facebook: 'https://www.facebook.com',
  ms_resources_tiktok: 'https://www.tiktok.com',
  ms_resources_instagram: 'https://www.instagram.com',
  ms_resources_twitter: 'https://twitter.com',
  ms_resources_snapchat: 'https://www.snapchat.com',

  itemsPerPage: 32,
  itemsPerPageSearch: 24,
  itemsPerPageDocument: 32,

  // INT
  ms_accounts_host: 'https://skillsmates.ga:8001',
  ms_media_host: 'https://skillsmates.ga:8003',
  ms_pages_host: 'https://skillsmates.ga:8002',
  ms_notifications_host: 'https://skillsmates.ga:8004',
  ms_messages_host: 'https://skillsmates.ga:8005',
  ms_settings_host: 'https://skillsmates.ga:8006',
  ms_statistics_host: 'https://skillsmates.ga:8007',
  ms_groups_host: 'https://skillsmates.ga:8008',
  aws_s3_multimedia_bucket: 'https://skillsmatesmedia.s3.us-east-2.amazonaws.com/int.multimedia/',

  // PROD
  // aws_s3_multimedia_bucket: 'https://skillsmatesmedia.s3.us-east-2.amazonaws.com/multimedia/',
  // ms_accounts_host: 'https://skillsmates.ga:9001',
  // ms_media_host: 'https://skillsmates.ga:9003',
  // ms_pages_host: 'https://skillsmates.ga:9002',
  // ms_notifications_host: 'https://skillsmates.ga:9004',
  // ms_messages_host: 'https://skillsmates.ga:9005',
  // ms_settings_host: 'https://skillsmates.ga:9006',
  // ms_statistics_host: 'https://skillsmates.ga:9007',
  // ms_groups_host: 'https://skillsmates.ga:9008',

  // countries_name_api: 'https://restcountries.eu/rest/v2/name/',
  // countries_code_api: 'https://restcountries.eu/rest/v2/alpha/',
  countries_api: './assets/json/countries.json',
  youtube_embed_host: 'https://www.youtube.com/embed/',
  GA_TRACKING_ID: 'G-GW8VN73YKL',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
